#
# Copyright (C) 2017 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os, re

from collections import OrderedDict

from jflow.workflow import Workflow


class CnvPrepare(Workflow):

    threads = 8

    def get_description(self):
        return "Prepare files for the CNV detection workflow + quality selection"

    def define_parameters(self, function="process"):
        # Protocol
        self.add_parameter("protocol", "Protocol used", display_name="Protocol", default="illumina_stranded",
                           choices=["illumina_stranded", "other"])

        # Inputs:
        self.add_multiple_parameter_list("sample", "List of sample reads", required=True, rules="FilesUnique",
                                         group="inputs")
        self.add_input_file("reads_1", "Which reads files should be used", add_to="sample",
                            file_format="fastq", rules="RequiredIf?ALL[sample>alignments=None]")
        self.add_input_file("reads_2", "Which reads files should be used (paired reads)", add_to="sample",
                            file_format="fastq", rules="RequiredIf?ALL[sample>alignments=None]")
        self.add_parameter("name", "Sample name", required=True, add_to="sample")
        self.add_input_file("alignments", "Reads already aligned on genome", file_format="bam", add_to="sample",
                            rules="RequiredIf?ALL[sample>alignments=*]")
        self.add_input_file("reference_genome", "Which genome should the read being align on", file_format="fasta",
                            group="inputs", required=True)
        self.add_parameter("genome_indexed", "Is the genome already indexed by BWA (into the same directory as Fasta "
                                             "file, with Fasta file as prefix)", group="inputs", type=bool)

        # Trimming:
        self.add_parameter("trim_reads", "Trim reads before analysis", group="trimming", type=bool)
        self.add_parameter("adapter_1", "Sequence of the adapter (paired data: of the first read)", group="trimming",
                           default="AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC",
                           rules="RequiredIf?ALL[trim_reads=True,protocol=other];"
                                 "DisabledIf?ANY[trim_reads=False,protocol=illumina_stranded]")
        self.add_parameter("adapter_2", "Sequence of the adapter for the second read (paired data only)",
                           group="trimming", default="AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTAGATCTCGGTGGTCGCCGTATCATT",
                           rules="RequiredIf?ALL[trim_reads=True,sample>reads_2=*,protocol=other];"
                                 "DisabledIf?ANY[trim_reads=False,protocol=illumina_stranded,sample>reads_2=None]")
        self.add_parameter("position_adapter", "Position of the adapter", choices=["3'", "5'", "3'/5'"],
                           display_name="Adapter's position", group="trimming",
                           rules="DisabledIf?ANY[trim_reads=False,protocol=illumina_stranded]")

        # Bam stats:
        self.add_parameter("chromosomes", "Chromosomes to study", default="genome", group="Bam stats",
                           rules="ForbiddenChars= ")

    def get_chromosomes_list(self):
        self.chromosomes_list = []
        if self.chromosomes == "genome":
            chr_candidates = os.popen("cat " + self.reference_genome +
                                      " | grep '>' | awk 'sub(/^>/, \"\")' | awk '{print $1}'").read().split(
                "\n")
            for chr in chr_candidates:
                if len(chr) > 0:
                    m = re.match(r"^ch(r)?(_)?(\d+)$", chr.lower())
                    if (chr.isdigit() and int(chr) < 200) or (m is not None and int(m.group(3)) < 200):
                        self.chromosomes_list.append(chr)
            if len(self.chromosomes_list) == 0:
                raise Exception("No chromosomes found. Please specify them manually")
        else:
            self.chromosomes_list = self.chromosomes.split(",")

    def process(self):
        alignments = OrderedDict()
        reads = {
            "reads_1": [],
            "reads_2": [],
            "names": []
        }
        for smple in self.sample:
            if not smple["reads_1"].is_None and not smple["reads_2"].is_None:
                # reads[smple["name"]] = [smple["reads_1"], smple["reads_2"]]
                reads["reads_1"].append(smple["reads_1"])
                reads["reads_2"].append(smple["reads_2"])
                reads["names"].append(smple["name"])
            else:
                alignments[smple["name"]] = smple["alignments"]

        # Align reads on reference genome if any reads to do so:
        if len(reads["reads_1"]) > 0:
            index_genome_directory = self.reference_genome[:self.reference_genome.rfind("/")]
            index_genome_prefix = self.reference_genome[self.reference_genome.rfind("/")+1:]
            #  First, index genome if needed:
            if not self.genome_indexed:
                bwaindex = self.add_component("BwaIndex", [self.reference_genome])
                index_genome_directory = bwaindex.genome_dir
                index_genome_prefix = "reference"

            #  Map reads:
            bwamap = self.add_component("BwaMap", [index_genome_directory, reads["reads_1"], reads["reads_2"],
                                                   reads["names"], False, True, True, index_genome_prefix,
                                                   self.threads])
            # Mark duplicates:
            mdupl = self.add_component("MarkDuplicates", [bwamap.map_files])
            for bam in mdupl.map_files:
                name = bam[bam.rfind("/")+1:].replace("-sorted-md.bam", "")
                alignments[name] = bam

        input_bam = list(alignments.values())
        indiv_list = list(alignments.keys())

        self.get_chromosomes_list()
        chromosomes = self.chromosomes_list

        bamstats = self.add_component("BamStats", [input_bam, indiv_list, self.reference_genome,
                                                   chromosomes, self.chromosomes == "genome"])

        summary = self.add_component("Summary", [bamstats.coverage_png, bamstats.isize_png, indiv_list])
