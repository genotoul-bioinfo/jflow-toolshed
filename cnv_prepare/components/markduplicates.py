from jflow.component import Component


class MarkDuplicates(Component):

    def define_parameters(self, bam_files):
        self.add_input_file_list("alignments", "BAM mapping files", file_format="bam", default=bam_files,
                                 required=True)
        self.add_output_file_list("map_files", "Map files", pattern='{basename_woext}-md.bam',
                                  items=bam_files)
        self.add_output_file_list("metrics", "Metrics values", pattern='{basename_woext}-md.bam.metrics',
                                  items=bam_files)
        self.add_output_file_list("summary_files", "Summary of mapping", pattern='{basename_woext}-md.bam.summary',
                                  items=bam_files)
        self.add_output_file_list("stdout", "Standard output", pattern='markDuplicates_{basename_woext}.stdout',
                                  items=bam_files)
        self.add_output_file_list("stderr", "Standard error", pattern='markDuplicates_{basename_woext}.stderr',
                                  items=bam_files)

    def process(self):
        cmd = "java -jar " + self.get_exec_path("picard") + " MarkDuplicates I=$1 O=$2 M=$3 REMOVE_DUPLICATES=true > $5" \
                                                                   " 2>> $6; " + self.get_exec_path("samtools") + \
            " flagstat $2 > $4"
        self.add_shell_execution(cmd, cmd_format="{EXE} {IN} {OUT}", inputs=self.alignments,
                                 outputs=[self.map_files, self.metrics, self.summary_files, self.stdout, self.stderr],
                                 map=True)
