from jflow.component import Component


def make_html(output_html, title, *indivs):
    import base64
    indiv_names = indivs[:int(len(indivs)/2)]
    ifiles = indivs[int(len(indivs)/2):]
    with open(output_html, "w") as html_file:
        template = "<html>\n<head>\n\t<title>{0}</title>\n" + \
                   "\t<style>div {{ display: inline-block; }}</style>\n" + \
                   "</head>\n<body>\n\t<h1>{0}</h1>\n"
        html_file.write(template.format(title))
        for ifile, name in zip(ifiles, indiv_names):
            html_file.write("\t<div>\n\t\t<p><strong>{0}</strong></p>".format(name))
            data_uri = base64.b64encode(open(ifile, 'rb').read()).decode("utf-8")
            img_tag = '<img src="data:image/png;base64,{0}"/>'.format(data_uri)
            html_file.write("\t\t{0}\n\t</div>\n".format(img_tag))
        html_file.write("</body>\n</html>")


class Summary(Component):

    def define_parameters(self, coverage, isize, indiv_names):
        self.add_input_file_list("coverage", "Coverage stats", default=coverage)
        self.add_input_file_list("isize", "Isize stats", default=isize)
        self.add_parameter_list("indiv_names", "Names for each individuals", default=indiv_names)
        self.add_output_file("coverage_html", "HTML summary for reads depth (coverage)", filename="read_depth.html")
        self.add_output_file("isize_html", "HTML summary for isize", filename="isize.html")

    def process(self):
        self.add_python_execution(make_html, cmd_format="{EXE} {OUT} {ARG} {IN}", inputs=self.coverage,
                                  outputs=self.coverage_html,
                                  arguments=["\"Read depth for each individual\"", self.indiv_names])
        self.add_python_execution(make_html, cmd_format="{EXE} {OUT} {ARG} {IN}", inputs=self.isize,
                                  outputs=self.isize_html,
                                  arguments=["\"Insert size for each individual\"", self.indiv_names])
