import os

from jflow.component import Component


def run_cnvnatorRdStats(bam_file, chr_dir, cnvnator, rscript, rdvariationr, indiv, tmp_dir,
                        output_directory, chromosomes, stats, stdout, stderr, stats_repr):
    hisroot = "sample.his.root"
    os.mkdir(tmp_dir)
    cmd = "cd " + tmp_dir + "; "
    chromosomes = chromosomes.replace(",", " ")
    cmd += cnvnator + " -root cnvnator.root -chrom " + chromosomes + " -d " + chr_dir + \
        " -tree " + bam_file + " -unique > " + stdout + " 2>> " + stderr + ";\n\n"
    for binsize in range(100, 1001, 100):
        cmd += cnvnator + " -root cnvnator.root -chrom " + chromosomes + " -outroot " + hisroot + \
               " -his " + str(binsize) + " -d " + chr_dir + " >> " + stdout + " 2>> " + stderr + ";\n"
        cmd += cnvnator + " -root " + hisroot + " -chrom " + chromosomes + " -stat " + str(binsize) + \
               " >> " + stdout + " 2>> " + stderr + ";\n"
        cmd += cnvnator + " -root " + hisroot + " -chrom " + chromosomes + " -eval " + str(binsize) + " > " + \
               output_directory + os.path.sep + indiv + "." + str(binsize) + ".cnvnator.eval" + " 2>> " + stderr + ";\n"

    cmd += "\n"

    for binsize in range(100, 1001, 100):
        cmd += "sed '1d' " + output_directory + "/" + indiv + "." + str(binsize) + ".cnvnator.eval | head -n 1 | " \
                                                                               "awk -v OFS=\"\t\" -v bin=" + str(
            binsize) + " '{ print bin,$(NF-3),$NF}' >> " + stats + " 2>> " + stderr + ";\n"

    cmd += "\n" + rscript + " " + rdvariationr + " " + indiv + " " + stats + " >> " + stdout + " 2>> " + stderr + ";"
    os.system(cmd)


class CnvnatorRdStats(Component):

    def define_parameters(self, bam_files, indiv_list, chromosomes, chr_dir):
        self.add_input_file_list("bam_files", "BAM mapping files", file_format="bam", default=bam_files, required=True)
        self.add_parameter_list("indiv_list", "Sample names", default=indiv_list, required=True)
        self.add_parameter_list("chromosomes", "Chromosomes to study (ChromosomesList object)", default=chromosomes)
        self.add_input_directory("chr_dir", "Directory containing the fasta of the chromosome", default=chr_dir)
        self.add_output_file_list("stats", "Stats results of the analysis", pattern="cnvnator_{basename_woext}.stats",
                                  items=indiv_list)
        self.add_output_file_list("stats_repr", "Representation of stats into a graph",
                                  pattern="cnvnator_{basename_woext}.stats.png", items=indiv_list)
        self.add_output_file_list("stdout", "Standard output of the analysis",
                                  pattern="cnvnator_{basename_woext}.stats.stdout", items=indiv_list)
        self.add_output_file_list("stderr", "Standard error of the analysis",
                                  pattern="cnvnator_{basename_woext}.stats.stderr", items=indiv_list)

    def process(self):
        for i in range(0, len(self.bam_files)):
            self.add_python_execution(run_cnvnatorRdStats, cmd_format="{EXE} {IN} {ARG} {OUT}",
                                      inputs=[self.bam_files[i], self.chr_dir],
                                      arguments=[self.get_exec_path("cnvnator"), self.get_exec_path("Rscript"),
                                                 self.get_exec_path("RDvariation.R"), self.indiv_list[i],
                                                 self.get_temporary_file(""), self.output_directory,
                                                 ",".join(self.chromosomes)],
                                      outputs=[self.stats[i], self.stdout[i], self.stderr[i], self.stats_repr[i]])
