import os

from jflow.component import Component


def run_bamstats(samtools, bamstat, isize_r, coverage_r, rscript, indiv_name, reference, output_directory,
                 tmp_dir, chromosomes_str, is_genome, bam_file, stdout, stderr, *outs):
    chromosomes = chromosomes_str.split(",")
    bam_filename = bam_file[bam_file.rfind("/")+1:]
    bam_chr_filename = bam_filename.replace(".bam", "-chr.bam")

    # Index bam:
    cmd = samtools + " index " + bam_file + " > " + stdout + " 2>> " + stderr
    os.system(cmd)

    # tmp_dir = os.popen("mktemp -d").read().replace("\n", "")

    if is_genome == "0":
        # Extract given chromosome(s):
        tmp_bam = tmp_dir + os.path.sep + bam_chr_filename
        cmd = samtools + " view -bh " + bam_file + " " + " ".join(chromosomes) + " | " + samtools + \
            " sort - " + tmp_bam.replace(".bam", "") + " >> " + stdout + " 2>> " + stderr
        os.system(cmd)
    else:
        tmp_bam = bam_file

    # Index new generated bam:
    cmd = samtools + " index " + tmp_bam
    os.system(cmd)

    output_prefix = output_directory + os.path.sep + indiv_name

    # Launch bamStats:
    cmd = bamstat + " -o " + output_prefix + " -r " + reference + " " + tmp_bam + " >> " \
        + stdout + " 2>> " + stderr
    os.system(cmd)

    # Launch isizeR:
    cmd = rscript + " " + isize_r + " " + output_prefix + ".isize.tsv >> " + stdout + " 2>> " + stderr
    os.system(cmd)

    # Launch coverageR:
    cmd = rscript + " " + coverage_r + " " + output_prefix + ".coverage.tsv >> " + stdout + " 2>> " + stderr
    os.system(cmd)

    os.system("rm -rf " + tmp_dir)


class BamStats(Component):

    def define_parameters(self, bam_files, indiv_list, reference, chromosomes, is_genome=False):
        self.add_input_file_list("alignments", "BAM mapping files", file_format="bam", default=bam_files,
                                 required=True)
        self.add_parameter_list("indiv_list", "Sample names", default=indiv_list, required=True)
        self.add_input_file("reference", "The reference genome", file_format="fasta", default=reference, required=True)
        self.add_parameter_list("chromosomes", "Chromosomes to study", default=chromosomes)
        self.add_parameter("is_genome", "Analyse whole genome", default=is_genome)
        self.add_output_file_list("coverage", "Coverage stats (tsv)", pattern="{basename_woext}.coverage.tsv",
                                  items=indiv_list)
        self.add_output_file_list("coverage_png", "Coverage stats (png)", pattern="{basename_woext}.coverage.tsv.png",
                                  items=indiv_list)
        self.add_output_file_list("isize", "Insert size (tsv)", pattern="{basename_woext}.isize.tsv", items=indiv_list)
        self.add_output_file_list("isize_png", "Insert size (png)", pattern="{basename_woext}.isize.tsv.png",
                                  items=indiv_list)
        self.add_output_file_list("stdout", "Standard output", pattern="bamstats_{basename_woext}.stdout",
                                  items=indiv_list)
        self.add_output_file_list("stderr", "Standard error", pattern="bamstats_{basename_woext}.stderr",
                                  items=indiv_list)

    def process(self):
        for i in range(0, len(self.alignments)):
            tmp_dir = self.get_temporary_file("")
            os.mkdir(tmp_dir)
            self.add_python_execution(run_bamstats, cmd_format="{EXE} {ARG} {IN} {OUT}", inputs=[self.alignments[i]],
                                      arguments=[self.get_exec_path("samtools"), self.get_exec_path("bamStats"),
                                                 self.get_exec_path("isize.R"), self.get_exec_path("coverage.R"),
                                                 self.get_exec_path("Rscript"),
                                                 self.indiv_list[i],
                                                 self.reference, self.output_directory, tmp_dir,
                                                 ",".join(self.chromosomes), ("1" if self.is_genome else "0")],
                                      includes=self.reference,
                                      outputs=[self.stdout[i], self.stderr[i], self.coverage[i], self.coverage_png[i],
                                               self.isize[i], self.isize_png[i]])
