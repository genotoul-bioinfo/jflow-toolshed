from jflow.component import Component


class SamtoolsIdxstats(Component):

    def define_parameters(self, bam_files):
        self.add_input_file_list("bam_files", "Bam alignment file", default=bam_files)
        self.add_output_file_list("idx_stats",
                                  "Retrieve and print stats in the index file corresponding to the input file",
                                  pattern="idxstats_{basename_woext}.tsv", items=bam_files)
        self.add_output_file_list("stderr", "Standard error", pattern="idxstats_{basename_woext}.stderr",
                                  items=bam_files)

    def process(self):
        cmd = "samtools index $1;\n" \
              "samtools idxstats $1 > $2 2>> $3;"
        self.add_shell_execution(cmd, cmd_format="{EXE} {IN} {OUT}", inputs=self.bam_files,
                                 outputs=[self.idx_stats, self.stderr], map=True)
