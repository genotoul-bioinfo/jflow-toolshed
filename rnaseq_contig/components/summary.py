from jflow.component import Component


def build_summary(nbfiles_cutadapt, nbfiles_map, nbfiles_idxstats, io_files_file, *io):
    import re, csv
    from collections import OrderedDict

    with open(io_files_file, "r") as io_f:
        iofiles = io_f.read().split("|")
    iofiles = list(iofiles)

    # Get outputs:
    cutadapt_summary = None
    if int(nbfiles_cutadapt) > 0:
        cutadapt_summary = iofiles.pop(0)
    mapping_summary = iofiles.pop(0)
    idxstats_summary = iofiles.pop(0)

    # Get inputs:
    cutadapt_logs = []
    if int(nbfiles_cutadapt) > 0:
        for i in range(0, int(nbfiles_cutadapt)):
            cutadapt_logs.append(iofiles.pop(0))
    mapping_summaries = []
    for i in range(0, int(nbfiles_map)):
        mapping_summaries.append(iofiles.pop(0))
    idxstats_files = []
    for i in range(0, int(nbfiles_idxstats)):
        idxstats_files.append(iofiles.pop(0))

    ####################
    # Cutadapt Summary #
    ####################

    def _build_lines_cutadapt(file):
        with open(file, "r") as log_file:
            line = log_file.readline()
            while line != "=== Summary ===\n":
                line = log_file.readline()
            line = log_file.readline().replace("\n", "")
            occurences = 1
            while not line.startswith("==="):
                match = re.match(r"(.+):\s*(.+)", line)
                if match:
                    head = match.group(1)
                    if head == "  Read 1" or head == "  Read 2":
                        if occurences < 3:
                            occurences += 1
                        else:
                            head = head.replace("  Read", "  Read")
                    value = match.group(2)
                    value = re.sub("\(.+\)", "", value)
                    value = value.replace(" bp", "").replace(",", "")
                    if head not in res_lines:
                        res_lines[head] = []
                    res_lines[head].append(value)
                line = log_file.readline().replace("\n", "")

    if cutadapt_summary is not None:
        sample_names = []
        res_lines = OrderedDict()
        for cutadapt_log in cutadapt_logs:
            _build_lines_cutadapt(cutadapt_log)
            sample_names.append(re.search(r"cutadapt_(.+)\.stdout", cutadapt_log).group(1))

        with open(cutadapt_summary, "w") as summary_file:
            summary_file.write("\t" + "\t".join(sample_names) + "\n")
            for head, value in res_lines.items():
                summary_file.write(head + "\t" + "\t".join(value) + "\n")

    ###################
    # Mapping summary #
    ###################

    def get_mapping_summary_data_for_file(file_s, nb_file, map_summ):
        with open(file_s, "r") as file_s_obj:
            lines = file_s_obj.readlines()
        for line in lines:
            m = re.match(r"(\d+) \+ \d+ ([\w\s]+\w( \(mapQ>=5\))?)( \(.+\))?", line)
            value = m.group(1)
            name = m.group(2)
            if name not in map_summ:
                map_summ[name] = []
                for i in range(0, nb_file):
                    map_summ[name].append("")
            map_summ[name].append(value)
        return map_summ

    map_summ = OrderedDict()
    nb_file = 0
    headers = [""]
    for summ in mapping_summaries:
        name_sample = summ[summ.rfind("/")+1:][:-19]
        headers.append(name_sample)
        map_summ = get_mapping_summary_data_for_file(summ, nb_file, map_summ)
        nb_file += 1

    with open(mapping_summary, "w") as mapping_summary_file:
        mapping_summary_file.write("\t".join(headers) + "\n")
        for line,values in map_summ.items():
            if len(values) < len(headers) - 1:
                for i in range(0, len(headers) - 1 - len(values)):
                    values.append("")
            mapping_summary_file.write(line + "\t" + "\t".join(values) + "\n")

    #####################
    # IDX Stats summary #
    #####################

    headers = ["Chromosome", "Length"]
    chromosomes = {}
    nb_samples = 0
    # Get data:
    for idx_stat in idxstats_files:
        sample_name = re.match("^.+idxstats_(.+)-sorted\.tsv$", idx_stat).group(1)
        headers.append(sample_name)
        with open(idx_stat, newline='') as csvfile:
            reader = csv.reader(csvfile, delimiter="\t")
            for row in reader:
                chromosome = row[0]
                if chromosome not in chromosomes:
                    chromosomes[chromosome] = {
                        "length": row[1],
                        "mapped": []
                    }
                nb_mapped = len(chromosomes[chromosome]["mapped"])
                if nb_mapped < nb_samples:
                    for i in range(nb_mapped, nb_samples):
                        chromosomes[chromosome]["mapped"].append("")
                chromosomes[chromosome]["mapped"].append(row[2])
        nb_samples += 1

    def chrsort(chr):
        if chr == "*":
            return "z" * 1000  # Force to be the last one
        return chr

    # Write data:
    with open(idxstats_summary, "w") as idxstats_f:
        idxstats_f.write("\t".join(headers) + "\n")
        keys = sorted(list(chromosomes.keys()), key=lambda x: chrsort(x))
        for chromosome in keys:
            data = chromosomes[chromosome]
            idxstats_f.write(chromosome + "\t" + data["length"] + "\t")
            nb_mapped = len(data["mapped"])
            if nb_mapped < nb_samples:
                for i in range(nb_mapped, nb_samples):
                    data["mapped"].append("")
            idxstats_f.write("\t".join(data["mapped"]) + "\n")


class Summary(Component):

    def get_description(self):
        return "Summary of jobs" if self.description is None else self.description

    def define_parameters(self, summary_mapping_files, idxstats, cutadapt_logs=None):
        self.add_input_file_list("summary_mapping_files", "summary for each mapping", default=summary_mapping_files)
        self.add_input_file_list("idxstats",
                                 "Retrieve and print stats in the index file corresponding to the input file",
                                 default=idxstats)
        if cutadapt_logs is not None:
            self.add_input_file_list("cutadapt_logs", "logs of cutadapt", default=cutadapt_logs)
            self.add_output_file("summary_cutadapt", "Summary of cutadapt trimming", filename="summary_trimming.tsv")
        self.add_output_file("summary_mapping", "Summary of mapping", filename="summary_mapping.tsv")
        self.add_output_file("summary_idxstats", "Summary of idxstats", filename="summary_idxstats.tsv")

    def process(self):
        build_cutadapt_summary = hasattr(self, "cutadapt_logs")
        arguments = ["0", str(len(self.summary_mapping_files)), str(len(self.idxstats))]
        inputs = self.summary_mapping_files + self.idxstats
        outputs = [self.summary_mapping, self.summary_idxstats]
        if build_cutadapt_summary:
            arguments[0] = str(len(self.cutadapt_logs))
            inputs = self.cutadapt_logs + inputs
            outputs.insert(0, self.summary_cutadapt)
        io_files = self.output_directory + "/iio_files.txt"
        with open(io_files, "w") as i_file:
            i_file.write("|".join([str(x) for x in outputs + inputs]))
        arguments.append(io_files)
        self.add_python_execution(build_summary, cmd_format="{EXE} {ARG} {OUT} {IN}", includes=inputs,
                                  outputs=outputs, arguments=arguments, local=True)
