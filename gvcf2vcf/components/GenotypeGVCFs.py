#
# Copyright (C) 2015 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Kenza BAZI KABBAJ INRA - SIGENAE'
__version__ = '1.1.2'


from jflow.component import Component
import os

class GenotypeGVCFs (Component):
    
    """
    @summary: This component will perform joint genotyping on gVCF files (GATK calling) using GenotypeGVCFs tool.
    @return: gatk vcf file (multisample, all regions)
    """
    
    def define_parameters(self, ref_genome, gvcfs, beds):
        
        """
        @param ref_genome: [str] path to the indexed reference genome
        @param gvcfs: [list] path to gvcfs inputs (by sample or combined gvcfs)
        @param beds: [list] path to bed files for parallelization per region
        """
        
        # Inputs
        self.add_input_file("ref_genome", "indexed reference file", default=ref_genome, file_format="fasta", required=True)
        self.add_input_file_list("gvcfs", "gvcfs inputs (combined gvcfs)", default=gvcfs, required=True)
        self.add_input_file_list("beds", "beds files used for parallelization per region", default=beds, required=True)
        
        # Outputs
        self.add_output_file("genotyping_stderr", "stderr file for genotyping gvcfs", filename='GenotypeGVCFs.stderr')
        self.add_output_file("concat_stderr", "stderr file for concatenating vcf files", filename='vcf_concat.stderr')
        self.add_output_file("gatk_output", "vcf results (multisample, all regions)", filename='gatk_variants.vcf.gz')
        
    def process(self):
        
        # Get memory number
        mem="1G"
        if self.get_memory() != None :
            mem=self.get_memory()
        xmx_option  = "-Xmx" + str(mem).lower()
        
        # Genotyping gvcf inputs
        tmp_stderr = self.get_outputs( '{basename_woext}_genotyping.stderr', self.beds )
        tmp_outputs=self.get_outputs('{basename_woext}_tmp.vcf', self.beds)
        self.add_shell_execution(self.get_exec_path("java") + " " + xmx_option + " -jar " + self.get_exec_path("gatk") + " -T GenotypeGVCFs -L $1 -R "+self.ref_genome+" --variant " + " --variant ".join(self.gvcfs)+" -o $2 2>> $3", cmd_format='{EXE} {IN} {OUT}', inputs=self.beds, outputs=[tmp_outputs, tmp_stderr], includes=[self.ref_genome, self.ref_genome+".fai", os.path.splitext(self.ref_genome)[0]+".dict", self.gvcfs], map=True)
        
        # Concat stderr files
        self.add_shell_execution("cat "+" ".join(tmp_stderr)+" > $1", cmd_format='{EXE} {OUT}', outputs=self.genotyping_stderr, includes=tmp_stderr, map=False, local=True)
        
        # Concat regions + zip and index vcf file
        self.add_shell_execution(self.get_exec_path("vcf-concat") + " " + " ".join(tmp_outputs)  + " | bgzip -c > $1 2>> $2 ; tabix -p vcf $1", cmd_format='{EXE} {OUT}', outputs=[self.gatk_output, self.concat_stderr], includes=tmp_outputs, map=False)

