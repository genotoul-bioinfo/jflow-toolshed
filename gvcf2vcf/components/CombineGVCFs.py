#
# Copyright (C) 2015 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Kenza BAZI KABBAJ INRA - SIGENAE'
__version__ = '1.1.3'


from jflow.component import Component
import os

class CombineGVCFs (Component):
    
    """
    @summary: This component will merge gvcfs gatk results using CombineGVCFs tool from GATK on small batches. 
    @return: combined gvcfs (multisample) per group
    """
    
    def define_parameters(self, ref_genome, gvcfs, beds, group_nb=60):
        
        """
        @param ref_genome: [str] path to the indexed reference genome
        @param gvcfs: [list] path to gvcfs inputs (by sample)
        @param beds: [list] path to bed files for parallelization per region
        @param group_nb: [int] number of gvcf inputs that constitute a group to be merged
        """
        
        # Inputs
        self.add_input_file("ref_genome", "indexed reference file", default=ref_genome, file_format="fasta", required=True)
        self.add_input_file_list("gvcfs", "gvcfs inputs (by sample)", default=gvcfs, required=True)
        self.add_input_file_list("beds", "beds files used for parallelization per region", default=beds, required=True)
        self.add_parameter("group_nb", "number of gvcf inputs that constitute a group to be merged", default=group_nb, type='int')
        
        # Get gvcf inputs in small batches
        self.all_groups=self.get_groups(gvcfs, group_nb)
        
        # Get group names
        group_names=list()
        for i in self.all_groups:
            group_name=(os.path.basename(i[0])).split(".")[0]+"_"+(os.path.basename(i[-1])).split(".")[0]
            group_names.append(group_name)
        
        # Outputs
        self.add_output_file_list("output_files", "combined gvcfs results (multisample) per group", pattern="{basename_woext}_combined.g.vcf", items=group_names)
        self.add_output_file_list("combining_stderr", "stderr files for combining gvcfs per group", pattern="{basename_woext}_CombineGVCFs.stderr", items=group_names)
        self.add_output_file_list("concat_stderr", "stderr files for vcf concatenation per group", pattern="{basename_woext}_vcfconcat.stderr", items=group_names)
    
    def get_groups(self, gvcfs, group_nb):
        """ Splitting gvcfs inputs in small batches """
        all_groups=list()
        for i in range(0,len(gvcfs),group_nb):
            if len(gvcfs[i:i+group_nb]) == 1:
                all_groups[-1].extend(gvcfs[i:i+group_nb])
            else:
                all_groups.append(gvcfs[i:i+group_nb])
        return all_groups

    def process(self):
        
        # Get memory number
        mem="1G"
        if self.get_memory() != None :
            mem=self.get_memory()
        xmx_option  = "-Xmx" + str(mem).lower()

        # Combining GVCFs inputs by region and by group
        for (idx, group) in enumerate(self.all_groups):
            tmp_outputs=self.get_outputs(str(idx)+'_{basename_woext}_tmp.g.vcf', self.beds)
            tmp_stderr=self.get_outputs(str(idx)+'_{basename_woext}_combining.stderr', self.beds)
            
            # Combining gvcf groups
            self.add_shell_execution(self.get_exec_path("java") + " " + xmx_option + " -jar " + self.get_exec_path("gatk") + " -T CombineGVCFs -L $1 -R "+self.ref_genome+" --variant " + " --variant ".join(group) + " -o $2 2>> $3", cmd_format='{EXE} {IN} {OUT}', inputs=self.beds, outputs=[tmp_outputs, tmp_stderr], includes=[self.ref_genome, self.ref_genome+".fai", os.path.splitext(self.ref_genome)[0]+".dict"], map=True)
            
            # Concat stderr files
            self.add_shell_execution("cat "+" ".join(tmp_stderr)+" > $1", cmd_format='{EXE} {OUT}', outputs=self.combining_stderr[idx], includes=tmp_stderr, map=False, local=True)
            
            # Concat chromosoms
            self.add_shell_execution(self.get_exec_path("vcf-concat") + " " + " ".join(tmp_outputs)  + " > $1 2>> $2", cmd_format='{EXE} {OUT}', outputs=[self.output_files[idx], self.concat_stderr[idx]], includes=tmp_outputs, map=False)
