#
# Copyright (C) 2015 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Kenza BAZI KABBAJ INRA - SIGENAE'
__version__ = '2.1.0'

import os
from jflow.component import Component


class IndexFaiDict (Component):
    
    """
    @summary: Indexing reference genome (fai) and creating dictionary
    @return: the index file (.fai) and/or the dictionary file (.dict).
    """
    def define_parameters( self, input_file ):
        
        """
        @param input_file: [str] The fasta file to index
        """
        # input files
        self.add_input_file( "input_file", "The fasta file to index.", default=input_file, file_format="fasta", required=True )
        
        # output files
        self.add_output_file( "dict_path", "The dictionary file.", filename=(os.path.splitext(os.path.basename(input_file))[0] + ".dict"))
        self.add_output_file( "stderr_createdict", "The stderr output file of dictionary creation.", filename="createdict.stderr")
        self.add_output_file( "fai", "The fai index file", filename=(os.path.basename(input_file) + ".fai") )
        self.add_output_file( "stderr_faidx", "The stderr output file of fai index creation.", filename="faidx.stderr")
        self.add_output_file( "databank", "fasta file linked to the dictionary and the fai index file.", filename=os.path.basename(input_file), file_format="fasta")

    def process(self):
        
        # get memory number
        mem="2g"
        if self.get_memory() != None :
            mem=self.get_memory()
        xmx_option  = "-Xmx" + str(mem).lower()
        
        self.add_shell_execution("ln -sv $1 $2", cmd_format="{EXE} {IN} {OUT}", inputs=self.input_file, outputs=self.databank, local=True)
        
        # create dict index
        if not os.path.exists(os.path.splitext(self.input_file)[0] + ".dict"):
            self.add_shell_execution(self.get_exec_path("java") + " "+ xmx_option+" -jar "+self.get_exec_path("picard")+" CreateSequenceDictionary R=$1 O=$2 2>> $3", cmd_format="{EXE} {IN} {OUT}", inputs=self.databank, outputs=[self.dict_path, self.stderr_createdict])
        else:
            ref_dict=os.path.splitext(self.input_file)[0] + ".dict"
            self.add_shell_execution("ln -sv $1 $2", cmd_format="{EXE} {IN} {OUT}", inputs=ref_dict, outputs=self.dict_path, local=True)
            
        # create fai index
        if not os.path.exists(self.input_file + ".fai"):
            self.add_shell_execution(self.get_exec_path("samtools") + " faidx $1 2> $2", cmd_format="{EXE} {IN} {OUT}", inputs=self.databank, outputs=[self.stderr_faidx, self.fai])
        else:
            ref_fai=self.input_file + ".fai"
            self.add_shell_execution("ln -sv $1 $2", cmd_format="{EXE} {IN} {OUT}", inputs=ref_fai, outputs=self.fai, local=True)
