#
# Copyright (C) 2015 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Kenza BAZI KABBAJ INRA - SIGENAE'
__version__ = '1.2.7'


from jflow.workflow import Workflow
import os
from jflow import seqio

def gvcf_files(folder):
    gvcf_files = []
    for file in os.listdir(folder):
        if file.endswith(".g.vcf") or file.endswith(".gvcf") or file.endswith("g.vcf.gz"):
            gvcf_files.append(file)
    return gvcf_files

class gvcf2vcf (Workflow):

    def get_description(self):
        return "Converting gatk gvcf files to vcf"
        
    def define_parameters(self, function="process"):
        
        #Input files
        self.add_input_file("ref_genome", "The reference genome in fasta format", file_format="fasta", required=True, group="input files")
        self.add_input_directory("gvcf", "directory containing gvcf files (by sample)", get_files_fn=gvcf_files, required=True, group="input files")
        self.add_input_directory("combined_gvcf", "directory containing combined gvcf files (multisample)", get_files_fn=gvcf_files, group="input files")
        
        #Combining Parameters
        self.add_parameter("group_number", "number of gvcf files used to constitute groups, which are used as inputs for CombineGVCFs. Note that it's recommanded by Gatk to have less than 200 individual gvcfs as inputs for GenotypeGVCFs. Default=60", type='int', group="Combining parameters", default=60)
        
        #General options
        self.add_parameter("remove_tmp_files", "add this option to remove temporary files at the end of the pipeline execution, default=false", type='bool', default=False, group="General options")

    def update_sequenced_regions(self, bed_files):
        """
        Mainly, this function will regroup sequences which are below the threshold limit in bed files.
        output=list of bed files
        """
        reader= seqio.FastaReader(self.ref_genome)
        threshold= 40000000
        seq_length= 0
        ID= list()
        for id, desc, seq, qualities in reader:
            current_len = len(seq)
            seq_length = seq_length + current_len
            ident= id+"\t"+"1"+"\t"+str(len(seq))+"\n"
            ID.append(ident)
            if seq_length > threshold:
                bed = self.get_temporary_file("_"+ID[0].split("\t")[0]+".bed" if len(ID)==1 else "_"+ID[0].split("\t")[0]+"_"+ID[-1].split("\t")[0]+".bed")
                intervals = open(bed, "w")
                intervals.write("".join(ID))
                bed_files.append(bed)
                intervals.close()
                seq_length = 0
                ID = []
        
        if seq_length != 0:
            bed = self.get_temporary_file("_"+ID[0].split("\t")[0]+".bed" if len(ID)==1 else "_"+ID[0].split("\t")[0]+"_"+ID[-1].split("\t")[0]+".bed")
            intervals = open(bed, "w")
            intervals.write("".join(ID))
            bed_files.append(bed)
            intervals.close()
    
    def process(self):
        
        
        #Create gvcf input list for Genotyping
        all_gvcf_inputs=list()
        
        #Create bed files per region for parallelization 
        beds=list()
        self.update_sequenced_regions(beds)

        #Index ref genome: fai_dict
        databank_indexed = self.ref_genome
        if not os.path.exists(self.ref_genome + ".fai") or not os.path.exists(os.path.splitext(self.ref_genome)[0] + ".dict"):
            index_fai_dict = self.add_component( "IndexFaiDict", [self.ref_genome] )
            databank_indexed = index_fai_dict.databank
        
        #Get combined gvcf inputs if provided
        if self.combined_gvcf != None:
            all_gvcf_inputs.extend(self.combined_gvcf.get_files())
        
        #Combining individual gvcf inputs by group
        if len(self.gvcf.get_files()) != 1:
            combine_gvcfs=self.add_component("CombineGVCFs", [databank_indexed, self.gvcf.get_files(), beds, self.group_number])
            all_gvcf_inputs.extend(combine_gvcfs.output_files)
        else:
            all_gvcf_inputs.extend(self.gvcf.get_files())
        
        #Genotyping gvcf inputs
        genotype_gvcfs=self.add_component("GenotypeGVCFs", [databank_indexed, all_gvcf_inputs, beds])
        
    def post_process(self):
        
        #delete intermediate files:
        if self.remove_tmp_files:
            
            #CombineGVCFs Component:
            os.system("rm -f "+os.path.join(self.get_component_output_directory("CombineGVCFs", "default"), "*_combining.stderr")+" ;rm -f "+os.path.join(self.get_component_output_directory("CombineGVCFs", "default"), "*_tmp.g.vcf*"))
            
            #GenotypeGVCFs Component:
            os.system("rm -f "+os.path.join(self.get_component_output_directory("GenotypeGVCFs", "default"), "*_genotyping.stderr")+ " ;rm -f "+os.path.join(self.get_component_output_directory("GenotypeGVCFs", "default"), "*_tmp.vcf*"))
