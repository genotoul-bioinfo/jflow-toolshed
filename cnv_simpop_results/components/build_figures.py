from jflow.component import Component


def run_build_figures(input_dir, nb_inds, haploid, stripplot, out_dir, stdout, stderr, *outs):
    import build_figures
    import sys

    sys.stdout = open(stdout, 'w')
    sys.stderr = open(stderr, 'w')

    build_figures.init(input_dir=input_dir, nb_inds=int(nb_inds), haploid=True if haploid == "1" else False,
                       stripplot=True if stripplot == "1" else False, output=out_dir)


class BuildFigures(Component):

    def define_parameters(self, input_dir, nb_inds, haploid, stripplot):
        self.add_input_directory("input_dir", help="Input directory containing tsv files", default=input_dir)
        self.add_parameter("nb_inds", help="Number of individuals", default=nb_inds, type=int)
        self.add_parameter("haploid", help="Is an haploid", type=bool, default=haploid)
        self.add_parameter("stripplot", help="Build stripplots instead of boxplots", default=stripplot, type=bool)
        self.add_output_file("gt_quality", help="Quality of genotypes comparison", filename="gt_quality.pdf")
        self.add_output_file("precision", help="Precision comparison", filename="precision_per_tool.pdf")
        self.add_output_file("recall", help="Recall comparison", filename="recall_per_tool.pdf")
        self.add_output_file("stdout", "Standard output", filename="logs.stdout")
        self.add_output_file("stderr", "Standard error", filename="logs.stderr")

    def process(self):
        inputs = [self.input_dir]
        arguments = [str(self.nb_inds), "1" if self.haploid else "0", "1" if self.stripplot else "0",
                     self.output_directory]
        outputs = [self.stdout, self.stderr, self.gt_quality, self.precision, self.recall]
        self.add_python_execution(run_build_figures, cmd_format="{EXE} {IN} {ARG} {OUT}", inputs=inputs,
                                  arguments=arguments, outputs=outputs)
