import os, re
from jflow.component import Component


class Bowtie2Index(Component):

    def get_command(self):
        return "bowtie2-build"

    def get_version(self):
        stdout = os.popen(self.get_exec_path("bowtie2-build") + " --version").read()
        search = re.search(r"version ([^\n]+)", stdout)
        if search:
            return search.group(1)
        return "Unknown"

    def get_description(self):
        return "Index genome (bowtie)" if self.description is None else self.description

    def define_parameters(self, input_fasta):
        self.add_input_file("input_fasta", "Input fasta file", required=True, default=input_fasta, file_format="fasta")
        self.add_output_file("stdout", "Standard output", filename="bwa_index.stdout")
        self.add_output_file("stderr", "Standard error", filename="bwa_index.stderr")
        self.add_output_directory("genome_dir", "Genome directory", dirname="genome")

    def process(self):
        cmd = "mkdir $2; bowtie2-build $1 $2/reference > $3 2>> $4"
        self.add_shell_execution(cmd, cmd_format="{EXE} {IN} {OUT}", inputs=self.input_fasta, outputs=[self.genome_dir,
                                 self.stdout, self.stderr])
