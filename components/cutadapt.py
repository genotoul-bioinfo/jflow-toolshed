import os
from jflow.component import Component


class Cutadapt (Component):

    def get_command(self):
        return "cutadapt"

    def get_version(self):
        return os.popen(self.get_exec_path("cutadapt") + " --version").read().replace("\n", "")

    def get_description(self):
        return "Trimming (Cutadapt)" if self.description is None else self.description

    def define_parameters(self, reads_1, reads_2=[], adapter_1=None, adapter_2=None, position_adapters="3'",
                          minimum_length=0):
        if position_adapters == "3'":
            a1_flag = "-a"
            a2_flag = "-A"
        elif position_adapters == "5'":
            a1_flag = "-g"
            a2_flag = "-G"
        else:
            a1_flag = "-b"
            a2_flag = "-B"
        self.add_parameter("adapter_1", "Sequence of the adapter (paired data: of the first read)", cmd_format=a1_flag,
                           default=adapter_1, required=True)
        self.add_parameter("adapter_2", "Sequence of the adapter for the second read (paired data only)",
                           cmd_format=a2_flag,
                           default=adapter_2)
        self.add_parameter("position_adapter", "Position of the adapter", choices=["3'", "5'", "3'/5'"],
                           default=position_adapters, required=True)
        self.add_parameter("minimum_length", "Discard trimmed reads that are shorter than the given value", type=int,
                           default=minimum_length, cmd_format="-m")
        self.add_input_file_list("reads_1", "Which reads files should be used", required=True, default=reads_1)
        self.add_input_file_list("reads_2", "The paired reads", required=False, default=reads_2)
        self.add_output_file_list("o_reads_1", "Trimmed reads", pattern='{basename_woext}.trim.fastq.gz',
                                  items=self.reads_1)
        self.add_output_file_list("o_reads_2", "Trimmed reads (paired reads)", pattern='{basename_woext}.trim.fastq.gz',
                                  items=self.reads_2)
        self.add_output_file_list("stdout", "Standard output", pattern='cutadapt_{basename_woext}.stdout', items=reads_1)
        self.add_output_file_list("stderr", "Standard error", pattern='cutadapt_{basename_woext}.stderr', items=reads_1)

    def process(self):

        # Build command:
        command = self.get_exec_path("cutadapt")
        is_paired = False
        if len(self.reads_2) > 0:
            is_paired = True
        command += " " + self.adapter_1.cmd_format + " " + self.adapter_1
        if is_paired:
            command += " " + self.adapter_2.cmd_format + " " + self.adapter_2
        if self.minimum_length is not None:
            command += " " + self.minimum_length.cmd_format + " " + str(self.minimum_length)

        # Build command outputs, and define inputs and outputs for shell execution:
        if is_paired:
            command += " -o $3 -p $4 $1 $2 > $5 2>> $6"
            self.add_shell_execution(command, cmd_format="{EXE} {IN} {OUT}", inputs=[self.reads_1, self.reads_2],
                                     outputs=[self.o_reads_1, self.o_reads_2, self.stdout, self.stderr], map=True)
        else:
            command += " -o $2 $1 > $3 2>> $4"
            self.add_shell_execution(command, cmd_format="{EXE} {IN} {OUT}", map=True, inputs=self.reads_1,
                                     outputs=[self.o_reads_1, self.stdout, self.stderr])
