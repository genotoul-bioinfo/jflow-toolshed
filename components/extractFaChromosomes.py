import os

from jflow.component import Component


class ExtractFaChromosomes(Component):

    def define_parameters(self, fasta_file, chromosomes):
        self.add_input_file("fasta_file", "The fasta file of the full genome", file_format="fasta", default=fasta_file)
        self.add_parameter_list("chromosomes", "List of chromosomes to extract",
                              default=chromosomes)
        self.add_output_directory("fa_chrs", "Directory containing fasta files for each chromosome", dirname="chrs")
        self.add_output_file("stderr", "Standard error", filename="faidx.stderr")

    def process(self):
        os.mkdir(self.fa_chrs)
        cmd = ""
        for chromosome in self.chromosomes:
            cmd += self.get_exec_path("samtools") + " faidx $1 " + chromosome + " > $2/" + chromosome + ".fa 2>> $3;\n"
            cmd += self.get_exec_path("samtools") + " faidx $2/" + chromosome + ".fa 2>> $3;\n"
        self.add_shell_execution(cmd, cmd_format="{EXE} {IN} {OUT}", inputs=[self.fasta_file],
                                 outputs=[self.fa_chrs, self.stderr])
