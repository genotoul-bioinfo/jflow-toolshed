import re, os
from subprocess import Popen, PIPE
from jflow.component import Component


def run_bwa_map(bwa_cmd, samtools_cmd, prefix, genome, n_sorted, mark_shorter, rg_flag, threads, name, bam_file,
                summary, stdout, stderr, reads1, reads2=None):
    cmd = bwa_cmd + " mem -t " + threads + " "
    if mark_shorter == "1":
        cmd += "-M "
    if rg_flag == "1":
        cmd += "-R '@RG\\tID:" + name + "\\tPL:ILLUMINA\\tPU:-\\tSM:" + name + "\\tLB:" + name + "' "
    cmd += genome + "/" + prefix + " " + reads1 + ((" " + reads2) if reads2 is not None else "") \
        + " 2>> " + stderr + \
        " | " + samtools_cmd + " view -bS - " + " 2>> " + \
        stderr + " | " + samtools_cmd + " sort"
    if n_sorted == "1":
        cmd += " -n "
    cmd += " -o " + bam_file + " >> " + stdout + " 2>> " + \
          stderr
    os.system(cmd)

    # Index bam:
    cmd = samtools_cmd + " index " + bam_file
    os.system(cmd)

    # Statistics:
    cmd = samtools_cmd + " flagstat " + bam_file + " > " + summary
    os.system(cmd)


class BwaMap(Component):

    def get_command(self):
        return "bwa map"

    def get_version(self):
        cmd = [self.get_exec_path("bwa")]
        p = Popen(cmd, stdout=PIPE, stderr=PIPE)
        stdout, stderr = p.communicate()
        search = re.search(r"Version: ([^\n]+)", stderr.decode("utf-8"))
        if search:
            return search.group(1)
        return "Unknown"

    def get_description(self):
        return "Map reads to the genome (bwa)" if self.description is None else self.description

    def define_parameters(self, genome, reads_1, reads_2=None, names=None, n_sorted=False, mark_shorter=False,
                          rmdup=False, prefix="reference", threads=4):
        self.add_parameter("threads", "Number of threads to use", cmd_format="-t", default=threads)
        self.add_parameter("prefix", "Prefix of genome files", default=prefix)
        self.add_parameter("n_sorted", "Sort by name", default=n_sorted, type=bool)
        self.add_parameter("mark_shorter", "Mark shorter split hits as secondary", cmd_format="-M", default=mark_shorter,
                           type=bool)
        self.add_parameter("rg_flag", "Remove duplicates",
                           cmd_format="-R '@RG\\tID:$name\\tPL:ILLUMINA\\tPU:-\\tSM:$name\\tLB:$name'", default=rmdup,
                           type=bool)
        self.add_input_directory("genome_dir", "Index of the reference genome", required=True, default=genome)
        self.add_input_file_list("reads_1", "Which reads files should be used", required=True, default=reads_1)
        self.add_input_file_list("reads_2", "The paired reads", required=False, default=reads_2)

        if names is None:
            names = []
            for read1 in self.reads_1:
                names.append(os.path.splitext(os.path.basename(read1))[0])

        self.add_output_file_list("map_files", "Map files", pattern='{basename_woext}-sorted.bam',
                                  items=names)
        self.add_output_file_list("summary_files", "Summary of mapping", pattern='{basename_woext}-sorted.bam.summary',
                                  items=names)
        self.add_output_file_list("stdout", "Standard output", pattern='bwa_map_{basename_woext}.stdout',
                                  items=names)
        self.add_output_file_list("stderr", "Standard error", pattern='bwa_map_{basename_woext}.stderr',
                                  items=names)
        self.add_parameter_list("names", "names of the samples", default=names)

    def process(self):
        arguments = [self.get_exec_path("bwa"), self.get_exec_path("samtools"), self.prefix, self.genome_dir,
                     "1" if self.n_sorted else "0", "1" if self.mark_shorter else "0", "1" if self.rg_flag else "0",
                     self.threads]

        for i in range(len(self.names)):

            if len(self.reads_2) > 0:
                inputs = [self.reads_1[i], self.reads_2[i]]
            else:
                inputs = self.reads_1[i]

            outputs = [self.map_files[i], self.summary_files[i], self.stdout[i], self.stderr[i]]

            run_args = arguments + [self.names[i]]

            self.add_python_execution(run_bwa_map, cmd_format="{EXE} {ARG} {OUT} {IN}",
                                      arguments=run_args, inputs=inputs, outputs=outputs, includes=[self.genome_dir])

