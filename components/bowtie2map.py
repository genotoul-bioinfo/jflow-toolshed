import re, os
from jflow.component import Component


def run_bowtie2_map(bwa_cmd, samtools_cmd, prefix, genome, bam_file, summary, stdout, stderr, reads1, reads2=None):
    cmd = bwa_cmd + " -x " + genome + "/" + prefix
    if reads2 is not None:
        cmd += " -1 " + reads1 + " -2 " + reads2
    else:
        cmd += " -U " + reads1
    cmd += " 2>> " + stderr + \
          " | " + samtools_cmd + " view -bS - " + " 2>> " + \
          stderr + " | " + samtools_cmd + " sort -o " + bam_file + " >> " + stdout + " 2>> " + \
          stderr
    os.system(cmd)

    # Statistics:
    cmd = samtools_cmd + " flagstat " + bam_file + " > " + summary
    os.system(cmd)


class Bowtie2Map(Component):

    def get_command(self):
        return "bowtie2"

    def get_version(self):
        stdout = os.popen(self.get_exec_path("bowtie2") + " --version").read()
        search = re.search(r"version ([^\n]+)", stdout)
        if search:
            return search.group(1)
        return "Unknown"

    def get_description(self):
        return "Map reads to the genome (bwa)" if self.description is None else self.description

    def define_parameters(self, genome, reads_1, reads_2=None, prefix="reference"):
        self.add_parameter("prefix", "Prefix of genome files", default=prefix)
        self.add_input_directory("genome_dir", "Index of the reference genome", required=True, default=genome)
        self.add_input_file_list("reads_1", "Which reads files should be used", required=True, default=reads_1)
        self.add_input_file_list("reads_2", "The paired reads", required=False, default=reads_2)
        self.add_output_file_list("map_files", "Map files", pattern='{basename_woext}-sorted.bam',
                                  items=reads_1)
        self.add_output_file_list("summary_files", "Summary of mapping", pattern='{basename_woext}-sorted.bam.summary',
                                  items=reads_1)
        self.add_output_file_list("stdout", "Standard output", pattern='bwa_map_{basename_woext}.stdout',
                                  items=reads_1)
        self.add_output_file_list("stderr", "Standard error", pattern='bwa_map_{basename_woext}.stderr', items=reads_1)

    def process(self):
        if len(self.reads_2) > 0:
            inputs = [self.reads_1, self.reads_2]
        else:
            inputs = self.reads_1

        arguments = [self.get_exec_path("bowtie2"), self.get_exec_path("samtools"), self.prefix, self.genome_dir]

        outputs = [self.map_files, self.summary_files, self.stdout, self.stderr]

        self.add_python_execution(run_bowtie2_map, cmd_format="{EXE} {ARG} {OUT} {IN}",
                                  arguments=arguments, inputs=inputs, outputs=outputs, includes=[self.genome_dir],
                                  map=True)

