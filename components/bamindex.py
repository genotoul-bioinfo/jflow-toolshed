from jflow.component import Component


class BamIndex(Component):

    def define_parameters(self, bam_files):
        self.add_input_file_list("bam_files", "Bam files", default=bam_files)
        self.add_output_file_list("idx_bam_files", "Bam files index", pattern="{FULL}.bai", items=bam_files)
        self.add_output_file_list("stdout", "Standard output", pattern="index_{basename_woext}.stdout", items=bam_files)
        self.add_output_file_list("stderr", "Standard error", pattern="index_{basename_woext}.stderr", items=bam_files)

    def process(self):
        self.add_shell_execution(self.get_exec_path("samtools") + " index $1 > $3 2>> $4",
                                 cmd_format="{EXE} {IN} {OUT}", inputs=self.bam_files, outputs=[self.idx_bam_files,
                                 self.stdout, self.stderr])