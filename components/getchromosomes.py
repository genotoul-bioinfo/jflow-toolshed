import os

from jflow.component import Component


def get_chromosomes(fasta_file, chromosomes_list: "ChromosomesList", chrs_in: str):
    import re

    if chrs_in == "genome":
        chr_candidates = os.popen("cat " + fasta_file + " | grep '>' | awk 'sub(/^>/, \"\")'").read().split("\n")
        for chr in chr_candidates:
            if len(chr) > 0:
                m = re.match(r"^chr(_)?(\d+)$", chr.lower())
                if (chr.isdigit() and int(chr) < 200) or (m is not None and int(m.group(2)) < 200):
                    chromosomes_list.add_chromosome(chr)
        print(chromosomes_list.get_chromosomes_string())
        if len(chromosomes_list.chromosomes) == 0:
            raise Exception("No chromosomes found. Please specify them manually")
    else:
        chromosomes_list.set_chromosomes_from_string(chrs_in)
    return chromosomes_list


class GetChromosomes(Component):

    def define_parameters(self, fasta_file, chromosomes_list, chromosomes="genome"):
        self.add_input_file("fasta_file", "Fasta file of the genome", file_format="fasta", default=fasta_file)
        self.add_output_object("chromosomes", "List of chromosomes, comma separated")
        self.add_input_object("chr_list", "List of chromosomes object", default=chromosomes_list)
        self.add_parameter("chrs_in", "List of chromosomes given by user", default=chromosomes)

    def process(self):
        self.add_python_execution(get_chromosomes, cmd_format="{EXE} {IN} {OUT} {ARG}",
                                  inputs=[self.fasta_file, self.chr_list], outputs=self.chromosomes,
                                  arguments=self.chrs_in)
