from jflow.workflow import Workflow


class CnvSimpop(Workflow):

    threads = 8

    def get_description(self):
        return "Simulate a population with random CNV on samples"

    def define_parameters(self, function="process"):
        self.add_parameter("nb_inds", "Number of individuals to generate", type=int, required=True)
        self.add_input_file("reference", "reference genome fasta file", file_format="fasta", required=True)
        self.add_input_file("nstretches", "N-stretches positions bed file", required=False)
        self.add_input_file("sv_list", "list of variants with probabilities", required=False)
        self.add_parameter("coverage", "Coverage of generated reads", default=15)
        self.add_parameter("force_polymorphism", "Force polymorphism for each SV", type=bool,
                           default=False)
        self.add_parameter("haploid", "Make a haploid genome, instead of diploid one", type=bool, default=False)
        self.add_parameter("proba_del", "Probabilty to have a deletion", type=float, default=0.000001)
        self.add_parameter("read_len", "Generate reads having a length of LEN", type=int, default=100)
        self.add_parameter("insert_len_mean", "Generate inserts (fragments) having an average length of LEN", type=int,
                           default=300)
        self.add_parameter("insert_len_sd", "Set the standard deviation of the insert (fragment) length (percents)",
                           type=int, default=30)
        self.add_parameter("min_deletions", "Minimum of deletions to generate (>=1)", default=1)

    def process(self):
        pop = self.add_component("BuildPop", [self.nb_inds, self.reference, self.nstretches, self.sv_list, self.coverage,
                                              self.force_polymorphism, self.haploid, self.proba_del, self.read_len,
                                              self.insert_len_mean, self.insert_len_sd, self.min_deletions,
                                              self.threads])
