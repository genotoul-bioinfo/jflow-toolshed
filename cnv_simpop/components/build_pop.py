import os
from jflow.component import Component


def launch_build_pop(reference, nstretches, sv_list, nb_inds, coverage, force_polymorphism, haploid, proba_del, read_len,
                     insert_len_mean, insert_len_sd, min_deletions, threads, stdout, stderr, *output_files):
    import build_pop
    output_dir = os.path.dirname(output_files[0])
    exit(build_pop.init(output_dir=output_dir, force_outputdir=True, quiet=True,
                        nstretches=nstretches if nstretches != "None" else None,
                        sv_list=sv_list if sv_list != "None" else None, nb_inds=int(nb_inds), reference=reference,
                        proba_del=float(proba_del), haploid=haploid=="1", force_polymorphism=force_polymorphism=="1",
                        coverage=int(coverage), read_len=int(read_len), insert_len_mean=int(insert_len_mean),
                        insert_len_sd=int(insert_len_sd), min_deletions=int(min_deletions), threads=int(threads),
                        stdout=stdout, stderr=stderr))



class BuildPop(Component):

    def define_parameters(self, nb_inds, reference, nstretches=None, sv_list=None, coverage=15,
                          force_polymorphism=False, haploid=False, proba_del=0.000001, read_len=100,
                          insert_len_mean=300, insert_len_sd=30, min_deletions=1, threads=4):
        self.add_parameter("nb_inds", "Number of individuals to generate", type=int, default=nb_inds)
        self.add_input_file("reference", "reference genome fasta file", file_format="fasta", default=reference)
        self.add_input_file("nstretches", "N-stretches positions bed file", default=nstretches)
        self.add_input_file("sv_list", "list of variants with probabilities", default=sv_list)
        self.add_parameter("coverage", "Coverage of generated reads", default=coverage)
        self.add_parameter("force_polymorphism", "Force polymorphism for each SV", type=bool,
                           default=force_polymorphism)
        self.add_parameter("haploid", "Make a haploid genome, instead of diploid one", type=bool, default=haploid)
        self.add_parameter("proba_del", "Probabilty to have a deletion", type=float, default=proba_del)
        self.add_parameter("read_len", "Generate reads having a length of LEN", type=int, default=read_len)
        self.add_parameter("insert_len_mean", "Generate inserts (fragments) having an average length of LEN", type=int,
                           default=insert_len_mean)
        self.add_parameter("insert_len_sd", "Set the standard deviation of the insert (fragment) length (%)", type=int,
                           default=insert_len_sd)
        self.add_parameter("min_deletions", "Minimum of deletions to generate (>=1)", default=min_deletions)
        self.add_parameter("threads", "Number of threads", type=int, default=threads)
        self.add_output_file_list("reads_1", "output reads fastq file, first of pairs", file_format="fastq.gz",
                                  pattern="pop/indiv{basename_woext}_1.fq.gz",
                                  items=[nb_ind for nb_ind in range(1, nb_inds+1)])
        self.add_output_file_list("reads_2", "output reads fastq file, first of pairs", file_format="fastq.gz",
                                  pattern="pop/indiv{basename_woext}_2.fq.gz",
                                  items=[nb_ind for nb_ind in range(1, nb_inds + 1)])
        self.add_output_file_list("genotypes", "true genotypes of all generated individuals VCF file", file_format="vcf"
                                  , pattern="pop/genotypes{basename_woext}.vcf.gz", items=[""])
        # Output_file can't be in a subdirectory, so use an astuce... TODO: fix it correctly!!!
        self.add_output_file("stdout", "Standard output", filename="logs.stdout")
        self.add_output_file("stderr", "Standard error", filename="logs.stderr")

    def process(self):
        inputs = [self.reference]
        arguments = [self.nb_inds, self.coverage, "1" if self.force_polymorphism else "0",
                     "1" if self.haploid else "0", str(self.proba_del), str(self.read_len), str(self.insert_len_mean),
                     str(self.insert_len_sd), str(self.min_deletions), str(self.threads)]
        if not self.nstretches.is_None:
            inputs.append(self.nstretches)
        else:
            arguments.insert(0, "None")
        if not self.sv_list.is_None:
            inputs.append(self.sv_list)
        else:
            arguments.insert(0, "None")
        self.add_python_execution(launch_build_pop, cmd_format="{EXE} {IN} {ARG} {OUT}", inputs=inputs,
                                  arguments=arguments, outputs=[self.stdout, self.stderr, self.reads_1, self.reads_2,
                                                                self.genotypes])
