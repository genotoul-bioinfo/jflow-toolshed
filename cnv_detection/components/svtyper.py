from jflow.component import Component


class Svtyper(Component):

    svtypes = ["DEL", "INV"]

    def define_parameters(self, merged_vcf_DEL, merged_vcf_INV, bam_files, reference, chromosomes, threads=4):
        self.add_input_file_list("merged_vcf_DEL", "Merged VCF files for DEL", default=merged_vcf_DEL,
                                 file_format="vcf")
        self.add_input_file_list("merged_vcf_INV", "Merged VCF files for INV", default=merged_vcf_INV,
                                 file_format="vcf")
        self.add_input_file_list("bam_files", "Bam alignment files", file_format="bam", default=bam_files)
        self.add_input_file("reference", "Reference genome fasta file (with other files for genomeSTRIP)",
                            file_format="fasta", default=reference)
        self.add_parameter("threads", "Number of threads", default=threads, type=int)
        for svtype in self.svtypes:
            self.add_output_file_list("output_" + svtype, "Genotype output files",
                                      pattern="genotype_chr_{basename}_" + svtype + ".vcf.gz", items=chromosomes)
            self.add_output_file_list("stderr_" + svtype, "Standard output",
                                      pattern="genotype_chr_{basename}_" + svtype + ".stderr", items=chromosomes)

    def process(self):
        for svtype in self.svtypes:
            bamlist = ",".join(self.bam_files)
            cmd = " ".join(["zcat $1 |", self.get_exec_path("svtyper"), "-B", bamlist, " 2> $3 |",
                            self.get_exec_path("bgzip"), "-c > $2; tabix -p vcf $2;"])
            self.add_shell_execution(cmd, cmd_format="{EXE} {IN} {OUT}",
                                     inputs=self.__getattribute__("merged_vcf_" + svtype),
                                     outputs=[self.__getattribute__("output_" + svtype),
                                              self.__getattribute__("stderr_" + svtype)],
                                     map=True)

