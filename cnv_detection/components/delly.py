import os

from pathlib import Path
import inspect

from jflow.component import Component


class Delly(Component):

    def define_parameters(self, input_bam, reference_genome, indiv_list, chromosomes, min_mapq=30,
                          min_len=100, max_threads=4):
        self.add_input_file_list("input_bam", "One bam file by individual/condition", default=input_bam,
                                 file_format="bam", required=True)
        self.add_input_file("reference_genome", "On which genome should the reads being aligned on",
                            default=reference_genome, required=True)
        self.add_parameter_list("indiv_list", "String list of studied individuals name", default=indiv_list, type=str,
                                required=True)
        self.add_parameter_list("chromosomes", "List of chromosomes to study", default=chromosomes)
        self.add_parameter("min_mapq", "min. paired-end mapping quality", type=int, default=min_mapq)
        self.add_parameter("min_len", "Minimum length of a variant", type=int, default=min_len)
        self.add_parameter("max_threads", "Max number of threads", default=max_threads, type=int)
        self.add_output_file_list("output_file_DEL", "Delly output file", pattern="delly_chr_{basename}_DEL.bcf",
                                  items=chromosomes)
        self.add_output_file_list("output_file_INV", "Delly output file", pattern="delly_chr_{basename}_INV.bcf",
                                  items=chromosomes)
        self.add_output_file_list("output_file_DUP", "Delly output file", pattern="delly_chr_{basename}_DUP.bcf",
                                  items=chromosomes)
        self.add_output_file_list("stdout", "Standard output", pattern='delly_chr_{basename}.stdout', items=chromosomes)
        self.add_output_file_list("stderr", "Standard error", pattern='delly_chr_{basename}.stderr', items=chromosomes)

    def process(self):
        samtools = self.get_exec_path("samtools")
        bcftools = self.get_exec_path("bcftools")
        delly = self.get_exec_path("delly")
        wf_dir = str(Path(os.path.dirname(inspect.getfile(self.__class__))).parent)
        i=0
        for chromosome in self.chromosomes:
            tmp_dir = self.get_temporary_file("")
            os.mkdir(tmp_dir)
            cmd = "export OMP_NUM_THREADS=" + str(min(self.max_threads, len(self.input_bam))) + ";\n"
            bams_chr = []
            for bam_file in self.input_bam:
                tmp_bam = os.path.join(tmp_dir,
                                       os.path.basename(bam_file).replace(".bam", ".chr_" + chromosome + ".bam"))
                cmd += samtools + " view -bh " + bam_file + " " + chromosome + " | " + samtools + \
                      " sort -o " + tmp_bam + " >> $1 2>> $2;\n"
                cmd += "samtools index " + tmp_bam + ";\n"
                bams_chr.append(tmp_bam)
            for svtype in ["DEL", "INV", "DUP"]:
                output_f = self.output_directory + os.path.sep + "delly_chr_" + chromosome + "_" + svtype
                # Detect SV:
                cmd += delly + " call -g " + self.reference_genome + " -t " + svtype + " -q " + \
                    str(self.min_mapq) + " -o " + output_f + "_raw.bcf " + " ".join(bams_chr) + " >> $1 2>> $2;\n"
                cmd += "if [ -f " + output_f + "_raw.bcf ]; then\n"
                # Filter SV:
                # cmd += delly + " filter -f germline -m " + str(self.min_len) + " -g " + self.reference_genome + \
                #     " -t " + svtype + " -o " + output_f + ".bcf" + " " + output_f + "_raw.bcf >> $1 2>> $2;\n"
                cmd += "mv " + output_f + "_raw.bcf " + output_f + ".bcf;\n"
                cmd += "else\n"
                # Generate empty file (will be overriden if SV founds):
                cmd += bcftools + " convert -O b " + " -o " + output_f + ".bcf " + \
                       os.path.join(wf_dir, "lib", "svreader", "resources", "template.vcf") + ";\n"
                cmd += "fi;\n"

            cmd += "rm -rf " + tmp_dir + ";\n"

            self.add_shell_execution(cmd,
                                     cmd_format='{EXE} {OUT} {IN}', inputs=self.input_bam,
                                     outputs=[self.stdout[i], self.stderr[i], self.output_file_DEL[i],
                                              self.output_file_INV[i], self.output_file_DUP[i]],
                                     includes=[self.reference_genome])
            i += 1
