import os
import sys
import re

from jflow.component import Component


def convert_svtool_to_vcf(inputfile, reference, toolname, svtype, stdout, stderr, outputfile, filter_bed=None,
                          minlen=100, maxlen=5000000, max_overlap=0.4, doFilter=True):

    import pysam
    from svreader.genomestrip import GenomeSTRIPReader, GenomeSTRIPWriter
    from svreader.delly import DellyReader, DellyWriter
    from svreader.lumpy import LumpyReader, LumpyWriter
    from svreader.breakdancer import BreakDancerReader, BreakDancerWriter
    from svreader.cnvnator import CNVnatorReader, CNVnatorWriter
    from svreader.pindel import PindelReader, PindelWriter
    from svrunner_utils import eprint, get_contigs, sorting

    with open(stdout, "w") as sout, open(stderr, "w") as serr:
        sys.stdout = sout
        sys.stderr = serr

        tool_to_reader = {"pindel": PindelReader, "breakdancer": BreakDancerReader, "delly": DellyReader,
                          "lumpy": LumpyReader, "genomestrip": GenomeSTRIPReader, "cnvnator": CNVnatorReader}
        tool_to_writer = {"pindel": PindelWriter, "breakdancer": BreakDancerWriter, "delly": DellyWriter,
                          "lumpy": LumpyWriter, "genomestrip": GenomeSTRIPWriter, "cnvnator": CNVnatorWriter}

        # The reference handle
        reference_handle = pysam.Fastafile(reference) if reference else None
        reference_contigs = get_contigs(reference)

        SVReader = tool_to_reader[toolname](inputfile, reference_handle=reference_handle, svs_to_report=[svtype])
        SVWriter = tool_to_writer[toolname](outputfile, reference_contigs, SVReader)

        # The records
        eprint("Now reading the records")
        records = []
        for record in SVReader:
            records.append(record)
        eprint("%d records" % (len(records)))

        # Now filtering the records
        if doFilter:
            eprint("Now filtering")
            records = SVReader.filtering(records, max_overlap, minlen, maxlen, filter_bed)

        # Sorting the vcf records
        records = sorting(records, reference_contigs)

        eprint("Now writing the records")
        for record in records:
            SVWriter.write(record)
        SVWriter.close()
        eprint("%d records" % (len(records)))

        pysam.tabix_index(outputfile, force=True, preset='vcf')


class ParseResults(Component):

    def define_parameters(self, results, reference_fasta):
        self.results = results
        self.add_input_file("reference_fasta", "Reference fasta file", default=reference_fasta)
        self.add_output_directory("outputs", "Output directory", dirname="parsed")
        self.add_output_file("stdout", "Standard output", filename="parsing.stdout")
        self.add_output_file("stderr", "Standard error", filename="parsing.stderr")

    def process(self):
        one_file = ["breakdancer", "lumpy", "genomestrip", "cnvnator"]
        by_type = ["delly", "pindel"]
        for tool in self.results.keys():
            if tool in self.results:
                output_dir = self.outputs + os.path.sep + tool
                self.add_shell_execution("mkdir -p $1", cmd_format="{EXE} {OUT}", outputs=output_dir, local=True)
                outputs = []
                for svtype in ["DEL", "INV"]:
                    if svtype == "DEL" or tool not in ["genomestrip"]:
                        if tool in one_file:
                            outputs = self.results[tool]
                        elif tool in by_type:
                            outputs = self.results[tool][svtype]
                        for output in outputs:
                            base = os.path.basename(output)
                            basename = base.split(".")[0]
                            if svtype == "DEL":
                                outbase = re.sub(r"_D(EL)?$", "", basename) + "_DEL.parsed.vcf.gz"
                            else:
                                outbase = "".join([basename.replace("_INV", ""), "_", svtype, ".parsed.vcf.gz"])
                            out_vcf = os.path.join(output_dir, outbase)
                            self.add_python_execution(convert_svtool_to_vcf, cmd_format="{EXE} {IN} {ARG} {OUT}",
                                                      inputs=[output, self.reference_fasta],
                                                      arguments=[tool, svtype, self.stdout, self.stderr],
                                                      outputs=[out_vcf], includes=output_dir)
