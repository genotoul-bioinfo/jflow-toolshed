import os
from jflow.component import Component


def create_bamlist(bamlist_file, *bams):
    with open(bamlist_file, "w") as bam_list_w:
        for ind in bams:
            bam_list_w.write(ind + "\n")


class GenomestripPreprocess(Component):

    def define_parameters(self, reference_fasta, bam_files, gender_map):
        self.add_input_file("reference_fasta", "Reference fasta file", file_format="fasta", default=reference_fasta)
        self.add_input_file("gender_map", "Gender map for each individual", default=gender_map)
        self.add_input_file_list("bam_files", "Bam alignment files", default=bam_files)
        self.add_output_directory("results", "Results files", dirname="results")
        self.add_output_file("stdout", "Standard output", filename="gstrip_preprocess.stdout")
        self.add_output_file("stderr", "Standard error", filename="gstrip_preprocess.stderr")

    def process(self):
        tmp_dir = self.get_temporary_file("")
        outp_dir = self.output_directory
        bam_list = outp_dir + os.path.sep + "bamlist.list"
        res_dir = self.results
        reference_prefix = self.reference_fasta.replace(".fasta", "")
        self.add_python_execution(create_bamlist, cmd_format="{EXE} {OUT} {IN}", outputs=[bam_list],
                                  inputs=[self.bam_files], local=True)
        sv_dir = self.get_exec_path("svtoolkit_dir")
        cmd = self.get_exec_path("run_genomestrip_preprocess.sh") + " \"" + tmp_dir + "\" \"" + sv_dir + "\" \"" + \
              bam_list + "\" \"" + res_dir + "\" \"" + reference_prefix + "\" \"" + self.gender_map + "\" > \"" + \
              self.stdout + "\" 2>> \"" + self.stderr + "\""

        self.add_shell_execution(cmd, cmd_format="{EXE} {IN} {OUT}",
                                 inputs=[self.reference_fasta, self.gender_map, self.bam_files],
                                 outputs=[self.results, self.stdout, self.stderr],
                                 includes=[bam_list])
