import os
import sys
from jflow.component import Component


def concatenate(outputfile, stdout, stderr, bcftools, vcf_sort, tabix, bgzip, tempdir, reference, *vcf_files_in):
    import shutil
    from subprocess import call
    from svrunner_utils import addContigInfosTovcf
    os.mkdir(tempdir)

    vcf_files = []
    for vcf_file in vcf_files_in:
        if os.path.isfile(vcf_file):
            vcf_files.append(vcf_file)

    if not os.path.isdir(tempdir):
        os.makedirs(tempdir)

    vcfFileslist = tempdir + "/" + "vcffile.list"
    with open(vcfFileslist, "w") as outfh:
        for gfile in vcf_files:
            vcf_sample_dropped_file = tempdir + "/" + os.path.basename(gfile)
            cmd = bcftools + " view --drop-genotypes " + gfile + " | " + vcf_sort + " -c 2>> " + stderr + \
                " | bgzip -c >" + vcf_sample_dropped_file + " 2>> " + stderr
            cmd += " && " + tabix + " -p vcf " + vcf_sample_dropped_file + " >> " + stdout + " 2>> " + stderr
            call(cmd, shell=True)
            outfh.write(vcf_sample_dropped_file + "\n")
    temp_outputfile = tempdir + "/" + "merge.vcf"
    cmd = bcftools + " concat -f " + vcfFileslist + " -a | vcf-sort -c >" + temp_outputfile + " 2>> " + stderr
    call(cmd, shell=True)
    # Correct contig infos here using bcftools annotate
    addContigInfosTovcf(temp_outputfile, outputfile, reference)
    shutil.rmtree(tempdir)


class MergeResults(Component):

    svtypes = ["DEL", "INV"]

    def define_parameters(self, vcf_dir, tools, reference, chromosomes):
        self.add_input_directory("vcf_dir", "Folder containing parsed vcf files", default=vcf_dir)
        self.add_input_file("reference", "Reference genome fasta file", file_format="fasta", default=reference)
        self.add_parameter_list("chromosomes", "Chromosomes list", default=chromosomes)
        self.add_parameter_list("tools", "list of tools launched", default=tools)
        for svtype in self.svtypes:
            self.add_output_file_list("merged_vcf_" + svtype, "Merged vcf files",
                                      pattern="merged_chr_{basename}_" + svtype + ".vcf.gz", items=chromosomes)
            self.add_output_file_list("stdout_" + svtype, "Standard output", pattern="merged_chr_{basename}_" + svtype + ".stdout",
                                      items=chromosomes)
            self.add_output_file_list("stderr_" + svtype, "Standard error", pattern="merged_chr_{basename}_" + svtype + ".stderr",
                                      items=chromosomes)

    def process(self):
        i = 0
        for chromosome in self.chromosomes:
            tmp_dir = self.get_temporary_file("")
            arguments = [self.get_exec_path("bcftools"), self.get_exec_path("vcf-sort"), self.get_exec_path("tabix"),
                         self.get_exec_path("bgzip"), tmp_dir]
            for svtype in self.svtypes:
                vcf_files = []
                for tool in self.tools:
                    if svtype == "DEL" or tool != "genomestrip":
                        vcf_tool_dir = os.path.join(self.vcf_dir, tool)
                        vcf_file_name = tool + "_chr_" + chromosome + "_" + svtype + ".parsed.vcf.gz"
                        vcf_file = os.path.join(vcf_tool_dir, vcf_file_name)
                        vcf_files.append(vcf_file)
                self.add_python_execution(concatenate, cmd_format="{EXE} {OUT} {ARG} {IN}",
                                          inputs=[self.reference, vcf_files], arguments=arguments,
                                          outputs=[self.__getattribute__("merged_vcf_" + svtype)[i],
                                                   self.__getattribute__("stdout_" + svtype)[i],
                                                   self.__getattribute__("stderr_" + svtype)[i]])
            i += 1
