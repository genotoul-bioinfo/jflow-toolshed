import os
import re
from jflow.component import Component


def create_bamlist(bamlist_file, *bams):
    with open(bamlist_file, "w") as bam_list_w:
        for ind in bams:
            bam_list_w.write(ind + "\n")


def add_depthratio(input_vcf, output_vcf, auxprefix):
    from svreader.vcfwrapper import VCFReader, VCFWriter
    from svfilter import add_depth_ratio_info_sv, read_auxfiles

    expected, counts = read_auxfiles(auxprefix)
    samples = list(expected.columns.values)

    reader = VCFReader(input_vcf, "genotype")
    # Adding DEPTHRATIO info (equivalent to GSDEPTHRATIO)
    reader.addInfo("DEPTHRATIO", 1, "Float", "Read depth ratio between cases and controls")
    # Adding  DEPTHCALLTHRESHOLD (equivalent to GSDEPTHCALLTHRESHOLD)
    reader.addInfo("DEPTHCALLTHRESHOLD", 1, "Float", "Read depth ratio between cases and controls")
    vcf_out = VCFWriter(output_vcf, reader)
    for record in reader:
        add_depth_ratio_info_sv(record, samples, expected, counts)
        vcf_out.write(record)
    vcf_out.close()


class Genotype(Component):

    def define_parameters(self, merged_vcf, bam_files, gendermap, reference, svtype, chromosomes, preprocess_dir,
                          overlapcutoff=0.5, threads=4):
        self.add_input_file_list("merged_vcf", "Merged VCF files", default=merged_vcf, file_format="vcf")
        self.add_input_file_list("bam_files", "Bam alignment files", file_format="bam", default=bam_files)
        self.add_input_file("gendermap", "Gender map file", file_format="txt", default=gendermap)
        self.add_input_directory("gstrip_preprocess", "GenomeStrip metadata folder", default=preprocess_dir)
        self.add_input_file("reference", "Reference genome fasta file (with other files for genomeSTRIP)",
                            file_format="fasta", default=reference)
        self.add_parameter("svtype", "Type of SV", default=svtype)
        self.add_parameter("threads", "Number of threads", default=threads, type=int)
        self.add_parameter("overlapcutoff", "cutoff for reciprocal overlap", default=overlapcutoff, type=float)
        self.add_output_file_list("output", "Genotype output files", pattern="genotype_chr_{basename}_" + svtype +
                                                                             ".vcf.gz",
                                  items=chromosomes)
        self.add_output_file_list("stdout", "Standard output", pattern="genotype_chr_{basename}_" + svtype + ".stdout",
                                  items=chromosomes)
        self.add_output_file_list("stderr", "Standard output", pattern="genotype_chr_{basename}_" + svtype + ".stderr",
                                  items=chromosomes)

    def process(self):
        for merged_vcf in self.merged_vcf:
            vcf_name = os.path.basename(merged_vcf)
            chromosome = re.match("merged_chr_(.+)_" + self.svtype + "\.vcf\.gz", vcf_name).group(1)
            output_base = os.path.join(self.output_directory, "_".join(["genotype_chr", chromosome, self.svtype]))
            final_output = output_base + ".vcf.gz"
            stdout = output_base + ".stdout"
            stderr = output_base + ".stderr"
            tmp_dir = self.get_temporary_file("")
            os.mkdir(tmp_dir)
            metadata = os.path.join(self.gstrip_preprocess, "metadata")
            bamlist_file = os.path.join(tmp_dir, "bamFiles.list")
            sv_dir = self.get_exec_path("svtoolkit_dir")
            merged_vcf_prefix = vcf_name.replace(".vcf.gz", "")
            genotypefile = os.path.join(tmp_dir, merged_vcf_prefix + "_genotype.vcf.gz")
            auxprefix = os.path.join(tmp_dir, merged_vcf_prefix + "_genotype.genotypes")
            reportdir = os.path.join(self.output_directory, "reports" + os.path.sep + chromosome)
            self.add_python_execution(create_bamlist, cmd_format="{EXE} {OUT} {IN}", outputs=[bamlist_file],
                                      inputs=[self.bam_files], local=True)
            cmd = "cd " + tmp_dir + ";\n"
            cmd += " ".join([self.get_exec_path("gstrip_genotyper.sh"), bamlist_file, chromosome, self.reference,
                             metadata, tmp_dir, self.gendermap, merged_vcf, genotypefile, sv_dir, str(self.threads)]) +\
                " > " + stdout + " 2>> " + stderr + ";\n"
            cmd += " ".join([self.get_exec_path("gstrip_cnvannotator.sh"), genotypefile, final_output, self.reference,
                             str(self.overlapcutoff), auxprefix, reportdir, sv_dir]) + " >> " + stdout + " 2>> " + \
                   stderr + ";\n"
            self.add_shell_execution(cmd, cmd_format="{EXE} {IN} {OUT}", inputs=[merged_vcf, self.gstrip_preprocess],
                                     outputs=[final_output, stdout, stderr], includes=[self.reference, bamlist_file])
            # self.add_shell_execution(cmd, cmd_format="{EXE} {IN} {OUT}", inputs=[merged_vcf, self.gstrip_preprocess],
            #                          outputs=[output, stdout, stderr], includes=[self.reference, bamlist_file])
            # self.add_python_execution(add_depthratio, cmd_format="{EXE} {IN} {OUT} {ARG}", inputs=output,
            #                           outputs=final_output, arguments=[auxprefix])

