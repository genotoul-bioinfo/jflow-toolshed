import os

from jflow.component import Component


class Lumpy(Component):

    def define_parameters(self, input_bam, chromosomes, threads=4):
        self.add_input_file_list("input_bam", "One bam file by individual/condition", default=input_bam,
                                 file_format="bam", required=True)
        self.add_parameter_list("chromosomes", "List of chromosomes", default=chromosomes, required=True)
        self.add_output_file_list("output_file", "The vcf result file", pattern="lumpy_chr_{basename}.vcf.gz",
                                  items=chromosomes)
        self.add_output_file_list("stdout", "Standard output", pattern='lumpy_chr_{basename}.stdout', items=chromosomes)
        self.add_output_file_list("stderr", "Standard error", pattern='lumpy_chr_{basename}.stderr', items=chromosomes)
        self.add_parameter("threads", "Number of threads", type=int, default=threads)

    def process(self):
        samtools = self.get_exec_path("samtools")
        lumpyexpress = self.get_exec_path("lumpyexpress")
        lumpy_extract_splitters = self.get_exec_path("lumpy_extract_splitters")
        lumpy_get_coverage = self.get_exec_path("lumpy_get_coverage")
        lumpy_get_exclude_regions = self.get_exec_path("lumpy_get_exclude_regions")
        svtyper = self.get_exec_path("svtyper")
        bgzip = self.get_exec_path("bgzip")
        tabix = self.get_exec_path("tabix")
        vcf_concat = self.get_exec_path("vcf-concat")
        vcf_sort = self.get_exec_path("vcf-sort")
        i = 0
        for chromosome in self.chromosomes:
            tmp_dir = self.get_temporary_file("")
            cmd = "mkdir " + tmp_dir + ";"
            bams_chr = []
            bams_splitters = []
            bams_discordants = []
            for bam_file in self.input_bam:
                tmp_bam = tmp_dir + os.path.sep + \
                          bam_file[bam_file.rfind("/")+1:].replace(".bam", ".chr_" + chromosome + ".bam")
                cmd += samtools + " view -bh " + bam_file + " " + chromosome + " | " + samtools + \
                       " sort -o " + tmp_bam + " >> $1 2>> $2;\n"
                cmd += samtools + " index " + tmp_bam + ";\n"
                bams_chr.append(tmp_bam)

                # Extract splitters:
                bam_splitter = tmp_bam.replace(".bam", ".splitters.bam")
                bams_splitters.append(bam_splitter)
                cmd += samtools + " view -h " + tmp_bam + " | " + lumpy_extract_splitters + " -i stdin | " + samtools +\
                    " view -Sb - | " + samtools + " sort -o " + bam_splitter + ";\n"
                cmd += samtools + " index " + bam_splitter + ";\n"

                # Extract discordants:
                bam_discordant = tmp_bam.replace(".bam", ".discordants.bam")
                bams_discordants.append(bam_discordant)
                cmd += samtools + " view -b -F 1294 " + tmp_bam + " | " + samtools + " sort -o " + bam_discordant + ";\n"
                cmd += samtools + " index " + bam_discordant + ";\n"

            cmd += "cd " + tmp_dir + ";\n"

            cmd += self.get_exec_path("run_lumpy.sh") + " \"" + \
                "\" \"".join([" ".join(bams_chr), " ".join(bams_discordants), " ".join(bams_splitters),
                              self.output_file[i].replace(".vcf.gz", ""), lumpy_get_coverage,
                              lumpy_get_exclude_regions, lumpyexpress, svtyper, vcf_concat, vcf_sort, bgzip, tabix,
                              str(self.threads), tmp_dir, chromosome, self.get_exec_path("vawk")]) + \
                "\" >> $1 2>> $2;\n"

            cmd += "cd; rm -rf " + tmp_dir + ";\n"

            self.add_shell_execution(cmd, cmd_format="{EXE} {OUT} {IN}", inputs=self.input_bam,
                                     outputs=[self.stdout[i], self.stderr[i], self.output_file[i]])

            i += 1
