import os

from jflow.component import Component


def build_cfg_file(bam2cfg, min_mapq, cfg_file, *bam_files):
    # Bam2Cfg
    cmd = bam2cfg + " -q " + min_mapq + " -g -h -v 2 " + " ".join(bam_files) + " > " + cfg_file
    os.system(cmd)


def run_breakdancer(breakdancer_max, min_sup, min_mapq, max_len, chromosome, output_file, stderr, cfg_file):

    # BreakDancer_max
    cmd = breakdancer_max + " -h -r " + min_sup + " -m " + max_len + " -q " + min_mapq
    if chromosome != "genome":
        cmd += " -o" + chromosome
    cmd += " " + cfg_file + " 2>> " + stderr + " | gzip -c > " + \
        output_file
    os.system(cmd)


class BreakDancer(Component):

    def define_parameters(self, input_bam, min_sup, min_mapq, max_len, names, chromosomes):
        self.add_input_file_list("input_bam", "One bam file by individual/condition", default=input_bam,
                                 file_format="bam", required=True)
        self.add_output_file_list("output_file", "Breakdancer output file", pattern="breakdancer_chr_{BASE}.ctx.gz",
                                  items=chromosomes)
        self.add_output_file_list("stderr", "Standard error", pattern='breakdancer_chr_{BASE}.stderr',
                                  items=chromosomes)
        self.add_parameter("min_sup", "Minimum number of read supporting a SV", type=int, default=min_sup)
        self.add_parameter("max_len", "Maximum length of a variant (8 500 000)", type=int, default=max_len)
        self.add_parameter("min_mapq", "min. paired-end mapping quality", type=int, default=min_mapq)
        self.add_parameter_list("chromosomes", "Chromosomes to study, comma separated", default=chromosomes)

    def process(self):
        cfg_file = self.get_temporary_file(".bd.cfg")
        self.add_python_execution(build_cfg_file, outputs=cfg_file, inputs=self.input_bam,
                                  arguments=[self.get_exec_path("bam2cfg"), str(self.min_mapq)],
                                  cmd_format="{EXE} {ARG} {OUT} {IN}")
        i = 0
        for chromosome in self.chromosomes:
            self.add_python_execution(run_breakdancer, inputs=cfg_file,
                                      outputs=[self.output_file[i], self.stderr[i]],
                                      arguments=[self.get_exec_path("breakdancer-max"),
                                                 str(self.min_sup), str(self.min_mapq), str(self.max_len),
                                                 chromosome], cmd_format="{EXE} {ARG} {OUT} {IN}")
            i += 1

