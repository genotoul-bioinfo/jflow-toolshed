import re
from jflow.component import Component


def filtering(outputfile, output_filtered, output_excluded, overlap_cutoff, min_sv_length, inputfile):
    """ Filtering the candidate CNVs according to the following criteria
          - non duplicate sites
          - variant sites
          - call rate > 0.8
          - at least one variant (homozygous or heterozygote) has a genotype quality > 20 
          - the variant is not everywhere heterozygote or homozygote (use NONVARIANTSCORE in both cases)
          - sv with no overlap with CNVR filtered out by genomeSTRIP
    """

    from svreader.vcfwrapper import VCFReader, VCFWriter
    from svrunner_utils import eprint
    from svfilter import AddDepthRatioInfo, GenomeSTRIPLikefiltering

    # Reading the vcf file
    SVSet = []
    eprint(" Reading file %s" % (inputfile))
    reader = VCFReader(inputfile, "merge")
    for record in reader:
        SVSet.append(record)
    eprint("Working with " + str(len(SVSet)) + " records")

    # Adding specific filters
    reader.addFilter("CALLRATE", "Call rate <0.75")
    reader.addFilter("POLYMORPH", "All samples have the same genotype")
    reader.addFilter("ALIGNLENGTH", "GSELENGTH < " + min_sv_length)
    reader.addFilter("DUPLICATE", "GSDUPLICATESCORE>0")
    reader.addFilter("NONVARIANT", "GSNONVARSCORE>13")
    reader.addFilter("DDEPTH", "DEPTHRATIO>0.8 (genomestrip-like)")
    reader.addFilter("CCOVERAGE", "DEPTHCALLTHRESHOLD>1 (genomestrip-like)")
    reader.addFilter("CLUSTERSEP", "GSM1<=-0.5 || GSM1>=2 (genomestrip best practice)")

    # Now genomeSTRIP inspired variant filtration
    if reader.numSamples() > 0:
        GenomeSTRIPLikefiltering(SVSet)

    VCFout = VCFWriter(outputfile, reader)
    VCFout_f = VCFWriter(output_filtered, reader)
    VCFout_e = VCFWriter(output_excluded, reader)
    for record in sorted(SVSet, key=lambda k: k.start):
        # Remove bad genomestrip flags:
        if "DEPTH" in record.filter.keys():
            del record.filter["DEPTH"]
        if "DEPTHPVAL" in record.filter.keys():
            del record.filter["DEPTHPVAL"]
        # Write records:
        VCFout.write(record)
        if len(record.filter) == 0 or (len(record.filter) == 1 and "PASS" in record.filter):
            VCFout_f.write(record)
        else:
            VCFout_e.write(record)
    VCFout.close()
    VCFout_f.close()
    VCFout_e.close()


class Filter(Component):

    def define_parameters(self, genotype_vcf, svtype, chromosomes, overlap_cutoff, min_len):
        self.add_input_file_list("genotype_vcf", "Genotype vcf for each chromosome", default=genotype_vcf)
        self.add_parameter("min_len", "Minimum length of a variant", default=min_len, type=int)
        self.add_parameter_list("chromosomes", "Chromosomes to study", default=chromosomes)
        self.add_parameter("overlapcutoff", "cutoff for reciprocal overlap", default=overlap_cutoff, type=float)
        self.add_output_file_list("output_vcf", "Output vcf files for each chromosomes",
                                  pattern="all_tagged_chr_{basename}_" + svtype + ".vcf.gz", items=chromosomes)
        self.add_output_file_list("output_filtered", "Output vcf files filtered for each chromosome",
                                  pattern="filtered_chr_{basename}_" + svtype + ".vcf.gz", items=chromosomes)
        self.add_output_file_list("output_excluded", "Output vcf files filtered for each chromosome",
                                  pattern="excluded_chr_{basename}_" + svtype + ".vcf.gz", items=chromosomes)

    @staticmethod
    def sort_chr(string):
        chrom = re.match(r"^.*chr_(.+).vcf.gz$", string.replace("_DEL.vcf.gz", ".vcf.gz"))  # TODO: add other SVs
        return chrom.group(1)

    def process(self):
        genotype_vcfs = sorted(self.genotype_vcf, key=lambda x: self.sort_chr(x))
        output_vcfs = sorted(self.output_vcf, key=lambda x: self.sort_chr(x))
        output_filtered = sorted(self.output_filtered, key=lambda x: self.sort_chr(x))
        output_excluded = sorted(self.output_excluded, key=lambda x: self.sort_chr(x))
        self.add_python_execution(filtering, cmd_format="{EXE} {OUT} {ARG} {IN}",
                                  arguments=[self.overlapcutoff, str(self.min_len)],
                                  inputs=[genotype_vcfs],
                                  outputs=[output_vcfs, output_filtered, output_excluded], map=True)
