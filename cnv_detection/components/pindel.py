import os

from jflow.component import Component


def run_pindel(output_file_D, output_file_SI, output_file_TD, output_file_INV, stdout, stderr,
               pindel, samtools, chromosome, min_mapq, min_sup, min_perfect_match_around_bp, max_range_index, threads,
               reference_genome, cfg_file_path, *bam_and_names):

    indiv_names = bam_and_names[:int(len(bam_and_names)/2)]
    bam_files = bam_and_names[int(len(bam_and_names)/2):]

    # Samtools faidx:
    cmd = samtools + " faidx " + reference_genome + " 2>> " + stderr
    os.system(cmd)

    # Samtolls index bam:
    # cmd = samtools + " index " + input_bam + " 2>> " + stderr
    # os.system(cmd)

    # Pindel:

    with open(cfg_file_path, 'w') as config_file:
        for bam,name in zip(bam_files, indiv_names):
            # Estimate mean insert:
            cmd = samtools + " view " + bam + \
                  " | head -100000 | awk '{if ($9 > 0 && $9 < 10000) {S+=$9; T+=1}}END{print S/T}' 2>> " + stderr
            mean_insert = str(int(float(os.popen(cmd).read().replace("\n", ""))))
            line = [bam, mean_insert, name]
            true_line = "\t".join(line) + "\n"
            config_file.write(true_line)
    output_prefix = output_file_D.replace("_D.gz", "")
    cmd = pindel + " -f " + reference_genome + " -i " + cfg_file_path + " -o " + output_prefix + " -T " + threads + \
        " -A " + min_mapq + " -M " + min_sup + " -m " + min_perfect_match_around_bp + " -x " + \
        max_range_index + " -c " + chromosome + " > " + stdout + " 2>> " + stderr
    os.system(cmd)

    cmd = "gzip " + output_prefix + "_*"
    os.system(cmd)

    # Concatenate:
    # outputs_list = []
    # for type in ["D", "SI", "TD", "INV"]:
    #     outputs_list.append(output_prefix + "*_" + type)
    # cmd = "cat " + " ".join(outputs_list) + " | grep -P \"^[0-9]\" | sort -k 8,8 -k10,10n > " + output_file
    # os.system(cmd)


class Pindel(Component):

    def define_parameters(self, input_bam, reference_genome, output_file, indiv_list, chromosomes,
                          min_mapq=20, min_sup=3, min_perfect_match_around_bp=20, max_range_index=6, threads=4):
        self.add_input_file_list("input_bam", "One bam file by individual/condition", default=input_bam,
                                 file_format="bam", required=True)
        self.add_input_file("reference_genome", "Animal ref file", default=reference_genome, file_format="fasta",
                            required=True)
        self.add_output_file_list("output_file_D", "Pindel concatenated output files [D]",
                                  pattern="pindel_chr_{basename}_D.gz", items=chromosomes)
        self.add_output_file_list("output_file_SI", "Pindel concatenated output files [SI]",
                                  pattern="pindel_chr_{basename}_SI.gz", items=chromosomes)
        self.add_output_file_list("output_file_TD", "Pindel concatenated output files [TD]",
                                  pattern="pindel_chr_{basename}_TD.gz", items=chromosomes)
        self.add_output_file_list("output_file_INV", "Pindel concatenated output files [INV]",
                                  pattern="pindel_chr_{basename}_INV.gz", items=chromosomes)
        self.add_output_file_list("stdout", "Standard output", pattern='pindel_chr_{basename}.stdout',
                                  items=chromosomes)
        self.add_output_file_list("stderr", "Standard error", pattern='pindel_chr_{basename}.stderr', items=chromosomes)
        self.add_parameter_list("indiv_list", "String list of studied individuals name", default=indiv_list, type=str,
                                required=True)
        self.add_parameter_list("chromosomes", "List of chromosomes", default=chromosomes, required=True)
        self.add_parameter("min_mapq", "min. paired-end mapping quality", type=int, default=min_mapq)
        self.add_parameter("min_sup", "Minimum number of read supporting a SV (3)",
                           type=int, default=min_sup)
        self.add_parameter("min_perfect_match_around_bp", "at the point where the read is split into two, there should \
                            at least be this number of perfectly matching bases between read and reference", type=int,
                           default=min_perfect_match_around_bp)
        self.add_parameter("max_range_index", "the maximum size of structural variations to be detected; the higher" +
                           "this number, the greater the number of SVs reported, but the " +
                           "computational cost and memory requirements increase, as does the rate" +
                           "of false positives. 1=128, 2=512, 3=2,048, 4=8,092, 5=32,368, " +
                           "6=129,472, 7=517,888, 8=2,071,552, 9=8,286,208. (maximum 9, default 2)", type=int,
                           default=max_range_index)
        self.add_parameter("threads", "Number of threads to use", default=threads, type=int)

    def process(self):
        for i in range(0, len(self.chromosomes)):
            cfg_file_path = self.get_temporary_file(".pdl.cfg")
            self.add_python_execution(run_pindel, inputs=self.input_bam,
                                      outputs=[self.output_file_D[i], self.output_file_SI[i], self.output_file_TD[i],
                                               self.output_file_INV[i], self.stdout[i], self.stderr[i]],
                                      arguments=[self.get_exec_path("pindel"), self.get_exec_path("samtools"),
                                                 self.chromosomes[i], str(self.min_mapq),
                                                 str(self.min_sup), str(self.min_perfect_match_around_bp),
                                                 str(self.max_range_index), str(self.threads), self.reference_genome,
                                                 cfg_file_path, self.indiv_list],
                                      includes=[self.reference_genome],
                                      cmd_format="{EXE} {OUT} {ARG} {IN}")
