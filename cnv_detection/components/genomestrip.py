import os

from jflow.component import Component


def create_bamlist(bamlist_file, *bams):
    with open(bamlist_file, "w") as bam_list_w:
        for ind in bams:
            bam_list_w.write(ind + "\n")


class Genomestrip(Component):

    def define_parameters(self, bam_files, reference_genome, preprocess_dir, chromosomes, gendermap):
        self.add_input_file_list("bam_files", "Bam alignment files", default=bam_files)
        self.add_input_file("reference_fasta", "Reference genom file", default=reference_genome)
        self.add_input_directory("gstrip_preprocess", "GenomeStrip metadata folder", default=preprocess_dir)
        self.add_input_file("gendermap", "Gender map file for each individual", default=gendermap)
        self.add_parameter_list("chromosomes", "List of chromosomes to study", default=chromosomes)
        self.add_output_file_list("output_file", "Output result file", pattern="genomestrip_chr_{basename}.vcf.gz",
                                  items=chromosomes)
        self.add_output_file_list("stdout", "Standard output", pattern="genomestrip_chr_{basename}.stdout",
                                  items=chromosomes)
        self.add_output_file_list("stderr", "Standard error", pattern="genomestrip_chr_{basename}.stderr",
                                  items=chromosomes)

    def process(self):
        outp_dir = self.output_directory
        res_dir = outp_dir + os.path.sep + "run"
        bam_list = outp_dir + os.path.sep + "bamlist.list"
        reference_prefix = self.reference_fasta.replace(".fasta", "")
        self.add_python_execution(create_bamlist, cmd_format="{EXE} {OUT} {IN}", outputs=[bam_list],
                                  inputs=[self.bam_files], local=True)
        sv_dir = self.get_exec_path("svtoolkit_dir")

        for i in range(0, len(self.chromosomes)):
            tmp_dir = self.get_temporary_file("")
            cmd = self.get_exec_path("run_genomestrip.sh") + " \"" + tmp_dir + "\" \"" + sv_dir + "\" \"" + \
                bam_list + "\" \"" + res_dir + "\" \"" + reference_prefix + "\" \"" + self.gstrip_preprocess + \
                os.path.sep + "metadata\" \"" + self.output_file[i].replace(".gz", "") + "\" \"" + \
                self.chromosomes[i] + "\" \"" + self.gendermap + "\" > \"" + self.stdout[i] + "\" 2>> \"" + \
                self.stderr[i] + "\""

            self.add_shell_execution(cmd, cmd_format="{EXE} {IN} {OUT}",
                                     inputs=[self.reference_fasta, self.gstrip_preprocess, self.bam_files,
                                             self.gendermap],
                                     outputs=[self.output_file[i], self.stdout[i], self.stderr[i]],
                                     includes=[bam_list])
