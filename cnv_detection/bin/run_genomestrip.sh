#!/usr/bin/env bash

set -e

SV_TMPDIR=$1;
export SV_DIR=$2;
inputFile=$3;
runDir=$4;
reference_prefix=$5;
metadata=$6;
sites=$7 #Output file
chrom=$8
gendermap=$9

# For SVAltAlign, you must use the version of bwa compatible with Genome STRiP.
export PATH=${SV_DIR}/bwa:${PATH}
export LD_LIBRARY_PATH=${SV_DIR}/bwa:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=/SGE/ogs/lib/linux-x64:$LD_LIBRARY_PATH

mx="-Xmx4g"
classpath="${SV_DIR}/lib/SVToolkit.jar:${SV_DIR}/lib/gatk/GenomeAnalysisTK.jar:${SV_DIR}/lib/gatk/Queue.jar"

mkdir -p ${SV_TMPDIR}
mkdir -p ${runDir}/logs || exit 1

# Display version information.
java -cp ${classpath} ${mx} -jar ${SV_DIR}/lib/SVToolkit.jar

echo "-- Run Discovery for $chrom -- "
# Run discovery.
LC_ALL=C java -cp ${classpath} ${mx} \
    org.broadinstitute.gatk.queue.QCommandLine \
    -S ${SV_DIR}/qscript/SVDiscovery.q \
    -S ${SV_DIR}/qscript/SVQScript.q \
    -gatk ${SV_DIR}/lib/gatk/GenomeAnalysisTK.jar \
    --disableJobReport \
    -cp ${classpath} \
    -jobProject Capri \
    -jobRunner Drmaa \
    -gatkJobRunner Drmaa \
    -jobQueue workq \
    -jobNative '-l mem=24G -l h_vmem=24G' \
    -configFile ${SV_DIR}/conf/genstrip_parameters.txt \
    -tempDir ${SV_TMPDIR} \
    -R ${reference_prefix}.fasta \
    -genderMaskBedFile  ${reference_prefix}.gendermask.bed \
    -genomeMaskFile     ${reference_prefix}.svmask.fasta \
    -ploidyMapFile      ${reference_prefix}.ploidymap.txt \
    -genderMapFile      ${gendermap} \
    -runDirectory ${runDir} \
    -md ${metadata} \
    -disableGATKTraversal \
    -jobLogDir ${runDir}/logs \
    -L ${chrom} \
    -minimumSize 50 \
    -maximumSize 1000000 \
    -suppressVCFCommandLines \
    -I ${inputFile} \
    -O ${sites} \
    -run \
    || exit 1

rm -fr ${SV_TMPDIR}

rm -f ${sites}.gz
bgzip -f ${sites}
tabix -f -p vcf ${sites}.gz