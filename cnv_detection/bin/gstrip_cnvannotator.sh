#!/usr/bin/env bash

# If you adapt this script for your own use, you will need to set these two variables based on your environment.
# SV_DIR is the installation directory for SVToolkit - it must be an exported environment variable.
# SV_TMPDIR is a directory for writing temp files, which may be large if you have a large data set.

echo "---"
echo "Running gstrip_cnvannotator.sh $1 $2 $3 $4 $5 $6"
echo "Starting "`date +%Y-%m-%d`" at "`date +%H:%M:%S`
echo "---"

input=$1
output=$2
genome=$3
overlapcutoff=$4
auxprefix=$5
report=$6
export SV_DIR=$7

if [[ -z ${input} || -z ${output} || -z ${genome} || -z ${report} ]]
then
   usage
   echo "At least one argument is missing"
   exit 1
fi

if [ ! -d "${report}" ]
then
   mkdir -p ${report}
fi

# For SVAltAlign, you must use the version of bwa compatible with Genome STRiP.
export PATH=${SV_DIR}/bwa:${PATH}
export LD_LIBRARY_PATH=${SV_DIR}/bwa:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=/SGE/ogs/lib/linux-x64:$LD_LIBRARY_PATH

mx="-Xmx4g"
classpath="${SV_DIR}/lib/SVToolkit.jar:${SV_DIR}/lib/gatk/GenomeAnalysisTK.jar:${SV_DIR}/lib/gatk/Queue.jar"

echo "-- Run CNV annotator -- "
LC_ALL=C java -Xmx4g -cp ${classpath} ${mx}\
     org.broadinstitute.sv.main.SVAnnotator \
     -A Redundancy \
     -A GCContent \
     -A VariantsPerSample \
     -A NonVariant \
     -A ClusterSeparation \
     -R  ${genome} \
     -vcf ${input} \
     -comparisonFile ${input} \
     -duplicateOverlapThreshold ${overlapcutoff} \
     -auxFilePrefix ${auxprefix} \
     -O ${output} \
     -writeReport true \
     -writeSummary true \
     -reportDirectory ${report} || exit 1

echo "Script genomestrip.sh completed successfully "`date +%Y-%m-%d`" at "`date +%H:%M:%S`

