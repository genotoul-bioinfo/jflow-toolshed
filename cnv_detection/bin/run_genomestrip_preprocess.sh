#!/usr/bin/env bash

SV_TMPDIR=$1;
export SV_DIR=$2;
inputFile=$3;
runDir=$4;
reference_prefix=$5;
gendermap=$6;

# For SVAltAlign, you must use the version of bwa compatible with Genome STRiP.
export PATH=${SV_DIR}/bwa:${PATH}
export LD_LIBRARY_PATH=${SV_DIR}/bwa:${LD_LIBRARY_PATH}

mx="-Xmx4g"
classpath="${SV_DIR}/lib/SVToolkit.jar:${SV_DIR}/lib/gatk/GenomeAnalysisTK.jar:${SV_DIR}/lib/gatk/Queue.jar"

mkdir -p ${SV_TMPDIR}
mkdir -p ${runDir}/logs || exit 1
mkdir -p ${runDir}/metadata || exit 1

# Display version information.
java -cp ${classpath} ${mx} -jar ${SV_DIR}/lib/SVToolkit.jar

echo "-- Run Preprocessing -- "
# Run preprocessing.
export LD_LIBRARY_PATH=/SGE/ogs/lib/linux-x64:$LD_LIBRARY_PATH
classpath="${SV_DIR}/lib/SVToolkit.jar:${SV_DIR}/lib/gatk/GenomeAnalysisTK.jar:${SV_DIR}/lib/gatk/Queue.jar"
LC_ALL=C java -Xmx4g -cp ${classpath} \
      org.broadinstitute.gatk.queue.QCommandLine \
     -S ${SV_DIR}/qscript/SVPreprocess.q \
     -S ${SV_DIR}/qscript/SVQScript.q \
     -gatk ${SV_DIR}/lib/gatk/GenomeAnalysisTK.jar \
     -cp ${classpath} \
     -jobProject Capri \
     -jobRunner Drmaa \
     -gatkJobRunner Drmaa \
     -jobQueue workq \
     -jobNative '-l mem=32G -l h_vmem=32G' \
     -configFile ${SV_DIR}/conf/genstrip_parameters.txt \
     -tempDir ${SV_TMPDIR} \
     -R ${reference_prefix}.fasta \
     -I ${inputFile} \
     -runDirectory ${runDir} \
     -md ${runDir}/metadata \
     -jobLogDir ${runDir}/logs \
     -disableGATKTraversal \
     -bamFilesAreDisjoint true \
     -computeGCProfiles  true \
     -computeReadCounts  true \
     -copyNumberMaskFile ${reference_prefix}.gcmask.fasta \
     -genderMaskBedFile  ${reference_prefix}.gendermask.bed \
     -genomeMaskFile     ${reference_prefix}.svmask.fasta \
     -ploidyMapFile      ${reference_prefix}.ploidymap.txt \
     -readDepthMaskFile  ${reference_prefix}.rdmask.bed \
     -genderMapFile      ${gendermap} \
     -run \
     || exit 1

rm -fr ${SV_TMPDIR}