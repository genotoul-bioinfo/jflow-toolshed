#!/usr/bin/env bash

set -e

function join_by { local IFS="$1"; shift; echo "$*"; }

bamfiles=$1
bams=`join_by , $1`
discordants=`join_by , $2`
splitters=`join_by , $3`
outprefix=$4
get_coverages=$5
get_exclude_regions=$6
lumpyexpress=$7
svtyper=$8
vcf_concat=$9
vcf_sort=${10}
bgzip=${11}
tabix=${12}
ncpus=${13}
tmp_dir=${14}
chromosome=${15}
vawk=${16}



python ${get_coverages} ${bamfiles} > ${tmp_dir}/coverage.txt
cutoff=`cat ${tmp_dir}/coverage.txt | awk '{ split($4,a,":"); max=a[2]>max?a[2]:max}END{print int(6*max)}'`
python ${get_exclude_regions} ${cutoff} ${tmp_dir}/excluded_chr_${chromosome}.bed ${bamfiles}
exclude_option=""
if [ -s ${tmp_dir}/excluded_chr_${chromosome}.bed ]
then
exclude_option="-x ${tmp_dir}/excluded_chr_${chromosome}.bed"
fi
${lumpyexpress} ${exclude_option} -B ${bams} -D ${discordants} -S ${splitters} -o ${outprefix}.raw.vcf;
cat ${outprefix}.raw.vcf | ${vcf_sort} | ${bgzip} -c > ${outprefix}.raw.vcf.gz
${tabix} -f -p vcf ${outprefix}.raw.vcf.gz

rm -f ${outprefix}.raw.vcf

if [ ! -z "`zcat ${outprefix}.raw.vcf.gz | ${vawk} '{print $0}'`" ]; then # If vcf not empty
    # Now genotype the Structural variants (and parallelize the job)
    zcat ${outprefix}.raw.vcf.gz | head -n 10000 |  grep "^#" > ${tmp_dir}/lumpy_header
    zcat ${outprefix}.raw.vcf.gz | grep -v "^#"  |  split -l 100 - ${tmp_dir}/lumpy_temp_
    for i in ${tmp_dir}/lumpy_temp_*;
    do cat ${tmp_dir}/lumpy_header $i > $i.raw.vcf
    rm -f $i;done
    ls ${tmp_dir}/lumpy_temp_*.raw.vcf | parallel --no-notice -P ${ncpus} ${svtyper} -i {} -B ${bams} '>' {.}.output
    rm -f ${tmp_dir}/lumpy_temp_*.raw.vcf
    ${vcf_concat} ${tmp_dir}/lumpy_temp_*.raw.output | ${vcf_sort} > ${outprefix}.vcf
    ${bgzip} -f ${outprefix}.vcf
    ${tabix} -f -p vcf ${outprefix}.vcf.gz

else
    mv ${outprefix}.raw.vcf.gz ${outprefix}.vcf.gz
fi