#!/usr/bin/env bash

set -e

export SV_DIR=$1;
inputFile=$2;
outdir=$3;
outprefix=$4
reference_prefix=$5;
metadata=$6;
intervallist=$7  # = chromosome
gendermap=$8

if [[ -z ${SV_DIR} || -z ${inputFile} || -z ${outdir} || -z ${outprefix} || -z ${reference_prefix} || -z ${metadata} \
      || -z ${intervallist} || -z ${gendermap} ]]
then
   echo "At least one argument is missing"
   exit 1
fi

if [ ! -d "${metadata}" ] ; then
  echo "Directory ${metadata} absent  ... exiting"
  exit 1
fi

runDir=${outdir}/${outprefix}_run

mkdir -p ${runDir}/logs || exit 1

# For SVAltAlign, you must use the version of bwa compatible with Genome STRiP.
export PATH=${SV_DIR}/bwa:${PATH}
export LD_LIBRARY_PATH=${SV_DIR}/bwa:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=/SGE/ogs/lib/linux-x64:${LD_LIBRARY_PATH}

mx="-Xmx4g"
classpath="${SV_DIR}/lib/SVToolkit.jar:${SV_DIR}/lib/gatk/GenomeAnalysisTK.jar:${SV_DIR}/lib/gatk/Queue.jar"

echo "-- Run CNVPipeline -- "
LC_ALL=C java -cp ${classpath} ${mx} \
    org.broadinstitute.gatk.queue.QCommandLine \
    -S ${SV_DIR}/qscript/discovery/cnv/CNVDiscoveryPipeline.q \
    -S ${SV_DIR}/qscript/SVQScript.q \
    -cp ${classpath} \
    -gatk ${SV_DIR}/lib/gatk/GenomeAnalysisTK.jar \
    -configFile ${SV_DIR}/conf/genstrip_parameters.txt \
    --disableJobReport \
    -jobProject Capri \
    -jobRunner Drmaa \
    -gatkJobRunner Drmaa \
    -jobQueue workq \
    -jobNative '-l mem=16G -l h_vmem=16G' \
    -R ${reference_prefix}.fasta \
    -genderMaskBedFile  ${reference_prefix}.gendermask.bed \
    -genomeMaskFile     ${reference_prefix}.svmask.fasta \
    -genderMapFile      ${gendermap} \
    -ploidyMapFile      ${reference_prefix}.ploidymap.txt \
    -md ${metadata} \
    -runDirectory ${runDir} \
    -jobLogDir ${runDir}/logs \
    -I ${inputFile} \
    -intervalList ${intervallist} \
    -tilingWindowSize 5000 \
    -tilingWindowOverlap 2500 \
    -maximumReferenceGapLength 2500 \
    -boundaryPrecision 200 \
    -minimumRefinedLength 2500 \
    -run \
    || exit 1

sites=${runDir}/results/gs_cnv.genotypes.vcf.gz
genotypes=${outdir}/${outprefix}.vcf.gz

# Run genotyping on the discovered sites.
LC_ALL=C  java -Xmx4g -cp ${classpath} ${mx} \
     org.broadinstitute.sv.apps.GenerateHaploidCNVGenotypes \
     -R ${reference_prefix}.fasta \
     -ploidyMapFile ${reference_prefix}.ploidymap.txt \
     -genderMapFile ${gendermap}  \
     -vcf ${sites} \
     -O ${genotypes} \
     -estimateAlleleFrequencies true \
     -genotypeLikelihoodThreshold 0.001 || exit 1

echo "Script CNVPipeline completed successfully at "`date +%Y-%m-%d`