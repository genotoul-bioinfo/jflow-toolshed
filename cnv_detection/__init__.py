#
# Copyright (C) 2017 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os, re
from collections import OrderedDict

from jflow.workflow import Workflow


class CnvDetection(Workflow):

    nb_threads = 8

    def get_description(self):
        return "Detect structural variations in whole genomes."

    def define_parameters(self, function="process"):
        # Inputs:
        self.add_multiple_parameter_list("sample", "List of sample reads", required=True, rules="FilesUnique",
                                         group="inputs")
        self.add_parameter("name", "Sample name", required=True, add_to="sample")
        self.add_input_file("alignments", "Reads already aligned on genome", file_format="bam", add_to="sample",
                            required=True)
        self.add_input_file("reference_genome", "Fasta file of the genome (will be indexed)", file_format="fasta",
                            group="inputs", required=True)

        # Choose tools to run:
        self.add_parameter("delly", "Enable delly tool", type=bool, group="Select tools",
                           rules="AtLeastOneAmong=lumpy,pindel,genomestrip,only_cnv")
        self.add_parameter("lumpy", "Enable lumpy tool", type=bool, group="Select tools")
        self.add_parameter("pindel", "Enable pindel tool", type=bool, group="Select tools")
        self.add_parameter("genomestrip", "Enable genome strip tool", type=bool, group="Select tools")

        # General parameters:
        self.add_parameter("chromosomes", "Chromosomes to study, comma separated", default="genome",
                           group="General parameters", rules="ForbiddenChars= ")
        self.add_parameter("min_sup", "Minimum number of read supporting a SV (3)",
                           type=int, default=3, group="General parameters")
        self.add_parameter("min_len", "Minimum length of a variant (200)", type=int, default=50,
                           group="General parameters")
        # TODO: max calculed % chr size
        self.add_parameter("max_len", "Maximum length of a variant (8 500 000)", type=int, default=8500000,
                           group="General parameters")
        # TODO in future: minmapq -> just after aligment
        self.add_parameter("min_mapq", "min. paired-end mapping quality", type=int, default=10,
                           group="General parameters")
        self.add_parameter("li_range", "Range of localisation interval around breakpoints", type=int, default=1000,
                           group="General parameters")

        # Specific parameters:
        self.add_input_file("gendermap", "Gender map file for each individual", group="GenomeStrip parameters",
                           rules="DisabledIf?ALL[genomestrip=False]")
        self.add_parameter("overlapcutoff", "cutoff for reciprocal overlap", default=0.5, type=float,
                           group="Genotyping")

        # Variants type:
        self.add_parameter("only_cnv", "Only detect copy number variations", type=bool ,default=False)
        self.add_parameter("no_cnv", "No detect copy number variations (only detect DEL, INV and DUP)", type=bool,
                           default=False, rules="Exclude=only_cnv")

    def get_chromosomes_list(self):
        self.chromosomes_list = []
        if self.chromosomes == "genome":
            chr_candidates = os.popen("cat " + self.reference_genome +
                                      " | grep '>' | awk 'sub(/^>/, \"\")' | awk '{print $1}'").read().split(
                "\n")
            for chr in chr_candidates:
                if len(chr) > 0:
                    m = re.match(r"^ch(r)?(_)?(\d+)$", chr.lower())
                    if (chr.isdigit() and int(chr) < 200) or (m is not None and int(m.group(3)) < 200):
                        self.chromosomes_list.append(chr)
            if len(self.chromosomes_list) == 0:
                raise Exception("No chromosomes found. Please specify them manually")
        else:
            self.chromosomes_list = self.chromosomes.split(",")

    def process(self):
        alignments = OrderedDict()
        required_files = [self.reference_genome + ".fai"]
        for smple in self.sample:
            alignments[smple["name"]] = smple["alignments"]
            required_files.append(smple["alignments"] + ".bai")

        for rf in required_files:
            if not os.path.isfile(rf):
                raise Exception("Index file is missing: " + rf)

        input_bam = list(alignments.values())
        indiv_list = list(alignments.keys())

        self.get_chromosomes_list()

        results = {}

        gendermap = self.gendermap
        if self.gendermap.is_None:
            gendermap = self.get_temporary_file(".map")
            lines = []
            for name in alignments.keys():
                lines.append(name + "\tF")
            with open(gendermap, "w") as f:
                f.write("\n".join(lines))

        ################
        # Launch tools #
        ################

        # BreakDancer:
        # if self.breakdancer:
        #     breakdancer = self.add_component("BreakDancer", [input_bam,
        #                                      self.min_sup, self.min_mapq, self.max_len,
        #                                        indiv_list, self.chromosomes_list])
        #     results["breakdancer"] = breakdancer.output_file

        # Genomestrip preprocess:
        genomestrip_preprocess = self.add_component("GenomestripPreprocess", [self.reference_genome, input_bam,
                                                                              gendermap])

        if not self.only_cnv:

            # Pindel:
            if self.pindel:
                pindel = self.add_component("Pindel", kwargs={"input_bam": input_bam,
                                                              "min_mapq": self.min_mapq, "min_sup": self.min_sup,
                                                              "threads": self.nb_threads,
                                                              "reference_genome": self.reference_genome,
                                                              "output_file": "pindel.concat.out", "indiv_list": indiv_list,
                                                              "chromosomes": self.chromosomes_list})
                results["pindel"] = {"DEL": pindel.output_file_D, "SI": pindel.output_file_SI, "TD": pindel.output_file_TD,
                                     "INV": pindel.output_file_INV}

            # Delly:
            if self.delly:
                delly = self.add_component("Delly", [input_bam, self.reference_genome, indiv_list,
                                                     self.chromosomes_list, self.min_mapq, self.min_len, self.nb_threads])
                results["delly"] = {"DEL": delly.output_file_DEL, "INV": delly.output_file_INV,
                                    "DUP": delly.output_file_DUP}

            # Lumpy:
            if self.lumpy:
                lumpy = self.add_component("Lumpy", [input_bam, self.chromosomes_list,
                                                     self.nb_threads])
                results["lumpy"] = lumpy.output_file

            # GenomeStrip:
            if self.genomestrip:
                genomestrip = self.add_component("Genomestrip", [input_bam, self.reference_genome,
                                                                 genomestrip_preprocess.results, self.chromosomes_list,
                                                                 gendermap])
                results["genomestrip"] = genomestrip.output_file

            if len(results) > 0:

                ###########
                # PARSING #
                ###########
                parses = self.add_component("ParseResults", [results, self.reference_genome])

                ###########
                # MERGING #
                ###########
                merges = self.add_component("MergeResults", [parses.outputs, list(results.keys()), self.reference_genome
                                            , self.chromosomes_list])

                ##############
                # GENOTYPING #
                ##############
                genotype = self.add_component("Genotype", [merges.merged_vcf_DEL, input_bam, gendermap,
                                                           self.reference_genome,
                                                           "DEL", self.chromosomes_list, genomestrip_preprocess.results,
                                                           self.overlapcutoff, self.nb_threads])
                svtyper = self.add_component("Svtyper", kwargs={"merged_vcf_DEL": merges.merged_vcf_DEL,
                                                                "merged_vcf_INV": merges.merged_vcf_INV,
                                                                "bam_files": input_bam,
                                                                "reference": self.reference_genome,
                                                                "chromosomes": self.chromosomes_list,
                                                                "threads": self.nb_threads})

                #############
                # FILTERING #
                #############
                filter = self.add_component("Filter", [genotype.output, "DEL", self.chromosomes_list,
                                                       self.overlapcutoff, self.min_len])

        if not self.no_cnv:
            cnv = self.add_component("GsMulticopy", [input_bam, self.reference_genome,
                                                             genomestrip_preprocess.results, self.chromosomes_list,
                                                             gendermap])