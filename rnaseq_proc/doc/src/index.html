<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Jflow RNA-SEQ for procaryotes workflow User Manual</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <script type="application/javascript" src="js/jquery-3.1.1.min.js"></script>
    <script type="application/javascript" src="js/build_page.js"></script>
</head>
<body>
<h1>Jflow RNA-SEQ for procaryotes workflow User Manual</h1>
<p id="subtitle">&ndash; command line version &ndash;</p>

<div id="author_info">
    <p><strong>Written by:</strong> Floréal Cabanettes &lt;<a href="mailto:floreal.cabanettes@inra.fr">floreal.cabanettes@inra.fr</a>&gt;<br/>
    <strong>Version of the documentation:</strong> 1.0 (09-05-2017)</p>
</div>

<div class="summary_document">
    <p>This document explains how to use the RNA-seq for procaryotes pipeline through Jflow on the Genotoul cluster. All available options
        availables are described below, illustrated by some examples.</p>
</div>

<p id="sommaire_title">Summary</p>
<div id="sommaire_div"></div>

<div id="trouver_jflow">
    <h2>Where to find Jflow on the genotoul cluster?</h2>
    <p>Jflow is available on the genotoul cluster at this URL:<br/>
        <em>/usr/local/bioinfo/src/Jflow/jflow</em>.</p>
</div>

<div id="parametres_workflow">
    <h2>Workflow parameters</h2>
    <div id="type_protocol">
        <h3>Sequencing protocol</h3>
        <p>Default protocol is <em>illumina_stranded</em>. It's the one used on the platform. You can also use another protocol, by adding this parameter:<br/>
            <em class="code">--protocol other</em>.</p>
        <p>This parameter allows you to add advanced settings of each component of the workflow (see below).</p>
    </div>
    <div id="input_files">
        <h3>Input files</h3>
        <p>The workflow requires several input files:</p>
        <ul>
            <li>reads in <strong>fastq.gz</strong> format (single or paired)</li>
            <li>a reference genome in <strong>fasta</strong> format</li>
            <li>a genome model in <strong>gtf</strong> format.</li>
        </ul>
        <h4>Reads</h4>
        <p>Use the <em class="code">--sample</em> parameter to set reads. For each sample, set the <em class="code">reads-1</em> sub-parameter, and if reads are paired, also the <em class="code">reads-2</em> sub-parameter.
            Repeat <em>--sample</em> parameter for each sample.</p>
        <p>Examples:</p>
        <ul>
            <li>One paired end sample:<br/><em class="code">--sample reads-1=my_file_reads_1.fastq.gz reads-2=my_file_reads_2.fastq.gz</em></li>
            <li>One single end sample:<br/><em class="code">--sample reads-1=my_file_single.fastq.gz</em></li>
            <li>Two samples, one paired end, the other single end:<br/>
                <em class="code">--sample reads-1=my_file_r1.fastq.gz reads-2=my_file_r2.fastq.gz --sample reads-1=my_file_single.fastq.gz</em></li>
        </ul>
        <h4>Reference genome</h4>
        <p>Use the <em class="code">--reference-genome</em> parameter to specify reference genome fasta file.
            If genome is already indexed by bwa or bowtie (according to parameters below), add the <em class="code">--genome-indexed</em> option.</p>
        <p>Note: if genome is pre-indexed, it must be pre-indexed with default prefix.</p>
        <p>Exemples :</p>
        <ul>
            <li>Only fasta file:<br/><em class="code">--reference-genome myfile.fasta</em></li>
            <li>Fasta file + index:<br/><em class="code">--reference-genome myfile.fasta --genome-indexed</em></li>
        </ul>
        <h4>Gene model</h4>
        <p>Use the <em class="code">--gtf-file</em> parameter to set a gene model file.</p>
        <p>Example: <em class="code">--gtf-file my_file.gtf</em></p>
    </div>
    <div id="trimming">
        <h3>Trimming</h3>
        <p>To trim your reads before alignment, use the <em class="code">--trim-reads</em> parameter.</p>
        <p><strong class="important">Warning:</strong> If you use the <em>illumina_stranded</em> protocol (default), additionnal parameters below are not expected.</p>
        <h4>Additional parameters</h4>
        <p><strong>--adapter-1</strong></p>
        <p class="indent-1">
            Adapter sequence (adapter 1 if reads are paired end).<br/>
            <em>Required:</em> <span class="yes">yes</span>.<br/>
            <em>Example:</em> <em class="code">--adapter-1 AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC</em></p>
        <p><strong>--adapter-2</strong></p>
        <p class="indent-1">
            Adapter 2 sequence (if paired end reads).<br/>
            <em>Required:</em> <span class="yes">yes, if paired end reads are given</span>.<br/>
            <em>Example:</em> <em class="code">--adapter-2 AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTAGATCTCGGTGGTCGCCGTATCATT</em></p>
        <p><strong>--position-adapter</strong></p>
        <p class="indent-1">
            Adapter position.<br/>
            <em>Available choices:</em> 3', 5', 3'/5'<br/>
            <em>Default:</em> 3'<br/>
            <em>Required:</em> <span class="no">no</span><br/>
            <em>Example:</em> <em class="code">--position-adapter 5'</em>
        </p>
        <h4>Example</h4>
        <em class="code">--trim-reads --adapter-1 AGATCGGAAGAGCACA --adapter-2 AGATCGGAAGAGCGTCGTGTAGGGAAAGA</em>
        <h4>Software and fixed parameters</h4>
        <p><strong>Software used:</strong> <a href="https://github.com/marcelm/cutadapt">cutadapt</a> [VERSION: 1.8.3]</p>
        <p><strong>Fixed parameter:</strong></p>
        <ul><li>Max read length filter: equals to 1/3 of the max read length among all samples.</li></ul>
    </div>
    <div id="mapping">
        <h3>Mapping reads on reference genome</h3>
        <p><em>Bwa</em> is the default software for this step. To use <em>bowtie</em> instead, add the parameter:</p>
        <p><em class="code">--use-bowtie2</em></p>
        <h4>Software and fixed parameters</h4>
        <p><strong>Software used:</strong> <a href="http://bio-bwa.sourceforge.net/">BWA</a> [VERSION: 0.7.12] or
            <a href="http://bowtie-bio.sourceforge.net/bowtie2/index.shtml">Bowtie2</a> [VERSION: 2.1.0]</p>
        <p><strong>Fixed parameter:</strong> None</p>
    </div>

    <div id="quanification">
        <h3>Quantification</h3>
        <p>Several parameters for this step.</p>
        <p><strong>--stranded</strong></p>
        <p class="indent-1">
            Set data are <em>stranded</em>, <em>reverse stranded</em> or <em>unstranded</em>.<br/>
            <em>Available choices:</em> stranded, reverse_stranded, unstranded<br/>
            <em>Default:</em> reverse_stranded<br/>
            <em>Required:</em> <span class="no">no</span>.<br/>
            <em>Example:</em> <em class="code">--stranded unstranded</em>
        </p>
        <p><strong>--multi-map</strong></p>
        <p class="indent-1">
            With this parameter, one read can be mapped to several points on the genome.<br/>
            <em>Usage:</em> add this parameter (without value) to enable it.<br/>
            <em>Required:</em> <span class="no">no</span>.
        </p>
        <p><strong>--multi-assign</strong></p>
        <p class="indent-1">
            With this parameter, one read can have several <em>meta-features</em>.<br/>
            <em>Usage:</em> add this parameter (without value) to enable it.<br/>
            <em>Required:</em> <span class="no">no</span>.
        </p>
        <h4>Software and fixed parameters</h4>
        <p><strong>Software used:</strong> <a href="http://bioinf.wehi.edu.au/featureCounts/">FeatureCounts</a> [VERSION: 1.4.5]</p>
        <p><strong>Fixed parameter:</strong> None</p>
    </div>
</div>

<div id="launch_workflow">
    <h2>Lancement du workflow</h2>
    <p>Launch the workflow using the command:</p>
    <p><strong>/usr/local/bioinfo/src/Jflow/jflow/bin/jflow_cli.py rnaseqproc &lt;parameters...&gt;</strong></p>
    <p>Replacing <em>&lt;parameters...&gt;</em> by the parameters described above.</p>
    <h3>Example</h3>
    <p>We have 3 samples to analyse:</p>
    <ul>
        <li>Sample 1 (paired end) : <em>sample1.R1.fastq.gz, sample1.R2.fastq.gz</em></li>
        <li>Sample 2 (single end) : <em>sample2.fastq.gz</em></li>
        <li>Sample 3 (paired end) : <em>sample3.R1.fastq.gz, sample3.R2.fastq.gz</em></li>
    </ul>
    <p>Also, a fasta file of the reference genome (we have not already indexed it):</p>
    <ul>
        <li>Reference genome : <em>reference.fa</em></li>
    </ul>
    <p>And a gene model:</p>
    <ul>
        <li>Gene model: <em>reference.gtf</em></li>
    </ul>
    <p>All files above are localized in the working directory. The sequencing has been performed on our local sequencing plateform,
        therefore we use the default protocol <em>illumina_stranded</em>.
        We want to trim reads. As possible, we will keep the default parameters of the workflow.
        We launch, so, the following command:</p>
    <p class="align-left"><em class="code">/usr/local/bioinfo/src/Jflow/jflow/bin/jflow_cli.py rnaseqproc --sample reads-1=sample1.R1.fastq.gz <br/>
        reads-2=sample1.R2.fastq.gz --sample reads-1=sample2.fastq.gz --sample reads-1=sample3.R1.fastq.gz <br/>
        reads-2=sample3.R2.fastq.gz --reference-genome reference.fa --gtf-file reference.gtf --trim-reads</em></p>
</div>
<div id="results">
    <h2>Results</h2>
    <p>Results of the last launched workflow are inside the folder with the highest workflow-id in directory name,
        inside the folder <em>jflow_results/rnaseqproc</em> of your <em>work</em> directory.
        The examples shown here are results of the workflow described in the section above. Sample file names are based on the reads-1 file names.</p>
    <p>At the end of the workflow execution, a recap show the resulting files for each component:</p>
    <pre>
###########
# Results #
###########

    [...]</pre>
    <p>If you mix paired and single reads, results below are separated for single and paired data.</p>
    <div id="trimming_2">
        <h3>Trimming</h3>
        <p>Results are inside the folders <em>Cutadapt_single</em> and/or <em>Cutadapt_paired</em>. There are as many files as fastq files given as inputs. Each file
            is the trimmed original file.</p>
        <pre><span class="underline">1. Trimming (Cutadapt) {PAIRED}:</span>
  - /work/fcabanettes/jflow_results/rnaseqproc/wf000011/<strong>Cutadapt_paired/</strong>sample3.R1.fastq<strong>.trim.fastq.gz</strong>
  - /work/fcabanettes/jflow_results/rnaseqproc/wf000011/<strong>Cutadapt_paired/</strong>sample1.R1.fastq<strong>.trim.fastq.gz</strong>
  - /work/fcabanettes/jflow_results/rnaseqproc/wf000011/<strong>Cutadapt_paired/</strong>sample1.R2.fastq<strong>.trim.fastq.gz</strong>
  - /work/fcabanettes/jflow_results/rnaseqproc/wf000011/<strong>Cutadapt_paired/</strong>sample3.R2.fastq<strong>.trim.fastq.gz</strong>

<span class="underline">2. Trimming (Cutadapt) {SINGLE}:</span>
  - /work/fcabanettes/jflow_results/rnaseqproc/wf000011/<strong>Cutadapt_single/</strong>sample2.fastq<strong>.trim.fastq.gz</strong>
        </pre>
    </div>
    <div id="indexing_2">
        <h3>Reference genome index</h3>
        <p>If you don't give a pre-indexed genome, the <em>BwaIndex_default</em> folder contains the BWA or Bowtie2 reference genome index.</p>
        <pre><span class="underline">3. Index genome (bwa):</span>
  - /work/fcabanettes/jflow_results/rnaseqproc/wf000011/<strong>BwaIndex_default/genome</strong></pre>
    </div>
    <div id="mapping_2">
        <h3>Mapping reads of reference genome</h3>
        <p>Mapping bam files are located into the <em>BwaMap_single</em> (or Bowtie2Map_single) and/or <em>BwaMap_paired</em> (or Bowtie2Map_paired) folders.
        <pre><span class="underline">4. Map reads to the genome (bwa) {PAIRED}</span>:
  - /work/fcabanettes/jflow_results/rnaseqproc/wf000011/BwaMap_paired/sample1.R1.trim-sorted<strong>.bam</strong>
  - /work/fcabanettes/jflow_results/rnaseqproc/wf000011/BwaMap_paired/sample1.R1.trim-sorted<strong>.bam.summary</strong>
  - /work/fcabanettes/jflow_results/rnaseqproc/wf000011/BwaMap_paired/sample3.R1.trim-sorted<strong>.bam.summary</strong>
  - /work/fcabanettes/jflow_results/rnaseqproc/wf000011/BwaMap_paired/sample3.R1.trim-sorted<strong>.bam</strong>

<span class="underline">5. Map reads to the genome (bwa) {SINGLE}:</span>
  - /work/fcabanettes/jflow_results/rnaseqproc/wf000011/BwaMap_single/sample2.trim-sorted<strong>.bam</strong>
  - /work/fcabanettes/jflow_results/rnaseqproc/wf000011/BwaMap_single/sample2.trim-sorted<strong>.bam.summary</strong></pre>
    </div>
    <div id="quantification_2">
        <h3>Quantification</h3>
        <p>Count results for each gene as TSV file and a summary of counts, for each sample, in the folder <em>FeatureCounts_single</em> and/or <em>FeatureCounts_paired</em>.</p>
        <pre><span class="underline">6. Count features {PAIRED}:</span>
  - /work/fcabanettes/jflow_results/rnaseqproc/wf000011/<strong>FeatureCounts_paired/</strong>Quant-sample3.R1.trim-sorted<strong>.tsv</strong>
  - /work/fcabanettes/jflow_results/rnaseqproc/wf000011/<strong>FeatureCounts_paired/</strong>Quant-sample1.R1.trim-sorted<strong>.tsv</strong>
  - /work/fcabanettes/jflow_results/rnaseqproc/wf000011/<strong>FeatureCounts_paired/</strong>Quant-sample1.R1.trim-sorted<strong>.tsv.summary</strong>
  - /work/fcabanettes/jflow_results/rnaseqproc/wf000011/<strong>FeatureCounts_paired/</strong>Quant-sample3.R1.trim-sorted<strong>.tsv.summary</strong>

<span class="underline">7. Count features {SINGLE}:</span>
  - /work/fcabanettes/jflow_results/rnaseqproc/wf000011/<strong>FeatureCounts_paired/</strong>Quant-sample2.trim-sorted<strong>.tsv.summary</strong>
  - /work/fcabanettes/jflow_results/rnaseqproc/wf000011/<strong>FeatureCounts_paired/</strong>Quant-sample2.trim-sorted<strong>.tsv</strong></pre>

    </div>
    <div id="summary">
        <h3>Summary</h3>
        <p>The <em>Summary_default</em> folder contains files which summarize the different components of the workflow : one file per important
            step:</p>
        <ul>
            <li>Summary of statistics generated by Samtools on sample mappings</li>
            <li>Summary of trimming generated by Cutadapt for all samples</li>
            <li>Summary of the FeatureCount quantification, for all genes and all samples, for all samples</li>
        </ul>
        <pre><span class="underline">8. Summary:</span>
  - /work/fcabanettes/jflow_results/rnaseqproc/wf000011/<strong>Summary_default/summary_trimming.tsv</strong>
  - /work/fcabanettes/jflow_results/rnaseqproc/wf000011/<strong>Summary_default/summary_featureCounts.tsv</strong>
  - /work/fcabanettes/jflow_results/rnaseqproc/wf000011/<strong>Summary_default/summary_mapping.tsv</strong></pre>
    </div>
    <div id="logs">
        <h3>Logging</h3>
        <p>In addition to the files described above, there are two files for each sample and each component into are stored standard output (<em>.stdout</em> files)
            and standard error (<em>.stderr</em> files). If an error occured during the workflow execution, please explore these files to see why.</p>
    </div>
</div>

</body>
</html>