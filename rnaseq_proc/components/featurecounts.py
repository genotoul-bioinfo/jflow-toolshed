import os
import re
from subprocess import Popen, PIPE

from jflow.component import Component


def run_featureCounts(bam_file, gtf_file, fcounts_exec, paired, stranded, multi_map, multi_assign, threads, counts_file,
                      counts_summary_file, stdout, stderr):
    cmd = fcounts_exec + " -a " + gtf_file + " -o " + counts_file + " -s " + stranded
    if multi_map == "1":
        cmd += " -M"
    if multi_assign == "1":
        cmd += " -O"
    if paired == "1":
        cmd += " -p"
    cmd += " -T " + threads + " " + bam_file + " > " + stdout + " 2>> " + stderr

    os.system(cmd)


class FeatureCounts(Component):

    def get_command(self):
        return "featureCounts"

    def get_version(self):
        cmd = [self.get_exec_path("featureCounts")]
        p = Popen(cmd, stdout=PIPE, stderr=PIPE)
        stdout, stderr = p.communicate()
        search = re.search(r"Version ([^\n]+)", stderr.decode("utf-8"))
        if search:
            return search.group(1)
        return "Unknown"

    def get_description(self):
        return "Count features" if self.description is None else self.description

    def define_parameters(self, bam_files, gtf_file, paired, stranded, multi_map, multi_assign, threads=4):
        self.add_input_file_list("bam_files", "Reads alignments", default=bam_files)
        self.add_input_file("gtf_file", "Gtf model file", default=gtf_file)
        self.add_parameter("paired", "Is data paired", default=paired, type=bool, cmd_format="-p")
        stranded_value = "0" if stranded == "unstranded" else ("1" if stranded == "stranded" else "2")
        self.add_parameter("stranded", "Is data stranded", choices=["0", "1", "2"],
                           default=stranded_value, cmd_format="-s")
        self.add_parameter("multi_map", "If specified, multi-mapping reads/fragments will be counted (ie. \
            a multi-mapping read will be counted up to N times if it has N reported mapping locations)", type=bool,
                           default=multi_map, cmd_format="-M")
        self.add_parameter("multi_assign", "If specified, reads will \
            be allowed to be assigned to more than one matched meta-feature", type=bool, default=multi_assign,
                           cmd_format="-O")
        self.add_parameter("threads", "Number of threads", type=int, default=threads, cmd_format="-T")
        self.add_output_file_list("counts", "Counts for all genes", pattern="Quant-{basename_woext}.tsv",
                                  items=bam_files)
        self.add_output_file_list("summary", "Summary of featureCounts", pattern="Quant-{basename_woext}.tsv.summary",
                                  items=bam_files)
        self.add_output_file_list("stdout", "Standard output", pattern="Quant-{basename_woext}.stdout", items=bam_files)
        self.add_output_file_list("stderr", "Standard error", pattern="Quant-{basename_woext}.stderr", items=bam_files)

    def process(self):

        self.add_python_execution(run_featureCounts, cmd_format="{EXE} {IN} {ARG} {OUT}", inputs=[self.bam_files],
                                  includes=[self.gtf_file], arguments=[self.gtf_file,
                                  self.get_exec_path("featureCounts"), "1" if self.paired else "0", self.stranded,
                                  "1" if self.multi_map else "0", "1" if self.multi_assign else "0", str(self.threads)],
                                  outputs=[self.counts, self.summary, self.stdout, self.stderr], map=True)
