#
# Copyright (C) 2015 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import gzip, re
from jflow.workflow import Workflow

class RnaseqProc(Workflow):

    nb_threads = 8

    def get_description(self):
        return "A complete RNA-seq workflow for procaryotes"

    def define_parameters(self, function="process"):
        self.add_parameter("protocol", "Protocol used", display_name="RNA-seq protocol", default="illumina_stranded",
                           choices=["illumina_stranded", "other"])

        # Input files:
        self.add_multiple_parameter_list("sample", "List of sample reads", required=True, group="inputs",
                                         rules="FilesUnique", paired_columns="fastq(.gz)?[reads_1,reads_2]")
        self.add_input_file("reads_1", "Which reads files should be used", required=True, add_to="sample",
                            file_format="fastq")
        self.add_input_file("reads_2", "Which reads files should be used (paired reads)", required=False,
                            add_to="sample", file_format="fastq")
        self.add_input_file("reference_genome", "Which genome should the read being align on", file_format="fasta",
                            group="inputs", required=True)
        self.add_parameter("genome_indexed", "Is the genome already indexed by BWA (into the same directory as Fasta "
                                             "file, with Fasta file as prefix)", group="inputs", type=bool)
        self.add_input_file("gtf_file", "the gene annotation in GTF format", group="inputs",
                            required=True)

        # Trimming:
        self.add_parameter("trim_reads", "Trim reads before analysis", group="trimming", type=bool)
        self.add_parameter("adapter_1", "Sequence of the adapter (paired data: of the first read)", group="trimming",
                           default="AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC",
                           rules="RequiredIf?ALL[trim_reads=True,protocol=other];"
                                 "DisabledIf?ANY[trim_reads=False,protocol=illumina_stranded]")
        self.add_parameter("adapter_2", "Sequence of the adapter for the second read (paired data only)",
                           group="trimming", default="AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTAGATCTCGGTGGTCGCCGTATCATT",
                           rules="RequiredIf?ALL[trim_reads=True,sample>reads_2=*,protocol=other];"
                                 "DisabledIf?ANY[trim_reads=False,protocol=illumina_stranded]")
        self.add_parameter("position_adapter", "Position of the adapter", choices=["3'", "5'", "3'/5'"],
                           display_name="Adapter's position", group="trimming",
                           rules="DisabledIf?ANY[trim_reads=False,protocol=illumina_stranded]")

        # Mapping/indexing
        self.add_parameter("use_bowtie2", "Use bowtie2 instead of bwa", group="Mapping", type=bool)

        # FeatureCounts:
        self.add_parameter("stranded", "Is data stranded", choices=["unstranded", "stranded", "reversely_stranded"],
                           default="reversely_stranded", group="Quantification")
        self.add_parameter("multi_map", "If specified, multi-mapping reads/fragments will be counted (ie. \
                    a multi-mapping read will be counted up to N times if it has N reported mapping locations)",
                           type=bool,
                           default=False, group="Quantification")
        self.add_parameter("multi_assign", "If specified, reads will \
                    be allowed to be assigned to more than one matched meta-feature", type=bool, default=False,
                           group="Quantification")

    @staticmethod
    def get_reads_length(reads_file):
        r_file = gzip.open(reads_file, "r")
        r_file.readline()  # ignore first line
        read = r_file.readline().decode("utf-8")
        match = re.search(r"[A-Za-z]+", read)
        if match:
            read = match.group(0)
        return len(read)

    def process(self):
        paired = {
            "reads1": [],
            "reads2": []
        }
        single = []
        max_read_length = 0
        has_paired = False
        has_single = False
        for smple in self.sample:
            max_read_length = max(max_read_length, self.get_reads_length(smple["reads_1"]))
            if not smple["reads_2"].is_None:
                paired["reads1"].append(smple["reads_1"])
                paired["reads2"].append(smple["reads_2"])
                max_read_length = max(max_read_length, self.get_reads_length(smple["reads_2"]))
                has_paired = True
            else:
                single.append(smple["reads_1"])
                has_single = True

        cutadapt_logs = []

        # Trim reads
        if self.trim_reads:
            if has_paired:
                cutadapt_paired = self.add_component("Cutadapt", [paired["reads1"], paired["reads2"], self.adapter_1,
                                                                  self.adapter_2, self.position_adapter,
                                                                  max_read_length / 3],
                                                     component_prefix="paired")
                paired["reads1"] = cutadapt_paired.o_reads_1
                paired["reads2"] = cutadapt_paired.o_reads_2
                cutadapt_logs += cutadapt_paired.stdout
                if has_single:
                    cutadapt_paired.description = cutadapt_paired.get_description() + " {PAIRED}"
            if has_single:
                cutadapt_single = self.add_component("Cutadapt", [single, None, self.adapter_1,
                                                                  None, self.position_adapter, max_read_length / 3],
                                                     component_prefix="single")
                single = cutadapt_single.o_reads_1
                cutadapt_logs += cutadapt_single.stdout
                if has_paired:
                    cutadapt_single.description = cutadapt_single.get_description() + " {SINGLE}"

        # Index genome
        index_genome_directory = self.reference_genome[:self.reference_genome.rfind("/")]
        index_genome_prefix = self.reference_genome[self.reference_genome.rfind("/") + 1:]
        #  Index genome if needed:
        if not self.genome_indexed:
            if not self.use_bowtie2:
                bwaindex = self.add_component("BwaIndex", [self.reference_genome])
            else:
                bwaindex = self.add_component("Bowtie2Index", [self.reference_genome])
            index_genome_directory = bwaindex.genome_dir
            index_genome_prefix = "reference"

        # Map reads
        if has_paired:
            if not self.use_bowtie2:
                bwamap_paired = self.add_component("BwaMap", [index_genome_directory, paired["reads1"],
                                                              paired["reads2"], None, False, False, False,
                                                              index_genome_prefix],
                                                   component_prefix="paired")
            else:
                bwamap_paired = self.add_component("Bowtie2Map", [index_genome_directory, paired["reads1"],
                                                                  paired["reads2"], index_genome_prefix],
                                                   component_prefix="paired")
            if has_single:
                bwamap_paired.description = bwamap_paired.get_description() + " {PAIRED}"
        if has_single:
            if not self.use_bowtie2:
                bwamap_single = self.add_component("BwaMap", [index_genome_directory, single, None, None, False, False,
                                                              False, index_genome_prefix], component_prefix="single")
            else:
                bwamap_single = self.add_component("Bowtie2Map", [index_genome_directory, single, None,
                                                                  index_genome_prefix], component_prefix="single")
            if has_paired:
                bwamap_single.description = bwamap_single.get_description() + " {SINGLE}"

        # FeatureCounts
        if has_paired:
            fcounts_paired = self.add_component("FeatureCounts", [bwamap_paired.map_files, self.gtf_file, True,
                                                self.stranded, self.multi_map, self.multi_assign, self.nb_threads],
                                                component_prefix="paired")
            if has_single:
                fcounts_paired.description = fcounts_paired.get_description() + " {PAIRED}"
        if has_single:
            fcounts_single = self.add_component("FeatureCounts", [bwamap_single.map_files, self.gtf_file, False,
                                                self.stranded, self.multi_map, self.multi_assign, self.nb_threads],
                                                component_prefix="single")
            if has_paired:
                fcounts_single.description = fcounts_single.get_description() + " {SINGLE}"

        summary_mapping_files = []
        if has_paired:
            summary_mapping_files += bwamap_paired.summary_files
        if has_single:
            summary_mapping_files += bwamap_single.summary_files
        summary_fcounts_files = []
        if has_paired:
            summary_fcounts_files += fcounts_paired.summary
        if has_single:
            summary_fcounts_files += fcounts_single.summary
        cutadapt_logs = None
        if self.trim_reads:
            cutadapt_logs = []
            if has_paired:
                cutadapt_logs += cutadapt_paired.stdout
            if has_single:
                cutadapt_logs += cutadapt_single.stdout
        summary = self.add_component("Summary", [summary_mapping_files, summary_fcounts_files, cutadapt_logs])
