import os

from jflow.component import Component


def build_summary(nbfiles_starmap, nbfiles_starmapnew,
                  nbfiles_cutadapt, build_cutadapt_summary, io_files_file, *io):
    import re
    from collections import OrderedDict

    #################
    # STAR Summary #
    #################

    def _build_lines_star(file, res_lines):
        key_value_line = r"\s*(.+)\s\|\t(.+)\n"
        with open(file, "r") as log_file:
            lines = log_file.readlines()
            for i in range(5, len(lines)):  # Remove header
                line = lines[i]
                match = re.match(key_value_line, line)
                if match:
                    name = match.group(1)
                    value = match.group(2).replace("%","")
                    if name not in res_lines:
                        res_lines[name] = []
                    res_lines[name].append(value)
        return res_lines

    def _build_star_summary(star_summary_file, logs_files):
        with open(star_summary_file, "w") as sy_file:
            res_lines = OrderedDict()
            for log in logs_files:
                res_lines = _build_lines_star(log, res_lines)
                name_sample = os.path.basename(log)
                name_sample = re.sub(r"(\.trim)?-Log\.final\.out", "", name_sample)
                sy_file.write("\t" + name_sample)
            sy_file.write("\n")
            for name_line, values_line in res_lines.items():
                sy_file.write(name_line)
                if values_line is not None:
                    sy_file.write("\t" + "\t".join(values_line))
                sy_file.write("\n")
            sy_file.write("\n")

    iofiles = None

    with open(io_files_file, "r") as io_f:
        iofiles = io_f.read().split("|")

    iofiles = list(iofiles)

    #Get outputs:
    starmap_summary = iofiles.pop(0)
    starmap_new_summary = None
    if int(nbfiles_starmapnew) > 0:
        starmap_new_summary = iofiles.pop(0)
    cutadapt_summary = None
    if build_cutadapt_summary == "True":
        cutadapt_summary = iofiles.pop(0)

    star_logs = []
    iofiles = list(iofiles)
    for i in range(0, int(nbfiles_starmap)): # Get logs from STAR mapping
        star_logs.append(iofiles.pop(0))

    star_logs_new = []
    if int(nbfiles_starmapnew) > 0:
        for i in range(0, int(nbfiles_starmapnew)):  # Get logs from STAR mapping
            star_logs_new.append(iofiles.pop(0))

    # Build summary for STAR mapping:
    _build_star_summary(starmap_summary, star_logs)
    if int(nbfiles_starmapnew) > 0:
        _build_star_summary(starmap_new_summary, star_logs_new)

    ####################
    # Cutadapt Summary #
    ####################

    def _build_lines_cutadapt(file):
        with open(file, "r") as log_file:
            line = log_file.readline()
            while line != "=== Summary ===\n":
                line = log_file.readline()
            line = log_file.readline().replace("\n","")
            occurences = 1
            while not line.startswith("==="):
                match = re.match(r"(.+):\s*(.+)", line)
                if match:
                    head = match.group(1)
                    if head == "  Read 1" or head == "  Read 2":
                        if occurences < 3:
                            occurences += 1
                        else:
                            head = head.replace("  Read", "  Read")
                    value = match.group(2)
                    value = re.sub("\(.+\)", "", value)
                    value = value.replace(" bp", "").replace(",", "")
                    if head not in res_lines:
                        res_lines[head] = []
                    res_lines[head].append(value)
                line = log_file.readline().replace("\n", "")

    if build_cutadapt_summary == "True":
        sample_names = []
        res_lines = OrderedDict()
        for i in range(0, int(nbfiles_cutadapt)):
            cutadapt_log = iofiles.pop(0)
            _build_lines_cutadapt(cutadapt_log)
            sample_names.append(re.search(r"cutadapt_(.+)\.stdout", cutadapt_log).group(1))

        with open(cutadapt_summary, "w") as summary_file:
            summary_file.write("\t" + "\t".join(sample_names) + "\n")
            for head, value in res_lines.items():
                summary_file.write(head + "\t" + "\t".join(value) + "\n")


def build_rsem_stats_files(rsem_genes_tpm_summary, rsem_transcripts_tpm_summary, rsem_statistic, nbfiles_rsem_genes,
                          nbfiles_rsem_transcripts, iofiles_list, *io):
    ################
    # RSEM Summary #
    ################
    import re
    from collections import OrderedDict

    with open(iofiles_list, "r") as iofiles_l:
        iofiles = iofiles_l.read().strip("\n").split("|")

    def write_rsem_file(file, res_type, nb_files):
        result_lines = OrderedDict()
        with open(file, "w") as rsem_summary_file:
            # Header :
            if res_type == "genes":
                rsem_summary_file.write("gene_id\ttranscript_id(s)")
            elif res_type == "transcripts":
                rsem_summary_file.write("transcript_id\tgene_id")
            for i in range(0, int(nb_files)):
                rs_file = iofiles.pop(0)
                name_sample = os.path.basename(rs_file)
                name_sample = re.sub(r"(\.trim)?-Aligned\.toTranscriptome\.out\.(genes|isoforms)\.results", "",
                                     name_sample[6:])
                rsem_summary_file.write("\t" + name_sample)
                with open(rs_file, "r") as rsem_file:
                    lines = rsem_file.readlines()
                    header = lines[0].replace("\n", "").split("\t")
                    tpm_col = header.index(rsem_statistic)

                    for j in range(1, len(lines)):
                        line = lines[j].replace("\n", "").split("\t")
                        if (line[0], line[1]) not in result_lines:
                            result_lines[(line[0], line[1])] = []
                        if len(result_lines[(line[0], line[1])]) < i:
                            for k in range(0, i):
                                result_lines[(line[0], line[1])].append("")
                        result_lines[(line[0], line[1])].append(line[tpm_col])

            rsem_summary_file.write("\n")
            for h_line, v_line in result_lines.items():
                rsem_summary_file.write(h_line[0] + "\t" + h_line[1] + "\t" + "\t".join(v_line) + "\n")

    write_rsem_file(rsem_genes_tpm_summary, "genes", nbfiles_rsem_genes)
    write_rsem_file(rsem_transcripts_tpm_summary, "transcripts", nbfiles_rsem_transcripts)


class Summary(Component):

    def define_parameters(self, rsem_statistics, star_map_logs, rsem_genes_files, rsem_transcripts_files, cutadapt_logs=None,
                          star_map_new_logs=None):
        self.add_input_file_list("star_map_logs", "Logs from RNA-STAR mapping", default=star_map_logs)
        self.add_input_file_list("rsem_genes_results", "Results of RSEM for genes", default=rsem_genes_files)
        self.add_input_file_list("rsem_transcripts_results", "Results of RSEM for transctipts",
                                 default=rsem_transcripts_files)
        if star_map_new_logs:
            self.add_input_file_list("star_map_new_logs", "Logs from RNA-STAR mapping", default=star_map_new_logs)
            self.add_output_file("star_map_summary_new", "Summary of RNA-STAR mapping",
                                 filename="RNA-STAR_new-gtf_summary.tsv")
        self.add_output_file("star_map_summary", "Summary of RNA-STAR mapping", filename="RNA-STAR_summary.tsv")
        self.add_output_file_list("rsem_genes_tpm_summary", "Summary of RSEM genes results for TPM",
                                  pattern="RSEM_genes_{basename}_summary.tsv", items=rsem_statistics)
        self.add_output_file_list("rsem_transcripts_tpm_summary", "Summary of RSEM transcripts results for TPM",
                                  pattern="RSEM_transcripts_{basename}_summary.tsv", items=rsem_statistics)
        if cutadapt_logs is not None:
            self.add_input_file_list("cutadapt_logs", "Logs from cutadapt executions", default=cutadapt_logs)
            self.add_output_file("cutadapt_summary", "Summary of cutadapt", filename="Cutadapt_summary.tsv")

        self.add_parameter_list("rsem_statistics", "Rsem statistics values", default=rsem_statistics)

    def process(self):
        build_cutadapt_summary = hasattr(self, "cutadapt_logs")
        arguments = [str(len(self.star_map_logs)), "0", "0", str(build_cutadapt_summary)]
        inputs = self.star_map_logs
        outputs = [self.star_map_summary]
        if hasattr(self, "star_map_new_logs"):
            inputs += self.star_map_new_logs
            outputs.append(self.star_map_summary_new)
            arguments[1] = str(len(self.star_map_new_logs))
        if build_cutadapt_summary:
            inputs += self.cutadapt_logs
            arguments[2] = str(len(self.cutadapt_logs))
            outputs.append(self.cutadapt_summary)
        io_files = self.output_directory + "/iio_files.txt"
        with open(io_files, "w") as i_file:
            i_file.write("|".join(map(str, outputs + inputs)))
        arguments.append(io_files)
        self.add_python_execution(build_summary, cmd_format="{EXE} {ARG} {OUT} {IN}", includes=inputs,
                                  outputs=outputs, arguments=arguments, local=True)

        #Rsem summaries:
        inputs = self.rsem_genes_results + self.rsem_transcripts_results
        io_files_rsem = self.output_directory + "/iio_files_rsem.txt"
        with open(io_files_rsem, "w") as i_file:
            i_file.write("|".join(map(str, inputs)))
        arguments = [str(len(self.rsem_genes_results)), str(len(self.rsem_transcripts_results)), io_files_rsem]
        for rsem_statistic in self.rsem_statistics:
            my_arguments = [rsem_statistic] + arguments
            rsem_genes_tpm_summary = os.path.join(self.output_directory,
                                                  "RSEM_genes_" + rsem_statistic + "_summary.tsv")
            rsem_transcripts_tpm_summary = os.path.join(self.output_directory,
                                                        "RSEM_transcripts_" + rsem_statistic + "_summary.tsv")
            outputs = [rsem_genes_tpm_summary, rsem_transcripts_tpm_summary]
            self.add_python_execution(build_rsem_stats_files, cmd_format="{EXE} {OUT} {ARG} {IN}", inputs=inputs,
                                      outputs=outputs, arguments=my_arguments)
