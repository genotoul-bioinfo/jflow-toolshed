import os, re

from jflow.component import Component


def rsemcalculate_run(cmd, rsem_index, prefix, estimate_rspd, calc_ci, seed, paired_end, forward_prob, ci_memory,
                      threads, bam_file, quant_genes, quant_isoforms, stdout, stderr):
    output_prefix = quant_genes.replace(".genes.results", "")
    command = cmd + " --bam --no-bam-output"
    if estimate_rspd == "True":
        command += " --estimate-rspd"
    if calc_ci == "True":
        command += " --calc-ci"
    command += " --seed " + seed + " -p " + threads + " --ci-memory " + ci_memory
    if paired_end == "True":
        command += " --paired-end"
    if forward_prob != "Off":
        command += " --forward-prob " + forward_prob
    command += "".join([" ", bam_file, " ", os.path.join(rsem_index, prefix), " ", output_prefix, " > ", stdout, " 2>> ",
                        stderr])

    os.system(command)


class RsemCalculate(Component):

    def get_command(self):
        return "rsem-calculate-expression"

    def get_version(self):
        stdout = os.popen(self.get_exec_path("rsem-calculate-expression") + " --version").read()
        search = re.search(r"RSEM v([^\n]+)", stdout)
        if search:
            return search.group(1)
        return "Unknown"

    def get_description(self):
        return "Estimate RNA quantification (Rsem)" if self.description is None else self.description

    def define_parameters(self, bam_files, rsem_index, rsem_prefix="RSEMref", estimate_rspd=False, calc_ci=False,
                          seed=12345, paired_end=False, forward_prob=0, threads=4, ci_memory=30000):
        self.add_input_file_list("bam_files", "Bam files", default=bam_files)
        self.add_input_directory("rsem_index", "Rsem Index files directory", default=rsem_index)
        self.add_parameter("rsem_prefix", "Prefix of rsem index files", default=rsem_prefix)
        self.add_parameter("estimate_rspd", "estimate the read start position distribution (RSPD) from data", type=bool,
                           default=estimate_rspd, cmd_format="--estimate-rspd")
        self.add_parameter("calc_ci", "Calculate 95% credibility intervals and posterior mean estimates", type=bool,
                           default=calc_ci, cmd_format="--calc-ci")
        self.add_parameter("seed", "Set the seed for the random number generators used in calculating posterior mean \
                           estimates and credibility intervals", type=int, default=seed, cmd_format="--seed")
        self.add_parameter("paired_end", "Input reads are paired-end reads", type=bool, default=paired_end,
                           cmd_format="--paired-end")
        self.add_parameter("forward_prob", "Probability of generating a read from the forward strand of a \
                           transcript. Set to 1 for a strand-specific protocol where all \
                           (upstream) reads are derived from the forward strand, 0 for a \
                           strand-specific protocol where all (upstream) read are derived from \
                           the reverse strand, or 0.5 for a non-strand-specific protocol.", choices=["Off", "0", "0.5",
                           "1"], default=forward_prob, cmd_format="--forward-prob")
        self.add_parameter("ci_memory", "Maximum size (in memory, MB) of the auxiliary buffer used for computing \
                            credibility intervals (CI)", default=ci_memory, cmd_format="--ci-memory")
        self.add_parameter("threads", "Number of threads", default=threads, cmd_format="-p")
        self.add_output_file_list("quant_genes", "Genes quantification", pattern="Quant-{basename_woext}.genes.results",
                                  items=bam_files)
        self.add_output_file_list("quant_isoforms", "Isoforms quantification",
                                  pattern="Quant-{basename_woext}.isoforms.results", items=bam_files)
        self.add_output_file_list("stdout", "Standard output", pattern="rsem-calculate-{basename_woext}.stdout",
                                  items=bam_files)
        self.add_output_file_list("stderr", "Standard error", pattern="rsem-calculate-{basename_woext}.stderr",
                                  items=bam_files)

    def process(self):

        self.add_python_execution(rsemcalculate_run, cmd_format="{EXE} {ARG} {IN} {OUT}", inputs=[self.bam_files],
                                 arguments=[self.get_exec_path("rsem-calculate-expression"), self.rsem_index,
                                 self.rsem_prefix, str(self.estimate_rspd), str(self.calc_ci), str(self.seed),
                                 str(self.paired_end), self.forward_prob, str(self.ci_memory), str(self.threads)],
                                 outputs=[self.quant_genes, self.quant_isoforms, self.stdout, self.stderr],
                                 includes=[self.rsem_index], map=True)
