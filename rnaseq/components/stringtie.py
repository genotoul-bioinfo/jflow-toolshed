import os
from jflow.component import Component


class Stringtie(Component):

    def get_command(self):
        return "stringtie"

    def get_version(self):
        return os.popen(self.get_exec_path("stringtie") + " --version").read().replace("\n", "")

    def get_description(self):
        return "Gene model reconstruction (Stringtie)" if self.description is None else self.description

    def define_parameters(self, bam_files, gtf_file, coverage=2.5, minimum_gap=50, minimum_isoform_fraction=0.1,
                          threads=4, extra_parameters=None):
        self.add_input_file_list("bam_files", "Mapping reads to the genome bam files", default=bam_files)
        self.add_input_file("gtf_file", "Existing gtf file", default=gtf_file, cmd_format="-G")
        self.add_parameter("coverage", "minimum reads per bp coverage to consider for transcript assembly",
                           default=coverage, type=float, cmd_format="-c")
        self.add_parameter("minimum_gap", "gap between read mappings triggering a new bundle", default=minimum_gap,
                           type=int, cmd_format="-g")
        self.add_parameter("minimum_isoform_fraction", "minimum isoform fraction", default=minimum_isoform_fraction,
                           type=int, cmd_format="-f")
        self.add_parameter("threads", "number of threads to use", default=threads, type=int, cmd_format="-p")
        self.add_parameter("extra_parameters", "Add custom parameters to stringtie",
                           default=extra_parameters, display_name="custom parameters", cmd_format=" ")
        self.add_output_file_list("o_gtf_file", "New gtf model created", pattern="{basename_woext}-newModel.gtf",
                                  items=bam_files)
        self.add_output_file_list("stdout", "Standard output", pattern="stringtie-{basename_woext}.stdout",
                                  items=bam_files)
        self.add_output_file_list("stderr", "Standard error", pattern="stringtie-{basename_woext}.stderr",
                                  items=bam_files)

    def process(self):
        cmd = self.get_exec_path("stringtie") + " $1 -p " + str(self.threads) + " -c " + str(self.coverage) + " -g " + \
            str(self.minimum_gap) + " -f " + str(self.minimum_isoform_fraction) + " -o $2 > $3 2>> $4"
        if not self.gtf_file.is_None:
            cmd += " -G " + self.gtf_file

        self.add_shell_execution(cmd, cmd_format="{EXE} {IN} {OUT}", map=True, inputs=[self.bam_files],
                                 outputs=[self.o_gtf_file, self.stdout, self.stderr], includes=[self.gtf_file])
