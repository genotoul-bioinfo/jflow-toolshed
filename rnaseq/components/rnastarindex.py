import os
from jflow.component import Component


class RnaStarIndex(Component):

    def get_command(self):
        return "STAR"

    def get_version(self):
        return os.popen(self.get_exec_path("STAR") + " --version").read().split("_")[1].replace("\n", "")

    def get_description(self):
        return "Index genome (RNA-STAR)" if self.description is None else self.description

    def define_parameters(self, input_fasta, sjdb_overhang=None, gtf_file=None, threads=4):
        self.add_input_file("input_fasta", "Input fasta file", required=True, default=input_fasta, file_format="fasta")
        self.add_parameter("threads", "The number of threads for parallelizing the task", required=True,
                           default=threads, type=int)
        self.add_parameter("sjdb_overhang", "length of the genomic sequence around the annotated junction to be used"
                           "in constructing the splice junction database. Should be set to read length minus one",
                           default=sjdb_overhang, type=int, cmd_format="--sjdbOverhang")
        self.add_input_file("gtf_file", "the gene annotation in GTF format", default=gtf_file,
                            cmd_format="--sjdbGTFfile")
        self.add_parameter("runMode", "run mode of STAR", default="genomeGenerate", cmd_format="--runMode")  # Fixed
        # to genomeGenerate to index genome
        self.add_output_file("stdout", "Standard output", filename="rnastar_index.stdout")
        self.add_output_file("stderr", "Standard error", filename="rnastar_index.stderr")
        self.add_output_directory("genome_dir", "Genome directory", dirname="genome")

    def process(self):
        command = "mkdir $1; " + self.get_exec_path("STAR") + " --runThreadN " + \
                  str(self.threads) + " --runMode genomeGenerate" + \
                  " --genomeDir $1 --genomeFastaFiles $4" + " --outFileNamePrefix " + \
                  self.genome_dir
        if not self.gtf_file.is_None:
            command += " --sjdbOverhang " + str(self.sjdb_overhang) + " --sjdbGTFfile $5"
            inputs = [self.input_fasta, self.gtf_file]
        else:
            inputs = self.input_fasta
        command += " > $2 2>> $3"

        self.add_shell_execution(command, cmd_format="{EXE} {OUT} {IN}", inputs=inputs, outputs=[self.genome_dir,
                                 self.stdout, self.stderr])
