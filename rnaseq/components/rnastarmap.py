import os, re

from jflow.component import Component


def compute_mapping(cmd, genome_dir, threads, out_filter_type, out_sam_unmapped, out_sam_attr_ih_start,
                    out_filter_intron_motifs, bam_on_transcriptome, align_soft_clips_on_refs, out_sam_strand_field,
                    limit_bam_sort_ram, map_file_tr, map_file, stats_logs, stdout, stderr, reads_1, reads_2=None):
    command = cmd + " --genomeDir " + genome_dir + \
              " --readFilesIn " + reads_1 + " "
    if reads_2 is not None:
        command += " " + reads_2 + " "
    command += "--readFilesCommand zcat --outFileNamePrefix " + map_file.replace("Aligned.sortedByCoord.out.bam", "") + \
               " --runThreadN " + threads + \
               " --outFilterType " + out_filter_type + " --outSAMunmapped " + out_sam_unmapped + \
               " --outSAMtype BAM SortedByCoordinate --outSAMattrIHstart " + out_sam_attr_ih_start + \
               " --outFilterIntronMotifs " + out_filter_intron_motifs
    if bam_on_transcriptome == "True":
        command += " --quantMode TranscriptomeSAM"
    if align_soft_clips_on_refs == "True":
        command += " --alignSoftClipAtReferenceEnds Yes"
    else:
        command += " --alignSoftClipAtReferenceEnds No"
    command += " --outSAMstrandField " + out_sam_strand_field
    command += " --limitBAMsortRAM " + limit_bam_sort_ram
    command += " > " + stdout + " 2>> " + stderr

    os.system(command)


class RnaStarMap (Component):

    def get_command(self):
        return "STAR"

    def get_version(self):
        return os.popen(self.get_exec_path("STAR") + " --version").read().split("_")[1].replace("\n", "")

    def get_description(self):
        return "Map reads to genome (RNA-STAR)" if self.description is None else self.description

    def define_parameters(self, genome, reads_1, reads_2=[], out_filter_type="BySJout",
                          out_filter_intron_motifs="RemoveNoncanonical", align_sc_on_refs=True,
                          bam_on_transcriptome=True, threads=4, out_sam_unmapped="Within", out_sam_attr_ih_start=0,
                          out_sam_strand_field="None", limit_bam_sort_ram=16000000000):
        self.add_input_directory("genome_dir", "Index of the reference genome", required=True, default=genome)
        self.add_input_file_list("reads_1", "Which reads files should be used", required=True, default=reads_1)
        self.add_input_file_list("reads_2", "The paired reads", required=False, default=reads_2)
        self.add_parameter("threads", "The number of threads for parallelizing the task", required=True,
                           default=threads, type=int, cmd_format="--runThreadN")
        self.add_parameter("out_filter_type", "type of filtering", choices=["Normal", "BySJout"],
                           default=out_filter_type, cmd_format="--outFilterType")
        self.add_parameter("out_sam_unmapped", "output of unmapped reads in the SAM format",
                           choices=["None", "Within"], default=out_sam_unmapped, cmd_format="--outSAMunmapped")
        self.add_parameter("out_sam_attr_ih_start", "start value for the IH attribute. 0 may be required by some "
                           "downstream software, such as Cufflinks or StringTie", default=out_sam_attr_ih_start,
                           type=int, cmd_format="--outSAMattrIHstart")
        self.add_parameter("out_filter_intron_motifs", "filter alignment using their motifs", choices=["None",
                           "RemoveNoncanonical", "RemoveNoncanonicalUnannotated"], default=out_filter_intron_motifs,
                           cmd_format="--outFilterIntronMotifs")
        self.add_parameter("bam_on_transcriptome", "Build a BAM file on transcriptome, in addition to the one on"
                           "genome", type=bool, default=bam_on_transcriptome, cmd_format="--quantMode TranscriptomeSAM")
        self.add_parameter("align_soft_clips_on_refs", "allow the soft-clipping of the alignments past the end of "
                           "the chromosomes", type=bool, default=align_sc_on_refs,
                           cmd_format="--alignSoftClipAtReferenceEnds Yes")
        # To store parameter only:
        self.add_parameter("no_align_soft_clips_on_refs", "allow the soft-clipping of the alignments past the end of "
                                                       "the chromosomes", type=bool, default=not align_sc_on_refs,
                           cmd_format="--alignSoftClipAtReferenceEnds No")
        self.add_parameter("out_sam_strand_field", "Cufflinks-like strand field flag", choices=["None", "intronMotif"],
                           default=out_sam_strand_field, cmd_format="--outSAMstrandField")
        self.add_parameter("limit_bam_sort_ram", "maximum available RAM for sorting BAM", type=int,
                           default=limit_bam_sort_ram, cmd_format="--limitBAMsortRAM")
        self.add_output_file_list("stdout", "Standard output", pattern='star_map_{basename_woext}.stdout',
                                  items=reads_1)
        self.add_output_file_list("stderr", "Standard error", pattern='star_map_{basename_woext}.stderr', items=reads_1)
        self.add_output_file_list("map_files", "Map files", pattern='{basename_woext}-Aligned.sortedByCoord.out.bam',
                                  items=reads_1)
        self.add_output_file_list("stats_logs", "Raw statistics of the alignment",
                                  pattern="{basename_woext}-Log.final.out", items=reads_1)
        if bam_on_transcriptome:
            self.add_output_file_list("map_files_tr", "Map files on transcriptome",
                                      pattern='{basename_woext}-Aligned.toTranscriptome.out.bam', items=reads_1)

    def process(self):

        if len(self.reads_2) > 0:
            inputs = [self.reads_1, self.reads_2]
        else:
            inputs = self.reads_1

        arguments = [self.get_exec_path("STAR"), self.genome_dir, str(self.threads), self.out_filter_type,
                                             self.out_sam_unmapped, str(self.out_sam_attr_ih_start),
                                             self.out_filter_intron_motifs, str(self.bam_on_transcriptome),
                                             str(self.align_soft_clips_on_refs), self.out_sam_strand_field,
                                             str(self.limit_bam_sort_ram)]

        if self.bam_on_transcriptome:
            outputs = [self.map_files_tr, self.map_files, self.stats_logs, self.stdout, self.stderr]
        else:
            outputs = [self.map_files, self.stats_logs, self.stdout, self.stderr]
            arguments.append("None")

        self.add_python_execution(compute_mapping, cmd_format="{EXE} {ARG} {OUT} {IN}", inputs=inputs,
                                  outputs=outputs, includes=[self.genome_dir],
                                  arguments=arguments, map=True)
