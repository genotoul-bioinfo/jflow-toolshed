import re, os
from subprocess import Popen, PIPE
from jflow.component import Component


def cufflinks_run(cmd, threads, overlap_radius, max_intron_length, min_intron_length, library_type,
                  min_frags_per_transfrags, min_isoform_fraction, extra_parameters, gtf_file, bam_file, o_gtf_file,
                  o_gtf_skipped, stdout, stderr):

    # Remove BAM_CSOFT_CLIP:
    import pysam, re
    os.system("samtools index " + bam_file)

    bam_file_orig = pysam.AlignmentFile(bam_file, "rb")
    bam_file_trim_fname = bam_file.replace(".bam", ".trim.bam")
    bam_file_trim = pysam.AlignmentFile(bam_file_trim_fname, "wb", template=bam_file_orig)
    for read in bam_file_orig.fetch():
        read.cigarstring = re.sub(r"\d+S", "", read.cigarstring)
        bam_file_trim.write(read)
    bam_file_trim.close()
    bam_file_orig.close()

    command = cmd + " --num-threads " + threads + " --overlap-radius " + overlap_radius + " --library-type " + \
        library_type + " --max-intron-length " + max_intron_length + " --min-intron-length " + min_intron_length + \
        " --min-frags-per-transfrag " + min_frags_per_transfrags + " -F " + min_isoform_fraction
    if gtf_file != "None":
        command += " --GTF-guide " + gtf_file
    output_dir = o_gtf_file[:o_gtf_file.rfind("/")]
    command += " -o " + output_dir + " " + extra_parameters + " " + \
               bam_file_trim_fname + " > " + stdout + " 2>> " + stderr

    command += "; mv " + output_dir + "/transcripts.gtf " + o_gtf_file
    command += "; mv " + output_dir + "/skipped.gtf " + o_gtf_skipped

    os.system(command)


class Cufflinks (Component):

    def get_command(self):
        return "cufflinks"

    def get_version(self):
        cmd = [self.get_exec_path("cufflinks")]
        p = Popen(cmd, stdout=PIPE, stderr=PIPE)
        stdout, stderr = p.communicate()
        search = re.search(r"cufflinks v([^\n]+)", stderr.decode("utf-8"))
        if search:
            return search.group(1)
        return "Unknown"

    def get_description(self):
        return "Gene model reconstruction (Cufflinks)" if self.description is None else self.description

    def define_parameters(self, bam_files, overlap_radius=50, max_intron_length=300000, min_intron_length=50,
                          library_type="fr-unstranded", gtf_file=None, min_frags_per_transfrags=10,
                          min_isoform_fraction=0.1, threads=4, extra_parameters=None):
        self.add_parameter("threads", "Number of threads", type=int, default=threads, cmd_format="--num-threads")
        self.add_parameter("overlap_radius", "maximum gap size to fill between transfrags (in bp)", type=int,
                           default=overlap_radius, cmd_format="--overlap-radius")
        self.add_parameter("max_intron_length", "the maximum intron length", type=int, default=max_intron_length,
                           cmd_format="--max-intron-length")
        self.add_parameter("library_type", "library prep used for input reads", default=library_type,
                           choices=["ff-firststrand", "ff-secondstrand", "ff-unstranded", "fr-firststrand",
                                    "fr-secondstrand", "fr-unstranded", "transfrags"],
                           cmd_format="--library-type")
        self.add_parameter("extra_parameters", "Add custom parameters to cufflinks",
                           default=extra_parameters, display_name="custom parameters", cmd_format=" ")
        self.add_parameter("min_intron_length", "minimum intron size allowed in genome", default=min_intron_length,
                           type=int, cmd_format="--min-intron-length")
        self.add_parameter("min_frags_per_transfrags", "minimum number of fragments needed for new transfrags",
                           default=min_frags_per_transfrags, type=float, cmd_format="--min-frags-per-transfrag")
        self.add_parameter("min_isoform_fraction", "minimum isoforms fractions", default=min_isoform_fraction,
                           type=float, cmd_format="-F")
        self.add_input_file("gtf_file", "use reference transcript annotation to guide assembly", default=gtf_file,
                            cmd_format="--GTF-guide")
        self.add_input_file_list("bam_files", "the bam file", default=bam_files)
        self.add_output_file_list("o_gtf_file", "the built gtf file",
                                  pattern="cufflinks_{basename_woext}/{basename_woext}-transcripts.gtf",
                                  items=bam_files)
        self.add_output_file_list("o_gtf_skipped", "skipped reads while building gtf file",
                                  pattern="cufflinks_{basename_woext}/{basename_woext}-skipped.gtf", items=bam_files)
        self.add_output_file_list("stdout", "Standard output", pattern='cufflinks_{basename_woext}.stdout',
                                  items=bam_files)
        self.add_output_file_list("stderr", "Standard error", pattern='cufflinks_{basename_woext}.stderr',
                                  items=bam_files)

    def process(self):
        includes=None
        if not self.gtf_file.is_None:
            includes = self.gtf_file

        self.add_python_execution(cufflinks_run, cmd_format="{EXE} {ARG} {IN} {OUT}", inputs=self.bam_files,
                                  outputs=[self.o_gtf_file, self.o_gtf_skipped, self.stdout, self.stderr],
                                  includes=includes, arguments=[self.get_exec_path("cufflinks"), str(self.threads),
                     str(self.overlap_radius), str(self.max_intron_length), str(self.min_intron_length),
                     self.library_type, str(self.min_frags_per_transfrags), str(self.min_isoform_fraction),
                     ("'" + self.extra_parameters + "'") if not self.extra_parameters.is_None else "''",
                     str(self.gtf_file)], map=True)
