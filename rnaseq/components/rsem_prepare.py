import os, re

from jflow.component import Component


class RsemPrepare(Component):

    def get_command(self):
        return "rsem-prepare-reference"

    def get_version(self):
        stdout = os.popen(self.get_exec_path("rsem-calculate-expression") + " --version").read()
        search = re.search(r"RSEM v([^\n]+)", stdout)
        if search:
            return search.group(1)
        return "Unknown"

    def define_parameters(self, gtf_file, reference_genome, threads=4, prefix="RSEMref"):
        self.add_parameter("threads", "The number of threads for parallelizing the task", required=True,
                           default=threads, type=int, cmd_format="-p")
        self.add_input_file("gtf_file", "GTF annotations of the genome", default=gtf_file)
        self.add_input_file("reference_genome", "Reference genome in FASTA format", default=reference_genome,
                            file_format="fasta")
        self.add_output_directory("rsem_index", "Rsem index files", dirname="rsem_index")
        self.add_output_file("stdout", "Standard output", filename="rsem_index.stdout")
        self.add_output_file("stderr", "Standard error", filename="rsem_index.stderr")
        self.add_parameter("prefix", "Prefix to add to rsem index files", default=prefix)

    def process(self):
        command = "mkdir $3;" + self.get_exec_path("rsem-prepare-reference") + " -p " + str(self.threads) + \
                  " --gtf $1 $2 ${3}/" + self.prefix + " > $4 2>> $5"
        inputs=[self.reference_genome]
        if not self.gtf_file.is_None:
            inputs = [self.gtf_file] + inputs

        self.add_shell_execution(command, cmd_format="{EXE} {IN} {OUT}", inputs=inputs,
                                 outputs=[self.rsem_index, self.stdout, self.stderr])
