import re, os
from subprocess import Popen, PIPE

from jflow.component import Component


def cuffmerge_run(gtf_merged, stdout, stderr, cmd, cmd_fix, threads, gtf_files, gtf_ref, reference_genome):
    output_dir = gtf_merged[:gtf_merged.rfind("/")]

    command = cmd + " --num-threads " + threads
    if gtf_ref != "None":
        command += " --ref-gtf " + gtf_ref
    command += " --ref-sequence " + reference_genome + " -o " + output_dir + " " + gtf_files + " > " + stdout + " 2>> " + \
               stderr

    os.system(command)

    os.system(cmd_fix + " \"" + output_dir + "/merged.gtf\" \"" + gtf_merged
              + "\"")


class Cuffmerge (Component):

    def get_command(self):
        return "cuffmerge"

    def get_version(self):
        cmd = [self.get_exec_path("cuffmerge"), "--version"]
        p = Popen(cmd, stdout=PIPE, stderr=PIPE)
        stdout, stderr = p.communicate()
        search = re.search(r"merge_cuff_asms v([^\n]+)", stdout.decode("utf-8"))
        if search:
            return search.group(1)
        return "Unknown"

    def get_description(self):
        return "Merge of gene models (Cuffmerge)" if self.description is None else self.description

    def define_parameters(self, gtf_files, reference_genome, gtf_ref=None, threads=4):
        self.add_parameter("threads", "Number of threads to use", type=int, default=threads,
                           cmd_format="--num-threads")
        self.add_input_file_list("gtf_files", "Gtf files to be merged", default=gtf_files)
        self.add_input_file("reference_genome", "Reference genome fasta", file_format="fasta", default=reference_genome)
        self.add_input_file("gtf_ref", "Gtf reference file", default=gtf_ref, cmd_format="--ref-gtf")
        self.add_output_file("gtf_merged", "Output merged GTF file", filename="merged-fixed.gtf")
        self.add_output_file("stdout", "Standard output", filename="cuffmerge.stdout")
        self.add_output_file("stderr", "Standard error", filename="cuffmerge.stderr")

    def process(self):
        gtf_files = self.output_directory + "/gtf-files.txt"
        arguments = [self.get_exec_path("cuffmerge"), self.get_exec_path("fixGtf.sh"), str(self.threads), gtf_files]
        inputs = [self.reference_genome]
        if not self.gtf_ref.is_None:
            inputs = [self.gtf_ref] + inputs
        else:
            arguments.append("None")

        with open(gtf_files, "w") as inputs_gtf:
            for gtf_file in self.gtf_files:
                inputs_gtf.write(gtf_file + "\n")

        self.add_python_execution(cuffmerge_run, cmd_format="{EXE} {OUT} {ARG} {IN}", inputs=inputs,
                                  includes=self.gtf_files, outputs=[self.gtf_merged, self.stdout, self.stderr],
                                  arguments=arguments)
