#
# Copyright (C) 2015 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import gzip, re, os, logging, sys

from jflow.workflow import Workflow


class RnaSeq (Workflow):

    nb_threads = 8

    def get_description(self):
        return "A complete RNASeq workflow"

    def get_tools_description(self):
        return "\n".join(("1. Cutadapt", "2. STAR", "3. Cufflinks OR Stringtie", "4. Rsem"))

    def define_parameters(self, function="process"):

        #Protocol
        self.add_parameter("protocol", "Protocol used", display_name="RNA-seq protocol", default="illumina_stranded",
                           choices=["illumina_stranded", "other"])

        # Input files:
        self.add_multiple_parameter_list("sample", "List of sample reads", required=True, group="inputs",
                                         rules="FilesUnique", paired_columns="fastq(.gz)?[reads_1,reads_2]")
        self.add_input_file("reads_1", "Which reads files should be used", required=True, add_to="sample",
                            file_format="fastq")
        self.add_input_file("reads_2", "Which reads files should be used (paired reads)", required=False,
                            add_to="sample", file_format="fastq")
        self.add_multiple_parameter("reference_genome", "Which genome should the read being align on", group="inputs")
        self.add_input_file("fasta_file", "Fasta file of the genome (will be indexed)", file_format="fasta",
                            add_to="reference_genome", group="inputs", required=True)
        self.add_input_directory("index_directory", "STAR index directory of the pre-indexed genome",
                                 add_to="reference_genome", group="inputs")
        self.add_input_file("gtf_file", "the genes annotations in GTF format", group="inputs",
                            rules="RequiredIf?ALL[compute_gtf_model=False]")

        # Trimming:
        self.add_parameter("trim_reads", "Trim reads before analysis", group="trimming", type=bool)
        self.add_parameter("adapter_1", "Sequence of the adapter (paired data: of the first read)", group="trimming",
                           default="AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC",
                           rules="RequiredIf?ALL[trim_reads=True,protocol=other];"
                                 "DisabledIf?ANY[trim_reads=False,protocol=illumina_stranded]")
        self.add_parameter("adapter_2", "Sequence of the adapter for the second read (paired data only)",
                           group="trimming", default="AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTAGATCTCGGTGGTCGCCGTATCATT",
                           rules="RequiredIf?ALL[trim_reads=True,sample>reads_2=*,protocol=other];"
                                 "DisabledIf?ANY[trim_reads=False,protocol=illumina_stranded]")
        self.add_parameter("position_adapter", "Position of the adapter", choices=["3'", "5'", "3'/5'"],
                           display_name="Adapter's position", group="trimming",
                           rules="DisabledIf?ANY[trim_reads=False,protocol=illumina_stranded]")

        #Mapping genome:
        self.add_parameter("out_filter_type", "type of filtering", choices=["Normal", "BySJout"],
                           default="BySJout", group="mapping")
        self.add_parameter("out_filter_intron_motifs", "filter alignment using their motifs", choices=["None",
                           "RemoveNoncanonical", "RemoveNoncanonicalUnannotated"], default="RemoveNoncanonical",
                           group="mapping")
        self.add_parameter("align_soft_clips_on_refs", "allow the soft-clipping of the alignments past the end of "
                           "the chromosomes", type=bool, default=True, group="mapping")

        #Cufflinks
        self.add_parameter("compute_gtf_model", "compute the GTF model from the data", type=bool, default=False,
                           group="GTF model")
        self.add_parameter("modeling_software", "software to be used to generate new GTF model", default="cufflinks",
                           choices=["cufflinks", "stringtie"], group="GTF model",
                           rules="DisabledIf?ALL[compute_gtf_model=False]")
        self.add_parameter("overlap_radius", "maximum gap size to fill between transfrags (in bp)", type=int,
                           default="5", group="GTF model",
                           rules="DisabledIf?ANY[compute_gtf_model=False,modeling_software=stringtie]")
        self.add_parameter("min_intron_length", "minimum intron size allowed in genome", default=50, group="GTF model",
                           rules="DisabledIf?ALL[compute_gtf_model=False]", type=int)
        self.add_parameter("max_intron_length", "the maximum intron size", type=int, default=25000, group="GTF model",
                           rules="DisabledIf?ANY[compute_gtf_model=False,modeling_software=stringtie]")
        self.add_parameter("min_coverage", "minimum reads per bp coverage to consider for transcript assembly",
                           default=10, group="GTF model", rules="DisabledIf?ALL[compute_gtf_model=False]", type=int)
        self.add_parameter("min_isoform_fraction", "minimum isoforms fractions", default=0.1, type=float,
                           group="GTF model", rules="DisabledIf?ALL[compute_gtf_model=False]")
        self.add_parameter("library_type", "library prep used for input reads", default="fr-firststrand",
                           choices=["ff-firststrand", "ff-secondstrand", "ff-unstranded", "fr-firststrand",
                                    "fr-secondstrand", "fr-unstranded", "transfrags"], group="GTF model",
                           rules="DisabledIf?ANY[compute_gtf_model=False,protocol=illumina_stranded,"
                                 "modeling_software=stringtie]")
        self.add_parameter("modeling_extra_parameters", "Add extra parameters to cufflinks or stringtie",
                           display_name="extra parameters", group="GTF model",
                           rules="DisabledIf?ALL[compute_gtf_model=False]")

        #Rsem
        self.add_parameter("forward_prob", "Probability of generating a read from the forward strand of a "
                           "transcript. Set to 1 for a strand-specific protocol where all "
                           "(upstream) reads are derived from the forward strand, 0 for a "
                           "strand-specific protocol where all (upstream) read are derived from "
                           "the reverse strand, or 0.5 for a non-strand-specific protocol.",
                           choices=["Off", "0", "0.5", "1"], default="0", group="RSEM quantification estimation",
                           rules="DisabledIf?ALL[protocol=illumina_stranded]")

    @staticmethod
    def get_reads_length(reads_file):
        r_file = gzip.open(reads_file, "r");
        r_file.readline()  # ignore first line
        read = r_file.readline().decode("utf-8")
        match = re.search(r"[A-Za-z]+", read)
        if match:
            read = match.group(0)
        return len(read)

    def process(self):
        # Prepare reads:
        paired = {
            "reads1": [],
            "reads2": []
        }
        single = []
        max_read_length = 0
        has_paired = False
        has_single = False
        for smple in self.sample:
            max_read_length = max(max_read_length, self.get_reads_length(smple["reads_1"]))
            if not smple["reads_2"].is_None:
                paired["reads1"].append(smple["reads_1"])
                paired["reads2"].append(smple["reads_2"])
                max_read_length = max(max_read_length, self.get_reads_length(smple["reads_2"]))
                has_paired = True
            else:
                single.append(smple["reads_1"])
                has_single = True

        # Trimming:
        cutadapt_logs = []
        if self.trim_reads:
            if has_paired:
                cutadapt_paired = self.add_component("Cutadapt", [paired["reads1"], paired["reads2"], self.adapter_1,
                                                     self.adapter_2, self.position_adapter, max_read_length / 3],
                                                     component_prefix="paired")
                paired["reads1"] = cutadapt_paired.o_reads_1
                paired["reads2"] = cutadapt_paired.o_reads_2
                cutadapt_logs += cutadapt_paired.stdout
                if has_single:
                    cutadapt_paired.description = cutadapt_paired.get_description() + " {PAIRED}"
            if has_single:
                cutadapt_single = self.add_component("Cutadapt", [single, None, self.adapter_1,
                                                     None, self.position_adapter, max_read_length / 3],
                                                     component_prefix="single")
                single = cutadapt_single.o_reads_1
                cutadapt_logs += cutadapt_single.stdout
                if has_paired:
                    cutadapt_single.description = cutadapt_single.get_description() + " {SINGLE}"

        reindex = False
        if not self.reference_genome["index_directory"].is_None and \
                not os.path.isfile(os.path.join(self.reference_genome["index_directory"], "transcriptInfo.tab")) \
                and not self.compute_gtf_model:
            message = "WARN: transcriptome file missing on index directory. Genome will be reindexed!"
            print(message, file=sys.stderr)
            logging.getLogger("wf." + str(self.id))
            logging.warning(message)
            reindex = True

        if self.reference_genome["index_directory"].is_None or reindex:
            #Indexing reference genome :
            rnastarindex = self.add_component("RnaStarIndex", [self.reference_genome["fasta_file"], max_read_length - 1,
                                              self.gtf_file, self.nb_threads])
            genome_dir = rnastarindex.genome_dir
        else:
            genome_dir = self.reference_genome["index_directory"]

        # Mapping:
        star_logs = []
        if has_paired:
            rnastarmap_paired = self.add_component("RnaStarMap", [genome_dir, paired["reads1"], paired["reads2"],
                                            self.out_filter_type if self.compute_gtf_model else "Normal",
                                            self.out_filter_intron_motifs if self.compute_gtf_model else "None",
                                            self.align_soft_clips_on_refs, not self.gtf_file.is_None and
                                            not self.compute_gtf_model, self.nb_threads], component_prefix="paired")
            star_logs += rnastarmap_paired.stats_logs
            rnastarmap_paired_new = rnastarmap_paired
            if has_single:
                rnastarmap_paired.description = rnastarmap_paired.get_description() + " {PAIRED}"
        if has_single:
            rnastarmap_single = self.add_component("RnaStarMap", [genome_dir, single, None,
                                            self.out_filter_type if self.compute_gtf_model else "Normal",
                                            self.out_filter_intron_motifs if self.compute_gtf_model else "None",
                                            self.align_soft_clips_on_refs, not self.gtf_file.is_None and
                                            not self.compute_gtf_model, self.nb_threads], component_prefix="single")
            star_logs += rnastarmap_single.stats_logs
            rnastarmap_single_new = rnastarmap_single
            if has_paired:
                rnastarmap_single.description = rnastarmap_single.get_description() + " {SINGLE}"

        final_gtf = self.gtf_file

        # Cufflinks:
        star_logs_new = []
        if self.compute_gtf_model:
            gtf_files = []
            if has_paired:
                if self.modeling_software == "cufflinks":
                    cufflinks_paired = self.add_component("Cufflinks", [rnastarmap_paired.map_files, self.overlap_radius,
                                                          self.max_intron_length, self.min_intron_length, self.library_type,
                                                          self.gtf_file, self.min_coverage, self.min_isoform_fraction,
                                                          self.nb_threads, self.modeling_extra_parameters],
                                                          component_prefix="paired")
                elif self.modeling_software == "stringtie":
                    cufflinks_paired = self.add_component("Stringtie",
                                                          [rnastarmap_paired.map_files, self.gtf_file,
                                                           self.min_coverage, self.min_intron_length,
                                                           self.min_isoform_fraction,
                                                           self.nb_threads, self.modeling_extra_parameters],
                                                          component_prefix="paired")
                gtf_files += cufflinks_paired.o_gtf_file
                if has_single:
                    cufflinks_paired.description = cufflinks_paired.get_description() + " {PAIRED}"
            if has_single:
                if self.modeling_software == "cufflinks":
                    cufflinks_single = self.add_component("Cufflinks", [rnastarmap_single.map_files, self.overlap_radius,
                                                          self.max_intron_length, self.min_intron_length, self.library_type,
                                                          self.gtf_file, self.min_coverage, self.min_isoform_fraction,
                                                          self.nb_threads, self.modeling_extra_parameters],
                                                          component_prefix="single")
                elif self.modeling_software == "stringtie":
                    cufflinks_single = self.add_component("Stringtie",
                                                          [rnastarmap_single.map_files, self.gtf_file,
                                                           self.min_coverage, self.min_intron_length,
                                                           self.min_isoform_fraction,
                                                           self.nb_threads, self.modeling_extra_parameters],
                                                          component_prefix="single")
                gtf_files += cufflinks_single.o_gtf_file
                if has_paired:
                    cufflinks_single.description = cufflinks_single.get_description() + " {SINGLE}"

            # Cuffmerge (only if several bam files):
            cuffmerge = self.add_component("Cuffmerge", [gtf_files, self.reference_genome["fasta_file"],
                                           self.gtf_file, self.nb_threads])
            final_gtf = cuffmerge.gtf_merged

            # Re-Indexing & re-Mapping with the new GTF:
            rnastarindex = self.add_component("RnaStarIndex",
                                              [self.reference_genome["fasta_file"], max_read_length - 1, final_gtf,
                                               self.nb_threads], component_prefix="new_gtf")
            if has_paired:
                rnastarmap_paired_new = self.add_component("RnaStarMap",
                                                    [rnastarindex.genome_dir, paired["reads1"], paired["reads2"],
                                                     self.out_filter_type, self.out_filter_intron_motifs,
                                                     self.align_soft_clips_on_refs, True, self.nb_threads],
                                                    component_prefix="paired_new_gtf")
                rnastarmap_paired_new.description = "Re-map reads to the genome, with new GTF (RNA-STAR)"
                star_logs_new += rnastarmap_paired_new.stats_logs
                if has_single:
                    rnastarmap_paired_new.description += " {PAIRED}"
            if has_single:
                rnastarmap_single_new = self.add_component("RnaStarMap",
                                                           [rnastarindex.genome_dir, single, None,
                                                            self.out_filter_type, self.out_filter_intron_motifs,
                                                            self.align_soft_clips_on_refs, True, self.nb_threads],
                                                           component_prefix="single_new_gtf")
                rnastarmap_single_new.description = "Re-map reads to the genome, with new GTF (RNA-STAR)"
                star_logs_new += rnastarmap_single_new.stats_logs
                if has_paired:
                    rnastarmap_single_new.description += " {SINGLE}"


        # RSEM prepare:
        rsemprepare = self.add_component("RsemPrepare", [final_gtf, self.reference_genome["fasta_file"],
                                                         self.nb_threads])

        # RSEM calculate:
        rsem_logs_genes = []
        rsem_logs_isoforms = []
        if has_paired:
            rsemcalculate_paired = self.add_component("RsemCalculate", [rnastarmap_paired_new.map_files_tr,
                                                      rsemprepare.rsem_index, rsemprepare.prefix, True, True, 12345,
                                                      True, self.forward_prob, self.nb_threads],
                                                      component_prefix="paired")
            rsem_logs_genes += rsemcalculate_paired.quant_genes
            rsem_logs_isoforms += rsemcalculate_paired.quant_isoforms
            if has_single:
                rsemcalculate_paired.description = rsemcalculate_paired.get_description() + " {PAIRED}"
        if has_single:
            rsemcalculate_single = self.add_component("RsemCalculate", [rnastarmap_single_new.map_files_tr,
                                                      rsemprepare.rsem_index, rsemprepare.prefix, True, True, 12345,
                                                      False, self.forward_prob, self.nb_threads],
                                                      component_prefix="single")
            rsem_logs_genes += rsemcalculate_single.quant_genes
            rsem_logs_isoforms += rsemcalculate_single.quant_isoforms
            if has_paired:
                rsemcalculate_single.description = rsemcalculate_single.get_description() + " {SINGLE}"

        # Summary statistics:
        logs_files = [star_logs, rsem_logs_genes, rsem_logs_isoforms]
        if self.trim_reads:
            logs_files.append(cutadapt_logs)
        else:
            logs_files.append(None)
        if self.compute_gtf_model:
            logs_files.append(star_logs_new)
        stats = self.add_component("Summary", [["expected_count", "TPM", "FPKM"]] + logs_files)

    def show_outputs(self, outputs, key):
        for output in outputs[key]:
            if not output.endswith(".stdout") and not output.endswith(".stderr"):
                print("    * " + outputs[key][output])
