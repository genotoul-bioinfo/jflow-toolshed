/**
 * Created by fcabanettes on 09/01/17.
 */

!function ($) {
    $(window).on("load", function () {
        init();
    });

    var init = function() {
        build_sommaire();
    }

    var build_sommaire = function() {
        var nb_1 = 1;
        var nb_2 = 1;
        var sommaire = "<ul id=\"sommaire\">";
        var in_sublist = false;
        $("h2[id!=sommaire_title][id!=subtitle],h3").each(function() {
            if ($(this).prop("tagName") == "H2") {
                if (in_sublist) {
                    sommaire += "</ul>";
                    nb_2 = 1; //Reinitialize h3 numbers
                    in_sublist = false;
                }
                var id = nb_1.toString();
                $(this).attr("id", id);
                sommaire += "<li><a href='#" + id + "'>" + $(this).html() + "</a></li>";
                nb_1++;
            }
            else if ($(this).prop("tagName") == "H3") {
                if (!in_sublist) {
                    sommaire += "<ul class=\"sub-list1\">";
                }
                in_sublist = true;
                var id = nb_1.toString() + "." + nb_2.toString();
                $(this).attr("id", id);
                sommaire += "<li><a href='#" + id + "'>" + $(this).html() + "</a></li>";
                nb_2++;
            }
        });

        $("div#sommaire_div").html(sommaire);
    }
}(window.jQuery)