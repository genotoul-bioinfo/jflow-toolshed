<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Jflow RNA-SEQ workflow User Manual</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <script type="application/javascript" src="js/jquery-3.1.1.min.js"></script>
    <script type="application/javascript" src="js/build_page.js"></script>
</head>
<body>
<h1>Jflow RNA-SEQ workflow User Manual</h1>
<p id="subtitle">&ndash; command line version &ndash;</p>

<div id="author_info">
    <strong>Written by:</strong> Floréal Cabanettes &lt;<a href="mailto:floreal.cabanettes@inra.fr">floreal.cabanettes@inra.fr</a>&gt;<br/>
    <strong>Version of the documentation:</strong> 1.0 (18-01-2017)</p>
</div>

<div class="summary_document">
    <p>This document explains how to use the RNA-seq pipeline through Jflow on the Genotoul cluster. All available options
    availables are described below, illustrated by some examples.</p>
</div>

<p id="sommaire_title">Summary</p>
<div id="sommaire_div"></div>

<div id="trouver_jflow">
    <h2>Where to find Jflow on the genotoul cluster?</h2>
    <p>Jflow is available on the genotoul cluster at this URL:<br/>
        <em>/usr/local/bioinfo/src/Jflow/jflow</em>.</p>
</div>

<div id="parametres_workflow">
    <h2>Workflow parameters</h2>
    <div id="type_protocol">
        <h3>Sequencing protocol</h3>
        <p>Default protocol is <em>illumina_stranded</em>. It's the one used on the platform. You can also use another protocol, by adding this parameter:<br/>
            <em class="code">--protocol other</em>.</p>
        <p>This parameter allows you to add advanced settings of each component of the workflow (see below).</p>
    </div>
    <div id="input_files">
        <h3>Input files</h3>
        <p>The workflow requires several input files:</p>
        <ul>
            <li>reads in <strong>fastq.gz</strong> format (single or paired)</li>
            <li>a reference genome in <strong>fasta</strong> format (and eventually an already STAR-indexed genome directory)</li>
            <li>optionally a genome model in <strong>gtf</strong> format.</li>
        </ul>
        <h4>Reads</h4>
        <p>Use the <em class="code">--sample</em> parameter to set reads. For each sample, set the <em class="code">reads-1</em> sub-parameter, and if reads are paired, also the <em class="code">reads-2</em> sub-parameter.
            Repeat <em>--sample</em> parameter for each sample.</p>
        <p>Examples:</p>
        <ul>
            <li>One paired end sample:<br/><em class="code">--sample reads-1=my_file_reads_1.fastq.gz reads-2=my_file_reads_2.fastq.gz</em></li>
            <li>One single end sample:<br/><em class="code">--sample reads-1=my_file_single.fastq.gz</em></li>
            <li>Two samples, one paired end, the other single end:<br/>
                <em class="code">--sample reads-1=my_file_r1.fastq.gz reads-2=my_file_r2.fastq.gz --sample reads-1=my_file_single.fastq.gz</em></li>
        </ul>
        <h4>Reference genome</h4>
        <p>Reference genome is set with the <em class="code">--reference-genome</em> parameter. Set the fasta file with the <em class="code">fasta-file</em> sub-parameter.
            Eventually, add also the <em class="code">index-directory</em> sub-parameter to set the directory containing the STAR-indexed genome (which will then not be re-indexed).</p>
        <p>Examples:</p>
        <ul>
            <li>Only fasta:<br/><em class="code">--reference-genome fasta-file=my__fasta_file.fa</em></li>
            <li>With indexed genome:<br/><em class="code">--reference-genome fasta-file=my_fasta_file.fa index-directory=/path/to/the/index/of/the/genome</em></li>
        </ul>
        <h4>Gene model</h4>
        <p>Add the <em class="code">--gtf-file</em> parameter to set a gene model file. This parameter is optional if you compute a new gene model (see below).</p>
        <p>Example: <em class="code">--gtf-file my_file.gtf</em></p>
    </div>
    <div id="trimming">
        <h3>Trimming</h3>
        <p>To trim your reads before alignment, use the <em class="code">--trim-reads</em> parameter.</p>
        <p><strong class="important">Warning:</strong> If you use the <em>illumina_stranded</em> protocol (default), additionnal parameters below are not expected.</p>
        <h4>Additional parameters</h4>
        <p><strong>--adapter-1</strong></p>
        <p class="indent-1">
            Adapter sequence (adapter 1 if reads are paired end).<br/>
            <em>Required:</em> <span class="yes">yes</span>.<br/>
            <em>Example:</em> <em class="code">--adapter-1 AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC</em></p>
        <p><strong>--adapter-2</strong></p>
        <p class="indent-1">
            Adapter 2 sequence (if paired end reads).<br/>
            <em>Required:</em> <span class="yes">yes, if paired end reads are given</span>.<br/>
            <em>Example:</em> <em class="code">--adapter-2 AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTAGATCTCGGTGGTCGCCGTATCATT</em></p>
        <p><strong>--position-adapter</strong></p>
        <p class="indent-1">
            Adapter position.<br/>
            <em>Available choices:</em> 3', 5', 3'/5'<br/>
            <em>Default:</em> 3'<br/>
            <em>Required:</em> <span class="no">no</span><br/>
            <em>Example:</em> <em class="code">--position-adapter 5'</em>
        </p>
        <h4>Example</h4>
        <em class="code">--trim-reads --adapter-1 AGATCGGAAGAGCACA --adapter-2 AGATCGGAAGAGCGTCGTGTAGGGAAAGA</em>
        <h4>Software and fixed parameters</h4>
        <p><strong>Software used:</strong> <a href="https://github.com/marcelm/cutadapt">cutadapt</a> [VERSION: 1.8.3]</p>
        <p><strong>Fixed parameter:</strong></p>
        <ul><li>Max read length filter: equals to 1/3 of the max read length among all samples.</li></ul>
    </div>
    <div id="mapping">
        <h3>Mapping reads on reference genome</h3>
        <p>The pipeline use several parameters to customize the way to map the reads onto the genome.</p>
        <p><strong>--out-filter-type</strong></p>
        <p class="indent-1">
            Filter type.<br/>
            <em>Available choices:</em> BySJout, Normal<br/>
            <em>Default:</em> BySJout<br/>
            <em>Required:</em> <span class="no">no</span><br/>
            <em>Example:</em> <em class="code">--out-filter-type Normal</em>
        </p>
        <p><strong>--out-filter-intron-motifs</strong></p>
        <p class="indent-1">
            Filter alignment using their motifs.<br/>
            <em>Usage:</em> simply add the parameter to enable it.<br/>
            <em>Required:</em> <span class="no">no</span>
        </p>
        <p><strong>--align-soft-clips-on-refs</strong></p>
        <p class="indent-1">
            Allow the soft-clipping of the alignments past the end of the chromosomes.<br/>
            <em>Usage:</em> simply add the parameter to enable it.<br/>
            <em>Required :</em> <span class="no">no</span>
        </p>
        <p><strong>--out-sam-strand-field</strong></p>
        <p class="indent-1">
            Cufflinks-like strand field flag.<br/>
            <em>Available choices:</em> None, intronMotif <br/>
            <em>Explanations:</em> None : Not used. intronMotif : strand derived from the intron motif.
            Reads with inconsistent and/or non-canonical introns are filtered out.<br/>
            <em>Default:</em> None<br/>
            <em>Required:</em> <span class="no">no</span><br/>
            <em>Example:</em> <em class="code">--out-sam-strand-field intronMotif</em>
        </p>
        <h4>Software and fixed parameters</h4>
        <p><strong>Software used:</strong> <a href="https://github.com/alexdobin/STAR">RNA-STAR</a> [VERSION: 2.5.2b]</p>
        <p><strong>Fixed parameter:</strong></p>
        <ul><li>On genome index: <ul>
            <li>--sjdbOverhang: fixed to max read length among all samples minus one.</li>
        </ul></li>
        <li>On mapping:
        <ul>
            <li>--outSAMunmapped Within (doc : output of unmapped reads in the SAM format, None or Within SAM file)</li>
            <li>--outSAMattrIHstart 0 (doc : start value for the IH attribute. Setting it to 0 may be required for compatibility with downstream software such as Cufflinks or StringTie)</li>
            <li>--outSAMstrandField None (doc Cufflinks-like strand field flag)</li>
        </ul></li></ul>
    </div>
    <div id="modeling">
        <h3>(Re)compute gene model</h3>
        <p>Add the <em class="code">--compute-gtf-model</em> parameter to compute or recompute a gene model. If a gene model is already given,
        it will lead the computing of the new one.</p>
        <p><strong class="important">Warning :</strong> This parameter is required if none gene model is given (see above).</p>
        <h4>Software</h4>
        <p>You can choose one among two softwares packages:</p>
        <ul>
            <li>Cufflinks (default)</li>
            <li>Stringtie</li>
        </ul>
        To use Stringtie, add the parameter:<br/>
        <em class="code">--modeling-software stringtie</em>
        <h4>Shared parameters</h4>
        <p>Several parameters are available for both Cufflinks and Stringtie.</p>
        <p><strong>--min-intron-length</strong></p>
        <p class="indent-1">
            Minimum intron size allowed in genome.<br/>
            <em>Default:</em> 50<br/>
            <em>Required:</em> <span class="no">true</span><br/>
            <em>Example:</em> <em class="code">--min-intron-length 100</em>
        </p>
        <p><strong>--min-coverage</strong></p>
        <p class="indent-1">
            Cufflinks: minimum number of fragments needed for new transfrags.<br/>
            StringTie: minimum input transcript coverage to include in the merge.<br/>
            <em>Default:</em> 10<br/>
            <em>Required:</em> <span class="no">no</span><br/>
            <em>Example:</em> <em class="code">--min-coverage 2.5</em>
        </p>
        <p><strong>--min-isoform-fraction</strong></p>
        <p class="indent-1">
            Suppress transcripts below this abundance level.<br/>
            <em>Minimum:</em> 0.0<br/>
            <em>Maximum:</em> 1.0<br/>
            <em>Default:</em> 0.1<br/>
            <em>Required:</em> <span class="no">no</span><br/>
            <em>Example:</em> <em class="code">--min-isoform-fraction 0.5</em>
        </p>
        <h4>Cufflinks specific parameters</h4>
        <p>Some parameters are only available for Cufflinks.</p>
        <p><strong>--max-intron-length</strong></p>
        <p class="indent-1">
            Ignore alignments with gaps longer than this.<br/>
            <em>Default:</em> 25000<br/>
            <em>Required:</em> <span class="no">no</span><br/>
            <em>Example:</em> <em class="code">--max-intron-length 30000</em>
        </p>
        <p><strong>--overlap-radius</strong></p>
        <p class="indent-1">
            Maximum gap size to fill between transfrags (in bp).<br/>
            <em>Default:</em> 5<br/>
            <em>Required:</em> <em class="no">no</em><br/>
            <em>Example:</em> <em class="code">--overlap-radius 8</em>
        </p>
        <p><strong>--library-type</strong></p>
        <p class="indent-1">
            Library prep used for input reads.<br/>
            <strong class="important">Warning:</strong> If you use the illumina_stranded protocol (default), this parameter is already fixed.<br/>
            <em>Available choices:</em> ff-firststrand, ff-secondstrand, ff-unstranded, fr-firststrand, fr-secondstrand, fr-unstranded, transfrags<br/>
            <em>Default:</em> fr-firststrand<br/>
            <em>Required:</em> <span class="no">no</span><br/>
            <em>Example:</em> <em class="code">--library-type ff-unstranded</em>
        </p>
        <h4>Advanced parameters</h4>
        <p>You can add any other parameter to Cufflinks or Stringtie. Set them as a string chain with the parameter:<br/>
            <em class="code">--modeling-extra-parameters</em>.</p>
        <p>Please refer to the Cufflinks or Stringtie documentation to know all available parameters.</p>
        <p>Example: <em class="code">--modeling-extra-parameters "--max-mle-iterations 10000 --total-hits-norm FALSE"</em>.</p>
        <h4>Software and fixed parameters</h4>
        <p><strong>Software used:</strong> <a href="http://cole-trapnell-lab.github.io/cufflinks/">Cufflinks</a> [VERSION: 2.2.1] or
            <a href="https://ccb.jhu.edu/software/stringtie/">Stringtie</a> [VERSION: 1.2.1] (depending of parameters used, see above)</p>
        <p><strong>Fixed parameter:</strong> None</p>
    </div>
    <div id="quantification">
        <h3>Quantification</h3>
        <h4>Parameters for the protocol Other</h4>
        <p>If you select protocol <em>other</em> (see above), you have access to 1 parameter:</p>
        <p><strong>--forward-prob</strong></p>
        <p class="indent-1">
            Probability of generating a read from the forward strand of a transcript.<br/>Set to 1 for a strand-specific protocol where all
            (upstream) reads are derived from the forward strand.<br/>Set to 0 for a
            strand-specific protocol where all (upstream) read are derived from
            the reverse strand.<br/>Set to 0.5 for a non-strand-specific protocol.<br/>
            <em>Available choices:</em> 1, 0, 0.5<br/>
            <em>Default:</em> 0<br/>
            <em>Required:</em> <span class="no">no</span><br/>
            <em>Example:</em> <em class="code">--forward-prob 0.5</em>
        </p>
        <h4>Quantification summary</h4>
        <p>This workflow will generate a TSV file which summarizes the quantification results by gene and by transcript for each sample. For this summary, you must
        choose one statistic among:</p>
        <ul>
            <li>expected_count (default)</li>
            <li>TPM</li>
            <li>FPKM</li>
        </ul>
        <p>To use a non-default statistic, use the option:<br/>
        <em class="code">--rsem-statistic</em>.</p>
        <p>Example: <em class="code">--rsem-statistic TPM</em></p>
        <h4>Software and fixed parameters</h4>
        <p><strong>Software used:</strong> <a href="https://deweylab.github.io/RSEM/">RSEM</a> [VERSION: 1.3.0]</p>
        <p><strong>Fixed parameter:</strong></p>
        <ul>
            <li>--estimate-rspd (doc : set this option if you want to estimate the read start position distribution (RSPD) from data. Otherwise, RSEM will use a uniform RSPD)</li>
            <li>--calc-ci (doc : calculate 95% credibility intervals and posterior mean estimates. The credibility level can be changed by setting '--ci-credibility-level')</li>
            <li>--seed 12345 (doc : Set the seed for the random number generators used in calculating posterior mean estimates and credibility intervals. The seed must be a non-negative 32 bit interger.)</li>
            <li>--forward-prob 0 (doc : Probability of generating a read from the forward strand of a transcript. Set to 1 for a strand-specific protocol where all (upstream) reads are derived from the forward strand, 0 for a strand-specific protocol where all (upstream) read are derived from the reverse strand, or 0.5 for a non-strand-specific protocol.)</li>
        </ul>
    </div>
</div>

<div id="launch_workflow">
    <h2>Launch workflow</h2>
    <p>Launch the workflow using the command:</p>
    <p><strong>/usr/local/bioinfo/src/Jflow/jflow/bin/jflow_cli.py rnaseq &lt;parameters...&gt;</strong></p>
    <p>Replacing <em>&lt;parameters...&gt;</em> by the parameters described above.</p>
    <h3>Example</h3>
    <p>We have 3 samples to analyse:</p>
    <ul>
        <li>Sample 1 (paired end) : <em>sample1.R1.fastq.gz, sample1.R2.fastq.gz</em></li>
        <li>Sample 2 (single end) : <em>sample2.fastq.gz</em></li>
        <li>Sample 3 (paired end) : <em>sample3.R1.fastq.gz, sample3.R2.fastq.gz</em></li>
    </ul>
    <p>Also, a fasta file of the reference genome (we have not already indexed it):</p>
    <ul>
        <li>Reference genome : <em>reference.fa</em></li>
    </ul>
    <p>And a gene model:</p>
    <ul>
        <li>Gene model: <em>reference.gtf</em></li>
    </ul>
    <p>All files above are localized in the working directory. The sequencing has been performed on our local sequencing plateform, therefore we use the default protocol <em>illumina_stranded</em>.
    We want to trim reads and compute a new GTF model (with cufflinks, like default). As possible, we will keep the default parameters of the workflow.
    We launch, so, the following command:</p>
    <p class="align-left"><em class="code">/usr/local/bioinfo/src/Jflow/jflow/bin/jflow_cli.py rnaseq --sample reads-1=sample1.R1.fastq.gz
        reads-2=sample1.R2.fastq.gz --sample reads-1=sample2.fastq.gz --sample reads-1=sample3.R1.fastq.gz
        reads-2=sample3.R2.fastq.gz --reference-genome fasta-file=reference.fa --gtf-file reference.gtf --trim-reads --compute-gtf-model</em></p>
</div>
<div id="results">
    <h2>Results</h2>
    <p>Results of the last launched workflow are inside the folder with the highest workflow-id in directory name, inside the folder <em>jflow_results/rnaseq</em> of your <em>work</em> directory.
        The examples shown here are results of the workflow described in the section above. Sample file names are based on the reads-1 file names.</p>
    <p>At the end of the workflow execution, a recap show the resulting files for each component:</p>
    <pre>
###########
# Results #
###########

    [...]</pre>
    <p>If you mix paired and single reads, results below are separated for single and paired data.</p>
    <div id="trimming_2">
        <h3>Trimming</h3>
        <p>Results are inside the folders <em>Cutadapt_single</em> and/or <em>Cutadapt_paired</em>. There are as many files as fastq files given as inputs. Each file
        is the trimmed original file.</p>
        <pre><span class="underline">1. Trimming (Cutadapt) {PAIRED}:</span>
  - /work/fcabanettes/jflow_results/rnaseq/wf000011/<strong>Cutadapt_paired/</strong>sample3.R1.fastq<strong>.trim.fastq.gz</strong>
  - /work/fcabanettes/jflow_results/rnaseq/wf000011/<strong>Cutadapt_paired/</strong>sample1.R1.fastq<strong>.trim.fastq.gz</strong>
  - /work/fcabanettes/jflow_results/rnaseq/wf000011/<strong>Cutadapt_paired/</strong>sample1.R2.fastq<strong>.trim.fastq.gz</strong>
  - /work/fcabanettes/jflow_results/rnaseq/wf000011/<strong>Cutadapt_paired/</strong>sample3.R2.fastq<strong>.trim.fastq.gz</strong>

<span class="underline">2. Trimming (Cutadapt) {SINGLE}:</span>
  - /work/fcabanettes/jflow_results/rnaseq/wf000011/Cutadapt_single/sample2.fastq.trim.fastq.gz
        </pre>
    </div>
    <div id="indexing_2">
        <h3>Reference genome index</h3>
        <p>If you don't give a pre-indexed genome, the <em>RnaStarIndex_default</em> folder contains the STAR reference genome index folder.</p>
        <pre><span class="underline">3. Index genome (RNA-STAR):</span>
  - /work/fcabanettes/jflow_results/rnaseq/wf000011<strong>/RnaStarIndex_default/genome</strong></pre>
    </div>
    <div id="mapping_2">
        <h3>Mapping reads of reference genome</h3>
        <p>Mapping bam files are located into the <em>RnaStarMap_single</em> and/or <em>RnaStarMap_paired</em> folders.
        For each sample, these folders contain a log file (*-Log.final.out) including STAR mapping statistics.</p>
        <pre><span class="underline">4. Map reads to genome (RNA-STAR) {PAIRED}:</span>
  - /work/fcabanettes/jflow_results/rnaseq/wf000011/<strong>RnaStarMap_paired/</strong>sample3.R1.fastq.trim.fastq<strong>-Log.final.out</strong>
  - /work/fcabanettes/jflow_results/rnaseq/wf000011/<strong>RnaStarMap_paired/</strong>sample3.R1.fastq.trim.fastq-Aligned.sortedByCoord.out<strong>.bam</strong>
  - /work/fcabanettes/jflow_results/rnaseq/wf000011/<strong>RnaStarMap_paired/</strong>sample1.R1.fastq.trim.fastq<strong>-Log.final.out</strong>
  - /work/fcabanettes/jflow_results/rnaseq/wf000011/<strong>RnaStarMap_paired/</strong>sample1.R1.fastq.trim.fastq-Aligned.sortedByCoord.out<strong>.bam</strong>

<span class="underline">5. Map reads to genome (RNA-STAR) {SINGLE}:</span>
  - /work/fcabanettes/jflow_results/rnaseq/wf000011/<strong>RnaStarMap_single/</strong>sample2.fastq.trim.fastq<strong>-Log.final.out</strong>
  - /work/fcabanettes/jflow_results/rnaseq/wf000011/<strong>RnaStarMap_single/</strong>sample2.fastq.trim.fastq-Aligned.sortedByCoord.out<strong>.bam</strong></pre>
    </div>
    <div id="modeling_2">
        <h3>(Re)compute gene model</h3>
        <p>If the <em class="code">--compute-gtf-model</em> parameter was set, you will find in the <em>Cufflinks_single</em>
            and/or <em>Cufflinks_paired</em> folders (or <em>Stringtie_single</em> and/or <em>Stringtie_paired</em> if you choose Stringtie) the new sample gene models.</p>
        <pre><span class="underline">6. Gene model reconstruction (Cufflinks) {PAIRED}:</span>
  - /work/fcabanettes/jflow_results/rnaseq/wf000011/<strong>Cufflinks_paired/cufflinks_</strong>sample3.R1.fastq.trim.fastq-Aligned.sortedByCoord.out/sample3.R1.fastq.trim.fastq-Aligned.sortedByCoord.out-transcripts<strong>.gtf</strong>
  - /work/fcabanettes/jflow_results/rnaseq/wf000011/<strong>Cufflinks_paired/cufflinks_</strong>sample1.R1.fastq.trim.fastq-Aligned.sortedByCoord.out/sample1.R1.fastq.trim.fastq-Aligned.sortedByCoord.out-skipped<strong>.gtf</strong>
  - /work/fcabanettes/jflow_results/rnaseq/wf000011/<strong>Cufflinks_paired/cufflinks_</strong>sample1.R1.fastq.trim.fastq-Aligned.sortedByCoord.out/sample1.R1.fastq.trim.fastq-Aligned.sortedByCoord.out-transcripts<strong>.gtf</strong>
  - /work/fcabanettes/jflow_results/rnaseq/wf000011/<strong>Cufflinks_paired/cufflinks_</strong>sample3.R1.fastq.trim.fastq-Aligned.sortedByCoord.out/sample3.R1.fastq.trim.fastq-Aligned.sortedByCoord.out-skipped<strong>.gtf</strong>

<span class="underline">7. Gene model reconstruction (Cufflinks) {SINGLE}:</span>
  - /work/fcabanettes/jflow_results/rnaseq/wf000011/<strong>Cufflinks_single/cufflinks_</strong>sample2.fastq.trim.fastq-Aligned.sortedByCoord.out/sample2.fastq.trim.fastq-Aligned.sortedByCoord.out-transcripts<strong>.gtf</strong>
  - /work/fcabanettes/jflow_results/rnaseq/wf000011/<strong>Cufflinks_single/cufflinks_</strong>sample2.fastq.trim.fastq-Aligned.sortedByCoord.out/sample2.fastq.trim.fastq-Aligned.sortedByCoord.out-skipped<strong>.gtf</strong>
        </pre>
        <p>Also, you will find into the <em>Cuffmerge_default</em> folder the <em>merged-fixed.gtf</em> file, which corresponds to the fusion of all the sample models,
            which will be used for the quantification step.</p>
        <pre><span class="underline">8. Merge of gene models (Cuffmerge):</span>
  - /work/fcabanettes/jflow_results/rnaseq/wf000011/<strong>Cuffmerge_default/merged-fixed.gtf</strong></pre>
    </div>
    <div id="indexing_2_new_gtf">
        <h3>Reference genome reindex with new gene model</h3>
        <p>Even if you give a pre-indexed genome, if you compute a new gene model (see above), the genome will be reindexed with it. Results are into the <em>RnaStarIndex_new_gtf</em>
        folder.</p>
        <pre><span class="underline">9. Index genome (RNA-STAR):</span>
  - /work/fcabanettes/jflow_results/rnaseq/wf000011/<strong>RnaStarIndex_new_gtf/genome</strong></pre>
    </div>
    <div id="mapping_2_new_gtf">
        <h3>Remapping reads onto the reference genome</h3>
        <p>If you compute a new gene model (see above), reads have been remapped using the new genome index. The alignment files are
        localised into the <em>RnaStarMap_single_new_gtf</em> and/or <em>RnaStarMap_paired_new_gtf</em> folders. Also, there is for each sample another bam: mapping
        reads onto the transcriptome (for the quantification).</p>
        <p><strong>Note:</strong> if you don't compute a new gene model, these bam are present into the folder of the first mapping.</p>
        <pre>10. Re-map reads to the genome, with new GTF (RNA-STAR) {PAIRED}:
  - /work/fcabanettes/jflow_results/rnaseq/wf000011/<strong>RnaStarMap_paired_new_gtf/</strong>sample1.R1.fastq.trim.fastq-Aligned.sortedByCoord.out<strong>.bam</strong>
  - /work/fcabanettes/jflow_results/rnaseq/wf000011/<strong>RnaStarMap_paired_new_gtf/</strong>sample1.R1.fastq.trim.fastq-Aligned.<strong>toTranscriptome</strong>.out<strong>.bam</strong>
  - /work/fcabanettes/jflow_results/rnaseq/wf000011/<strong>RnaStarMap_paired_new_gtf/</strong>sample3.R1.fastq.trim.fastq-Aligned.sortedByCoord.out<strong>.bam</strong>
  - /work/fcabanettes/jflow_results/rnaseq/wf000011/<strong>RnaStarMap_paired_new_gtf/</strong>sample3.R1.fastq.trim.fastq-Aligned.<strong>toTranscriptome</strong>.out<strong>.bam</strong>
  - /work/fcabanettes/jflow_results/rnaseq/wf000011/<strong>RnaStarMap_paired_new_gtf/</strong>sample1.R1.fastq.trim.fastq<strong>-Log.final.out</strong>
  - /work/fcabanettes/jflow_results/rnaseq/wf000011/<strong>RnaStarMap_paired_new_gtf/</strong>sample3.R1.fastq.trim.fastq<strong>-Log.final.out</strong>

11. Re-map reads to the genome, with new GTF (RNA-STAR) {SINGLE}:
  - /work/fcabanettes/jflow_results/rnaseq/wf000011/<strong>RnaStarMap_single_new_gtf/</strong>sample2.fastq.trim.fastq-Aligned.<strong>toTranscriptome</strong>.out<strong>.bam</strong>
  - /work/fcabanettes/jflow_results/rnaseq/wf000011/<strong>RnaStarMap_single_new_gtf/</strong>sample2.fastq.trim.fastq<strong>-Log.final.out</strong>
  - /work/fcabanettes/jflow_results/rnaseq/wf000011/<strong>RnaStarMap_single_new_gtf/</strong>esample2.fastq.trim.fastq-Aligned.sortedByCoord.out<strong>.bam</strong></pre>
    </div>
    <div id="quantification_2">
        <h3>Quantification</h3>
        <p>The RSEM index is located in the <em>RsemPrepare_default</em> folder.</p>
        <pre><span class="underline">12. RsemPrepare:</span>
            - /work/fcabanettes/jflow_results/rnaseq/wf000011/<strong>RsemPrepare_default/rsem_index</strong></pre>
        <p>Then, RSEM quantification results are present for genes and isoforms, into the folder <em>RsemCalculate_single</em> and/or <em>RsemCalculate_paired</em>.</p>
        <pre><span class="underline">13. Estimate RNA quantification (Rsem) {PAIRED}:</span>
  - /work/fcabanettes/jflow_results/rnaseq/wf000011/<strong>RsemCalculate_paired/Quant-</strong>sample1.R1.fastq.trim.fastq-Aligned.toTranscriptome.out<strong>.genes.results</strong>
  - /work/fcabanettes/jflow_results/rnaseq/wf000011/<strong>RsemCalculate_paired/Quant-</strong>sample3.R1.fastq.trim.fastq-Aligned.toTranscriptome.out<strong>.isoforms.results</strong>
  - /work/fcabanettes/jflow_results/rnaseq/wf000011/<strong>RsemCalculate_paired/Quant-</strong>sample1.R1.fastq.trim.fastq-Aligned.toTranscriptome.out<strong>.isoforms.results</strong>
  - /work/fcabanettes/jflow_results/rnaseq/wf000011/<strong>RsemCalculate_paired/Quant-</strong>sample3.R1.fastq.trim.fastq-Aligned.toTranscriptome.out<strong>.genes.results</strong>

<span class="underline">14. Estimate RNA quantification (Rsem) {SINGLE}:</span>
  - /work/fcabanettes/jflow_results/rnaseq/wf000011/<strong>RsemCalculate_single/Quant-</strong>sample2.fastq.trim.fastq-Aligned.toTranscriptome.out<strong>.genes.results</strong>
  - /work/fcabanettes/jflow_results/rnaseq/wf000011/<strong>RsemCalculate_single/Quant-</strong>sample2.fastq.trim.fastq-Aligned.toTranscriptome.out<strong>.isoforms.results</strong></pre>
    </div>
    <div id="summary">
        <h3>Summary</h3>
        <p>The <em>Summary_default</em> folder contains files which summarize the different components of the workflow : one file per important
        step:</p>
        <ul>
            <li>Summary of statistics generated by STAR on sample mappings</li>
            <li>Summary of statistics generated by STAR on sample mappings with the new gene model (if computed)</li>
            <li>Summary of trimming generated by Cutadapt for all samples</li>
            <li>Summary of the RSEM quantification, for genes and for transcripts, using the selected statistics (default expected count, see above), for all samples</li>
        </ul>
        <pre><span class="underline">15. Summary:</span>
  - /work/fcabanettes/jflow_results/rnaseq/wf000011/<strong>Summary_default/RNA-STAR_summary.tsv</strong>
  - /work/fcabanettes/jflow_results/rnaseq/wf000011/<strong>Summary_default/RNA-STAR_new-gtf_summary.tsv</strong>
  - /work/fcabanettes/jflow_results/rnaseq/wf000011/<strong>Summary_default/Cutadapt_summary.tsv</strong>
  - /work/fcabanettes/jflow_results/rnaseq/wf000011/<strong>Summary_default/RSEM_genes_expected_count_summary.tsv</strong>
  - /work/fcabanettes/jflow_results/rnaseq/wf000011/<strong>Summary_default/RSEM_transcripts_expected_count_summary.tsv</strong></pre>
    </div>
    <div id="logs">
        <h3>Logging</h3>
        <p>In addition to the files described above, there are two files for each sample and each component into are stored standard output (<em>.stdout</em> files)
        and standard error (<em>.stderr</em> files). If an error occured during the workflow execution, please explore these files to see why.</p>
    </div>
</div>

</body>
</html>