#
# Copyright (C) 2015 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import os
from jflow.component import Component


def build_summary(column, summary_file, *quant_files):
    import os, re
    from collections import OrderedDict
    column_id = 3 if column == "estimated_count" else 4

    with open(summary_file, "w") as summ_file:
        summ_file.write("target_id\tlength\teff_length\t")
        lines = OrderedDict()
        headers = []
        nb_file = 1
        for quant_file in quant_files:
            name_sample = re.search(r"quant_(.+)/abundance.+\.tsv", quant_file).group(1)
            headers.append(name_sample)
            with open(quant_file, "r") as q_file:
                for line in q_file.readlines()[1:]:
                    line = line.replace("\n", "")
                    parts = line.split("\t")
                    id_r = parts[0]
                    if id_r not in lines:
                        lines[id_r] = [id_r, parts[1], parts[2]]
                        for nb in range(0, len(quant_files)):
                            lines[id_r].append("")
                    lines[id_r][nb_file + 2] = parts[column_id]
            nb_file += 1
        summ_file.write("\t".join(headers) + "\n")
        for id_l, line in lines.items():
            summ_file.write("\t".join(line) + "\n")


class Summary(Component):

    def define_parameters(self, quant_tsv, columns):
        self.add_input_file_list("quant_tsv", "Quantification tsv result file", default=quant_tsv)
        self.add_parameter_list("columns", "Column of the results to use in summary", default=columns)
        self.add_output_file_list("quantification_summary", "Summary of quantification",
                                  pattern="Quantification_summary_{basename}.tsv", items=columns)

    def process(self):
        inputs = [self.quant_tsv]
        for column in self.columns:
            arguments = [column]
            outputs = [os.path.join(self.output_directory, "Quantification_summary_" + column + ".tsv")]
            self.add_python_execution(build_summary, cmd_format="{EXE} {ARG} {OUT} {IN}", arguments=arguments,
                                      inputs=inputs, outputs=outputs, local=True)
