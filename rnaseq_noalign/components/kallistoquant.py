#
# Copyright (C) 2015 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import os
import re
from subprocess import Popen, PIPE
from jflow.component import Component


def run_kallisto_quant(cmd, bootstrap, threads, transcripts_index, fragment_length_mean, fragment_length_sd, results,
                       stdout, stderr, read1, read2=None):
    res_dir = os.path.abspath(os.path.join(results, os.pardir))
    command = cmd + " quant -i " + transcripts_index + " -o " + res_dir + " -b " + bootstrap + " -t " + threads
    if read2 is None:
        command += " --single -l " + fragment_length_mean + " -s " + fragment_length_sd
    command += " " + read1
    if read2 is not None:
        command += " " + read2
    command += " > " + stdout + " 2>> " + stderr + ";\n"
    command += "mv {0}/abundance.tsv {0}/abundance_{1}.tsv".format(res_dir, os.path.basename(read1).replace(".fastq.gz", ""))

    os.system(command)


class KallistoQuant(Component):

    def get_command(self):
        return "kallisto quant"

    def get_version(self):
        cmd = [self.get_exec_path("kallisto")]
        p = Popen(cmd, stdout=PIPE, stderr=PIPE)
        stdout, stderr = p.communicate()
        search = re.search(r"kallisto ([^\n]+)", stdout.decode("utf-8"))
        if search:
            return search.group(1)
        return "Unknown"

    def get_description(self):
        return "Kallisto quantification" if self.description is None else self.description

    def define_parameters(self, transcripts, reads1, reads2=None, threads=4, bootstrap=100, fragment_length_mean=300,
                          fragment_length_sd=20):
        self.add_input_file("transcripts", "Kallisto index file", default=transcripts)
        self.add_input_file_list("reads_1", "Which reads files should be used", file_format="fastq", default=reads1)
        self.add_input_file_list("reads_2", "Which reads files should be used (paired reads)", file_format="fastq",
                                 default=reads2)
        self.add_parameter("bootstrap", "Number of bootstrap samples", default=bootstrap, cmd_format="-b")
        self.add_parameter("threads", "Number of threads", default=threads, cmd_format="-t")
        self.add_parameter("fragment_length_mean", "Mean of the length of the fragments", default=fragment_length_mean,
                           cmd_format="-l" if reads2 is None else "")
        self.add_parameter("fragment_length_sd", "Standard deviation of the length of the fragments",
                           default=fragment_length_sd, cmd_format="-s" if reads2 is None else "")
        self.add_parameter("single", "is single", cmd_format="--single", type=bool, default=reads2 is None) # For print
        # only
        self.add_output_file_list("results", "Kallisto quantification results",
                                  pattern="quant_{basename_woext}/abundance_{basename_woext}.tsv", items=reads1)
        self.add_output_file_list("stdout", "Standard output", pattern="logs_{basename_woext}.stdout", items=reads1)
        self.add_output_file_list("stderr", "Standard error", pattern="logs_{basename_woext}.stderr", items=reads1)

    def process(self):
        inputs = [self.reads_1]
        if len(self.reads_2) > 0:
            inputs.append(self.reads_2)
        outputs = [self.results, self.stdout, self.stderr]
        self.add_python_execution(run_kallisto_quant, cmd_format="{EXE} {ARG} {OUT} {IN}",
                                  arguments=[self.get_exec_path("kallisto"), self.bootstrap, self.threads,
                                             self.transcripts, self.fragment_length_mean, self.fragment_length_sd],
                                  inputs=inputs, outputs=outputs, includes=[self.transcripts], map=True)
