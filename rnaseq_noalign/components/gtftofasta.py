#
# Copyright (C) 2015 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import re
from jflow.component import Component


class GtfToFasta(Component):

    def get_command(self):
        return "gtf_to_fasta"

    def get_version(self):
        stdout= os.popen(self.get_exec_path("tophat") + " --version").read()
        search = re.search(r"TopHat v([^\n]+)", stdout)
        if search:
            return search.group(1)
        return "Unknown"

    def get_description(self):
        return "Build transcriptome from genome and the gtf annotations file"

    def define_parameters(self, genome, gtf_file):
        self.add_input_file("genome", "the reference genome", file_format="fasta", default=genome)
        self.add_input_file("gtf_file", "the genes annotations in GTF format", default=gtf_file)
        self.add_output_file("transcriptome", "the resulting transcriptome", filename="transcriptome.fa",
                             file_format="fasta")
        self.add_output_file("stdout", "Standard output", filename="gtf_to_fasta.stdout")
        self.add_output_file("stderr", "Standard error", filename="gtf_to_fasta.stderr")

    def process(self):
        command = self.get_exec_path("gtf_to_fasta") + " $1 $2 $3 > $4 2>> $5"
        self.add_shell_execution(command, cmd_format="{EXE} {IN} {OUT}", inputs=[self.gtf_file, self.genome],
                                 outputs=[self.transcriptome, self.stdout, self.stderr])
