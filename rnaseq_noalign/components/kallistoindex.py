#
# Copyright (C) 2015 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import re
from subprocess import Popen, PIPE
from jflow.component import Component


class KallistoIndex(Component):

    def get_command(self):
        return "kallisto index"

    def get_version(self):
        cmd = [self.get_exec_path("kallisto")]
        p = Popen(cmd, stdout=PIPE, stderr=PIPE)
        stdout, stderr = p.communicate()
        search = re.search(r"kallisto ([^\n]+)", stdout.decode("utf-8"))
        if search:
            return search.group(1)
        return "Unknown"

    def get_description(self):
        return "Build kallisto index"

    def define_parameters(self, transcriptome):
        self.add_input_file("transcriptome", "Transcriptome onto align reads", file_format="fasta",
                            default=transcriptome)
        self.add_output_file("transcripts", "kallisto index to be constructed", filename="transcripts.idx")
        self.add_output_file("stdout", "Standard output", filename="kallisto_index.stdout")
        self.add_output_file("stderr", "Standard error", filename="kallisto_index.stderr")

    def process(self):
        command = self.get_exec_path("kallisto") + " index -i $2 $1 > $3 2>> $4"
        self.add_shell_execution(command, cmd_format="{EXE} {IN} {OUT}", inputs=[self.transcriptome],
                                 outputs=[self.transcripts, self.stdout, self.stderr])