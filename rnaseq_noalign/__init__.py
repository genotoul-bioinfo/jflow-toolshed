#
# Copyright (C) 2015 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import re, gzip
from jflow.workflow import Workflow


class RnaSeqNoAlign (Workflow):

    nb_threads = 8

    def get_description(self):
        return "A complete RNASeq workflow without alignment (Kallisto)"

    def define_parameters(self, function="process"):
        self.add_multiple_parameter_list("sample", "List of sample reads", required=True, group="Samples",
                                         rules="FilesUnique", paired_columns="fastq(.gz)?[reads_1,reads_2]")
        self.add_input_file("reads_1", "Which reads files should be used", required=True, add_to="sample",
                            file_format="fastq")
        self.add_input_file("reads_2", "Which reads files should be used (paired reads)", required=False,
                            add_to="sample", file_format="fastq")
        self.add_input_file("transcriptome", "Transcriptome onto align reads", file_format="fasta",
                            group="Transcriptome", rules="Exclude=reference_genome;Exclude=gtf_file;"
                                                         "RequiredIf?ANY[reference_genome=None,gtf_file=None]")
        self.add_input_file("reference_genome", "the reference genome", file_format="fasta", group="Transcriptome",
                            rules="RequiredIf?ALL[transcriptome=None]")
        self.add_input_file("gtf_file", "the genes annotations in GTF format", group="Transcriptome",
                            rules="RequiredIf?ALL[transcriptome=None]")

        #Kallisto quant
        self.add_parameter("fragment_length_mean", "Mean of the fragments length", default=300, group="Quantification",
                           rules="DisabledIf?ALL[sample>reads_2=*]")
        self.add_parameter("fragment_length_sd", "Standard deviation of the fragments length", default=100,
                           group="Quantification", rules="DisabledIf?ALL[sample>reads_2=*]")

    @staticmethod
    def get_reads_length(reads_file):
        r_file = gzip.open(reads_file, "r")
        r_file.readline()  # ignore first line
        read = r_file.readline().decode("utf-8")
        match = re.search(r"[A-Za-z]+", read)
        if match:
            read = match.group(0)
        return len(read)

    def process(self):
        # Prepare reads:
        paired = {
            "reads1": [],
            "reads2": []
        }
        single = []
        max_read_length = 0
        has_paired = False
        has_single = False
        for smple in self.sample:
            max_read_length = max(max_read_length, self.get_reads_length(smple["reads_1"]))
            if not smple["reads_2"].is_None:
                paired["reads1"].append(smple["reads_1"])
                paired["reads2"].append(smple["reads_2"])
                max_read_length = max(max_read_length, self.get_reads_length(smple["reads_2"]))
                has_paired = True
            else:
                single.append(smple["reads_1"])
                has_single = True

        #GTF to Transcriptome:
        transcriptome = self.transcriptome
        if not self.reference_genome.is_None and not self.gtf_file.is_None:
            gtftofasta = self.add_component("GtfToFasta", [self.reference_genome, self.gtf_file])
            transcriptome = gtftofasta.transcriptome

        # Kallisto index:
        kallisto_index = self.add_component("KallistoIndex", [transcriptome])

        # Kallisto quant:
        quant_results = []
        if has_paired:
            kallisto_quant_paired = self.add_component("KallistoQuant", [kallisto_index.transcripts, paired["reads1"],
                                                       paired["reads2"], self.nb_threads], component_prefix="paired")
            quant_results += kallisto_quant_paired.results
            if has_single:
                kallisto_quant_paired.description = kallisto_quant_paired.get_description() + " {PAIRED}"
        if has_single:
            kallisto_quant_single = self.add_component("KallistoQuant", [kallisto_index.transcripts, single, None,
                                                       self.nb_threads], component_prefix="single")
            quant_results += kallisto_quant_single.results
            if has_paired:
                kallisto_quant_single.description = kallisto_quant_single.get_description() + " {SINGLE}"

        summary = self.add_component("Summary", [quant_results, ["estimated_count", "TPM"]])
