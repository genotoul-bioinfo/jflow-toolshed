<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Jflow RNA-SEQ no-align workflow User Manual</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <script type="application/javascript" src="js/jquery-3.1.1.min.js"></script>
    <script type="application/javascript" src="js/build_page.js"></script>
</head>
<body>
<h1>Jflow RNA-SEQ no-align workflow User Manual</h1>
<p id="subtitle">&ndash; command line version &ndash;</p>

<div id="author_info">
    <p><strong>Written by:</strong> Floréal Cabanettes &lt;<a href="mailto:floreal.cabanettes@inra.fr">floreal.cabanettes@inra.fr</a>&gt;<br/>
    <strong>Version of the documentation:</strong> 1.0 (09-05-2017)</p>
</div>

<div class="summary_document">
    <p>This document explains how to use the RNA-seq no-align pipeline through Jflow on the Genotoul cluster. All available options
    availables are described below, illustrated by some examples.</p>
</div>

<p id="sommaire_title">Summary</p>
<div id="sommaire_div"></div>

<div id="trouver_jflow">
    <h2>Where to find Jflow on the genotoul cluster?</h2>
    <p>Jflow is available on the genotoul cluster at this URL:<br/>
        <em>/usr/local/bioinfo/src/Jflow/jflow</em>.</p>
</div>

<div id="parametres_workflow">
    <h2>Workflow parameters</h2>
    <div id="input_files">
        <h3>Input files</h3>
        <p>The workflow requires several input files:</p>
        <ul>
            <li>reads in <strong>fastq.gz</strong> format (single or paired)</li>
            <li>And one case among:<ul>
                <li>a reference genome in <strong>fasta</strong> format and a <strong>GTF</strong> file</li>
                <li>a reference transcriptome in <strong>fasta</strong> format</li>
            </ul></li>
        </ul>
        <h4>Reads</h4>
        <p>Use the <em class="code">--sample</em> parameter to set reads. For each sample, set the <em class="code">reads-1</em> sub-parameter, and if reads are paired, also the <em class="code">reads-2</em> sub-parameter.
            Repeat <em>--sample</em> parameter for each sample.</p>
        <p>Examples:</p>
        <ul>
            <li>One paired end sample:<br/><em class="code">--sample reads-1=my_file_reads_1.fastq.gz reads-2=my_file_reads_2.fastq.gz</em></li>
            <li>One single end sample:<br/><em class="code">--sample reads-1=my_file_single.fastq.gz</em></li>
            <li>Two samples, one paired end, the other single end:<br/>
                <em class="code">--sample reads-1=my_file_r1.fastq.gz reads-2=my_file_r2.fastq.gz --sample reads-1=my_file_single.fastq.gz</em></li>
        </ul>
        <h4>Reference genome or transcriptome</h4>
        <p>Reference genome is set with the <em class="code">--reference-genome</em> parameter. Gtf file is set by the <em class="code">--gtf-file</em> parameter.</p>
        <p>Transcriptome is set by the <em class="code">--transcriptome</em> parameter and exclude the previous parameters.</p>
        <p>Examples:</p>
        <ul>
            <li>With reference genome:<br/><em class="code">--reference-genome my__fasta_file.fasta --gtf-file model.gtf</em></li>
            <li>With transcriptome:<br/><em class="code">--transcriptome transcriptome.fasta</em></li>
        </ul>
    </div>
    <div id="kallisto">
        <h3>Quantification</h3>
        <p>Some parameters to customize the pipeline.</p>
        <p><strong>--fragment-length-mean</strong></p>
        <p class="indent-1">
            Mean of the fragments length (needed only for single reads).<br/>
            <em>Default:</em> 300<br/>
            <em>Required:</em> <span class="no">no</span>.<br/>
            <em>Example:</em> <em class="code">--fragment-length-mean 240</em></p>
        <p><strong>--fragment-length-sd</strong></p>
        <p class="indent-1">
            Standard deviation of the fragments length (needed only for single reads).<br/>
            <em>Default:</em> 100<br/>
            <em>Required:</em> <span class="no">no</span>.<br/>
            <em>Example:</em> <em class="code">--fragment-length-sd 80</em></p>
        <h4>Software and fixed parameters</h4>
        <p><strong>Software used:</strong> <a href="https://pachterlab.github.io/kallisto/">kallisto</a> [VERSION: 0.43.0]</p>
        <p><strong>Fixed parameter:</strong></p>
        <ul><li>-b 100 (bootstap)</li></ul>
    </div>
</div>

<div id="launch_workflow">
    <h2>Launch workflow</h2>
    <p>Launch the workflow using the command:</p>
    <p><strong>/usr/local/bioinfo/src/Jflow/jflow/bin/jflow_cli.py rnaseqnoalign &lt;parameters...&gt;</strong></p>
    <p>Replacing <em>&lt;parameters...&gt;</em> by the parameters described above.</p>
    <h3>Example</h3>
    <p>We have 3 samples to analyse:</p>
    <ul>
        <li>Sample 1 (paired end) : <em>sample1.R1.fastq.gz, sample1.R2.fastq.gz</em></li>
        <li>Sample 2 (single end) : <em>sample2.fastq.gz</em></li>
        <li>Sample 3 (paired end) : <em>sample3.R1.fastq.gz, sample3.R2.fastq.gz</em></li>
    </ul>
    <p>Also, a fasta file of the reference genome:</p>
    <ul>
        <li>Reference genome : <em>reference.fa</em></li>
    </ul>
    <p>And a gene model:</p>
    <ul>
        <li>Gene model: <em>reference.gtf</em></li>
    </ul>
    <p>All files above are localized in the working directory. As possible, we will keep the default parameters of the workflow.
    We launch, so, the following command:</p>
    <p class="align-left"><em class="code">/usr/local/bioinfo/src/Jflow/jflow/bin/jflow_cli.py rnaseqnoalign --sample reads-1=sample1.R1.fastq.gz
        reads-2=sample1.R2.fastq.gz --sample reads-1=sample2.fastq.gz --sample reads-1=sample3.R1.fastq.gz
        reads-2=sample3.R2.fastq.gz --reference-genome reference.fa --gtf-file reference.gtf</em></p>
</div>
<div id="results">
    <h2>Results</h2>
    <p>Results of the last launched workflow are inside the folder with the highest workflow-id in directory name, inside the folder <em>jflow_results/rnaseqnoalign</em> of your <em>work</em> directory.
        The examples shown here are results of the workflow described in the section above. Sample file names are based on the reads-1 file names.</p>
    <p>At the end of the workflow execution, a recap show the resulting files for each component:</p>
    <pre>
###########
# Results #
###########

    [...]</pre>
    <p>If you mix paired and single reads, results below are separated for single and paired data.</p>
    <div id="transcriptome">
        <h3>Reference genome to transcriptome</h3>
        <p>As we give a reference genome fasta, workflow first transform it, using the given gtf model file, to a transcriptome. Transcriptome
            fasta file is saved in the <em>GtfToFasta_default</em> folder.</p>
        <pre><span class="underline">1. Build transcriptome from genome and the gtf annotations file:</span>
  - /work/fcabanettes/jflow_results/rnaseqnoalign/wf000011/<strong>GtfToFasta_default/transcriptome.fa</strong></pre>
    </div>
    <div id="indexing">
        <h3>Kallisto index</h3>
        <p>The <em>KallistoIndex_default</em> folder contains the Kallisto transcriptome.</p>
        <pre><span class="underline">2. Build kallisto index:</span>
  - /work/fcabanettes/jflow_results/rnaseqnoalign/wf000011/<strong>KallistoIndex_default/transcripts.idx</strong></pre>
    </div>
    <div id="quantification">
        <h3>Quantification</h3>
        <p>In <em>KallistoQuant_paired</em> and/or <em>KallistoQuant_single</em> folder, we have for each sample, a TSV file that summarize the counts for each gene, expressed in estimated count or TPM.</p>
        <pre><span class="underline">3. Kallisto quantification {PAIRED}:</span>
  - /work/fcabanettes/jflow_results/rnaseqnoalign/wf000011/<strong>KallistoQuant_paired/quant_sample1.R1/abundance_sample1.R1.tsv</strong>
  - /work/fcabanettes/jflow_results/rnaseqnoalign/wf000011/<strong>KallistoQuant_paired/quant_sample3.R1/abundance_sample3.R1.tsv</strong>

<span class="underline">4. Kallisto quantification {SINGLE}:</span>
  - /work/fcabanettes/jflow_results/rnaseqnoalign/wf000011/<strong>KallistoQuant_single/quant_sample2/abundance_sample2.tsv</strong></pre>
    </div>

    <div id="summary">
        <h3>Summary</h3>
        <p>The <em>Summary_default</em> folder contains files which summarize the quantification
        step:</p>
        <ul>
            <li>Estimated counts for each target of all samples</li>
            <li>TPM for each target of all samples</li>
        </ul>
        <pre><span class="underline">5. Summary:</span>
  - /work/fcabanettes/jflow_results/rnaseqnoalign/wf000011/<strong>Summary_default/</strong>Quantification_summary_<strong>estimated_count.tsv</strong>
  - /work/fcabanettes/jflow_results/rnaseqnoalign/wf000011/<strong>Summary_default/</strong>Quantification_summary_<strong>TPM.tsv</strong></pre>
    </div>
    <div id="logs">
        <h3>Logging</h3>
        <p>In addition to the files described above, there are two files for each sample and each component into are stored standard output (<em>.stdout</em> files)
        and standard error (<em>.stderr</em> files). If an error occured during the workflow execution, please explore these files to see why.</p>
    </div>
</div>

</body>
</html>