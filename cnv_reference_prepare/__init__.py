import os
from jflow.workflow import Workflow


class CnvReferencePrepare(Workflow):

    nb_threads = 8

    def get_description(self):
        return "Prepare the reference for GenomeStrip for CNV Detection workflow"

    def define_parameters(self, function="process"):
        self.add_input_file("reference_genome", "The reference genome file", file_format="fasta", required=True)
        self.add_parameter("species", "Species name from NCBI Taxonomy database")
        self.add_input_file("reference_ploidymap", "Expected ploidy of the different chromosomal regions")
        self.add_input_file("reference_gendermask", "Regions with unreliable read depth on human sex chromosomes "
                                                    "(eg. pseudoautosomal regions)", file_format="bed")
        self.add_parameter("sex_chrs", "Sex chromosomes, comma separated")
        self.add_parameter("ploidy", "Ploidy of the organism", type=int, default=2)
        self.add_parameter("min_n_stretches_size", "Min size for N stretches", default=100)

    def process(self):
        wrk_dir = self.get_temporary_file("")
        chr_wrk_dir = os.path.join(wrk_dir, "chrs")
        chr_wrk_dir_gc = os.path.join(wrk_dir, "chrs-gc")
        if not os.path.isfile(self.reference_genome + ".fai"):
            os.system(self.get_exec_path("samtools") + " faidx " + self.reference_genome)
        with open(self.reference_genome + ".fai") as f:
            chrs = [line.split("\t")[0] for line in f]
        sex_chrs = self.sex_chrs if not self.sex_chrs.is_None else ""

        reference = self.add_component("GstripPrepareReference", [self.reference_genome])

        reference_out_prefix = os.path.basename(reference.reference_out).replace(".fasta", "")
        fa_chrs = self.add_component("ExtractFaChromosomes", [reference.reference_out, chrs])

        svmask_files = []
        for chri in chrs:
            svmask_files.append(chr_wrk_dir + os.path.sep + "svmask_" + chri + ".fasta")

        fastalength = self.add_component("GstripFastalength", [reference.reference_out])
        index = self.add_component("GstripIndex", [reference.reference_out, wrk_dir])
        svmask = self.add_component("GstripSvmask", [reference.reference_out, wrk_dir, chrs, chr_wrk_dir])
        svmask_c = self.add_component("GstripConcatenateFasta", [svmask_files, "reference.svmask.fasta"],
                                      component_prefix="svmask")
        rdmask = self.add_component("GstripRdmask", [reference.reference_out, sex_chrs])
        gcmask = self.add_component("GstripGcmask", [wrk_dir, reference_out_prefix,
                                                     fastalength.reference_fasta_length, self.species, sex_chrs,
                                                     fa_chrs.fa_chrs, chrs, chr_wrk_dir_gc, self.nb_threads])
        gcmask_c = self.add_component("GstripConcatenateFasta", [gcmask.reference_gcmask, "reference.gcmask.fasta"],
                                      component_prefix="gcmask")
        lcmask = self.add_component("GstripLcmask", [wrk_dir, reference_out_prefix, chrs, fa_chrs.fa_chrs,
                                                     chr_wrk_dir_gc])
        lcmask_c = self.add_component("GstripConcatenateFasta", [lcmask.reference_lcmask, "reference.lcmask.fasta"],
                                      component_prefix="lcmask")
        dictionary = self.add_component("GstripDictionary", [reference.reference_out])

        stretches = self.add_component("FindNstretches", [reference.reference_out, self.min_n_stretches_size])

        self.comp = self.add_component("GstripFinalOutputs",
                                       [reference.reference_out, fastalength.reference_fasta_length,
                                        svmask_c.out_fasta, rdmask.reference_rdmask, gcmask_c.out_fasta,
                                        lcmask_c.out_fasta, dictionary.reference_dict, self.ploidy, wrk_dir,
                                        stretches.stretches_bed, self.reference_ploidymap, self.reference_gendermask])

    def get_summary(self):
        return "Please use this Fasta file for the CNV detection workflow:\n\t" + self.comp.reference_fasta_out
