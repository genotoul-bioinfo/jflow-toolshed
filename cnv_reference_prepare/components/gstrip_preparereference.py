import os
from jflow.component import Component


def copyreffile(src, dest):
    from shutil import copyfile
    copyfile(src, dest)


class GstripPrepareReference(Component):

    def define_parameters(self, reference):
        self.add_input_file("reference", "Reference fasta file", file_format="fasta", default=reference)
        self.add_output_file("reference_out", "Reference fasta file", filename="reference.fasta")

    def process(self):
        self.add_python_execution(copyreffile, cmd_format="{EXE} {IN} {OUT}", inputs=[self.reference],
                                  outputs=[self.reference_out], local=True)
        if os.path.isfile(self.reference + ".fai"):
            self.add_python_execution(copyreffile, cmd_format="{EXE} {IN} {OUT}", inputs=[self.reference + ".fai"],
                                      outputs=[self.reference_out + ".fai"], local=True)
        else:
            cmd = self.get_exec_path("samtools") + " faidx $1"
            self.add_shell_execution(cmd, cmd_format="{EXE} {IN}", inputs=[self.reference_out],
                                     outputs=[self.reference_out + ".fai"])
