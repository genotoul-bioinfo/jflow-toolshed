import os
from jflow.component import Component


class FindNstretches(Component):

    def define_parameters(self, fasta_file, min_size=100):
        self.add_input_file("fasta_file", "Fasta reference file", default=fasta_file)
        self.add_parameter("min_size", "Min size for N stretches", default=min_size)
        self.add_output_file("stretches_bed", "N Stretches positions description",
                             filename=os.path.splitext(os.path.basename(fasta_file))[0] + ".Nstretch.bed")

    def process(self):
        self.add_shell_execution(self.get_exec_path("findNstretches.py") + " -f $1 -m $2 > $3",
                                 cmd_format="{EXE} {IN} {ARG} {OUT}", inputs=self.fasta_file, arguments=self.min_size,
                                 outputs=self.stretches_bed)
