import os
from jflow.component import Component


class GstripLcmask(Component):

    def define_parameters(self, wrk_dir, reference_out_prefix, chrs, fa_chrs, chr_dir):
        self.add_input_directory("wrk_dir", "working temp directory", default=wrk_dir)
        self.add_parameter("reference_out_prefix", "Prefix of the reference file in the working directory",
                           default=reference_out_prefix)
        self.add_input_directory("fa_chrs", "Directory containing fasta files for each chromosome", default=fa_chrs)
        self.add_parameter_list("chrs", "List of chromosomes", default=chrs)
        self.add_parameter("chr_dir", "Chromosome directory which will contain output files", default=chr_dir)
        self.add_output_file_list("reference_lcmask", "genome mask (in indexed fasta format) that masks low-complexity "
                                  "sequence and is used in read-depth analysis", file_format="fasta",
                                  pattern="{basename}.lcmask.fasta", items=chrs)
        self.add_output_file("stdout", "Standard output", filename="logs.stdout")
        self.add_output_file("stderr", "Standard error", filename="logs.stderr")

    def process(self):
        sv_dir = self.get_exec_path("svtoolkit_dir")
        for chri in self.chrs:
            reference_chr_prefix = os.path.join(self.output_directory, chri)
            self.add_shell_execution(
                self.get_exec_path("run_lcmask.sh") + " $3 $4 $5 $2 >> " + self.stdout + " 2>> " + self.stderr,
                cmd_format="{EXE} {IN} {ARG} {OUT}",
                outputs=[reference_chr_prefix + ".lcmask.fasta"],
                inputs=[self.wrk_dir, os.path.join(self.chr_dir, chri + ".fasta.out")],
                arguments=[self.chr_dir, reference_chr_prefix, sv_dir])
