import os

from jflow.component import Component


class GstripGcmask(Component):

    def define_parameters(self, wrk_dir, reference_out_prefix, reference_fasta_length, species, sex_chrs,
                          fa_chrs, chrs, chr_dir, nb_threads=4):
        self.add_input_directory("wrk_dir", "working temp directory", default=wrk_dir)
        self.add_input_directory("fa_chrs", "Directory containing fasta files for each chromosome", default=fa_chrs)
        self.add_input_file("reference_fasta_length", "Cumputed using fastalength",
                            default=reference_fasta_length)
        self.add_parameter("reference_out_prefix", "Prefix of the reference file in the working directory",
                           default=reference_out_prefix)
        self.add_parameter_list("chrs", "List of chromosomes", default=chrs)
        self.add_output_file_list("reference_gcmask", "genome mask (in indexed fasta format)that is used for gc-bias "
                                                 "estimation in each sequencing library", file_format="fasta",
                             pattern="{basename}.gcmask.fasta", items=chrs)
        self.add_parameter("species", "Species name from NCBI Taxonomy database", default=species)
        self.add_parameter("nb_threads", "Number of threads to be used", default=nb_threads)
        self.add_output_file("stdout", "Standard output", filename="logs.stdout")
        self.add_output_file("stderr", "Standard error", filename="logs.stderr")
        self.add_parameter("sex_chrs", "Sex chromosomes, comma separated", default=sex_chrs)
        self.add_parameter("chr_dir", "Chromosome directory which will contain output files", default=chr_dir)

    def process(self):
        chr_wrk_dir = self.chr_dir
        os.makedirs(chr_wrk_dir)

        sv_dir = self.get_exec_path("svtoolkit_dir")
        reference_out_prefix = os.path.join(self.output_directory, self.reference_out_prefix)
        sex_chr_out = os.path.join(self.wrk_dir, self.reference_out_prefix + ".sexchromosomes.bed")
        self.add_shell_execution(self.get_exec_path("gstrip_gcmask_sexchromosomes.sh") + " $1 $3 $2",
                                 cmd_format="{EXE} {IN} {ARG} {OUT}",
                                 inputs=[self.wrk_dir, self.reference_fasta_length], arguments=[reference_out_prefix],
                                 outputs=[sex_chr_out])
        for chri in self.chrs:
            reference_chr_prefix = os.path.join(self.output_directory, chri)

            os.symlink(self.fa_chrs + os.path.sep + chri + ".fa", os.path.join(chr_wrk_dir, chri + ".fasta"))
            os.symlink(self.fa_chrs + os.path.sep + chri + ".fa.fai", os.path.join(chr_wrk_dir, chri + ".fasta.fai"))
            os.symlink(sex_chr_out, os.path.join(chr_wrk_dir, chri + ".sexchromosomes.bed"))

            self.add_shell_execution(
                self.get_exec_path("run_gcmask.sh") + " $4 \"$5\" $6 $7 $8 $9 ${10} >> " + self.stdout + "  2>> " +
                self.stderr,
                cmd_format="{EXE} {IN} {ARG} {OUT}",
                outputs=[os.path.join(chr_wrk_dir, chri + ".fasta.out"),
                         reference_chr_prefix + ".gcmask.fasta"],
                inputs=[self.wrk_dir, self.reference_fasta_length, sex_chr_out],
                arguments=[reference_chr_prefix,
                           "\"" + self.sex_chrs.replace(",", "\\|") + "\"", sv_dir,
                           self.get_exec_path("RepeatMasker"), self.species, chr_wrk_dir, self.nb_threads])
