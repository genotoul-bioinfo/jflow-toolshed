from jflow.component import Component


class GstripDictionary(Component):

    def define_parameters(self, reference):
        self.add_input_file("reference_fasta", "Reference fasta file", default=reference)
        self.add_output_file("reference_dict", "A picard-format dictionary file for the reference genome",
                             filename="reference.dict")
        self.add_output_file("stdout", "Standard output", filename="logs.stdout")
        self.add_output_file("stderr", "Standard error", filename="logs.stderr")

    def process(self):
        cmd = "java -jar " + self.get_exec_path("picard") + " CreateSequenceDictionary R= $1 O= $2 >> $3  2>> $4"
        self.add_shell_execution(cmd, cmd_format="{EXE} {IN} {OUT}", inputs=[self.reference_fasta],
                                 outputs=[self.reference_dict, self.stdout, self.stderr])
