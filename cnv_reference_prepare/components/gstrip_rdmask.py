from jflow.component import Component


class GstripRdmask(Component):

    def define_parameters(self, reference, sex_chrs):
        self.add_parameter("sex_chrs", "Sex chromosomes, comma separated", default=sex_chrs)
        self.add_input_file("reference_fasta", "Reference fasta file", default=reference)
        self.add_output_file("reference_rdmask", "bed file specifying a set of regions to use for overall sequencing "
                                                 "depth estimation", filename="reference.rdmask.bed",
                             file_format="bed")

    def process(self):
        sex_chrs = self.sex_chrs.split(",")
        if_conds_list = []
        for sex_chr in sex_chrs:
            if_conds_list.append("$2!=\"" + sex_chr + "\"")
        cmd = self.get_exec_path("fastalength") + " $1 | awk -v OFS=\"\t\" 'BEGIN{ print \"CHROM\",\"START\",\"END\"} " \
                                                  "{ if (" + " && ".join(if_conds_list) + ") {print $2,0,$1-1}}' > $2"
        self.add_shell_execution(cmd, cmd_format="{EXE} {IN} {OUT}", inputs=self.reference_fasta,
                                 outputs=self.reference_rdmask)
