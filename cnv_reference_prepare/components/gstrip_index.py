from jflow.component import Component


class GstripIndex(Component):

    def define_parameters(self, reference, wrk_dir):
        self.add_parameter("wrk_dir", "working temp directory", default=wrk_dir)
        self.add_input_file("reference_fasta", "Reference fasta file", default=reference)
        self.add_output_file("stdout", "Standard output", filename="logs.stdout")
        self.add_output_file("stderr", "Standard error", filename="logs.stderr")

    def process(self):
        sv_dir = self.get_exec_path("svtoolkit_dir")
        self.add_shell_execution(self.get_exec_path("gstrip_index.sh") + " $2 $1 $5 $6 >> $3  2>> $4",
                                 cmd_format="{EXE} {IN} {OUT} {ARG}",
                                 inputs=self.reference_fasta, outputs=[self.wrk_dir, self.stdout, self.stderr],
                                 arguments=[sv_dir, self.get_exec_path("bwa_svtoolkit")])