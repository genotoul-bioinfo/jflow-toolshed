import os

from jflow.component import Component


class GstripSvmask(Component):

    def define_parameters(self, reference, wrk_dir, chrs, chr_dir):
        self.add_input_directory("wrk_dir", "working temp directory", default=wrk_dir)
        self.add_input_file("reference_fasta", "Genome reference fasta file", default=reference, file_format="fasta")
        self.add_parameter_list("chrs", "Chromosome list", default=chrs)
        self.add_parameter("chr_dir", "Chromosome directory which will contain output files", default=chr_dir)
        self.add_output_file("stdout", "Standard output", filename="logs.stdout")
        self.add_output_file("stderr", "Standard error", filename="logs.stderr")

    def process(self):
        sv_dir = self.get_exec_path("svtoolkit_dir")

        chr_wrk_dir = self.chr_dir
        os.makedirs(chr_wrk_dir)
        chr_files = []
        for chri in self.chrs:
            chr_file = chr_wrk_dir + os.path.sep + "svmask_" + chri + ".fasta"
            chr_files.append(chr_file)
            self.add_shell_execution(self.get_exec_path("gstrip_genomemask.sh") + " $4 $2 $5 $6 >> " + self.stdout +
                                     " 2>> " + self.stderr,
                                     cmd_format="{EXE} {IN} {OUT} {ARG}",
                                     inputs=[self.wrk_dir, self.reference_fasta],
                                     outputs=[chr_file],
                                     arguments=[chr_wrk_dir, sv_dir, chri])
