import os
from jflow.component import Component


def concatane_files(output, output_fai, samtools, list_fa, *io):
    with open(list_fa, "r") as l_fa:
        inputs = l_fa.read().strip("\n").split("|")
    with open(output, "w") as out:
        for input_f in inputs:
            with open(input_f, "r") as inp:
                out.write(inp.read())
    os.system(samtools + " faidx " + output)


class GstripConcatenateFasta(Component):

    def define_parameters(self, fasta_files, output_file_name):
        self.add_input_file_list("fasta_files", "Fasta files to concatenate", default=fasta_files)
        self.add_output_file("out_fasta", "genome alignability mask (in indexed fasta format)",
                             filename=output_file_name, file_format="fasta")

    def process(self):
        # chr_files = []
        # for chri in self.chrs:
        #     chr_files.append(self.chr_dir + os.path.sep + "svmask_" + chri + ".fasta")

        list_fa = os.path.join(self.output_directory, ".input_files.txt")
        with open(list_fa, "w") as l_fa:
            l_fa.write("|".join(self.fasta_files))

        self.add_python_execution(concatane_files, cmd_format="{EXE} {OUT} {ARG} {IN}",
                                  outputs=[self.out_fasta, self.out_fasta + ".fai"],
                                  includes=self.fasta_files, arguments=[self.get_exec_path("samtools"), list_fa])