import os

from shutil import copyfile
from pathlib import Path

from jflow.component import Component


def linkfiles(*iofiles):
    ios = list(iofiles)
    input_files = ios[:int(len(ios)/2)]
    output_files = ios[int(len(ios)/2):]
    for i in range(0, len(input_files)):
        os.symlink(input_files[i], output_files[i])


def linkFastaIndexFiles(reference_fasta, output_dir):
    from glob import glob

    for file in glob(reference_fasta + ".*"):
        filename = os.path.basename(file)
        os.symlink(file, os.path.join(output_dir, filename))


class GstripFinalOutputs(Component):

    def define_parameters(self, reference, reference_fasta_length, reference_svmask, reference_rdmask, reference_gcmask,
                          reference_lcmask, reference_dict, ploidy, wrk_dir, nstreches_bed,
                          reference_ploidymap=None, reference_gendermask=None):
        self.add_input_file("reference_fasta", "Genome reference fasta file", default=reference, file_format="fasta")
        if reference_ploidymap is not None and not reference_ploidymap.is_None:
            self.add_input_file("reference_ploidymap", "Expected ploidy of the different chromosomal regions",
                                default=reference_ploidymap)
        self.add_output_file("reference_ploidymap_out", "Expected ploidy of the different chromosomal regions",
                             filename="reference.ploidymap.txt")
        if reference_gendermask is not None and not reference_gendermask.is_None:
            self.add_input_file("reference_gendermask", "Regions with unreliable read depth on human sex chromosomes "
                                                        "(eg. pseudoautosomal regions)", default=reference_gendermask)
        self.add_output_file("reference_gendermask_out",
                             "regions with unreliable read depth on human sex chromosomes "
                             "(eg. pseudoautosomal regions)",
                             filename="reference.gendermask.bed")
        self.add_input_directory("wrk_dir", "Working dir", default=wrk_dir)
        self.add_output_file("reference_fasta_out", "Genome reference fasta file", filename="reference.fasta",
                             file_format="fasta")

        self.add_input_file("reference_fasta_length_in", "Cumputed using fastalength",
                             default=reference_fasta_length)
        self.add_output_file("reference_fasta_length", "Cumputed using fastalength",
                             filename="reference.fasta.length")

        self.add_input_file("reference_svmask_in", "genome alignability mask (in indexed fasta format)",
                            default=reference_svmask)
        self.add_output_file("reference_svmask", "genome alignability mask (in indexed fasta format)",
                             filename="reference.svmask.fasta", file_format="fasta")

        self.add_input_file("reference_rdmask_in", "bed file specifying a set of regions to use for overall sequencing "
                                                 "depth estimation", default=reference_rdmask, file_format="bed")
        self.add_output_file("reference_rdmask", "bed file specifying a set of regions to use for overall sequencing "
                                                 "depth estimation", filename="reference.rdmask.bed",
                             file_format="bed")

        self.add_input_file("reference_gcmask_in", "genome mask (in indexed fasta format)that is used for gc-bias "
                                                 "estimation in each sequencing library", file_format="fasta",
                            default=reference_gcmask)
        self.add_output_file("reference_gcmask", "genome mask (in indexed fasta format)that is used for gc-bias "
                                                 "estimation in each sequencing library", file_format="fasta",
                             filename="reference.gcmask.fasta")

        self.add_input_file("reference_lcmask_in", "genome mask (in indexed fasta format) that masks low-complexity "
                                                 "sequence and is used in read-depth analysis", file_format="fasta",
                            default=reference_lcmask)
        self.add_output_file("reference_lcmask", "genome mask (in indexed fasta format) that masks low-complexity "
                                                 "sequence and is used in read-depth analysis", file_format="fasta",
                             filename="reference.lcmask.fasta")

        self.add_input_file("reference_dict_in", "A picard-format dictionary file for the reference genome",
                            default=reference_dict)
        self.add_output_file("reference_dict", "A picard-format dictionary file for the reference genome",
                             filename="reference.dict")
        self.add_parameter("ploidy", "Ploidy of the organism", type=int, default=ploidy)
        self.add_input_file("nstretches_bed", "N Stretches positions description", default=nstreches_bed)
        self.add_output_file("nstretches_bed_out", "N Stretches positions description",
                             filename="reference.Nstretch.bed")

    def process(self):
        if hasattr(self, "reference_ploidymap"):
            copyfile(self.reference_ploidymap, self.reference_ploidymap_out)
        else:
            with open(self.reference_ploidymap_out, "w") as f:
                f.write("*\t*\t*\t*\t" + str(self.ploidy) + "\n")
        if hasattr(self, "reference_gendermask"):
            copyfile(self.reference_gendermask, self.reference_gendermask_out)
        else:
            Path(self.reference_gendermask_out).touch()

        # Link output files:
        self.add_python_execution(linkfiles, cmd_format="{EXE} {IN} {OUT}",
                                  inputs=[self.reference_fasta,
                                          self.reference_fasta_length_in, self.reference_svmask_in,
                                          self.reference_svmask_in + ".fai",
                                          self.reference_rdmask_in, self.reference_gcmask_in,
                                          self.reference_gcmask_in + ".fai", self.reference_lcmask_in,
                                          self.reference_lcmask_in + ".fai", self.reference_dict_in,
                                          self.nstretches_bed],
                                  outputs=[self.reference_fasta_out,
                                           self.reference_fasta_length, self.reference_svmask,
                                           self.reference_svmask + ".fai",
                                           self.reference_rdmask, self.reference_gcmask,
                                           self.reference_gcmask + ".fai", self.reference_lcmask,
                                           self.reference_lcmask + ".fai", self.reference_dict,
                                           self.nstretches_bed_out])
        self.add_python_execution(linkFastaIndexFiles, inputs=[self.reference_fasta], arguments=[self.output_directory],
                                  cmd_format="{EXE} {IN} {ARG}", includes=self.wrk_dir)
