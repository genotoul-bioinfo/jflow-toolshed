from jflow.component import Component


class GstripFastalength(Component):

    def define_parameters(self, reference):
        self.add_input_file("reference_fasta", "Genome reference fasta file", default=reference, file_format="fasta")
        self.add_output_file("reference_fasta_length", "Cumputed using fastalength",
                             filename="reference.fasta.length")
        self.add_output_file("stderr", "Standard error", filename="logs.stderr")

    def process(self):
        self.add_shell_execution(self.get_exec_path("fastalength") + " $1 > $2 2>> $3",
                                 cmd_format="{EXE} {IN} {OUT}",
                                 inputs=self.reference_fasta, outputs=[self.reference_fasta_length, self.stderr])
