#!/usr/bin/env bash

set -e

reference_prefix=$1 # Final reference file (in output dir), containing the full path
sex_chrs=$2
export SV_DIR=$3
repeatmasker=$4
specie=$5
chr_dir=${6}
nbthreads=${7:-1}

readLength=100

local_reference_prefix=${chr_dir}/`echo ${reference_prefix} | awk -F / '{ print $NF }'`

echo "######################"
echo "# Start RepeatMasker #"
echo "######################"

# TODO: -species: donné par l'utilisateur
${repeatmasker} -pa ${nbthreads} -e wublast -species ${specie} -xsmall ${local_reference_prefix}.fasta

tail -n+4 ${local_reference_prefix}.fasta.out | grep 'Satellite\|Simple_repeat\|Line\|SINE\|LTR' | awk -v OFS="\t" '{ print $5,$6-1,$7}' | sort -k 1,1V -k 2,2n > ${local_reference_prefix}.repeats.bed;

for type in sexchromosomes repeats; do
  cat ${local_reference_prefix}.${type}.bed
done | sort -k 1,1V -k 2,2n > ${local_reference_prefix}.gcmask.unmerged.bed

bedtools merge -i ${local_reference_prefix}.gcmask.unmerged.bed | sort -k 1,1V -k 2,2n > ${local_reference_prefix}.gcmask.bed

#The directory containing libbwa.so must be on your LD_LIBRARY_PATH
export LD_LIBRARY_PATH=${SV_DIR}/bwa:${LD_LIBRARY_PATH}
classpath="${SV_DIR}/lib/SVToolkit.jar:${SV_DIR}/lib/gatk/GenomeAnalysisTK.jar"

java -cp ${classpath} -Xmx4g \
     org.broadinstitute.sv.apps.BedFileToGenomeMask \
    -I ${local_reference_prefix}.gcmask.bed \
    -R ${local_reference_prefix}.fasta  \
    -O ${reference_prefix}.gcmask.fasta