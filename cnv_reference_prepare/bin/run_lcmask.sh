#!/usr/bin/env bash

set -e

chr_dir=$1
reference_prefix=$2 # Final reference file (in output dir), containing the full path
export SV_DIR=$3
fasta_out=$4

local_reference_prefix=${chr_dir}/`echo ${reference_prefix} | awk -F / '{ print $NF }'`

cat ${fasta_out} | grep 'Satellite\|Simple_repeat\|Low_complexity' | awk -v OFS="\t" '{ print $5,$6,$7}' | sort -k 1,1V -k 2,2n > ${local_reference_prefix}.lcmask.bed

export LD_LIBRARY_PATH=${SV_DIR}/bwa:${LD_LIBRARY_PATH}
classpath="${SV_DIR}/lib/SVToolkit.jar:${SV_DIR}/lib/gatk/GenomeAnalysisTK.jar"

java -cp ${classpath} -Xmx4g \
     org.broadinstitute.sv.apps.BedFileToGenomeMask \
    -I ${local_reference_prefix}.lcmask.bed \
    -R ${local_reference_prefix}.fasta  \
    -O ${reference_prefix}.lcmask.fasta

