#! /usr/bin/env python3

import sys
import argparse
from Bio import SeqIO


def main(args):
    infile = args.infile
    cutoff = int(args.minsize)

    handle = open(infile, "rU")
    for record in SeqIO.parse(handle, "fasta"):
        print(record.id, file=sys.stderr)
        parse_fasta(record.id,record.seq,cutoff)
    handle.close()  

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def parse_fasta(seq_id,sequence,cutoff):
    i=0
    while i<len(sequence):
        cur_base = sequence[i]
        j = i+1
        while j<len(sequence) and sequence[j] == cur_base:
            j += 1
        l = j-i
        if l>cutoff and cur_base=='N':
            print("%s\t%d\t%d\t%c\t%d" % (seq_id,i,j,cur_base,l))
        i = j


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Construct a bed file describing the stretches of strictly greater than minsize N ")
    parser.add_argument('-f', '--file', required=True, dest='infile', help='name fo the fasta file')
    parser.add_argument('-m', '--minsize', required=True, help='minimum required stretch size of N')

    args = parser.parse_args()
    
    main(args)
