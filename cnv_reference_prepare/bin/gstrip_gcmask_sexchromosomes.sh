#!/usr/bin/env bash

wrk_dir=$1
reference_prefix=$2
sex_chrs=$3
fasta_length=$4

local_reference_prefix=${wrk_dir}/`echo ${reference_prefix} | awk -F / '{ print $NF }'`


if [[ ! -z ${sex_chrs} ]]; then
    cat ${fasta_length} | grep grep "${sex_chrs}" | awk -v OFS="\t" '{ print $2,0,$1 }' > ${local_reference_prefix}.sexchromosomes.bed;
else
    touch ${local_reference_prefix}.sexchromosomes.bed;
fi