#!/usr/bin/env bash

set -e

wrk_dir=$1;
reference=$2;
export SV_DIR=$3;
bwa=${4:-bwa}

export LD_LIBRARY_PATH=${SV_DIR}/bwa:${LD_LIBRARY_PATH};
classpath="${SV_DIR}/lib/SVToolkit.jar:${SV_DIR}/lib/gatk/GenomeAnalysisTK.jar";

mkdir -p ${wrk_dir};

localReference=${wrk_dir}/`echo ${reference} | awk -F / '{ print $NF }'`;
ln -s ${reference} ${localReference};

${bwa} index -a bwtsw ${reference}

ln -s ${reference}.* ${wrk_dir}/