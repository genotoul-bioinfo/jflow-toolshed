#!/usr/bin/env bash

set -e

wrk_dir=$1
reference=$2
export SV_DIR=$3
chr=$4
readLength=100
classpath="${SV_DIR}/lib/SVToolkit.jar:${SV_DIR}/lib/gatk/GenomeAnalysisTK.jar"

localReference=${wrk_dir}/../`echo ${reference} | awk -F / '{ print $NF }'`

export LD_LIBRARY_PATH=${SV_DIR}/bwa:${LD_LIBRARY_PATH};
java -cp ${classpath} -Xmx4g org.broadinstitute.sv.apps.ComputeGenomeMask -R ${localReference} \
-O ${wrk_dir}/svmask_${chr}.fasta -readLength ${readLength} -sequence ${chr}