# LI stands for localisation interval
# SV stands for structural variant

class BasicVariant(object):

    """ SHOULD NOT BE INSTANCIATED """

    def __init__(self, iD, chromosome, start, end, sv_type):
        try:
            self.start = int(start)
            self.end = int(end)
        except:
            print("start & end positions could not be casted to integer. Exiting")
            exit()
        self.id = iD
        self.chr = chromosome
        # uniform sv type
        if sv_type in ["D", "deletion", "DEL"]:
            self.type = "DEL"
        elif sv_type in ["duplication", "DUP", "TDUP", "TD"]:
            self.type = "DUP"
        elif sv_type in ["ITX", "CTX", "TRA"]:
            self.type = "TRA"
        elif sv_type in ["I","INS"]:
            self.type = "INS"
        elif sv_type in ["INV"]:
            self.type = "INV"
        else:
            print(sv_type+":Unrecognized StructuralVariant type.")
        
    def _get(self):
        return(self.id, self.chr, self.start, self.end, self.type)

    # yes, order matters
    def get_bed_features(self):
        return(self.chr, self.start, self.end, self.id, self.type)

    def size(self):
        return(self.end - self.start)


#==============================================================================

class StructuralVariant(BasicVariant):

    """ represent a single Structural Variant """

    def __init__(self, iD, chromosome, start, end, sv_type, support):
        BasicVariant.__init__(self, iD, chromosome, start, end, sv_type)
        self.groups = []
        self.support = support # or RD Signal for cnvnator

    def is_ambiguous(self): #not used yet
        """ a sv is ambiguous if he is found in more than one group """
        if len(self.groups) > 1:
            return True
        else:
            return False

    def match(self, b, p, li):
        """
            test if two sv, a (self) & b, match at "p" percent
            
            1) test if a & b are of the same type
            2) since sv are ordened, test if a & b are close enough on the chromosome to match
            3) check localisation interval
            4) check overlap percent
            5) if 1-4 are are trues return "ok" signal
            
            :param: b: Sv object B to compare
            :param: p: match percent threshold
            :param: li: localisation interval around each breakpoints

            :type: b: Sv object
            :type: p: float
            :type: li: int

            :return: -1: a & b are too far and no more b will match a --> "break" the calling loop
                      0: a & b do not match
                      1: a & b match 
            :rtype: int

            :Example: >>> my_sv.match(<Sv object b>, 0.7, 1000)
            0
        """
        if self.type == b.type: 
            #chr should already be the same
            if self.start + self.start * float(1 - p) < b.start:
                # We won't find anymore match on this chromosome
                # (if calls are well ordered by start)
                # Send signal to end loop
                return -1 
            if abs(self.start-b.start) > li or abs(self.end-b.end) > li:
                # breakpoints do not match
                return 0
            else :
                overlap = max(0, min(self.end,b.end) - max(self.start,b.start) + 1) # +1 because it is a seq
                # Now overlaps percents
                pA = overlap / float(self.size()+1)
                pB = overlap / float(b.size()+1)
                if (min(pA, pB)) >= p:
                    # both overlaps % are OK
                    return 1
        # one condition was not met
        return 0 

    def match_in_list(self, sv_list, p, li):
        """ return all matching possibilities with self inside the list """
        results = []
        for sv in sv_list:
            signal = self.match(sv, p, li)
            if signal == -1:
                return results
            elif signal == 1:
                results.append(sv)
            elif signal == 0:
                pass
        return results


#==============================================================================
class StructuralRegion(BasicVariant):

    """ represent a group (region) of close structural variants """

    def __init__(self, sv, svr_id):
        BasicVariant.__init__(self, *sv._get())
        self.members = [sv]
        self.id = svr_id
        sv.groups.append(self) # update the variant

    def add(self, sv):
        """ update : region, variant, start & end positions """
        self.members.append(sv)
        sv.groups.append(self)
        if self.start > sv.start:
            self.start = sv.start
        if self.end < sv.end:
            self.end = sv.end

    def group_match(self, sv, p, li):
        """
           test if a sv "A" matches (at p percent) with all members of a group of SV
           Also ensure that A does not belong to the group already.
        """
        for m in self.members:
            if m.match(sv, p, li) < 1 or m is sv:
                # at least, one member does not match
                return False
        # all members match with the candidate
        return True