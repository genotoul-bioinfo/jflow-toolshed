#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import re
from Sv import *

# LI stands for localisation interval


# ==============================================================================
def parse(file_path):
    """
        parse a file in a specific bed-like format to return a list of <StructuralVariant Object>
        
        :return: a list of sv separated by chromosome. It is actually a list of dictionnaries
                         with chromosome in keys and list of sv object in values
    """
    #TODO add test if ordered
    sv_list = {}
    with open(file_path, 'r') as file_in:
        last_chromosome = None
        last_start = 0
        for sv in file_in:
            if sv[0] == "#": # Escape comment lines
                continue
            chromosome, start, end, iD, sv_type, length, support = sv.split()[:7]
            # check if not bad sort
            if int(last_start) > int(start) and chromosome == last_chromosome:
                raise ValueError("start positions need to be ordered. On chr {0}, start position {1} is found before {2}".format(chromosome, last_start, start))
            last_chromosome = chromosome
            last_start = start
            # continue
            new_sv = StructuralVariant(iD, chromosome, start, end, sv_type, support)
            if chromosome in sv_list:
                sv_list[chromosome].append(new_sv)  # update value
            else:
                sv_list[chromosome] = [new_sv]      # add new key/value combination
    return sv_list


# ==============================================================================
def fuse_sv(tag, p, paths_files, li):
    """
        Main algorithm, compare each SV with each other individual in order to regroup them in regions

        :param: tag: a tag used to distinguish individuals in output file
        :param: p: percent of overlap between 2 SV to belong to the same group
        :param: paths_files: list of input files
        :param: li: Range of localisation interval around breakpoints
        
        :type: tag: string
        :type: p: float
        :type: path_files: list of strings (pathways)
        :type: li: int

        :return: all SVR
        :rtype: list of <StructuralRegion object>

        :note1 We do not allow a SVR to own more than one SV by sample/individual
        :note2 Start & stop of 1 SVR correspond to extremities of the region (so is length)

    """
    indiv_list = []
    group_list = []
    svr_number = 0
    # read all files
    for f in paths_files:
        indiv_list.append(parse(f))
    # do every relevant comparison :
    # for each individual "A"
    for i, individu_a in enumerate(indiv_list):
        #and each chromosome "k"
        for chromosome in individu_a.keys():
            # and each sv "a"
            for a in individu_a[chromosome]:
                # and  for each other individual "B"
                for individu_b in indiv_list[i+1:]:
                    # and each sv "b", in the same chromosome "k"
                    # look for a match betwheen "a" and "b"
                    if chromosome not in individu_b:
                        continue
                    matches = a.match_in_list(individu_b[chromosome], p, li)
                    for b in matches:
                        # if possible, add "b" to "a" 's groups
                        ab_linked = False
                        for g in a.groups:
                            if g.group_match(b, p, li):
                                g.add(b)
                            if b in g.members:
                                ab_linked = True
                        # if they are still not linked
                        if not ab_linked:
                            # create a new structural region
                            svr_number += 1
                            svr_id = "{0}-{1}".format(tag, svr_number)
                            new_svr = StructuralRegion(a, svr_id)
                            new_svr.add(b)
                            # and add it to the list
                            group_list.append(new_svr)
                # at this point, if "a" does not belong to any group
                if not a.groups:
                    # create a solo group
                    svr_number += 1
                    svr_id = "{0}-{1}".format(tag, svr_number)
                    new_svr = StructuralRegion(a, svr_id) 
                    # and add it to the list
                    group_list.append(new_svr)
    return group_list


# ==============================================================================
def write_sv_list(filename, sv_list, indiv_list):
    """
        for each SVR, write it as a processed line 
    """
    n = len(indiv_list)
    with open(filename, 'w') as filout:
        for i,g in enumerate(sv_list):
            # initialisation
            support_table = ['-'] * n
            member_table = []
            line = []
            # get bed-like infos
            bed_infos = g.get_bed_features()
            # get support and members infos
            for m in g.members:
                sample_name = m.id.split("-")[:-1]      # extract ID without number part
                sample_name = "-".join(sample_name)     # re-cast in str
                pos = indiv_list.index(sample_name)     #
                support_table[pos] = m.support          # position support in the right column
                member_table.append(m.id)               # members should be rightly ordered
            # construct line
            line.extend(bed_infos)
            line.append(g.size())
            line.append("/")
            line.append(";".join(member_table))
            line.extend(support_table)
            line = map(str,line)
            line ="\t".join(line) + "\n"
            # write it
            filout.write(line)


# ==============================================================================
# ===============================  MAIN  =======================================
# ==============================================================================
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--files', nargs='*', help='list of pathways to input files ')
    parser.add_argument('--names', nargs='*', help='list of individual s names')
    parser.add_argument('--output', help='path to output_file')
    parser.add_argument('--tag', help='tag to name SV')
    parser.add_argument('--p', help='Min threshold of percent overlap', type=float)
    parser.add_argument('--li', help='Range of localisation interval around breakpoints', type=int)
    args = parser.parse_args()
    group_list = fuse_sv(args.tag, args.p, args.files, args.li)
    write_sv_list(args.output, group_list, args.names)