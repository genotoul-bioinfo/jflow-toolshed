#!/usr/bin/env python
# -*- coding: utf-8 -*-
import re
import sys
import argparse
from bedify import filter_out

def main(input_f, out, tag, len_min, len_max):
    """
        Read a structural variant file generated from CNVnator and 
        transform it in a bed-like format.

        :param input_f: pathway to structural variant file
        :param out: pathway of the output file
        :param tag: tag for each variant
        :param len_min : minimum length of a SV
        :param len_max : maximum length of a SV

        :type input_f: string (pathway)
        :type out: string (pathway)
        :type tag: string
        :type len_min: integer
        :type len_max: integer

        :return: none
        
        :output: bed-like file. The columns corresponds to the following:
            -CHR
            -START
            -END
            -SVR_ID
            -LENGTH
            -MEMBERS
            -SUPPORT   (1 col/ind)

        :Example:

        >>> main("/home/sangoku/kamehameha_call", "kame.bed", "DBZ", 50, 9000, ["Krilin","Freezer","Boo"]) 
    """
    with open(input_f, 'r') as SV_list, open(out,'w') as output_file:
        for c,line in enumerate(SV_list):
            call_id = tag + "-" + str(c+1)
            results = line.split()
            sv_type, pos, length, support = results[:4]
            chromosome, pos = pos.split(":")
            if chromosome[:3] == "chr":
                chromosome = chromosome[3:]
            start, end = pos.split("-") 
            new_sv = [chromosome, start, end, call_id, sv_type, length, support, "/"]
            if filter_out(new_sv, len_min, len_max):
                pass
            else:
                new_sv = "\t".join(new_sv) + "\n"
                output_file.write(new_sv)
            
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--input_file', help='outfile of pindel/delly/breakdancer to translate to a bedlike format')
    parser.add_argument('--output_file', help='bedlike translation')
    parser.add_argument('--tag', help='tag of each structural variant')
    parser.add_argument('--min_length', help='min length of a variant', type=int)
    parser.add_argument('--max_length', help='max length of a variant', type=int)
    args = parser.parse_args()
    main(args.input_file, args.output_file, args.tag, args.min_length, args.max_length)