#
# Copyright (C) 2015 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from weaver.function import ShellFunction
from weaver.abstraction import Map
from jflow.component import Component
from jflow import utils
from collections import Counter
import os.path
import numpy
import itertools
import subprocess
import re

class Analyzebed (Component):
    
    def define_parameters(self, files, p, li, i_list, options, translo):
        self.add_input_file_list( "input_files", "The files to be archived.", default=files, required=True )
        self.add_input_file_list( "tra_files", "translocations to be archived.", default=translo, required=True )
        self.add_output_file_list( "output_links", "Link to the files.", pattern='link2_{basename}' , items=self.input_files )
        self.add_output_file( "software_merge", "TODO", filename="software_merge.bed")
        self.add_parameter("overlap_percent", "Minimum threshold of overlap percent between 2 SV", type=float, default=p)
        self.add_parameter("li_range", "Range of localisation interval around breakpoints", type=int, default=li)
        self.add_parameter("param_sum", "Summary of used parameters", type=str, default=options)
        self.add_parameter_list("indiv_list", "String list of studied individuals name", default=i_list, type=str, required=True)

    def process(self):
        #create links
        link = ShellFunction(self.get_exec_path("ln") + " -s $1 $2", cmd_format='{EXE} {IN} {OUT}')
        Map(link, inputs=self.input_files, outputs=self.output_links)
        # MERGE INTERSOFT
        mrg = ShellFunction(self.get_exec_path("merge_sv.py") + \
                                    " --output $1 \
                                      --tag $2\
                                      --p $3\
                                      --li $4" \
                                    + " --names" + utils.get_argument_pattern(self.indiv_list, 5)[0] \
                                    + " --files" + utils.get_argument_pattern(self.input_files, len(self.indiv_list)+5)[0],
                                    cmd_format='{EXE} {OUT} {ARG} {IN}')
        mrg(outputs=self.software_merge, inputs=self.input_files, arguments=["soft_merge", self.overlap_percent, self.li_range, self.indiv_list])
