#
# Copyright (C) 2015 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Kenza BAZI KABBAJ INRA - SIGENAE'
__version__ = '2.4.5'


from jflow.workflow import Workflow
import glob
import os


class vqsr_filter (Workflow):
    
    def get_description(self):
        return "Filtering Gatk vcf results with VQSR tool (Variant Quality Score Recalibration)"
        
    def define_parameters(self, function="process"):
        
        #Input files
        self.add_input_file("ref_genome", "The reference genome in fasta format", file_format="fasta", required=True, group="input files")

        self.add_multiple_parameter_list("training_set", "training set (sorted to the reference dictionary) used to train models in VQSR. Note that 2 training sets are built: a high confidence training set (common variants between 3 caller + hardfiltering) using these parameters: known=false, training=true, truth=true, prior=12.0 and a low confidence training set (specific or unique variants to each caller) using these parameters: known=false, training=true, truth=false, prior=10.0. For more information, please see: https://gatkforums.broadinstitute.org/gatk/discussion/2805/howto-recalibrate-variant-quality-scores-run-vqsr", group="resources")
        self.add_parameter("name", "Which name should be used for this training set", add_to = "training_set", required=True, group="resources")
        self.add_input_file("file", "path file of the training set in vcf format", add_to = "training_set", required=True, group="resources")
        self.add_parameter("known", "set true if you provided a database of known variants (dbSNP) as resources. It's used only for reporting purposes.", choices=["true", "false"], add_to = "training_set", required=False, group="resources")
        self.add_parameter("training", "set true to use this resource to train the recalibration model or false if not", required=True, choices=["true", "false"], group="resources", add_to = "training_set")
        self.add_parameter("truth", "set true if the variants in this resource are representative of true sites and false if not", required=True, choices=["true", "false"], group="resources", add_to = "training_set")
        self.add_parameter("prior", "the prior likelihood assigned to these variants", required=True, group="resources", add_to = "training_set")
        
        self.add_input_file_list("freebayes_variants", "freebayes VCF file(s) used to build a training set (multisample) ", group="input files", required=True)
        self.add_input_file_list("mpileup_variants", "mpileup VCF file(s) used to build a training set (multisample)", group="input files", required=True)
        self.add_input_file("gatk_variants", "one vcf file containing gatk variants (multisample/all regions)", group="input files", required=True)
        self.add_parameter("remove_tmp_files", "add this option to remove temporary files at the end of the pipeline execution, default=false", type='bool', default=False, group="General options")

    def process(self):
        
        ##Indexing reference genome if not existing: fai_dict
        databank_indexed = self.ref_genome
        if not os.path.exists(self.ref_genome + ".fai") or not os.path.exists(os.path.splitext(self.ref_genome)[0] + ".dict"):
            index_fai_dict = self.add_component( "IndexFaiDict", [self.ref_genome] )
            databank_indexed = index_fai_dict.databank

        ##Merging freebayes vcf files
        if len(self.freebayes_variants) > 1 :
            merged_freebayes=self.add_component("MergeVcf", [self.freebayes_variants], component_prefix="freebayes")
            variants_freebayes=merged_freebayes.output_file
        else:
            variants_freebayes=self.freebayes_variants[0]
    
        ##Merging mpileup vcf files
        if len(self.mpileup_variants) > 1 :
            merged_mpileup=self.add_component("MergeVcf", [self.mpileup_variants], component_prefix="mpileup")
            variants_mpileup=merged_mpileup.output_file
        else:
            variants_mpileup=self.mpileup_variants[0]
    
        ##Building a training set of variants by selecting those in common between the 3 tools (freebayes, mpileup, gatk) + a training set which contains uniq variants detected by gatk caller.
        intersection=self.add_component("IntersectVcf", [variants_freebayes, variants_mpileup, self.gatk_variants])
        if not ".gz" in self.gatk_variants:
            self.gatk_variants=self.gatk_variants+".gz"
        
        ##Splitting vcf calls in 2 separate files (snps, indels)
        inputs_vcfs=[self.gatk_variants, intersection.common_variants, intersection.uniq_variants]
        inputs_vcfs.extend(self.training_set["file"])
        outputs_snps=list()
        outputs_indels=list()
        
        for vcf in inputs_vcfs:
            prefix_name=os.path.splitext(os.path.basename(vcf))[0]
            split_calls=self.add_component("Split_snp_indel", [vcf ,databank_indexed], component_prefix=prefix_name)
            outputs_snps.append(split_calls.snp_variants)
            outputs_indels.append(split_calls.indel_variants)
            
        ##Filtering common variants (training set) to decrease the rate of false positives:
            # on SNPs
        hard_filtering_SNP=self.add_component("HardFiltering", [outputs_snps[1], databank_indexed, "SNP"], component_prefix="SNP")
        
            # on INDEls
        hard_filtering_INDEL=self.add_component("HardFiltering", [outputs_indels[1], databank_indexed, "INDEL"], component_prefix="INDEL")
        
        
        ##Filtering gatk variants with Variant Quality Score Recalibration (VQSR):
            
            #setting resource parameters (training sets)
        snp_training_sets=outputs_snps[3:]
        snp_training_sets.append(outputs_snps[2])
        snp_training_sets.append(hard_filtering_SNP.filtered_variants)
        
        indel_training_sets=outputs_indels[3:]
        indel_training_sets.append(outputs_indels[2])
        indel_training_sets.append(hard_filtering_INDEL.filtered_variants)
        
        names=self.training_set["name"]
        known=self.training_set["known"]
        training=self.training_set["training"]
        truth=self.training_set["truth"]
        priors=self.training_set["prior"]
        
        names.append("uniq_variants")
        known.append("false")
        training.append("true")
        truth.append("false")
        priors.append("10.0")
        
        names.append("common_variants")
        known.append("false")
        training.append("true")
        truth.append("true")
        priors.append("15.0")
        
            # VQSR on SNPs
        vqsr=self.add_component("Vqsr", [databank_indexed, outputs_snps[0], names, snp_training_sets, known, training, truth, priors, "SNP"], component_prefix="SNP")
        
            # VQSR on INDELs
        vqsr=self.add_component("Vqsr", [databank_indexed, outputs_indels[0], names, indel_training_sets, known, training, truth, priors, "INDEL"], component_prefix="INDEL")


    def post_process(self):
        
        #delete intermediate files:
        if self.remove_tmp_files:
            
            #IntersectVcf Component:
            os.system("rm -f "+os.path.join(self.get_component_output_directory("IntersectVcf", "default"), "tmp_common_variants.vcf")+"; rm -f "+os.path.join(self.get_component_output_directory("IntersectVcf", "default"), "tmp_gatk_uniq.vcf"))
            
            #Vqsr Component:
            os.system("rm -f "+os.path.join(self.get_component_output_directory("Vqsr", "default"), "*_SNP*.stderr")+"; rm -f "+os.path.join(self.get_component_output_directory("Vqsr", "default"), "*_INDEL*.stderr"))
