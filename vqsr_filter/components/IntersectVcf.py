#
# Copyright (C) 2015 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Kenza BAZI KABBAJ INRA - SIGENAE'
__version__ = '2.3.2'

from jflow.component import Component
import os
import gzip

def concat_regions (exec_path, zipped_vcf, uniq_gatk, uniq_mpileup, uniq_freebayes, uniq_variants_output, concat_stderr):
    
    import gzip
   
    # Get chromosom/contig names from vcf file (header) to get reference regions : ##contig=<ID=CL1Contig1_1,length=739>
    regions=list()
    for lines in gzip.open(zipped_vcf, "rt"):
        if "##contig" in lines:
            regions.append((lines.strip().split("=")[2]).split(",")[0])
    regions=",".join(regions)
    
    # Concat variants according to reference regions order
    os.system(exec_path+" concat "+uniq_gatk+" "+uniq_mpileup+" "+uniq_freebayes+ " -O v -a -o "+uniq_variants_output+" -r "+regions+" 2>> "+concat_stderr)



class IntersectVcf (Component):
    """
    @summary: This component will use vcf-isec tool to select variants in common between freebayes, mpileup and gatk (training set with high confidence) and to select specific or unique variants from each caller (training set with low confidence). 
    @return: 2 training sets (common_variants, uniq_variants) in vcf format (zipped and indexed)
    """
    
    def define_parameters(self, freebayes_variants, mpileup_variants, gatk_variants):
        
        """
        @param freebayes_variants: [str] path to vcf variants from freebayes (multisample, all regions)
        @param mpileup_variants: [str] path to vcf variants from mpileup (multisample, all regions)
        @param gatk_variants: [str] path to vcf variants from gatk (multisample, all region)
        """
        
        #Inputs
        self.add_input_file("freebayes_variants", "vcf variants from freebayes (multisample, all regions)", default=freebayes_variants, required=True)
        self.add_input_file("mpileup_variants", "vcf variants from mpileup (multisample, all regions)", default=mpileup_variants, required=True)
        self.add_input_file("gatk_variants", "vcf variants from mpileup (multisample, all regions)", default=gatk_variants, required=True)
        
        #Outputs
        self.add_output_file("index_stderr", "stderr file for indexing vcf files", filename="index.stderr")
        self.add_output_file("common_variants", "common variants between the 3 callers", filename="common_variants.vcf")
        self.add_output_file("intersect_stderr", "stderr file for variant intersection", filename="IntersectVcf.stderr")
        self.add_output_file("uniq_variants_stderr", "stderr file for variant complement (unique gatk variants)", filename="uniq_variants.stderr")
        self.add_output_file("uniq_variants", "variants presents in GATK but not in Freebayes and mpileup", filename="uniq_variants.vcf")
        self.add_output_file("concat_stderr", "stderr file for vcf variant concatenation", filename="ConcatVcf.stderr")
        
    def process(self):

        # Zip and index vcf variants if not already done.
        zipped_vcfs=list()
        variants=[self.gatk_variants, self.freebayes_variants, self.mpileup_variants]
        for idx, vcf in enumerate(variants):
            if not vcf.endswith('.gz') and not vcf.endswith('.gz.tbi'):
                zipped_vcfs.append(os.path.join(os.path.dirname(vcf), os.path.basename(vcf).replace(".vcf", ".vcf.gz")))
                self.add_shell_execution("bgzip $1 ; tabix -p vcf $2", cmd_format='{EXE} {IN} {OUT}', inputs=vcf, outputs=zipped_vcfs[idx], local=True, map=False)
            else:
                zipped_vcfs.append(vcf)
        
        # Intersection between vcf variants from the 3 callers by vcf-isec
        self.add_shell_execution(self.get_exec_path("vcf-isec")+" -f -n +3 " + " ".join(zipped_vcfs)+" > $1 2>> $2", cmd_format="{EXE} {OUT}", outputs=[self.common_variants, self.intersect_stderr], includes=zipped_vcfs, map=False)
        
        # Get variants which are present in Gatk but not present in Mpileup and Freebayes:
        tmp_uniq_gatk=self.get_outputs('{basename}', 'tmp_uniq_gatk.vcf.gz')[0]
        self.add_shell_execution(self.get_exec_path("vcf-isec")+" -f -c " + " ".join(zipped_vcfs)+"| cut -f1-8 | bgzip -c > $1 ; tabix -p vcf $1 2>> "+self.uniq_variants_stderr, cmd_format="{EXE} {OUT}", outputs=tmp_uniq_gatk, includes=zipped_vcfs, map=False)
        
        # Get variants which are present in Mpileup but not present in Gatk and Freebayes:
        tmp_uniq_mpileup=self.get_outputs('{basename}', 'tmp_uniq_mpileup.vcf.gz')[0]
        self.add_shell_execution(self.get_exec_path("vcf-isec")+" -f -c " + " ".join([zipped_vcfs[2], zipped_vcfs[0], zipped_vcfs[1]])+" | cut -f1-8 | bgzip -c > $1 ; tabix -p vcf $1 2>> "+self.uniq_variants_stderr, cmd_format="{EXE} {OUT}", outputs=tmp_uniq_mpileup, includes=zipped_vcfs, map=False)
        
        # Get variants which are present in Freebayes but not present in Gatk and Mpileup:
        tmp_uniq_freebayes=self.get_outputs('{basename}', 'tmp_uniq_freebayes.vcf.gz')[0]
        self.add_shell_execution(self.get_exec_path("vcf-isec")+" -f -c " + " ".join([zipped_vcfs[1], zipped_vcfs[0], zipped_vcfs[2]])+" | cut -f1-8 | bgzip -c > $1 ; tabix -p vcf $1 2>> "+self.uniq_variants_stderr, cmd_format="{EXE} {OUT}", outputs=tmp_uniq_freebayes, includes=zipped_vcfs, map=False)

        # Concat uniq variants from each caller and keeping chromosom order according to reference dict:
        self.add_python_execution(concat_regions, cmd_format="{EXE} {ARG} {IN} {OUT}", arguments=self.get_exec_path("bcftools"),inputs=[zipped_vcfs[0], tmp_uniq_gatk, tmp_uniq_mpileup, tmp_uniq_freebayes], outputs=[self.uniq_variants, self.concat_stderr])
        
