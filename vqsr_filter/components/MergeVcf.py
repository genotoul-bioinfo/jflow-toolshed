#
# Copyright (C) 2015 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Kenza BAZI KABBAJ INRA - SIGENAE'
__version__ = '1.1.1'



from jflow.component import Component
import os

class MergeVcf (Component):
    
    """
    @summary: This component will merge vcf files with vcf-merge tool. 
    @return: variants vcf file (multisample).
    """
    
    def define_parameters(self, variants):
        
        """
        @param variants: [list] path to the list of vcf variants
        """
        
        #Inputs
        self.add_input_file_list("variants", "Which variants vcf file should be used", default=variants)
        
        #Outputs
        self.add_output_file( "output_file", "The merged variants vcf file (zipped and indexed)", filename=self.prefix+"_merged.vcf.gz")
        self.add_output_file("Merge_stderr", "stderr file for vcf merge", filename='vcf_merge.stderr')
    
    def process(self):
        
        zipped_vcfs=list()
        for idx, vcf in enumerate(self.variants):
            if not vcf.endswith('.gz') and not vcf.endswith('.gz.tbi'):
                zipped_vcfs.append(os.path.join(os.path.dirname(vcf), os.path.basename(vcf).replace(".vcf", ".vcf.gz")))
                self.add_shell_execution("bgzip $1 ; tabix -p vcf $2", cmd_format='{EXE} {IN} {OUT}', inputs=vcf, outputs=zipped_vcfs[idx], local=True, map=False)
            else:
                zipped_vcfs.append(vcf)

        # Merge vcf files to a multiple sample file
        self.add_shell_execution(self.get_exec_path("vcf-merge")+" "+" ".join(zipped_vcfs)+" | bgzip -c > $1 2>> $2 ; tabix -p vcf $1", cmd_format="{EXE} {OUT}", outputs=[self.output_file, self.Merge_stderr], includes=zipped_vcfs, map=False)
