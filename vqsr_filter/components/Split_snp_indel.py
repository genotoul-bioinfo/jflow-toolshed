#
# Copyright (C) 2015 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Kenza BAZI KABBAJ INRA - SIGENAE'
__version__ = '1.0.0'

from jflow.component import Component
import os

class Split_snp_indel (Component):
    """
     @summary: Splitting vcf file in 2 separate call sets (snp, indels)
     @return: snp variants file (vcf) ; indel variants file (vcf)
    """
    def define_parameters(self, vcf_variants, reference):
        
        """
          @param vcf_variants : [str] path to variants vcf file
          @param reference : [str] path to the indexed reference genome
        """
        
        #Inputs
        self.add_input_file( "vcf_variants", "variants vcf file", default=vcf_variants, file_format="vcf", required=True )
        self.add_input_file( "reference", "The FASTA indexed reference file", default=reference, file_format="fasta", required=True )
        
        #Outputs
        self.add_output_file( "snp_variants", "snp variants file", filename=os.path.splitext(os.path.basename(vcf_variants))[0]+"_SNP.vcf")
        self.add_output_file( "split_stderr", "stderr for splitting variants", filename="split_variants.stderr")
        self.add_output_file( "indel_variants", "indel variants file", filename=os.path.splitext(os.path.basename(vcf_variants))[0]+"_INDEL.vcf")

    def process(self):
        
        # Get memory number
        mem="1G"
        if self.get_memory() != None :
            mem=self.get_memory()
        xmx_option  = "-Xmx" + str(mem).lower()
        
        # Select only SNPs
        self.add_shell_execution(self.get_exec_path("java") + " " + xmx_option + " -jar " + self.get_exec_path("gatk") + " -T SelectVariants -R "+self.reference+" -V $1 -selectType SNP -o $2 2>> "+self.split_stderr, cmd_format='{EXE} {IN} {OUT}', inputs=self.vcf_variants, outputs=self.snp_variants, includes=[os.path.splitext(self.reference)[0]+".dict", self.vcf_variants])
        
        # Select only indels
        self.add_shell_execution(self.get_exec_path("java") + " " + xmx_option + " -jar " + self.get_exec_path("gatk") + " -T SelectVariants -R "+self.reference+" -V $1 -selectType INDEL -o $2 2>> "+self.split_stderr, cmd_format='{EXE} {IN} {OUT}', inputs=self.vcf_variants, outputs=self.indel_variants, includes=[os.path.splitext(self.reference)[0]+".dict", self.vcf_variants])
