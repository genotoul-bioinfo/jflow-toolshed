#
# Copyright (C) 2015 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Kenza BAZI KABBAJ INRA - SIGENAE'
__version__ = '2.1.2'

from jflow.component import Component
import os

class HardFiltering (Component):
    """
     @summary: Filter common variants based on GATK recommandations using VariantFiltration tool in order to keep only variants with good quality. See GATK documentation: https://gatkforums.broadinstitute.org/gatk/discussion/6925/understanding-and-adapting-the-generic-hard-filtering-recommendations
     @return: filtered (SNP or INDEL) common variants
    """
    def define_parameters(self, variants, reference, mode):
        
        """
          @param variants : [str] path to vcf file to filter (common variants)
          @param reference : [str] path to the indexed reference genome
          @param mode: [str] mode SNP or INDEL
        """
        
        #Inputs
        self.add_input_file( "variants", "vcf file to filter (common variants)", default=variants, file_format="vcf", required=True )
        self.add_input_file( "reference", "The FASTA reference file", default=reference, file_format="fasta", required=True )
        self.add_parameter("mode", "mode SNP or INDEL", default=mode, required=True)

        # Outputs
        self.add_output_file( "filtered_variants", "filtered common variants", filename="filtered_common_variants_"+mode+".vcf")
        self.add_output_file( "filter_stderr", "Error file for hard filtering component", filename="hard_filtering.stderr" )

    def process(self):

        # Get memory number
        mem="1G"
        if self.get_memory() != None :
            mem=self.get_memory()
        xmx_option  = "-Xmx" + str(mem).lower()
        
        # Setting filter options
        filter_options=""
        if self.mode == "SNP":
            filter_options="\"QD < 2.0 || FS > 60.0 || MQ < 40.0 || MQRankSum < -12.5 || ReadPosRankSum < -8.0\""
            
        elif self.mode == "INDEL":
            filter_options="\"QD < 2.0 || FS > 200.0 || ReadPosRankSum < -20.0\""
        
        # Filter snps or indels variants
        tmp_tagged_variants=self.get_outputs('{basename}', 'tmp_tagged_variants.vcf')[0]
        self.add_shell_execution(self.get_exec_path("java") + " " + xmx_option + " -jar " + self.get_exec_path("gatk") + " -T VariantFiltration -R "+self.reference+" -V $1 --filterName \""+self.mode+"_Hard_Filtering\" --filterExpression "+filter_options+" -o $2 2>> "+self.filter_stderr, cmd_format='{EXE} {IN} {OUT}' , inputs=self.variants, outputs=tmp_tagged_variants, includes=[os.path.splitext(self.reference)[0]+".dict", self.variants])

        # Discard no passing filters
        self.add_shell_execution(self.get_exec_path("java") + " " + xmx_option + " -jar " + self.get_exec_path("gatk") + " -T SelectVariants -R "+self.reference+" -V $1 --excludeFiltered -o $2 2>> "+self.filter_stderr, cmd_format='{EXE} {IN} {OUT}' , inputs=tmp_tagged_variants, outputs=self.filtered_variants, includes=[os.path.splitext(self.reference)[0]+".dict"])
