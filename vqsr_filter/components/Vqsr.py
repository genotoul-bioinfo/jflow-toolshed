#
# Copyright (C) 2015 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Kenza BAZI KABBAJ INRA - SIGENAE'
__version__ = '2.1.2'


from jflow.component import Component
import os

class Vqsr (Component):
    """
    @summary: Variant Quality Score Recalibration (VQSR) from GATK is designed to filter gatk vcf results by building a variant recalibration model with VariantRecalibrator tool and applying the desired level of recalibration to the variants with ApplyRecalibration tool. It aims to refine the call set by reducing the amount of false positives.
    @return: recalibrated (snps or indels) variants (multisample vcf)
    """
    
    def define_parameters(self, ref_genome, gatk_variants, names, training_set, known, training, truth, prior, mode):
        
        """
        @param ref_genome: [str] path to the indexed reference genome (fasta)
        @param gatk_variants: [str] path to concatenated and sorted gatk variants (vcf)
        @param names: [list] resources names
        @param training_set: [list] paths to resources files (vcf format, snps or indels)
        @param known: [list] set true if the variants are known in dbSNP or false if not
        @param training: [list] set true to use this resource to train the recalibration model or false if not
        @param truth: [list] set true if the variants in this resource are representative of true sites and false if not
        @param prior: [list] the prior likelihood assigned to these variants
        @param mode: [str] mode SNP or INDEL
        """
        
        #Inputs
        self.add_input_file("ref_genome", "indexed reference genome", default=ref_genome, file_format="fasta", required=True)
        self.add_input_file("gatk_variants", "vcf raw variants from gatk (multi-sample, all regions)", default=gatk_variants, file_format="vcf", required=True)
        self.add_parameter_list("names", "resources namess", default=names, required=True)
        self.add_input_file_list( "training_set", "paths to resources files (vcf)", default=training_set, required=True)
        self.add_parameter_list( "known", "set true if the variants are known in dbSNP or false if not", default=known, required=True)
        self.add_parameter_list( "training", "set true to use this resource to train the recalibration model or false if not", default=training, required=True)
        self.add_parameter_list( "truth", "set true if the variants in this resource are representative of true sites and false if not", default=truth, required=True)
        self.add_parameter_list( "prior", "the prior likelihood assigned to these variants", default=prior, required=True)
        self.add_parameter("mode", "mode SNP or INDEL", default=mode, required=True)
        
        #Outputs
        self.add_output_file("filtered_variants", "filtered (snps or indels) variants output file (vcf)", filename="filtered_variants_"+mode+".vcf")
        self.add_output_file("vqsr_stderr", "stderr file for VQSR analysis", filename="VQSR.stderr")
        
    def process(self):
        
        # Get memory number
        mem="1G"
        if self.get_memory() != None :
            mem=self.get_memory()
        xmx_option  = "-Xmx" + str(mem).lower()
        
        # Setting resources options
        resources_options=" "
        for idx, val in enumerate(self.names):
            resources_options+="-resource:"+val+",known="+self.known[idx]+",training="+self.training[idx]+",truth="+self.truth[idx]+",prior="+self.prior[idx]+" "+self.training_set[idx]+" "
        
        annotations="-an QD -an DP -an FS -an MQRankSum -an ReadPosRankSum -an SOR "
        
        if self.mode == "SNP":
            annotations+="-an MQ "
            
        
        #Step 1: Building a variant recalibration model with VariantRecalibrator
        tmp_recal=self.get_outputs('{basename_woext}.recal', self.gatk_variants)[0]
        tmp_tranches=self.get_outputs('{basename_woext}.tranches', self.gatk_variants)[0]
        tmp_scriptR=self.get_outputs('{basename_woext}.R', self.gatk_variants)[0]
        
        self.add_shell_execution(self.get_exec_path("java") + " "+ xmx_option+" -jar "  + self.get_exec_path("gatk") + " -T VariantRecalibrator -R "+self.ref_genome+" -input $1"+resources_options+annotations+"-mode "+self.mode+" -recalFile $2 -tranchesFile $3 -rscriptFile $4 2>> "+self.vqsr_stderr, cmd_format='{EXE} {IN} {OUT}', inputs=self.gatk_variants, outputs=[tmp_recal, tmp_tranches, tmp_scriptR], includes=[self.ref_genome, self.ref_genome+".fai", os.path.splitext(self.ref_genome)[0]+".dict", self.training_set])
        
        #Step 2: Applying the desired level of recalibration to the variants with ApplyRecalibration
        self.add_shell_execution(self.get_exec_path("java") + " "+ xmx_option+" -jar "  + self.get_exec_path("gatk") + " -T ApplyRecalibration -R "+self.ref_genome+" -input $1 -mode "+self.mode+" --ts_filter_level 99.9 -recalFile $2 -tranchesFile $3 -o $4 2>> "+self.vqsr_stderr, cmd_format='{EXE} {IN} {OUT}', inputs=[self.gatk_variants, tmp_recal, tmp_tranches], outputs=self.filtered_variants, includes=[self.ref_genome, self.ref_genome+".fai", os.path.splitext(self.ref_genome)[0]+".dict"])
        
