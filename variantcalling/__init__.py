#
# Copyright (C) 2015 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from jflow.workflow import Workflow

import os
import sys
from io import StringIO
from configparser import ConfigParser

from jflow import seqio

class VariantCalling (Workflow):

    def get_description(self):
        return "Variation calling v1.1 (GATK 3.3 or GATK 3.5)"

    def define_parameters(self, function="process"):
        self.add_input_file("reference", "The reference used to align reads.", required=True, group="Input")
        
        # process
        self.add_parameter("is_rna", "The sequence in BAM are RNA (add '-recoverDanglingHeads -dontUseSoftClippedBases' to calling options). Keep in mind to change min confidence and quality when you used RNASeq data.", default=False, type="bool" , group="Process option")
        self.add_parameter( "rmdup", "Perform rmdup", type="bool", default=False , group="Process option")
        self.add_parameter( "filter", "Filter low quality alignment, do not use with STAR alignment", type="bool", default=False , group="Process option")
        self.add_parameter( "is_star_aln", "To set if alignment was perform with STAR. Set the alignement quality to 30 ", type="bool", default=False, group="Process option" )
        self.add_parameter( "is_single", "IF BAM PROVIDED : Set true if you provide alignment of single library",  type="bool", default=False,group="Process option")
        self.add_parameter( "add_rg", "Does your data need an addreadgroup to be performed ?", type="bool", default=False, group="Process option" )
        # Libraries
        self.add_multiple_parameter_list("library", "Library.", required=True, group="Input")
        self.add_input_file("bam", "Bam files, if bam provided, do not perform alignment.", required=True, add_to="library")
        self.add_parameter("sample", "Sample Name.", add_to="library")
        self.add_parameter("sequencer", "The sequencer type.", choices=["HiSeq2000", "ILLUMINA","SLX","SOLEXA","SOLID","454","COMPLETE","PACBIO","IONTORRENT","CAPILLARY","HELICOS","UNKNOWN"], default="ILLUMINA", add_to="library")
        
        # Parameter GATK
        self.add_parameter( "global_calling", "The variants discovery is lead with all samples and not sample by sample.", default=False, type="bool" , group="GATK options")
        self.add_parameter( "calling_options", "Add calling option to Haplotype caller (other than stand_call_conf, stand_emit_conf, mbq, ERC, variant_index_type, variant_index_parameter).", default=None , group="GATK options")
        self.add_parameter( "min_call_confidence", "The minimum phred-scaled confidence threshold at which variants should be called.", default=30, type="float" , group="GATK options")
        self.add_parameter( "min_emit_confidence", "The minimum phred-scaled confidence threshold at which variants should be emitted (and filtered with LowQual if less than the calling threshold)", default=10, type="float" , group="GATK options")
        self.add_parameter( "min_qual_score", "Minimum base quality required to consider a base for calling, gatk mbq option.", default=10, type="int" , group="GATK options")
        #self.add_parameter("restrictl","Set the left coordinate of your restricted region",type="int", default=1, group="GATK options")
        #self.add_parameter("restrictr","Set the right coordinate of your restricted region",type="int", default=16657, group="GATK options")

        self.add_parameter( "fs_snp_filter", "First step snp filter. \nDefault : -window 35 -cluster 3 --filterExpression \"QD < 2.0 || FS > 100.0 || MQ < 40.0 || HaplotypeScore > 13.0 || MappingQualityRankSum < -12.5 || ReadPosRankSum < -8.0\" --filterName \"hard_filter\".", 
                            default="-window 35 -cluster 3 --filterExpression \"QD < 2.0 || FS > 100.0 || MQ < 40.0 || HaplotypeScore > 13.0 || MappingQualityRankSum < -12.5 || ReadPosRankSum < -8.0\" --filterName \"hard_filter\"" , group="GATK filtering options")
        self.add_parameter( "fs_indel_filter", "Fist step indel filter. \n Default : -window 35 -cluster 3 --filterExpression \"QD < 2.0 || FS > 200.0 || ReadPosRankSum < -20.0\" --filterName \"hard_filter\"", 
                            default="-window 35 -cluster 3 --filterExpression \"QD < 2.0 || FS > 200.0 || ReadPosRankSum < -20.0\" --filterName \"hard_filter\"" ,group="GATK filtering options")
        self.add_parameter( "final_snp_filter", "Final step snp filter. \nDefault : "+'-filterName FS --filterExpression \"FS > 30.0\"', 
                            default='-filterName FS --filterExpression \"FS > 30.0\"' , group="GATK filtering options")
        self.add_parameter( "final_indel_filter", "Final step indel filter. \n Default : "+'-filterName FS --filterExpression \"FS > 30.0\"', 
                            default='-filterName FS --filterExpression \"FS > 30.0\"', group="GATK filtering options")
        
        
        self.add_parameter("two_steps_calling", "The SNP calling is realised in two step. The first step (recalibration, calling, filter) has hard filters. The second step (recalibration, calling, filter) has standard filters and the variants detected in the first step are used as database of known polymorphic sites.",
                           display_name="Two steps for SNP calling", type="bool", default=False)
        self.add_input_file("known_snp", "Known snp for the reference.", rules="Exclude=two_steps_calling")
        self.add_input_file("sequenced_regions", "Bed file which describe which region are sequenced. this information is used to reduce analysis region.", group="GATK options")

        
    def update_sequenced_regions(self):
        if self.sequenced_regions == None and self.global_calling:
            self.sequenced_regions = self.get_temporary_file(".intervals")
            FH_sequenced_regions = open(self.sequenced_regions, "w")
            reader = seqio.FastaReader(self.reference)
            for id, desc, seq, qualities in reader:
                FH_sequenced_regions.write( id + "\n" )
            FH_sequenced_regions.close()

    def process(self):
        
        JAVA_HUGE_MEM = "8G"
        self.update_sequenced_regions()
        samples = dict()
        lib_paired_idx = 0
        lib_single_idx = 0
        single_fastq, pair1_fastq, pair2_fastq = [], [], []
        single_librairies_sequencers, pair_librairies_sequencers = [], []
        
        #check if bam provided
        start_from_bam=False
        is_paired=not(self.is_single)
        ###############################
        # Index
        ###############################
        databank_fai= self.reference
        databank_bwa= self.reference
        if not os.path.exists(self.reference + ".fai") or not os.path.exists(os.path.splitext(self.reference)[0] + ".dict"):
            index_fai_dict = self.add_component( "IndexFaiDict", [self.reference] )
            databank_fai = index_fai_dict.databank

    
        # Variant calling and annotation
        ##############################
        # Pre-process
        spliced_aln = self.is_rna
        
        sort_bam_index = self.add_component("SamtoolsIndex", [self.library["bam"],"10G",4])
        

        if is_paired :
            variant_preprocess = self.add_component("VariantPreprocess", [databank_fai, sort_bam_index.sorted_bams, self.library["sequencer"], True, self.rmdup, self.add_rg, self.filter, 30, JAVA_HUGE_MEM], component_prefix="paired" )

        else :
            variant_preprocess = self.add_component("VariantPreprocess", [databank_fai, sort_bam_index.sorted_bams, self.library["sequencer"], False, self.rmdup, self.add_rg, self.filter, 30, JAVA_HUGE_MEM], component_prefix="single" )

        sorted_rg_bam = variant_preprocess.output_files
        idx_files = variant_preprocess.index_files


        set_value_qual_aln=None
        if self.is_star_aln :
            set_value_qual_aln=30
        gatk_preprocess    = self.add_component("GatkPreprocess", [variant_preprocess.output_files, variant_preprocess.index_files, databank_fai, JAVA_HUGE_MEM, spliced_aln, set_value_qual_aln, self.sequenced_regions] )
        
        # Merge BAM with the same sample
        bam_per_samples={}
        i=0
        for sample in self.library:
            if not sample["sample"] in bam_per_samples.keys() :
                bam_per_samples[sample["sample"]] = [gatk_preprocess.output_files[i]]
            else :
                bam_per_samples[sample["sample"]].append(gatk_preprocess.output_files[i])
            i+=1;
        gatk_preprocess_bams = []
        for key in bam_per_samples.keys() :
            if len(bam_per_samples[key])>1:
                merged_bam = self.add_component("SamtoolsMerge", [bam_per_samples[key], key], component_prefix=key)
                gatk_preprocess_bams.append(merged_bam.merged_bam)
            else :
                gatk_preprocess_bams.append(bam_per_samples[key][0])
         
        # Variant calling
        pre_vcf_file = self.known_snp
        if self.two_steps_calling: # if known_snp is not provide and two_steps_calling True
            # SNP calling with hards parameters to produce a database of safe polymorphic sites
            gatk_recalibration_pre_step    = self.add_component("GatkRecalibration", [gatk_preprocess_bams, databank_fai, None, self.sequenced_regions, JAVA_HUGE_MEM], component_prefix="pre_step")
            gatk_haplotype_caller_pre_step = self.add_component("GatkHaplotypeCaller", [gatk_recalibration_pre_step.output_files, databank_fai, self.min_call_confidence+10, self.min_emit_confidence+10, self.min_qual_score, self.is_rna, self.sequenced_regions, JAVA_HUGE_MEM, self.global_calling, self.calling_options], component_prefix="pre_step")
            gatk_filter_pre_step           = self.add_component("GatkVariantFilter", [gatk_haplotype_caller_pre_step.output_file, databank_fai,
                                                                                      self.fs_snp_filter, 
                                                                                      self.fs_indel_filter,
                                                                                      JAVA_HUGE_MEM],
                                                                component_prefix="pre_step")
            pre_vcf_file = gatk_filter_pre_step.output_file
        # SNP calling with standards parameters to produce finals variants
        gatk_recalibration    = self.add_component("GatkRecalibration", [gatk_preprocess_bams, databank_fai, pre_vcf_file, self.sequenced_regions, JAVA_HUGE_MEM], component_prefix="final")
        
        hc_final = self.add_component("GatkHaplotypeCaller", [gatk_recalibration.output_files, databank_fai, self.min_call_confidence, self.min_emit_confidence, self.min_qual_score, self.is_rna, self.sequenced_regions, JAVA_HUGE_MEM, self.global_calling, self.calling_options], component_prefix="final")
        vc_final = self.add_component("GatkVariantFilter", [hc_final.output_file, databank_fai,
                                                                         self.final_snp_filter, 
                                                                         self.final_indel_filter,
                                                                         JAVA_HUGE_MEM],
                                                   component_prefix="final")




  
