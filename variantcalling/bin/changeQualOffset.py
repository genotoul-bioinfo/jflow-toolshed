#!/usr/bin/env python2.7
#
# Copyright (C) 2014 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Frederic Escudie - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2014 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.2.0'
__email__ = 'support.genopole@toulouse.inra.fr'
__status__ = 'dev'


import os
import gzip
import argparse


##################################################################################################################################################
#
# FUNCTIONS
#
##################################################################################################################################################
def is_gzip( file ):
    """
    @return: [bool] True if the file is gziped.
    @param file : [str] Path to processed file.
    """
    is_gzip = None
    FH_input = gzip.open( file )
    try:
        FH_input.readline()
        is_gzip = True
    except:
        is_gzip = False
    finally:
        FH_input.close()
    return is_gzip


class Sequence:
    def __init__(self, id, string, description=None, quality=None):
        """
        @param id : [str] Id of the sequence.
        @param string : [str] Sequence of the sequence.
        @param description : [str] The sequence description.
        @param quality : [str] The quality of the sequence (same length as string).
        """
        self.id = id
        self.description = description
        self.string = string
        self.quality = quality


class FastqIO:
    def __init__( self, filepath, mode="r" ):
        """
        @param filepath : [str] The filepath.
        @param mode : [str] Mode to open the file ('r', 'w', 'a').
        """
        self.filepath = filepath
        self.mode = mode
        if (mode in ["w", "a"] and filepath.endswith('.gz')) or (mode not in ["w", "a"] and is_gzip(filepath)):
            self.file_handle = gzip.open( filepath, mode )
        else:
            self.file_handle = open( filepath, mode )
        self.current_line_nb = 1
        self.current_line = None

    def __del__( self ):
        self.close()

    def close( self ) :
        if hasattr(self, 'file_handle') and self.file_handle is not None:
            self.file_handle.close()
            self.file_handle = None
            self.current_line_nb = None

    def __iter__( self ):
        seq_id = None
        seq_desc = None
        seq_str = None
        seq_qual = None
        try:
            for line in self.file_handle:
                line = line.rstrip()
                if (self.current_line_nb % 4) == 1:
                    fields = line[1:].split(None, 1)
                    seq_id = fields[0]
                    seq_desc = fields[1] if len(fields) == 2 else None
                elif (self.current_line_nb % 4) == 2:
                    seq_str = line
                elif (self.current_line_nb % 4) == 0:
                    seq_qual = line
                    yield Sequence( seq_id, seq_str, seq_desc, seq_qual )
                self.current_line_nb += 1
        except:
            raise IOError( "The line " + str(self.current_line_nb) + " in '" + self.filepath + "' cannot be parsed by " + self.__class__.__name__ + "." )

#//////////////////////
    def next_seq( self ):
        """
        @summary : Returns the next sequence.
        @return : [Sequence] The next sequence.
        """
        seq_record = None
        try:
            # Header
            header = self.file_handle.readline().strip()
            fields = header[1:].split(None, 1)
            seq_id = fields[0]
            seq_desc = fields[1] if len(fields) == 2 else None
            self.current_line_nb += 1
            # Sequence
            seq_str = self.file_handle.readline().strip()
            self.current_line_nb += 1
            # Separator
            separator = self.file_handle.readline()
            self.current_line_nb += 1
            # Quality
            seq_qual = self.file_handle.readline().strip()
            self.current_line_nb += 1
            # Record
            seq_record = Sequence( seq_id, seq_str, seq_desc, seq_qual )
        except:
            raise IOError( "The line " + str(self.current_line_nb) + " in '" + self.filepath + "' cannot be parsed by " + self.__class__.__name__ + ".\n"
                            + "content : " + line )
        return seq_record

    @staticmethod
    def is_valid(filepath):
        try:
            FH_in = FastqIO(filepath)
            seq_idx = 0
            previous = None
            while seq_idx < 10 and (seq_idx != 0 and previous is not None):
                previous = FH_in.next_seq()
                seq_idx += 1
            return True
        except:
            return False

    def write( self, sequence_record ):
        self.file_handle.write( self.seqToFastqLine(sequence_record) + "\n" )

    def seqToFastqLine( self, sequence ):
        """
        @summary : Returns the sequence in fastq format.
        @param sequence : [Sequence] The sequence to process.
        @return : [str] The sequence.
        """
        seq = "@" + sequence.id + (" " + sequence.description if sequence.description is not None else "")
        seq += "\n" + sequence.string
        seq += "\n+"
        seq += "\n" + sequence.quality
        return seq


def get_offset( min_ascii, max_ascii ):
    """
    @summary: Returns the quality offset corresponding to an ascii interval.
    @param min_ascii: [int] The int representation of the minimum ascii find.
    @param max_ascii: [int] The int representation of the maximum ascii find.
    @return: [int] The quality offset.
    """
    if min_ascii >= 59:
        return 64
    else:
        return 33


def get_files_offset( paths ):
    """
    @summary: Returns the quality offset deduced from the files.
    @param paths: [list] Files to prase (R1 or R1 and R2).
    @return: [int] The quality offset.
    """
    min_ascii = 8000
    max_ascii = -8000
    for current_path in paths:
        FH_in = FastqIO( current_path )
        for record in FH_in:
            for qual in list(record.quality):
                ascii_nb = ord(qual)
                if ascii_nb > max_ascii:
                    max_ascii = ascii_nb
                if ascii_nb < min_ascii:
                    min_ascii = ascii_nb
        FH_in.close()
    return get_offset( min_ascii, max_ascii )


def change_offset( in_path, out_path, offset_diff ):
    """
    @summary: Writes a file with the new quality offset from a fastq.
    @param in_path: [str] The initial fastq.
    @param out_path: [str] The output file.
    @param offset_diff: [int] The offset difference (example: -31 for 'Solexa' to 'Illumina 1.8+').
    """
    FH_in = FastqIO( in_path )
    FH_out = FastqIO( out_path, "w" )
    for record in FH_in:
        new_qual = ""
        for qual in list(record.quality):
            new_qual += chr(ord(qual) + offset_diff)
        record.quality = new_qual
        FH_out.write( record )
    FH_in.close()
    FH_out.close()


##################################################################################################################################################
#
# MAIN
#
##################################################################################################################################################
if __name__ == "__main__":
    # Manage parameters
    parser = argparse.ArgumentParser( description='Changes quality offset of the input file or the pair of input files.' )
    parser.add_argument( '-i1', '--R1-file', required=True, help='The reads_1 file (format : FASTQ).' )
    parser.add_argument( '-i2', '--R2-file', default=None, help='The reads_2 file (format : FASTQ).' )
    parser.add_argument( '-o1', '--ouput-R1-file', required=True, help='The reads_1 file after recode.' )
    parser.add_argument( '-o2', '--ouput-R2-file', default=None, help='The reads_2 file after recode.' )
    parser.add_argument( '-nq', '--new-qual-offset', type=int, default=33, required=True, help='The new quality offset (example: 33 for Illumina 1.8+).' )
    parser.add_argument( '-oq', '--old-qual-offset', type=int, default=None, help='The old quality offset (example: 64 for Solexa). By default, this value is automatically deduced from your reads files.' )
    parser.add_argument( '-v', '--version', action='version', version=__version__ )
    args = parser.parse_args()

    # Retrieve offset diff
    offset_diff = None
    if args.old_qual_offset is not None:
        offset_diff = args.new_qual_offset - args.old_qual_offset
    else:
        files_offset = None
        if args.R2_file is None:
            files_offset = get_files_offset( [args.R1_file] )
        else:
            files_offset = get_files_offset( [args.R1_file, args.R2_file] )
        offset_diff = args.new_qual_offset - files_offset

    # Change file(s)
    if offset_diff == 0:
        if os.path.exists(args.ouput_R1_file): os.unlink(args.ouput_R1_file)
        os.symlink( args.R1_file, args.ouput_R1_file )
        if args.R2_file is not None:
            if os.path.exists(args.ouput_R2_file): os.unlink(args.ouput_R2_file)
            os.symlink( args.R2_file, args.ouput_R2_file )
    else:
        change_offset( args.R1_file, args.ouput_R1_file, offset_diff )
        if args.R2_file is not None:
            change_offset( args.R2_file, args.ouput_R2_file, offset_diff )