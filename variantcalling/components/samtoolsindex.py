#
# Copyright (C) 2015 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os

from jflow.component import Component

class SamtoolsIndex (Component):
    """
     @warning : samtools version 1.3
    """

    def define_parameters(self, bams, mem="4G", cpu=1):
        # Parameters
        self.add_parameter("mem", "The memory size to allocate (in Giga).", default=mem)
        if self.get_memory() != None :
            self.mem=self.get_memory()
        self.add_parameter("cpu", "The number of cpu to use.", default=cpu, type="int")
        if self.get_cpu() != None :
            self.cpu=self.get_cpu()
        # Files
        self.add_input_file_list("bams", "Which bam files should be indexed.", file_format="bam", default=bams, required=True)
        self.add_output_file_list("sorted_bams", "The sorted bam files.", pattern='{basename}', items=bams, file_format="bam")
        self.add_output_file_list("output_files", "The bai files.", pattern='{basename}.bai', items=bams, file_format="bai")
        self.add_output_file_list("stderrs", "The stderr files.", pattern='{basename}.stderr', items=bams)

# With version 1.3 need to set the output as -o output
    def process(self):
        self.add_shell_execution( self.get_exec_path("samtools") + " sort -m " + 
                                  str(self.mem)+" -@ " +str(self.cpu) + " $1 -o $2 ; " + 
                                  self.get_exec_path("samtools") + " index $2 2>> $3", 
                                  cmd_format='{EXE} {IN} {OUT}' , inputs=[self.bams], 
                                  outputs=[self.sorted_bams, self.stderrs, self.output_files] , 
                                  map=True)  
