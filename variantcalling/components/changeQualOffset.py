#
# Copyright (C) 2015 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os

from jflow.component import Component

class ChangeQualOffset (Component):
    """
     @summary : Changes quality offset of the input file or the pair of input files.
    """

    def define_parameters( self, R1_files, R2_files=None, new_qual_offset=33, old_qual_offset=None ):
        """
         @param R1_files : [list] The reads_1 files list.
         @param R2_files : [str] The reads_2 files list.
         @param new_qual_offset : [int] The new quality offset (example: 33 for Illumina 1.8+).
         @param old_qual_offset : [int] The old quality offset (example: 64 for Solexa).
        """
        # Parameters
        self.add_parameter( "new_qual_offset", "The new quality offset (example: 33 for Illumina 1.8+).", default=new_qual_offset, type="int" )
        self.add_parameter( "old_qual_offset", "The old quality offset (example: 64 for Solexa). By default, this value is automatically deduced from your reads files.", default=old_qual_offset, type="int" )

        # Files
        self.add_input_file_list( "R1_files", "The reads_1 files list.", default=R1_files, file_format="fastq", required=True )
        self.add_input_file_list( "R2_files", "The reads_2 files list.", default=R2_files, file_format="fastq")
        self.add_output_file_list( "R1_out_files", "The reads_1 files after recode.", pattern='{basename}', items=R1_files, file_format="fastq" )
        if R2_files != None:
            self.add_output_file_list( "R2_out_files", "The reads_2 files after recode.", pattern='{basename}', items=R2_files, file_format="fastq" )
        self.add_output_file_list( "stderrs", "The stderr output files.", pattern='{basename}.stderr', items=R1_files )

    def process(self):
        options = "--new-qual-offset " + str(self.new_qual_offset)
        options += " --old-qual-offset " + str(self.old_qual_offset) if self.old_qual_offset != None else ""
        if len(self.R2_files) != 0:
            self.add_shell_execution(self.get_exec_path("changeQualOffset.py") + " " + options + " --R1-file $1 --R2-file $2 --ouput-R1-file $3 --ouput-R2-file $4 2>> $5 ", 
                                     cmd_format='{EXE} {IN} {OUT}' , inputs=[self.R1_files, self.R2_files], 
                                     outputs=[self.R1_out_files, self.R2_out_files, self.stderrs] , map=True)
        else:
            self.add_shell_execution(self.get_exec_path("changeQualOffset.py") + " " + options + " --R1-file $1 --ouput-R1-file $2 2>> $3 ", 
                                     cmd_format='{EXE} {IN} {OUT}' , inputs=[self.R1_files], 
                                     outputs=[self.R1_out_files, self.stderrs] , map=True)