#
# Copyright (C) 2015 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os

from jflow.component import Component

def create_dictionary(exec_path, dict, xmx, input_fasta, databank, stdout_path, stderr_path):
    from subprocess import Popen, PIPE
    # first make the symbolic link
    os.symlink(input_fasta, databank)
    # then execute bwa index
    cmd = ["java",xmx, "-jar", exec_path, "R=", databank,"O=", dict]
    p = Popen(cmd, stdout=PIPE, stderr=PIPE)
    stdout, stderr = p.communicate()
    # write down the stdout
    stdoh = open(stdout_path, "w")
    stdoh.write(str(stdout))
    stdoh.close()
    # write down the stderr
    stdeh = open(stderr_path, "w")
    stdeh.write(str(stderr))
    stdeh.close()
    # check error status
    if p.returncode != 0:
        stdeh = open(stderr_path)
        error_msg = "".join( map(str, stdeh.readlines()) )
        stdeh.close()
        raise Exception( "Error in subprocess create_dictionary.\n\tCmd : " + " ".join(cmd) + "\n\tError : " + error_msg )

def samtools_faidx(exec_path, databank, fai, stdout_path, stderr_path):
    from subprocess import Popen, PIPE
    # then execute bwa index
    cmd = [exec_path, "faidx", databank]
    p = Popen(cmd, stdout=PIPE, stderr=PIPE)
    stdout, stderr = p.communicate()
    # write down the stdout
    stdoh = open(stdout_path, "w")
    stdoh.write(str(stdout))
    stdoh.close()
    # write down the stderr
    stdeh = open(stderr_path, "w")
    stdeh.write(str(stderr))
    stdeh.close()
    # check error status
    if p.returncode != 0:
        stdeh = open(stderr_path)
        error_msg = "".join( map(str, stdeh.readlines()) )
        stdeh.close()
        raise Exception( "Error in subprocess samtools_faidx.\n\tCmd : " + " ".join(cmd) + "\n\tError : " + error_msg )


class IndexFaiDict (Component):

    def define_parameters( self, input_file ):
        # Parameters
        self.memory="2g"
        if self.get_memory() != None :
            self.memory=self.get_memory()
        self.xmx_option  = "-Xmx" + str(self.memory).lower() 
        

        # Files
        self.add_input_file( "input_file", "The fasta file to index.", default=input_file, file_format="fasta", required=True )
        self.add_output_file( "databank", "The databank : fasta file linked to the dictionary and the index file.", filename=os.path.basename(input_file), file_format="fasta" )
        #    dictionary
        self.add_output_file( "dict", "The dictionary file.", filename=(os.path.splitext(os.path.basename(input_file))[0] + ".dict") )
        self.add_output_file( "stdout_createdict", "The stdout output file of dictionary creation.", filename="createdict.stdout")
        self.add_output_file( "stderr_createdict", "The stderr output file of dictionary creation.", filename="createdict.stderr")
        #    index
        self.add_output_file( "fai", "The index file", filename=(os.path.basename(input_file) + ".fai") )
        self.add_output_file( "stdout_faidx", "The stdout output file of index creation.", filename="faidx.stdout")
        self.add_output_file( "stderr_faidx", "The stderr output file of index creation.", filename="faidx.stderr")

    def process(self):
        # Create dict
        self.add_python_execution(create_dictionary, cmd_format="{EXE} {ARG} {IN} {OUT}", 
                                               inputs=self.input_file, 
                                               outputs=[self.databank, self.stdout_createdict, self.stderr_createdict], 
                                               arguments=[self.get_exec_path("create_dictionary"),self.dict,self.xmx_option])

        # create index
        self.add_python_execution(samtools_faidx, cmd_format="{EXE} {ARG} {IN} {OUT}",
                                  inputs=self.databank, 
                                  outputs=[self.fai, self.stdout_faidx, self.stderr_faidx], 
                                  arguments=[self.get_exec_path("samtools")])