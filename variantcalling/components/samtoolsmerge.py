#
# Copyright (C) 2015 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os

from jflow.component import Component

class SamtoolsMerge (Component):
    """
     @warning : samtools version 0.1.19 or later.
    """

    def define_parameters(self, bams, sample_name):
        # Parameters
        self.add_parameter("sample_name", "Sample name.", default=sample_name)

        # Files
        self.add_input_file_list("bams", "Which bam files should be merged.", file_format="bam", default=bams, required=True)
        self.add_output_file("merged_bam", "The merged bam file.", filename=sample_name + ".bam", file_format="bam")
        self.add_output_file("stderr", "The stderr file.", filename=sample_name + '.stderr')

    def process(self):
        self.add_shell_execution( self.get_exec_path("samtools") + " merge $1 " + " ".join( self.bams ) + " 2>> $2", 
                                  cmd_format='{EXE} {OUT}' , includes=self.bams, outputs=[self.merged_bam, self.stderr],
                                  map=False)  
