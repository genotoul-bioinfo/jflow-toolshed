#
# Copyright (C) 2015 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os

from jflow.component import Component

class GatkHaplotypeCaller (Component):
    """
    @summary : Call SNPs and indels simultaneously via local de-novo assembly of haplotypes in an active region.
    @see : http://www.broadinstitute.org/gatk/gatkdocs/org_broadinstitute_sting_gatk_walkers_haplotypecaller_HaplotypeCaller.html
    """

    def define_parameters(self, input_files, reference, min_call_confidence=30.0, min_emit_confidence=30.0, min_qual_score=10, 
                          is_rna=False, sequenced_regions_file=None, memory="2G", global_calling=False, calling_option=None):
        """
         @param input_files : [list] Path to the bam files processed.
         @param reference : [str] Path to the reference.
         @param min_call_confidence : [int] The minimum confidence needed for a given base for it
                                       to be used in variant calling.
         @param min_emit_confidence : [float] This argument allows you to emit low quality calls as
                                       filtered records.
         @param min_qual_score : [float] The minimum phred-scaled Qscore threshold to separate high
                                  confidence from low confidence calls. Only genotypes with
                                  confidence >= this threshold are emitted as called sites.
         @param sequenced_regions : [str] file with intervals that will be processed.
         @param memory : [int] the booked memory for process (in Go).
        """
        # Parameters
        self.add_parameter( "memory", "The memory size to allocate (in Giga).", default=memory)
        if self.get_memory() != None :
            self.memory=self.get_memory()
        self.add_parameter( "min_call_confidence", "The minimum confidence needed for a given base for it to be used in variant calling.", default=min_call_confidence, type="float" )
        self.add_parameter( "min_emit_confidence", "This argument allows you to emit low quality calls as filtered records.", default=min_emit_confidence, type="float" )
        self.add_parameter( "min_qual_score", "This argument allows you to emit low quality calls as filtered records.", default=min_qual_score, type="int" )
        self.add_parameter( "is_rna", "The sequence in BAM are RNA (add '-recoverDanglingHeads -dontUseSoftClippedBases' to calling options).", default=is_rna, type="bool" )
        self.add_parameter( "global_calling", "The variants discovery is lead with all samples and not sample by sample.", default=global_calling, type="bool" )
        self.add_parameter( "add_calling_options", "Add calling option to Haplotype caller (other than stand_call_conf, stand_emit_conf, mbq, ERC, variant_index_type, variant_index_parameter).", default=calling_option)
        # Files
        self.add_input_file_list( "input_files", "The BAM files processed.", default=input_files, file_format="bam", required=True )
        self.add_input_file( "reference", "The FASTA reference file.", default=reference, file_format="fasta", required=True )
        self.add_input_file( "sequenced_regions_file", "The processed regions.", default=sequenced_regions_file )
        if sequenced_regions_file == None and global_calling:
            raise Exception( "'sequenced_regions_file' must be defined with global_calling." )
        self.add_output_file( "output_file", "The variants file.", filename="variant.vcf", file_format="vcf" )
        self.add_output_file( "stderr", "The stderr output file.", filename="haplotypecaller.stderr" )
        if not global_calling : 
            self.add_output_file_list( "tmp_files", "Calling files per bam.", pattern="{basename_woext}.vcf", items=input_files )
        
    def process(self):
        xmx_option  = "-Xmx" + str(self.memory).lower() 
        calling_options = " -stand_call_conf " + str(self.min_call_confidence) \
                        + " -stand_emit_conf " + str(self.min_emit_confidence) \
                        + " -mbq " + str(self.min_qual_score)
        if self.is_rna:
            calling_options += " -dontUseSoftClippedBases "

           
        if self.add_calling_options != "" and self.add_calling_options != "None" :
            calling_options += " "+self.add_calling_options
            
        if not self.global_calling:
            include_files = [self.sequenced_regions_file] if self.sequenced_regions_file != None else []
            

            # Calling
            restriction_option = " -L " + self.sequenced_regions_file if self.sequenced_regions_file != None else ""
            if len(self.input_files) > 1: # multi-samples options
                calling_options += " -ERC GVCF -variant_index_type LINEAR -variant_index_parameter 128000"
                
            
            self.add_shell_execution(self.get_exec_path("java") + " " + xmx_option + " -jar " + self.get_exec_path("gatk") + " -T HaplotypeCaller"\
                                     + " -R " + self.reference + " -I $1" + calling_options + restriction_option + " -o $2 2>> " + self.stderr, 
                                     cmd_format='{EXE} {IN} {OUT}' ,inputs=self.input_files, outputs=self.tmp_files, 
                                     includes=[self.reference] + include_files , map=True)

            # Merge results for cohort with multiples samples
            if len(self.input_files) > 1:
                self.add_shell_execution(self.get_exec_path("java") + " " + xmx_option + " -jar " + self.get_exec_path("gatk") + " -T GenotypeGVCFs "\
                                       + restriction_option + " -R $1 --variant " + " --variant ".join(self.tmp_files)  + " -o $2 2>> " + self.stderr, 
                                       cmd_format='{EXE} {IN} {OUT}' , inputs=self.reference, outputs=self.output_file, includes=self.tmp_files + include_files, map=False )
        else:
            include_files = [self.sequenced_regions_file]

            # Retrieve regions
            FH_sequenced_regions = open( self.sequenced_regions_file )
            sequenced_regions = [line.strip() for line in FH_sequenced_regions]
            FH_sequenced_regions.close()

            # Calling
            tmp_outputs = self.get_outputs( '{basename}.vcf', [region.replace(":", "_") for region in sequenced_regions] )
            for idx, current_region in enumerate(sequenced_regions):
                restriction_option = " -L '" + current_region + "'"
                self.add_shell_execution(self.get_exec_path("java") + " " + xmx_option + " -jar " + self.get_exec_path("gatk") + " -T HaplotypeCaller"\
                                         + " -R " + self.reference + " -I " + " -I ".join( self.input_files ) + " " + calling_options + restriction_option\
                                         + " -o $1 2>> " + self.stderr, cmd_format='{EXE} {OUT}', outputs=tmp_outputs[idx], 
                                         includes=[self.reference] + include_files + self.input_files , map=False)

            # Concat results
            self.add_shell_execution(self.get_exec_path("vcf-concat") + " " + " ".join(tmp_outputs)  + " > $1 2>> " + self.stderr, 
                                     cmd_format='{EXE} {OUT}' , outputs=self.output_file, includes=tmp_outputs , map=False)
