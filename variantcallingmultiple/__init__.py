#
# Copyright (C) 2015 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Kenza BAZI KABBAJ INRA - SIGENAE'
__version__ = '2.5.2'


from jflow.workflow import Workflow
import os
from jflow import seqio


class Variant_Calling_Multiple (Workflow):
    
    def get_description(self):
        return "Variant calling by GATK or Freebayes or Samtools mpileup"
        
    def define_parameters(self, function="process"):
        
        #Input files
        self.add_input_file("ref_genome", "The reference genome in fasta format", file_format="fasta", required=True, group="input files")
        
        self.add_multiple_parameter_list("sample", "sample reads definition", group="input files", flag="--sample")
        self.add_input_file("reads_1", "Which fastq read 1 should be used", file_format="fastq", add_to = "sample", required=True)
        self.add_input_file("reads_2", "Which fastq read 2 should be used (PE data)", file_format="fastq", add_to = "sample")
        self.add_parameter("sequencer", "The sequencer type (default=ILLUMINA)", choices=["HiSeq2000", "ILLUMINA","SLX","SOLEXA","SOLID","454","COMPLETE","PACBIO","IONTORRENT","CAPILLARY","HELICOS","UNKNOWN"], default="ILLUMINA", add_to = "sample", required=True)
        self.add_parameter("name", "sample name", required=True, type='str', group="input files", add_to = "sample")
        
        self.add_input_file("sequenced_regions", "Bed file which describes which regions are sequenced. this information is used to reduce analysed regions.", group="input files")
        
        self.add_input_file("known_variant", "known variant for the GATK recalibration in VCF format", group="input files")
        
        # General options
        self.add_parameter_list("tools", "Which caller(s) should be used", group="General options", type='str', choices=["freebayes","gatk","mpileup"], required=True)
        self.add_parameter("min_mapping_quality", "Minimum read mapping quality required to consider a read for calling, default=30", type='int', default=30, group="General options")
        self.add_parameter("remove_tmp_files", "add this option to remove temporary files at the end of the pipeline execution, default=false", type='bool', default=False, group="General options")

        # Preprocessing options
        self.add_parameter("min_seed_length", "min seed length of bwa alignment, option=k in bwa mem, default=19", default=19, group="Preprocessing")
        
        # Gatk Haplotype caller options
        self.add_parameter( "stand_call_conf", "The minimum phred-scaled confidence threshold at which variants should be called,(and filtered with LowQual if less than the calling threshold), default=30.", default=30, type="float" , group="GATK options")
        self.add_parameter( "calling_options", "Add calling option to Haplotype caller using double-quote (Default=\"-stand_call_conf " + str(self.stand_call_conf) + " -mmq " + str(self.min_mapping_quality) + " -ERC GVCF -variant_index_type LINEAR -variant_index_parameter 128000\")", default="-stand_call_conf " + str(self.stand_call_conf) + " -mmq " + str(self.min_mapping_quality) + " -ERC GVCF -variant_index_type LINEAR -variant_index_parameter 128000", group="GATK options", type='str')
        
        # Freebayes caller options
        self.add_parameter("pvar", "Report sites if the probability that there is a polymorphism at the site is greater than N. (default=0.0001)", default=0.0001, type="float", group="Freebayes options")
        self.add_parameter("min_supporting_quality", "Consider any allele in which the sum of qualities of supporting observations is at least Q, (default=0)", default=0, type="int", group="Freebayes options")
        self.add_parameter("genotype_variant_threshold", "Limit posterior integration to samples where the second-best genotype likelihood is no more than log(N) from the highest genotype likelihood for the sample. (default=0)", default=0, type="int", group="Freebayes options")
    
    def update_sequenced_regions(self, bed_files):
        """
        Mainly, this function will regroup sequences which are below the threshold limit in bed files.
        output=list of bed files
        """
        reader= seqio.FastaReader(self.ref_genome)
        threshold= 40000000
        seq_length= 0
        ID= list()
        for id, desc, seq, qualities in reader:
            current_len = len(seq)
            seq_length += current_len
            ident= id+"\t"+"1"+"\t"+str(current_len)+"\n"
            ID.append(ident)
            if seq_length > threshold:
                bed = self.get_temporary_file("_"+ID[0].split("\t")[0]+".bed" if len(ID)==1 else "_"+ID[0].split("\t")[0]+"_"+ID[-1].split("\t")[0]+".bed")
                intervals = open(bed, "w")
                intervals.write("".join(ID))
                bed_files.append(bed)
                intervals.close()
                seq_length = 0
                ID = []
        
        if seq_length != 0:
            bed = self.get_temporary_file("_"+ID[0].split("\t")[0]+".bed" if len(ID)==1 else "_"+ID[0].split("\t")[0]+"_"+ID[-1].split("\t")[0]+".bed")
            intervals = open(bed, "w")
            intervals.write("".join(ID))
            bed_files.append(bed)
            intervals.close()

    def process(self):
        
        #Prepare fastq reads
        tmp_R1=[]
        tmp_R2=[]
        has_paired=False
        
        for smple in self.sample:
            tmp_R1.append(smple["reads_1"])
            if smple["reads_2"] != None:
                tmp_R2.append(smple["reads_2"])
                has_paired = True
        
        #Fastq Statistics: perform fastqc analysis and/or multiqc analysis (case of multiple samples) on reads 1 and/or reads 2.
        fastq_stats_R1=self.add_component("FastqStats", [tmp_R1], component_prefix="R1")
        fastq_R1_results=fastq_stats_R1.FastqStatsResults
        if has_paired:
            fastq_stats_R2=self.add_component("FastqStats", [tmp_R2], component_prefix="R2")
            fastq_R2_results=fastq_stats_R2.FastqStatsResults
        else:
            fastq_R2_results=None
        
        #Fastq Alignment:
        fastq_alignment=self.add_component("FastqAlignment", [self.ref_genome, self.sample["reads_1"], self.sample["reads_2"], self.sample["name"], self.sample["sequencer"], self.min_seed_length])
        
        
        #Merging bam reads with same sample
        bam_per_samples={}
        for idx, name in enumerate(self.sample["name"]):
            if not name in bam_per_samples.keys():
                bam_per_samples[name]=[fastq_alignment.bam_rg_sorted[idx]]
            else:
                bam_per_samples[name].append(fastq_alignment.bam_rg_sorted[idx])
        
        bams_aligned=[]
        for key in bam_per_samples.keys():
            if len(bam_per_samples[key])>1:
                merged_bam = self.add_component("SamtoolsMerge", [bam_per_samples[key], key], component_prefix=key)
                bams_aligned.append(merged_bam.merged_bam)
            else:
                bams_aligned.append(bam_per_samples[key][0])
        
        #Bams Preprocess:
        bams_preprocess=self.add_component("BamsPreprocess", [bams_aligned])
        final_flagstat=bams_preprocess.flagstat
        bams_cleaned=bams_preprocess.bam_rg_sorted_marked
        bams_cleaned_indexes=bams_preprocess.bam_rg_sorted_marked_indexes
        
        #Index ref genome: fai_dict
        databank_indexed = self.ref_genome
        if not os.path.exists(self.ref_genome + ".fai") or not os.path.exists(os.path.splitext(self.ref_genome)[0] + ".dict"):
            index_fai_dict = self.add_component( "IndexFaiDict", [self.ref_genome] )
            databank_indexed = index_fai_dict.databank
        
        #Bams realignment and recalibration:
        real_recal=self.add_component("RealRecal", [bams_cleaned, bams_cleaned_indexes, databank_indexed, self.sequenced_regions, self.known_variant])
        bams_real_recal=real_recal.output_files
        
        #Bams statistics:
        bams_stats=self.add_component("BamStats", [bams_real_recal])
        bams_report=bams_stats.Qualimap_Report

        #Html Report:
        report=self.add_component("Report", [fastq_R1_results, fastq_R2_results, final_flagstat, bams_report])

        #CALLING
        seq_regions=[self.sequenced_regions]
        if self.sequenced_regions == None:
            seq_regions=list()
            self.update_sequenced_regions(seq_regions)
        
        #Calling with gatk haplotypecaller:
        if "gatk" in self.tools:
            gatk_haplotype_caller= self.add_component("GatkHaplotypeCaller", [bams_real_recal, databank_indexed, self.stand_call_conf, self.min_mapping_quality, seq_regions, self.calling_options])
        
        #Calling with freebayes:
        if "freebayes" in self.tools:
            freebayes_caller=self.add_component("Freebayes", [databank_indexed, bams_real_recal, self.min_mapping_quality, self.pvar, self.min_supporting_quality, self.genotype_variant_threshold, seq_regions])
        
        #Calling with mpileup
        if "mpileup" in self.tools:
            mpileup_caller= self.add_component("Mpileup", [databank_indexed, bams_real_recal, self.min_mapping_quality, seq_regions])
    
    def post_process(self):
        #delete intermediate files:
        if self.remove_tmp_files:
            
                #FastqStats Component:
            os.system("rm -f "+os.path.join(self.get_component_output_directory("FastqStats", "R1"), "*_fastqc_zips.txt")+" ;rm -f "+os.path.join(self.get_component_output_directory("FastqStats", "R1"), "*_fastqc.zip")+" ; rm -f "+os.path.join(self.get_component_output_directory("FastqStats", "R2"), "*_fastqc.zip")+"; rm -f "+os.path.join(self.get_component_output_directory("FastqStats", "R2"), "*_fastqc_zips.txt"))

                #BamsPreprocess Component:
            os.system("rm -f "+os.path.join(self.get_component_output_directory("BamsPreprocess", "default"), "*.stderrs"))

                #RealRecal Component:
            os.system("rm -f "+os.path.join(self.get_component_output_directory("RealRecal", "default"), "*.intervals")+"; rm -f "+os.path.join(self.get_component_output_directory("RealRecal", "default"),"*_recalib.grp") + "; rm -f "+os.path.join(self.get_component_output_directory("RealRecal", "default"), "*_intervals.stderr")+"; rm -f "+os.path.join(self.get_component_output_directory("RealRecal", "default"), "*_realign.stderr")+ "; rm -f "+ os.path.join(self.get_component_output_directory("RealRecal", "default"),"*_print_reads.stderr")+ "; rm -f " +os.path.join(self.get_component_output_directory("RealRecal", "default"), "*_recalib.stderr"))

            if self.known_variant != None:
                os.system("rm -f "+os.path.join(self.get_component_output_directory("RealRecal", "default"), "*.real.bam*"))
            
                #BamStats Component:
            os.system("rm -f "+os.path.join(self.get_component_output_directory("BamStats", "default"), "*bam_list")+"; rm -f "+ os.path.join(self.get_component_output_directory("BamStats", "default"), "*.bam"))

                #GatkHaplotypeCaller Component:
            os.system("rm -f "+os.path.join(self.get_component_output_directory("GatkHaplotypeCaller", "default"), "*"+self.get_name()+"*.g.vcf*")+"; rm -f "+os.path.join(self.get_component_output_directory("GatkHaplotypeCaller", "default"), "*_chrcalling.stderr")+"; rm -f "+os.path.join(self.get_component_output_directory("GatkHaplotypeCaller", "default"), "*_concat.stderr"))

                #Freebayes Component:
            os.system("rm -f "+os.path.join(self.get_component_output_directory("Freebayes", "default"), "*_chrcalling.stderr")+" ; rm -f "+os.path.join(self.get_component_output_directory("Freebayes", "default"), "*"+self.get_name()+"*.vcf"))

                #Mpileup Component:
            os.system("rm -f "+os.path.join(self.get_component_output_directory("Mpileup", "default"), "*"+self.get_name()+"*.vcf")+" ; rm -f "+os.path.join(self.get_component_output_directory("Mpileup", "default"), "*_mpileup.stderr")+" ; rm -f "+os.path.join(self.get_component_output_directory("Mpileup", "default"), "*_concat*")+" ; rm -f "+os.path.join(self.get_component_output_directory("Mpileup", "default"), "*_bcftools.stderr")+" ; rm -f "+os.path.join(self.get_component_output_directory("Mpileup", "default"), "*.gz*")+" ; rm -f "+os.path.join(self.get_component_output_directory("Mpileup", "default"), "*.real.vcf"))
