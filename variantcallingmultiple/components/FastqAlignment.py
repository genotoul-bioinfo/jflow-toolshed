#
# Copyright (C) 2015 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Kenza BAZI KABBAJ INRA - SIGENAE'
__version__ = '2.3.2'

from jflow.component import Component
import os

class FastqAlignment (Component):
    
    """
    @summary: FastqAlignment is used to align fastq reads against reference genome. Firstly, Reference genome is indexed. After that, reads (PE or SE or both) are aligned and read groups are added too. Finally, SAM outputs are converted to bam files and sorted by coordinates with samtools.
    @return: bam file with added read group and sorted by coordinates
    """

    def define_parameters(self, ref_genome, fastq_R1, fastq_R2, sample_name, sequencer, min_seed_length=19):
        
        """
        @param ref_genome: [str] path to the reference genome
        @param fastq_R1: [list] input fastq files read1
        @param fastq_R2: [list] input fastq files read2
        @param sample_name: [list] sample names for each library
        @param sequencer: [list] sequencer names for each library
        @param min_seed_length: [int] min seed length of bwa alignment, option=k in bwa mem (default=19)
        """
        
        #Inputs
        self.add_input_file("ref_genome", "reference file index with bwa index", default=ref_genome, file_format="fasta", required=True)
        self.add_input_file_list( "fastq_R1", "input fastq files read1", default=fastq_R1, file_format="fastq", required=True)
        self.add_input_file_list( "fastq_R2", "input fastq files read2", default=fastq_R2, file_format="fastq")
        self.add_parameter_list("sample_name", "sample name of the library", default=sample_name, required=True)
        self.add_parameter_list("sequencer", "Sequencer names for each library.", default=sequencer, required=True)
        self.add_parameter("min_seed_length", "min seed length of bwa alignment, option=k in bwa mem", default=min_seed_length)

        #Outputs
        #Building bam prefixes: if multiple runs, we take the prefix from fastq reads name (R1). Otherwise, the prefix is extracted from the sample name.
        bams_prefix=[]
        for idx, name in enumerate(sample_name):
            if sample_name.count(name) > 1:
                bams_prefix.append(fastq_R1[idx])
            else:
                bams_prefix.append(name)
        self.add_output_file("databank_stderr", "The databank stderr", filename=os.path.basename(ref_genome)+"_databank.stderr")
        self.add_output_file_list("bam_rg_sorted", "sorted bam files with added read group", pattern='{basename_woext}.rg.sort.bam', items=bams_prefix)
        self.add_output_file_list("reads_align_stderr", "the stderr files alignment per sample", pattern='{basename_woext}.stderr', items=bams_prefix)
    
    def process(self):
        
        #get cpu number
        cpu=str(4)
        if self.get_cpu() != None :
            cpu=str(self.get_cpu())

        #Indexing reference genome
        if not os.path.exists(self.ref_genome + ".bwt"):
            databank=self.get_outputs('{basename_woext}.databank', self.ref_genome)[0]
            self.add_shell_execution("ln -s $1 $2; " + self.get_exec_path("bwa") + " index -p $2 $1 2>> $3", cmd_format="{EXE} {IN} {OUT}", inputs=self.ref_genome, outputs=[databank, self.databank_stderr])
        else:
            databank=self.ref_genome
        
        #fastq alignment with adding read groups and converting sam to bam outputs:
        for idx, path in enumerate(self.sample_name):
            #paired end data
            if "None" not in self.fastq_R2[idx]:
                self.add_shell_execution(self.get_exec_path("bwa") + ' mem -t '+str((int(cpu)-2))+' -M -R \'@RG\\tID:'+ str(idx) + '\\tSM:' + self.sample_name[idx] + '\\tPL:'+ self.sequencer[idx] +'\\tLB:' + self.sample_name[idx] + '\\tPU:-\' ' +databank+" -k "+self.min_seed_length+" $1 $2 2>> "+self.reads_align_stderr[idx]+" | "+self.get_exec_path("samtools")+" view -bS - | "+self.get_exec_path("samtools")+" sort -T $3 -@ 2 -o $3 - 2>> "+self.reads_align_stderr[idx], cmd_format="{EXE} {IN} {OUT}", inputs=[self.fastq_R1[idx], self.fastq_R2[idx]], outputs=[self.bam_rg_sorted[idx]], includes=databank)
            
            #single end data
            else:
                self.add_shell_execution(self.get_exec_path("bwa") + ' mem -t '+str((int(cpu)-2))+' -M -R \'@RG\\tID:'+ str(idx) + '\\tSM:' + self.sample_name[idx] + '\\tPL:'+ self.sequencer[idx] +'\\tLB:' + self.sample_name[idx] + '\\tPU:-\' ' +databank+" -k "+self.min_seed_length+" $1 2>> "+self.reads_align_stderr[idx]+" | "+self.get_exec_path("samtools")+" view -bS - | "+self.get_exec_path("samtools")+" sort -T $2 -@ 2 -o $2 - 2>> "+self.reads_align_stderr[idx], cmd_format="{EXE} {IN} {OUT}", inputs=[self.fastq_R1[idx]], outputs=[self.bam_rg_sorted[idx]], includes=databank)
