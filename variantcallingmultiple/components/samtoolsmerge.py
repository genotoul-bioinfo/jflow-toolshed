#
# Copyright (C) 2015 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


__author__ = 'Kenza BAZI KABBAJ INRA - SIGENAE'
__version__ = '1.0.0'

from jflow.component import Component
import os

class SamtoolsMerge (Component):
    """
    @summary: Merging bam files from different lanes
    @return: merged bam
    """

    def define_parameters(self, bams, sample_name):
        """
        @bams: [list] path to bam files
        @sample_name: [str] sample name
        """
        
        # Inputs
        self.add_input_file_list("bams", "Which bam files should be merged.", file_format="bam", default=bams, required=True)
        self.add_parameter("sample_name", "Sample name.", default=sample_name)
        
        # Outputs
        self.add_output_file("merged_bam", "The merged bam file.", filename=sample_name + ".rg.sort.bam", file_format="bam")
        self.add_output_file("stderr", "The stderr file.", filename=sample_name + '.stderr')

    def process(self):
        
        #get cpu number
        cpu=str(1)
        if self.get_cpu() != None :
            cpu=str(self.get_cpu())
        
        self.add_shell_execution( self.get_exec_path("samtools") + " merge $1 -@ "+cpu+ " "+" ".join( self.bams ) + " 2>> $2", cmd_format='{EXE} {OUT}' , includes=self.bams, outputs=[self.merged_bam, self.stderr], map=False)  
