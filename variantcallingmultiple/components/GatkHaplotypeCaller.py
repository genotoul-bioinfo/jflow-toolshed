#
# Copyright (C) 2015 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Kenza BAZI KABBAJ INRA - SIGENAE'
__version__ = '4.1.3'

from jflow.component import Component
import os

class GatkHaplotypeCaller (Component):

    """
    @summary : Call SNPs and indels simultaneously via local de-novo assembly of haplotypes in an active region.
    @return: The variants file in gvcf format for each individual.
    """

    def define_parameters(self, input_files, reference, stand_call_conf=30.0, min_mapping_quality=30, sequenced_regions_file=None, calling_option=None):
    
        """
        @param input_files : [list] Path to the bam files to process.
        @param reference : [str] Path to the indexed reference.
        @param stand_call_conf : [int] This is the minimum confidence threshold (phred-scaled) at which the program should emit variant sites as called (and filtered with LowQual if less than the calling threshold).
        @param min_mapping_quality : [int] Minimum read mapping quality required to consider a read for calling
        @param sequenced_regions : [list] bed file with intervals that will be processed and used to parallelize jobs (bed format).
        @param calling_option: [str] Add calling option to Haplotype caller
        """
        
        # parameters
        self.add_parameter( "stand_call_conf", "This is the minimum confidence threshold (phred-scaled) at which the program should emit variant sites as called (and filtered with LowQual if less than the calling threshold)", default=stand_call_conf, type="float" )
        self.add_parameter( "min_mapping_quality", "Minimum read mapping quality required to consider a read for calling", default=min_mapping_quality, type="int" )
        self.add_parameter( "add_calling_options", "Add calling option to Haplotype caller (Default=-stand_call_conf " + str(self.stand_call_conf) + " -mmq " + str(self.min_mapping_quality) + " -ERC GVCF -variant_index_type LINEAR -variant_index_parameter 128000)", default=calling_option)
        
        # input Files
        self.add_input_file_list( "input_files", "The BAM files to process.", default=input_files, file_format="bam", required=True )
        self.add_input_file( "reference", "The FASTA indexed reference file.", default=reference, file_format="fasta", required=True )
        self.add_input_file_list("sequenced_regions_file", "The regions to process (bed format).", default=sequenced_regions_file, file_format="bed")
    
        # output Files
        self.add_output_file_list("calling_stderr", "The stderr calling per sample", pattern="{basename_woext}.stderr", items=input_files)
        self.add_output_file_list("gvcfs", "gvcf results per individu", pattern="{basename_woext}.g.vcf.gz", items=input_files)

    def process(self):
        # Get memory number
        mem="1G"
        if self.get_memory() != None :
            mem=self.get_memory()
        xmx_option  = "-Xmx" + str(mem).lower()
        
        # Setting parameters
        calling_options = self.add_calling_options if self.add_calling_options != "" else ""
        
        if "-stand_call_conf" not in calling_options:
            calling_options += " -stand_call_conf " + str(self.stand_call_conf)
        if "-mmq" not in self.add_calling_options:
            calling_options += " -mmq " + str(self.min_mapping_quality)
        if "-ERC" not in self.add_calling_options:
            calling_options += " -ERC GVCF"
        if "-variant_index_type" not in self.add_calling_options:
            calling_options += " -variant_index_type LINEAR"
        if "-variant_index_parameter" not in self.add_calling_options:
            calling_options += " -variant_index_parameter 128000"
        
        calling_options = calling_options.strip()

        # Gatk HC Calling by sample and by chromosom
        tmp_concat_stderr=list()
        for idx1, current_bam in enumerate(self.input_files):
            sample_name=current_bam.split("/")[-1].split(".")[0]
            tmp_outputs = list()
            tmp_stderr = list()
            for idx2, current_bed in enumerate(self.sequenced_regions_file):
                tmp_outputs.append(os.path.join(self.output_directory, sample_name+"_"+os.path.basename(current_bed).replace(".bed", ".g.vcf")))
                tmp_stderr.append(os.path.join(self.output_directory, sample_name+"_"+os.path.basename(current_bed).replace(".bed", "_chrcalling.stderr")))
                restriction_option = " -L '" + current_bed + "'"
                self.add_shell_execution(self.get_exec_path("java") + " "+ xmx_option+" -XX:-DoEscapeAnalysis -jar "  + self.get_exec_path("gatk") + " -T HaplotypeCaller"+ " -R " + self.reference + " -I " + current_bam + " " + calling_options + restriction_option+ " -o $1 2>> $2", cmd_format='{EXE} {OUT}', outputs=[tmp_outputs[idx2], tmp_stderr[idx2]], includes=[self.reference] + [self.sequenced_regions_file] + self.input_files , map=False)
            # Concat chromosoms + zip and index
            tmp_concat_stderr.append(os.path.join(self.output_directory, sample_name+"_concat.stderr"))
            self.add_shell_execution(self.get_exec_path("vcf-concat") + " " + " ".join(tmp_outputs)  + " | bgzip -c > $1 ; tabix -p vcf $1 2>> $2", cmd_format='{EXE} {OUT}', outputs=[self.gvcfs[idx1], tmp_concat_stderr[idx1]], includes=tmp_outputs, map=False)
            
            # Concat stderr files
            self.add_shell_execution("cat "+" ".join(tmp_stderr)+" "+tmp_concat_stderr[idx1]+ " >$1", cmd_format='{EXE} {OUT}', outputs=self.calling_stderr[idx1], includes=tmp_stderr+[tmp_concat_stderr], local=True)
