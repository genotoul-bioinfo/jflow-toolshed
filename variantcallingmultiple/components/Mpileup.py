#
# Copyright (C) 2015 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Kenza BAZI KABBAJ INRA - SIGENAE'
__version__ = '2.1.4'


from jflow.component import Component
import os

class Mpileup (Component):
    """
    @summary: This component will launch Samtools mpileup and BCFtools for SNP/INDELS calling. Note that the calling is parallelized by sample and by chromosom.
    @return: variants vcf file (multisample).
    
    """
    def define_parameters(self, ref_genome, bam_files, min_mapping_quality=30, sequenced_regions=None):
        """
        @param ref_genome: [str] path to the reference genome
        @param bam_files: [list] path to the bam(s) file(s)
        @param min_mapping_quality: [int] Exclude alignments from analysis if they have a mapping quality less than Q. default: Q=30
        @param sequenced_regions: [list] bed file with intervals that will be processed and used to parallelize jobs (bed format).
        """
        # Input Files
        self.add_input_file("ref_genome", "Which reference file should be used", default=ref_genome, file_format="fasta", required=True)
        self.add_input_file_list("bam_files", "Which bam file should be used", default=bam_files, file_format="bam")
        self.add_input_file_list("sequenced_regions", "The regions to process (bed format).", default=sequenced_regions, file_format="bed")
        
        # Parameters
        self.add_parameter("min_mapping_quality", "Exclude alignments from analysis if they have a mapping quality less than Q. default: Q=30", default=min_mapping_quality, type="int")
        
        # Output Files
        self.add_output_file_list("calling_stderr", "The stderr calling per sample", pattern="{basename_woext}.stderr", items=bam_files)
        self.add_output_file( "output_file", "The variants mpileup file.", filename="variants_mpileup.vcf.gz")
        self.add_output_file("Merge_stderr", "stderr file for vcf merge", filename='vcf_merge.stderr')
        
        self.args = ""
        if self.min_mapping_quality:
            self.args += " -q " +str(self.min_mapping_quality)

    def process(self):
        
        tmp_concat_stderr=list()
        tmp_outputs=list()
        for idx1, current_bam in enumerate(self.bam_files):
            sample_name=current_bam.split("/")[-1].split(".")[0]
            tmp_stderr_mpileup =list()
            tmp_stderr_bcftools =list()
            tmp_vcfs=list()

            for idx2, current_bed in enumerate(self.sequenced_regions):
                tmp_stderr_mpileup.append(os.path.join(self.output_directory, sample_name+"_"+os.path.basename(current_bed).replace(".bed", "_mpileup.stderr")))
                tmp_stderr_bcftools.append(os.path.join(self.output_directory, sample_name+"_"+os.path.basename(current_bed).replace(".bed", "_bcftools.stderr")))
                tmp_vcfs.append(os.path.join(self.output_directory, sample_name+"_"+os.path.basename(current_bed).replace(".bed", ".vcf")))
                restriction_option = " -l "+ str(current_bed)
                #variant calling
                self.add_shell_execution(self.get_exec_path("samtools")+" mpileup -C 50 -ugf "+self.ref_genome+self.args+restriction_option+" "+current_bam+" 2>> $1 | "+ self.get_exec_path("bcftools")+" call -m -v --output-type v -o $2 - 2>> $3", cmd_format="{EXE} {OUT}", outputs=[tmp_stderr_mpileup[idx2], tmp_vcfs[idx2], tmp_stderr_bcftools[idx2]], includes=self.bam_files, map=False)
            
            # Concat chromosoms
            tmp_concat_stderr.append(os.path.join(self.output_directory, sample_name+"_concat.stderr"))
            tmp_outputs.append(os.path.join(self.output_directory, sample_name+"_concat.vcf"))
            self.add_shell_execution(self.get_exec_path("vcf-concat")+ " "+" ".join(tmp_vcfs)+" > $1 2>> $2", cmd_format="{EXE} {OUT}", outputs=[tmp_outputs[idx1], tmp_concat_stderr[idx1]], includes=tmp_vcfs, map=False)

            # Concat stderr files
            self.add_shell_execution("cat "+" ".join(tmp_stderr_mpileup)+" "+" ".join(tmp_stderr_bcftools)+" "+tmp_concat_stderr[idx1]+ " >$1", cmd_format='{EXE} {OUT}', outputs=self.calling_stderr[idx1], includes=tmp_stderr_mpileup+tmp_stderr_bcftools+[tmp_concat_stderr], local=True)
        
        if len(tmp_outputs) > 1:
            # Zip and index vcf files for vcf-merge
            zipped_vcfs=self.get_outputs('{basename}.gz', tmp_outputs)
            self.add_shell_execution("bgzip $1; tabix -p vcf $2", cmd_format='{EXE} {IN} {OUT}', inputs=tmp_outputs, outputs=zipped_vcfs , local=True, map=True)

            # Merge vcf individuals files to a multiple sample file + zip and index vcf output
            self.add_shell_execution(self.get_exec_path("vcf-merge")+" "+" ".join(zipped_vcfs)+" | bgzip -c > $1; tabix -p vcf $1 2>> $2", cmd_format="{EXE} {OUT}", outputs=[self.output_file, self.Merge_stderr], includes=zipped_vcfs, map=False)
        else:
            # zip and index vcf file
            variants=os.path.join(self.output_directory, "variants_mpileup.vcf")
            self.add_shell_execution("mv $1 "+variants+" ; bgzip "+variants+" ; tabix -p vcf $2", cmd_format='{EXE} {IN} {OUT}', inputs=tmp_outputs[0], outputs=self.output_file, map=False)
