#
# Copyright (C) 2015 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Kenza BAZI KABBAJ INRA - SIGENAE'
__version__ = '2.1.1'

from jflow.component import Component

class FastqStats (Component):
    """
    @summary: FastqStats allows to make fastqc and multiqc analysis on fastq reads. multiqc is used only for multiple reads to summarize the outputs from multiple fastqc results into a single report.
    @return: fastqc html report(s) and/or multiqc html report
    """

    def define_parameters(self, fastq_inputs):
        
        """
        @param fastq_inputs: [list] input fastq files
        """
        #Input Files:
        self.add_input_file_list( "fastq_inputs", "input fastq files", default=fastq_inputs, file_format="fastq")
        
        #Output Files:
        self.add_output_file("stderr", "stderrs file for FastqStats component", filename='FastqStats.stderr')
        self.add_output_file("FastqStatsResults", "report file of fastqc analysis", filename='report.html')

    def process(self):
        
        #get cpu number
        cpu=str(1)
        if self.get_cpu() != None :
            cpu=str(self.get_cpu())

        #fastqc analysis
        fastqc_zips=self.get_outputs('{basename_woext}_fastqc.zip', self.fastq_inputs)

        self.add_shell_execution(self.get_exec_path("fastqc")+" $1 -o "+self.output_directory+" -t "+cpu+" > $2 2>> "+self.stderr, cmd_format="{EXE} {IN} {OUT}", inputs=self.fastq_inputs, outputs=fastqc_zips, map=True)
        
        #multiqc analysis: case of multiple reads fastqc reports
        if len(self.fastq_inputs) > 1:
            self.add_shell_execution(self.get_exec_path("multiqc")+" "+" ".join(fastqc_zips)+" -o "+self.output_directory+" -n report -m fastqc 2>> "+self.stderr, cmd_format="{EXE}", includes=fastqc_zips, map=False)
        else:
            self.add_shell_execution("mv "+self.output_directory+"/*_fastqc.html $1", cmd_format="{EXE} {OUT}", outputs=self.FastqStatsResults, includes=fastqc_zips, local=True)
