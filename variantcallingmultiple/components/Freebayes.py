#
# Copyright (C) 2015 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Kenza BAZI KABBAJ INRA - SIGENAE'
__version__ = '2.0.2'

from jflow.component import Component
import os

class Freebayes (Component):
    
    """
    @summary: This component will launch Freebayes on bam reads. Freebayes is an haplotype-based variant detector designed to find small polymorphisms (SNPs, Indels, MNPs, complex events). Note that all samples are launched together, so it will suffer from N+1 problem (if you want to add a new sample, you have to reprocess all the steps).
    @return: variants vcf multisample file.
    """
    def define_parameters(self, ref_genome, bam_files, min_mapping_quality=30, pvar=0.0001, min_supporting_quality=0, genotype_variant_threshold=0, sequenced_regions=None):
        
        """
        @param ref_genome: [str] path to the reference genome
        @param bam_files: [list] path to the bam(s) file(s)
        @param min_mapping_quality: [int] Exclude alignments from analysis if they have a mapping quality less than Q. default: Q=30
        @param pvar: [float] Report sites if the probability that there is a polymorphism at the site is greater than N. (default=0.0001)
        @param min_supporting_quality: [int] Consider any allele in which the sum of qualities of supporting observations is at least Q. (default=0)
        @param genotype_variant_threshold: [int] Limit posterior integration to samples where the second-best genotype likelihood is no more than log(N) from the highest genotype likelihood for the sample. (default=0)
        @param sequenced_regions: [list] bed file with intervals that will be processed and used to parallelize jobs (bed format).
        """
        #~ Input Files
        self.add_input_file("ref_genome", "Which reference file should be used", default=ref_genome, file_format="fasta", required=True)
        self.add_input_file_list("bam_files", "Which bam file should be used", default=bam_files, file_format="bam")
        self.add_input_file_list("sequenced_regions", "The regions to process (bed format).", default=sequenced_regions, file_format="bed")
        
        #~ Parameters
        self.add_parameter("min_mapping_quality", "Exclude alignments from analysis if they have a mapping quality less than Q. default: Q=30", default=min_mapping_quality, type="int")
        self.add_parameter("pvar", "Report sites if the probability that there is a polymorphism at the site is greater than N. (default=0.0001)", default=pvar, type="float")
        self.add_parameter("min_supporting_quality", "Consider any allele in which the sum of qualities of supporting observations is at least Q.  default: 0", default=min_supporting_quality, type="int")
        self.add_parameter("genotype_variant_threshold", "Limit posterior integration to samples where the second-best genotype likelihood is no more than log(N) from the highest genotype likelihood for the sample. (default=0)", default=genotype_variant_threshold, type="int")
        
        #~ Output Files
        self.add_output_file("calling_stderr", "stderr file for freebayes calling", filename='calling.stderr')
        self.add_output_file( "output_file", "The variants freebayes file.", filename="variants_freebayes.vcf.gz")
        self.add_output_file("concat_stderr", "stderr file for vcf concatenation", filename='vcf_concat.stderr')
    
    def process(self):
        
        #Setting parameters
        args = ""
        if self.min_mapping_quality:
            args += " -m "+str(self.min_mapping_quality)
        if self.pvar:
            args += " -P "+str(self.pvar)
        if self.min_supporting_quality:
            args += " -R "+str(self.min_supporting_quality)
        if self.genotype_variant_threshold:
            args += " -S "+str(self.genotype_variant_threshold)

        # variant calling per chr
        tmp_outputs=list()
        tmp_stderr=list()
        for idx, current_bed in enumerate(self.sequenced_regions):
            tmp_outputs.append(os.path.join(self.output_directory, os.path.basename(current_bed).replace(".bed", ".vcf")))
            tmp_stderr.append(os.path.join(self.output_directory, os.path.basename(current_bed).replace(".bed", "_chrcalling.stderr")))
            restriction_option = " -t "+ str(current_bed)
            self.add_shell_execution(self.get_exec_path("freebayes") + " -f "+self.ref_genome+args+restriction_option+" "+" ".join(self.bam_files)+" > $1 2>> $2", cmd_format="{EXE} {OUT}", outputs=[tmp_outputs[idx], tmp_stderr[idx]], includes=[self.bam_files, self.ref_genome], map=False)
        # concat stderr files
        self.add_shell_execution("cat "+" ".join(tmp_stderr)+" > $1", cmd_format='{EXE} {OUT}', outputs=self.calling_stderr, includes=tmp_stderr, map=False, local=True)
        
        # concat vcf results + zip and index
        self.add_shell_execution(self.get_exec_path("vcf-concat")+ " "+" ".join(tmp_outputs)+" | bgzip -c > $1 ; tabix -p vcf $1 2>> $2", cmd_format="{EXE} {OUT}", outputs=[self.output_file, self.concat_stderr], includes=tmp_outputs, map=False)
