#
# Copyright (C) 2015 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Kenza BAZI KABBAJ INRA - SIGENAE'
__version__ = '2.0.2'

from jflow.component import Component

def report(fastqc_R1_results, fastqc_R2_results, bams_report, out_report, *flagstats):
    
    import os
    from dominate import document, tags as tags
    dico={}
    for file in flagstats:
        fichier = open(file, "r")
        values=[]
        sample_name=(os.path.basename(os.path.splitext(file)[0])).split(".rg")[0]
        for line in fichier:
            val="".join(line.strip().split(" ", 3)[0:3])
            values.append(val)
        dico[sample_name]=values

    with document(title='Report') as doc:
        with doc.head:
            tags.meta(charset="utf-8")
            tags.script(src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js")
            tags.style("""body {margin: 10px auto ;padding: 0 ;text-align: center ;font: 0.8em 'Trebuchet MS', helvetica, sans-serif ;background: #F0F8FF ;}
            div#conteneur{width: 1170px ;height: 1300px ;margin: 0 auto ;text-align: left ;border: 2px solid #048b9a ;background: #fff ;}
            h1#header{height: 300px ;margin: 0 ;background: url(http://nsm08.casimages.com/img/2017/01/13//17011312425322540214769773.jpg) no-repeat left top ;}
            div#contenu{padding: 0 25px 0 20px ; margin: 0 auto}
            div#contenu h2{padding-left: 25px ;line-height: 25px ;font-size: 1.4em ;color: #048b9a ;border-bottom: 1px solid #048b9a ;}
            div#contenu p{text-align: justify ;text-indent: 2em ;line-height: 1.7em ;}
            ul#menu{height: 25px ;margin: 0 ;padding: 0 ;background: url(http://nsm08.casimages.com/img/2017/01/13//17011312425322540214769772.jpg) repeat-x 0 -25px ;list-style-type: none ;}
            ul#menu li{float: left ;text-align: center ;}
            ul#menu li a{width: 130px ;line-height: 25px ;font-size: 1.2em ;font-weight: bold ;letter-spacing: 2px ;color: #fff ;display: block ;text-decoration: none ;border-right: 2px solid #048b9a ;}
            ul#menu li a:hover{background: url(http://nsm08.casimages.com/img/2017/01/13//17011312425322540214769772.jpg) repeat-x 0 0 ;}
            table{border-collapse: collapse;}
            td{border: 1px solid black;}
            th {background-color: #048b9a; color: white; border: 1px solid black}
            tr:nth-child(even){background-color: #f2f2f2}""")

        titles=["total_reads (QC-passed reads + QC-failed reads)", "secondary", "supplementary", "duplicates", "mapped", "paired in sequencing", "read1", "read2", "properly paired", "with itself and mate mapped", "singletons", "with mate mapped to a different chr", "with mate mapped to a different chr (mapQ)"]
        with doc.body:
            with tags.div(id='conteneur'):
                tags.h1(id="header")
                with tags.nav():
                    with tags.ul(id="menu"):
                        tags.li(tags.a("Fastqc_R1", href=fastqc_R1_results))
                        if fastqc_R2_results != "None":
                            tags.li(tags.a("Fastqc_R2", href=fastqc_R2_results))
                        tags.li(tags.a("Flagstats", href=out_report))
                        tags.li(tags.a("Qualimap", href=bams_report))
                with tags.div(id='contenu'):
                    tags.h2("Flagstat Analysis")
                    with tags.table():
                        with tags.thead():
                            tags.th("")
                            for title in titles:
                                tags.th(title, style = "font-size:90%;text-align:center;padding:0.1px")
                        with tags.tbody():
                            for i,j in dico.items():
                                with tags.tr():
                                    tags.td(i, style = "font-size:100%;text-align:center;padding:0.1px")
                                    for k in j:
                                        tags.td(k, style="font-size:100%;text-align:center;padding:0.1px")

    with open(out_report, "w") as f:
        f.write(str(doc))



class Report (Component):

    """
    @summary: Html report is created to summarize all fastq statistics (fastqc results), flagstat analysis (after marking duplicates) and bam statistics (qualimap results).
    @return: Html report
    """

    def define_parameters(self, fastqc_R1_results, fastqc_R2_results, flagstat, bams_report):
        
        """
        @param fastqc_R1_results: [str] fastqc or multiqc report of reads 1
        @param fastqc_R2_results: [str] fastqc or multiqc report of reads 2
        @param flagstat: [list] flagstat analysis results after marking duplicates
        @param bams_report: [str] bams quality report by qualimap tool
        """
        
        #Inputs
        self.add_input_file("fastqc_R1_results", "fastqc or multiqc report of reads 1", default=fastqc_R1_results, file_format="html", required=True)
        self.add_input_file("fastqc_R2_results", "fastqc or multiqc report of reads 2", default=fastqc_R2_results, file_format="html", required=True)
        self.add_input_file_list( "flagstat", "flagstat analysis results after marking duplicates", default=flagstat, required=True)
        self.add_input_file("bams_report", "bams quality report by qualimap tool", default=bams_report, file_format="html", required=True)

        #Outputs
        self.add_output_file( "report", "html report for fastq and bams statistics", filename="Report.html", file_format="html")
        
    def process(self):
        
        self.add_python_execution(report, cmd_format="{EXE} {ARG} {OUT} {IN}", arguments=[self.fastqc_R1_results, self.fastqc_R2_results, self.bams_report], outputs=self.report, inputs=self.flagstat)
        
