#
# Copyright (C) 2015 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Kenza BAZI KABBAJ INRA - SIGENAE'
__version__ = '2.0.0'

from jflow.component import Component
import os

class RealRecal (Component):
    """
    @summary: Firstly, RealRecal component will realign bam reads with RealignerTargetCreator Gatk tool. Secondly, it will reprocess quality score for each nucleotids in reads (Recalibration step).
    @return: bams realigned recalibrated
    """
    def define_parameters(self, input_files, bams_indexes, fasta, sequenced_regions_file=None, known_sites=None):
        
        """
        @param input_files : [list] bam files.
        @param bams_indexes : [list] indexed bam files (bai).
        @param fasta : [str] indexed fasta reference file (fai, dict).
        @param sequenced_regions_file : [str] file with intervals that will be processed, in bed format.
        @param known_sites : [str] vcf of known sites to recalibrate base qualities.
        """
    
        # input Files
        self.add_input_file_list( "input_files", "The BAM files", default=input_files, file_format="bam", required=True )
        self.add_input_file_list( "bams_indexes", "The indexes BAM files (bai)", default=bams_indexes, file_format="bai", required=True )
        self.add_input_file( "fasta", "indexed reference sequences (fai, dict).", default=fasta, file_format="fasta", required=True )
        self.add_input_file( "sequenced_regions_file", "file with intervals that will be processed, in bed format.", default=sequenced_regions_file )
        self.add_input_file( "known_sites", "The VCF of known sites to recalibrate base qualities.", default=known_sites, file_format="vcf" )
        
        # output Files
        if self.known_sites != None:
            self.add_output_file_list( "output_files", "Realigned and Recalibrated bams.", pattern="{basename_woext}.real.recal.bam", items=self.input_files, file_format="bam" )
        else:
            self.add_output_file_list( "output_files", "Realigned bams.", pattern="{basename_woext}.real.bam", items=self.input_files, file_format="bam" )
        self.add_output_file_list("Real_Recal_stderr", "stderr files resume per sample", pattern='{basename_woext}.stderr', items=self.input_files)
        
    def process(self):
        
        #get memory number
        mem="1G"
        if self.get_memory() != None :
            mem=self.get_memory()

        restriction_option = " -L " + self.sequenced_regions_file if self.sequenced_regions_file != None else ""
        
        # Realignemnt : step 1 : interval calculation
        interval_files=self.get_outputs('{basename_woext}.intervals', self.input_files)
        intervals_stderr=self.get_outputs('{basename_woext}_intervals.stderr', self.input_files)
        
        self.add_shell_execution( self.get_exec_path("java") + " -Xmx" + mem + " -jar " + self.get_exec_path("gatk") + " -T RealignerTargetCreator --allow_potentially_misencoded_quality_scores " + restriction_option + " -I $1 -R " + self.fasta + " -o $2 2>> $3", cmd_format='{EXE} {IN} {OUT}', inputs=self.input_files, outputs=[interval_files, intervals_stderr], includes=[self.fasta, self.fasta+".fai", os.path.splitext(self.fasta)[0]+".dict", self.bams_indexes], map=True)

        # Realignemnt : step 2 : indel realigner
        bams_realigned=self.get_outputs('{basename_woext}.real.bam', self.input_files)
        real_stderr=self.get_outputs('{basename_woext}_realign.stderr', self.input_files)
        
        self.add_shell_execution( self.get_exec_path("java") + " -Xmx" + mem + " -jar " + self.get_exec_path("gatk") +" -T IndelRealigner --allow_potentially_misencoded_quality_scores -I $1 -R " + self.fasta + " -targetIntervals $2 -o $3 2>> $4", cmd_format='{EXE} {IN} {OUT}' , inputs=[self.input_files, interval_files], outputs=[bams_realigned, real_stderr], includes=[self.fasta, self.fasta+".fai", os.path.splitext(self.fasta)[0]+".dict", self.bams_indexes], map=True)
        
        if self.known_sites != None:
            # Recalibration : step 1 : base recalibration
                
            recal_stderr=self.get_outputs("{basename_woext}_recalib.stderr", self.input_files)
            recal_files=self.get_outputs("{basename_woext}_recalib.grp", self.input_files)
            
            self.add_shell_execution(self.get_exec_path("java") + " -Xmx" + mem + " -jar " + self.get_exec_path("gatk") +" -T BaseRecalibrator -S SILENT --allow_potentially_misencoded_quality_scores -I $1 -R " + self.fasta + " -knownSites " + self.known_sites + restriction_option + " -o $2 2>> $3", cmd_format='{EXE} {IN} {OUT}' , inputs=bams_realigned, outputs=[recal_files, recal_stderr], includes=[self.fasta, self.fasta+".fai", os.path.splitext(self.fasta)[0]+".dict"], map=True)

            # Recalibration : step 2 : print reads
            print_reads_stderr=self.get_outputs("{basename_woext}_print_reads.stderr", self.input_files)

            self.add_shell_execution( self.get_exec_path("java") + " -Xmx" + mem + " -jar " + self.get_exec_path("gatk") + " -T PrintReads --allow_potentially_misencoded_quality_scores -I $1 -R "+ self.fasta + " -BQSR $2 -o $3 2>> $4", cmd_format='{EXE} {IN} {OUT}', inputs=[bams_realigned, recal_files], outputs=[self.output_files, print_reads_stderr], includes=[self.fasta, self.fasta+".fai", os.path.splitext(self.fasta)[0]+".dict"], map=True)
            
            #stderr file resume for each sample
            self.add_shell_execution("cat $1 $2 $3 $4> $5", cmd_format="{EXE} {IN} {OUT}", inputs=[intervals_stderr, real_stderr, recal_stderr, print_reads_stderr], outputs=self.Real_Recal_stderr, map=True, local=True)
        
        else:
            bams_realigned=self.output_files
            
            #stderr file resume for each sample (realigned bams)
            self.add_shell_execution("cat $1 $2 > $3", cmd_format="{EXE} {IN} {OUT}", inputs=[intervals_stderr, real_stderr], outputs=self.Real_Recal_stderr, map=True, local=True)
