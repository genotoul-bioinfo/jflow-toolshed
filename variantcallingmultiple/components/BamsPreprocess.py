#
# Copyright (C) 2015 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Kenza BAZI KABBAJ INRA - SIGENAE'
__version__ = '2.1.0'

from jflow.component import Component
import os

class BamsPreprocess (Component):
    """
    @summary: BamsPreprocess will mark duplicates, index bams and finally perform flagstat analysis.
    @return: bams marked ; bam indexes files;  flagstat results
    """

    def define_parameters(self, bams):
        
        """
        @param bams: [list] input bam files
        """
        
        #Inputs
        self.add_input_file_list( "bams", "input bam files with added read group and sorted by coordinates", default=bams, file_format="bam", required=True)
        
        #Outputs
        self.add_output_file_list("bam_rg_sorted_marked", "marked bams", pattern='{basename_woext}.md.bam', items=self.bams)
        
        self.add_output_file_list("bam_rg_sorted_marked_indexes", "bam indexes files", pattern='{basename}.bai', items=self.bam_rg_sorted_marked)
        
        self.add_output_file_list("flagstat", "flagstat analysis after marking duplicates", pattern='{basename_woext}.final_flagstat', items=self.bam_rg_sorted_marked)
    
        self.add_output_file_list("Bams_Preprocess_stderr", "stderr files resume of bams preprocess per sample", pattern='{basename_woext}.stderr', items=self.bams)
        
    def process(self):
        
        #get memory number
        mem="1G"
        if self.get_memory() != None :
            mem=self.get_memory()
        
        #marking duplicates
        metrics_md=self.get_outputs('{basename_woext}.metrics', self.bam_rg_sorted_marked)
        md_stderr=self.get_outputs('{basename_woext}.stderr', self.bam_rg_sorted_marked)
        
        self.add_shell_execution("java -jar -Xmx"+mem+" "+self.get_exec_path("picard")+" MarkDuplicates MAX_FILE_HANDLES_FOR_READ_ENDS_MAP=1000 INPUT=$1 OUTPUT=$2 METRICS_FILE=$3 2>> $4", cmd_format="{EXE} {IN} {OUT}", inputs=self.bams, outputs=[self.bam_rg_sorted_marked, metrics_md, md_stderr], includes=self.bams, map=True)
        
        #indexing bam files
        indexing_stderr=self.get_outputs('{basename}.stderr', self.bam_rg_sorted_marked_indexes)
        
        self.add_shell_execution(self.get_exec_path("samtools")+" index $1 > $2 2>> $3", cmd_format="{EXE} {IN} {OUT}", inputs=self.bam_rg_sorted_marked, outputs=[self.bam_rg_sorted_marked_indexes, indexing_stderr], map=True)
        
        #flagstat analysis
        flagstat_stderr=self.get_outputs('{basename_woext}_flagstat.stderr', self.bam_rg_sorted_marked)
        
        self.add_shell_execution(self.get_exec_path("samtools") + " flagstat $1 > $2 2>> $3", cmd_format='{EXE} {IN} {OUT}', inputs=[self.bam_rg_sorted_marked], outputs=[self.flagstat, flagstat_stderr], map=True)

        #stderr resume of Bams Preprocess file for each sample
        self.add_shell_execution("cat $1 $2 $3 > $4", cmd_format="{EXE} {IN} {OUT}", inputs=[md_stderr, indexing_stderr, flagstat_stderr], outputs=self.Bams_Preprocess_stderr, map=True, local=True)
