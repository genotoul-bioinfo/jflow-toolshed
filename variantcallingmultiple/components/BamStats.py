#
# Copyright (C) 2015 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

__author__ = 'Kenza BAZI KABBAJ INRA - SIGENAE'
__version__ = '2.2.0'

from jflow.component import Component
from weaver.function import PythonFunction


def qualimap_multi (exec_path, output_file, mem, BamStats_stderr, *input_files):
    
    import os
    out_directory=os.path.dirname(output_file)
    bam_list=out_directory+"/bam_list"
    input_files=list(input_files)
    
    #1-create a tab-delimited file list containing sample names and the basename of input bam files and create a symbolic link of input bam files in BamStats Directory
    a=open(bam_list,'w')
    for i in input_files:
        os.system("ln -sv "+i+" "+out_directory)
        sample_name=i.split("/")[-1].split(".rg")[0]
        sample_bam=i.split("/")[-1]
        a.write(sample_name+"\t"+sample_bam+"\n")
    a.close()
    
    #2-qualimap multi analysis
    os.system("unset DISPLAY; "+exec_path+" multi-bamqc -d "+bam_list+" -r -outdir "+out_directory+" --java-mem-size="+mem+" -outformat PDF 2>> "+BamStats_stderr)




class BamStats (Component):
    """
    @summary: Perform mapping quality on bam reads by qualimap bamqc or by qualimap multi-bamqc in case of multiple samples which resume all results in a single report.
    @return: html report (one sample or multi samples)
    """
    def define_parameters(self, input_files):
        """
        @param input_files : [list] bam files.
        """
        # input Files
        self.add_input_file_list( "input_files", "The BAM files.", default=input_files, file_format="bam", required=True )
        
        # output Files
        self.add_output_file("BamStats_stderr", "stderr file for BamStats component", filename='BamStats.stderr')
        self.add_output_file("Qualimap_Report", "report pdf file of qualimap analysis", filename='report.pdf')
        
        
    def process(self):
        
        #get cpu number
        cpu=str(1)
        if self.get_cpu() != None :
            cpu=str(self.get_cpu())
        
        #get memory number
        mem="1G"
        if self.get_memory() != None :
            mem=self.get_memory()

        if len(self.input_files) == 1:
            #qualimap analysis in case of one sample
            self.add_shell_execution ("unset DISPLAY; "+self.get_exec_path("qualimap")+" bamqc -bam $1 -outfile report -nt "+cpu+" -outdir "+self.output_directory+" -outformat PDF --java-mem-size="+mem+" 2>> $2", cmd_format="{EXE} {IN} {OUT}", inputs=self.input_files, outputs=self.BamStats_stderr)
        
        else:
            #qualimap analysis in case of multiple samples
            self.add_python_execution(qualimap_multi, cmd_format="{EXE} {ARG} {OUT} {IN}", arguments=[self.get_exec_path("qualimap"), self.Qualimap_Report, mem], outputs=self.BamStats_stderr, inputs=self.input_files)
